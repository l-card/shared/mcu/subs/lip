# makefile для включения в проект.
#
# на входе принимет параметры:
#  TARGET_CPU (или может быть переопределен через LIP_TARGET) - определяет,
#              какой порт будет использоваться. Должен быть задан один
#              из поддерживаемых портов
#
# устанавливает следующие параметры:
#  LIP_SRC         - исходные файлы на C
#  LIP_HDR         - заголовочные файлы lip (обычно не используются проектом)
#  LIP_INC_DIRS    - директории с заголовками, которые должны быть добавлены
#                    в список директорий для поиска заголовков проекта
#
# также создается цель $(LIP_DOC) для сборки документации и lip_clean для ее очистки


LIP_MAKEFILE = $(abspath $(lastword $(MAKEFILE_LIST)))
LIP_DIR := $(dir $(LIP_MAKEFILE))
LIP_SRC_DIR := $(LIP_DIR)/src/lip

LIP_TARGET ?= $(TARGET_CPU)

#lip sources
LIP_SRC = $(LIP_SRC_DIR)/phy/lip_phy.c \
          $(LIP_SRC_DIR)/phy/lip_phy_mdio.c \
          $(LIP_SRC_DIR)/phy/lip_phy_stats.c \
          $(LIP_SRC_DIR)/phy/dp83848j/lip_phy_dp83848j.c \
          $(LIP_SRC_DIR)/phy/lan8720/lip_phy_lan8720.c \
          $(LIP_SRC_DIR)/phy/lan8742/lip_phy_lan8742.c \
          $(LIP_SRC_DIR)/phy/ksz8567/lip_phy_ksz8567.c \
          $(LIP_SRC_DIR)/phy/ksz8567/lip_sw_ksz8567.c \
          $(LIP_SRC_DIR)/phy/ksz8567/kszsw_reg_acc.c \
          $(LIP_SRC_DIR)/phy/ksz8567/kszsw_reg_acc_spi.c \
          $(LIP_SRC_DIR)/phy/ksz9131/lip_phy_ksz9131.c \
          $(LIP_SRC_DIR)/phy/yt8521s/lip_phy_yt8521s.c \
          $(LIP_SRC_DIR)/lip_core.c \
          $(LIP_SRC_DIR)/misc/lip_rand.c \
          $(LIP_SRC_DIR)/misc/lip_misc_string.c \
          $(LIP_SRC_DIR)/link/lip_port_link.c \
          $(LIP_SRC_DIR)/link/lip_mac.c \
          $(LIP_SRC_DIR)/link/lip_mac_tx.c \
          $(LIP_SRC_DIR)/link/lip_mac_rx.c \
          $(LIP_SRC_DIR)/link/lip_eth.c \
          $(LIP_SRC_DIR)/link/lip_arp.c \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip.c \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip_link_local.c \
          $(LIP_SRC_DIR)/net/ipv4/lip_ipv4_acd.c \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip_reassembly.c \
          $(LIP_SRC_DIR)/net/ipv4/lip_icmp.c \
          $(LIP_SRC_DIR)/net/ipv4/lip_igmp.c \
          $(LIP_SRC_DIR)/transport/udp/lip_udp.c \
          $(LIP_SRC_DIR)/transport/tcp/lip_tcp.c \
          $(LIP_SRC_DIR)/app/ptp/lip_ptp.c \
          $(LIP_SRC_DIR)/app/ptp/misc/lip_ptp_tstmp_ops.c \
          $(LIP_SRC_DIR)/app/ptp/bmca/lip_ptp_bmca_state_sel.c \
          $(LIP_SRC_DIR)/app/ptp/bmca/lip_ptp_port_announce.c \
          $(LIP_SRC_DIR)/app/ptp/master/lip_ptp_master_sync.c \
          $(LIP_SRC_DIR)/app/ptp/gptpcap/lip_ptp_port_gptp_cap.c \
          $(LIP_SRC_DIR)/app/ptp/sync/lip_ptp_port_sync.c \
          $(LIP_SRC_DIR)/app/ptp/sync/lip_ptp_site_sync.c \
          $(LIP_SRC_DIR)/app/ptp/sync/lip_ptp_local_clk_sync.c \
          $(LIP_SRC_DIR)/app/ptp/nbrate/lip_ptp_nb_rate_ratio.c \
          $(LIP_SRC_DIR)/app/ptp/md/eth/pdelay/lip_ptp_pdelay_req.c \
          $(LIP_SRC_DIR)/app/ptp/md/eth/pdelay/lip_ptp_pdelay_resp.c \
          $(LIP_SRC_DIR)/app/ptp/md/eth/sync/lip_ptp_md_eth_sync_recv.c \
          $(LIP_SRC_DIR)/app/ptp/md/eth/sync/lip_ptp_md_eth_sync_send.c \
          $(LIP_SRC_DIR)/app/opcua/types/lip_opcua_tstmp.c \
          $(LIP_SRC_DIR)/app/opcua/types/lip_opcua_variant.c \
          $(LIP_SRC_DIR)/app/opcua/binary/lip_opcua_binary_types_procbuf.c \
          $(LIP_SRC_DIR)/app/opcua/binary/lip_opcua_binary_obj_enc.c \
          $(LIP_SRC_DIR)/app/opcua/server/lip_opcua_server.c \
          $(LIP_SRC_DIR)/app/opcua/server/lip_opcua_server_session.c \
          $(LIP_SRC_DIR)/app/opcua/server/monitems/lip_opcua_server_subscriptions.c \
          $(LIP_SRC_DIR)/app/opcua/server/monitems/lip_opcua_server_monitem.c \
          $(LIP_SRC_DIR)/app/opcua/server/monitems/lip_opcua_server_monitem_queue.c \
          $(LIP_SRC_DIR)/app/opcua/server/monitems/lip_opcua_server_monitem_cbs.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/lip_opcua_server_tree.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/lip_opcua_server_tree_std_var_descr.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes_descr.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_vartypes.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_objtypes.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/lip_opcua_server_tree_reftypes.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_struct.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_enum.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_buildinfo.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_server_status.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_server.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_range.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_complex_num.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_axis.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_items.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/objects/lip_opcua_server_tree_folders.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/objects/lip_opcua_server_tree_modeling_rules.c \
          $(LIP_SRC_DIR)/app/opcua/server/tree/items/objects/lip_opcua_server_tree_server_obj.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/lip_opcua_server_req.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/lip_opcua_server_reqproc.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/svcsets/lip_opcua_server_svcset_discovery.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/svcsets/lip_opcua_server_svcset_session.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/svcsets/lip_opcua_server_svcset_attr.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/svcsets/lip_opcua_server_svcset_view.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/svcsets/lip_opcua_server_svcset_subscription.c \
          $(LIP_SRC_DIR)/app/opcua/server/req/svcsets/lip_opcua_server_svcset_monitem.c \
          $(LIP_SRC_DIR)/app/opcua/server/lip_opcua_server_con.c \
          $(LIP_SRC_DIR)/app/opcua/server/lip_opcua_server_time.c \
          $(LIP_SRC_DIR)/app/opcua/server/lip_opcua_sc.c \
          $(LIP_SRC_DIR)/app/opcua/server/lip_opcua_sc_msgproc.c \
          $(LIP_SRC_DIR)/app/opcua/pubsub/uadp/lip_opcua_uadp_pub.c \
          $(LIP_SRC_DIR)/app/opcua/pubsub/uadp/lip_opcua_uadp_sub.c \
          $(LIP_SRC_DIR)/app/dhcp/lip_dhcpc.c \
          $(LIP_SRC_DIR)/app/tftp/lip_tftp.c \
          $(LIP_SRC_DIR)/app/dns/mdns/lip_mdns.c \
          $(LIP_SRC_DIR)/app/dns/dns_sd/lip_dns_sd.c \
          $(LIP_SRC_DIR)/app/modbus/lip_mb_tcp_server.c \
          $(LIP_SRC_DIR)/app/modbus/core/lip_mb_server.c \
          $(LIP_SRC_DIR)/app/http/lip_http_server.c

#lip headers
LIP_HDR = $(LIP_SRC_DIR)/conf/lip_conf.h \
          $(LIP_SRC_DIR)/phy/lip_phy.h \
          $(LIP_SRC_DIR)/phy/phy_regs.h \
          $(LIP_SRC_DIR)/lip.h \
          $(LIP_SRC_DIR)/lip_defs.h \
          $(LIP_SRC_DIR)/lip_ll.h \
          $(LIP_SRC_DIR)/lip_log.h \
          $(LIP_SRC_DIR)/lip_private.h \
          $(LIP_SRC_DIR)/misc/lip_rand.h \
          $(LIP_SRC_DIR)/link/lip_eth.h \
          $(LIP_SRC_DIR)/link/lip_arp.h \
          $(LIP_SRC_DIR)/net/lip_ip.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_ipv4.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip_private.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip_link_local.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip_link_local_private.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_ipv4_acd.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_ip_reassembly_private.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_icmp.h \
          $(LIP_SRC_DIR)/net/ipv4/lip_igmp.h \
          $(LIP_SRC_DIR)/transport/udp/lip_udp_private.h \
          $(LIP_SRC_DIR)/transport/udp/lip_udp.h \
          $(LIP_SRC_DIR)/transport/tcp/lip_tcp_private.h \
          $(LIP_SRC_DIR)/transport/tcp/lip_tcp.h \
          $(LIP_SRC_DIR)/transport/tcp/lip_sock.h \
          $(LIP_SRC_DIR)/app/ptp/lip_ptp.h \
          $(LIP_SRC_DIR)/app/ptp/lip_ptp_private.h \
          $(LIP_SRC_DIR)/app/dhcp/lip_dhcp_const.h \
          $(LIP_SRC_DIR)/app/dhcp/lip_dhcpc_private.h \
          $(LIP_SRC_DIR)/app/dhcp/lip_dhcpc.h \
          $(LIP_SRC_DIR)/app/tftp/lip_tftp_private.h \
          $(LIP_SRC_DIR)/app/tftp/lip_tftp.h \
          $(LIP_SRC_DIR)/app/dns/lip_dns_defs.h \
          $(LIP_SRC_DIR)/app/dns/mdns/lip_mdns_private.h \
          $(LIP_SRC_DIR)/app/dns/mdns/lip_mdns.h \
          $(LIP_SRC_DIR)/app/dns/dns_sd/lip_dns_sd.h \
          $(LIP_SRC_DIR)/app/dns/dns_sd/lip_dns_sd_private.h \
          $(LIP_SRC_DIR)/app/modbus/lip_mb_tcp_server.h \
          $(LIP_SRC_DIR)/app/modbus/lip_mb_tcp_defs.h \
          $(LIP_SRC_DIR)/app/modbus/core/lip_mb_server.h \
          $(LIP_SRC_DIR)/app/modbus/core/lip_mb_defs.h \
          $(LIP_SRC_DIR)/app/http/lip_http_server.h \
          $(LIP_SRC_DIR)/app/http/lip_http_server_private.h \
          
#include path
LIP_INC_DIRS = $(LIP_DIR)/src
LIP_INCDIRS  = $(LIP_INC_DIRS)

#documentation files
LIP_DOC = $(LIP_DIR)/doc/html/index.html

#append port file to sources

LIP_PORT_DIR = $(LIP_SRC_DIR)/ll/ports/$(LIP_TARGET)

ifeq ($(LIP_TARGET), lpc17xx)
    LIP_SRC += $(LIP_PORT_DIR)/lip_ll_lpc17xx.c
else ifeq ($(LIP_TARGET), lpc43xx)
    LIP_SRC += $(LIP_PORT_DIR)/lip_ll_lpc43xx.c
    LIP_SRC += $(LIP_PORT_DIR)/lip_ll_lpc43xx_descr.c
else ifeq ($(LIP_TARGET), bf609)
    LIP_SRC += $(LIP_PORT_DIR)/lip_ll_bf609.c
    LIP_SRC += $(LIP_SRC_DIR)/ports/lpc43xx/lip_ll_lpc43xx_descr.c
else ifeq ($(LIP_TARGET), stm32h7xx)
    LIP_SRC += $(LIP_PORT_DIR)/lip_ll_stm32h7xx.c
else ifeq ($(LIP_TARGET), imx8mp)
    LIP_SRC += $(LIP_PORT_DIR)/lip_ll_imx8mp.c
else
    $(warning lip warn: port architecture should be specified in LIP_TARGET variable)
endif

LIP_INC_DIRS += $(LIP_PORT_DIR)

ifdef LIP_ENABLE_DOC
#target for create lip doxygen documentation
$(LIP_DOC): $(LIP_SRC) $(LIP_HDR) $(LIP_DIR)/doc/Doxyfile $(LIP_DIR)/doc/mainpage.md
	doxygen $(LIP_DIR)/doc/Doxyfile

lip_clean:
	-rm -fR $(LIP_DIR)/doc/html
	-rm -fR $(LIP_DIR)/doc/rtf
endif
