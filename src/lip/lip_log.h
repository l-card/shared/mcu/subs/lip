/*
 * lip_log.h
 *  Файл содержит константы и макросы для работы с логом:
 *     уровни важности отладочных сообщений
 *     коды модулей, которым эти сообщения соответствуют
 *  Для работы с логом в lip_conf.h надо переопределить макрос
 *    или определить функцию lip_printf(module, level, err, ...)
 *      module - код модуля из LIP_LOG_MODULE_XXX
 *      level  - уровень отладки, котому соответствует сообщение LIP_LOG_LVL_XXX
 *      err    - код ошибки (если присутствует)
 *      ...    - переменное число параметров, соответствующих параметрам стандартной
 *               функции printf (формат, параметры)
 *  Если функция не определена, то отладочные сообщения не выводятся *
 *  Created on: 24.06.2011
 *      Author: Melkor
 */

#ifndef LIP_LOG_H__
#define LIP_LOG_H__

/************ коды модулей, которые могут генерировать отладочные сообщения *******/
#define LIP_LOG_MODULE_CORE           0
#define LIP_LOG_MODULE_PORT           1
#define LIP_LOG_MODULE_PHY            2
#define LIP_LOG_MODULE_ETH            3
#define LIP_LOG_MODULE_ARP            4
#define LIP_LOG_MODULE_IPV4           5
#define LIP_LOG_MODULE_ICMP           6
#define LIP_LOG_MODULE_UDP            7
#define LIP_LOG_MODULE_TCP            8
#define LIP_LOG_MODULE_DHCPC          9
#define LIP_LOG_MODULE_IP_LINK_LOCAL  10
#define LIP_LOG_MODULE_IGMP           11
#define LIP_LOG_MODULE_TFTP           12
#define LIP_LOG_MODULE_IPV4_ACD       13
#define LIP_LOG_MODULE_MDNS           14
#define LIP_LOG_MODULE_EXAMPLE        15
#define LIP_LOG_MODULE_HTTP           16
#define LIP_LOG_MODULE_MAC            17
#define LIP_LOG_MODULE_PTP            18
#define LIP_LOG_MODULE_OPCUA          19


/******************  уровни оталадочных сообщений ********************************/
#define LIP_LOG_LVL_ERROR_FATAL       0
#define LIP_LOG_LVL_ERROR_HIGH        1
#define LIP_LOG_LVL_ERROR_MEDIUM      2
#define LIP_LOG_LVL_ERROR_LOW         3
#define LIP_LOG_LVL_WARNING_HIGH      4
#define LIP_LOG_LVL_WARNING_MEDIUM    5
#define LIP_LOG_LVL_WARNING_LOW       6
#define LIP_LOG_LVL_PROGRESS          7
#define LIP_LOG_LVL_DETAIL            8
#define LIP_LOG_LVL_DEBUG_HIGH        9
#define LIP_LOG_LVL_DEBUG_MEDIUM      10
#define LIP_LOG_LVL_DEBUG_LOW         11

#endif
