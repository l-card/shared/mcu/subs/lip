/**************************************************************************//**
 @defgroup lip_core Базовые функции
 @brief Основные функции, управляющие работой стека целиком

 В данной группе содержатся основные функции управляющие стеком и его модулями,
 такие как инициализация, выполнение фоновых задач, останов работы стека.

 Подробнее о последовательности вызовов описано в разделе @ref sect_std_call_seq
 ******************************************************************************/

/***************************************************************************//**
  @addtogroup lip_core
  @{
  @file lip.h
  @brief Основной заголовочный файл, предназначенный для включения приложением,
         использующим стек

  Данный файл необходим и достаточен для включения внешним приложением для
  использования стека.

  Он включает в себя как объявление минимального набора базовых функций
  (не зависящие от модуля), так и включает все остальные необходимые заголовки,
  включая заголовки разрешенных в конфигурации модулей.

  Данный включает только функции для внешнего приложения и не включает
  опредления, используемые только внутри стека

  @author Borisov Alexey <borisov@lcard.ru>
 ******************************************************************************/

#ifndef LIP_H_
#define LIP_H_

#include "lip_config.h"
#include "lip_defs.h"
#include "lip_log.h"

#include "misc/lip_misc_defs.h"
#include "misc/lip_misc_string.h"
#include "link/lip_eth.h"
#include "link/lip_mac.h"
#include "link/lip_mac_rx.h"
#include "link/lip_mac_tx.h"
#include "link/lip_port_link.h"
#include "net/lip_ip.h"
#include "net/ipv4/lip_ipv4.h"
#include "app/ptp/lip_ptp_cfg_defs.h"
#include "app/opcua/lip_opcua.h"

#ifdef LIP_USE_LINK_LOCAL_ADDR
    #include "net/ipv4/lip_ip_link_local.h"
#endif

#ifdef LIP_USE_IGMP
    #include "net/ipv4/lip_igmp.h"
#endif

#ifdef LIP_USE_UDP
    #include "transport/udp/lip_udp.h"
#endif

#ifdef LIP_USE_TCP
    #include "transport/tcp/lip_tcp.h"
#endif

#if LIP_PTP_ENABLE
    #include "app/ptp/lip_ptp.h"
#endif

#ifdef LIP_USE_DHCPC
    #include "app/dhcp/lip_dhcpc.h"
#endif

#if defined LIP_USE_TFTP_CLIENT || defined LIP_USE_TFTP_SERVER
    #include "app/tftp/lip_tftp.h"
#endif

#ifdef LIP_USE_MDNS
    #include "app/dns/mdns/lip_mdns.h"
#endif

#ifdef LIP_USE_DNSSD
    #include "app/dns/dns_sd/lip_dns_sd.h"
#endif

#ifdef LIP_USE_MB_TCP_SERVER
    #include "app/modbus/lip_mb_tcp_server.h"
#endif

#ifdef LIP_USE_HTTP_SERVER
    #include "app/http/lip_http_server.h"
#endif


/** Состояния стека */
typedef enum {
    LIP_STATE_OFF,    /**< Стек не инициализирован, так как инициализация еще
                           не была выполнена, либо так как работа была полностью
                           остановлена. В этом состоянии можно вызывать только
                           lip_init() */
    LIP_STATE_REINIT, /**< Повтроная инициализация соединения в случае, если
                           пропала связь на физическом уровне и затем снова появился */
    LIP_STATE_INIT,   /**< Идет инициализация подключения на физическом уровне */
    LIP_STATE_GET_IP, /**< Связь на физ. уровне установлена (link up), но
                             пока нет действительного адреса IP-уровня
                             (идет получение по DHCP или Link-Local) */
    LIP_STATE_CONFIGURED, /**< Рабочее состояние --- есть физическое соединение
                                 с действительным адресом */
    LIP_STATE_ERROR,      /**< Произошла критическая ошибка. Дальнейшая
                                работа не возможна без останова и повторной
                                инициализации стека */
    LIP_STATE_CLOSE_REQUEST /**< Был запрос на останов работы стека, по каоторому все
                                модули начинают корректное завершение работы, но еще
                                не был запрещен сам контроллер, чтобы была возможность
                                обмена сообщениями для корректного завершения.
                                В этом состоянии можно вызывать только lip_pull()
                                и lip_close() */
} t_lip_state;

/** @brief Инициализация стека */
t_lip_errs lip_init(void);
/** @brief Продвижение стека (выполнение фоновых задач) */
t_lip_errs lip_pull(void);
/** @brief Запрос на завершение работы стека */
void lip_close_request(unsigned tout);
/** @brief Явное завершение работы стека с выключением аппаратных средств */
void lip_close(void);

/** @brief Получение текущего состояния стека */
t_lip_state lip_state(void);

/** @brief Проверка, возможно ли в данном состоянии стека посылать какие-либо пакеты */
t_lip_errs lip_check_tx_enabled(void);




/** @brief Проверка наличия свободного буфера для формирования пакета на передачу */
int lip_tx_buf_rdy(void);



#ifdef LIP_CONN_ESTABLISH_CB
    void lip_cb_conn_establish(void);
#endif

#ifdef LIP_CONN_LOST_CB
    void lip_cb_conn_lost(void);
#endif

#endif /* LIP_H_ */

/** @} */
