/***************************************************************************//**
  @addtogroup lip_udp
  @{
  @internal @file lip_udp_private.h
  @brief Файл содержит определения функций и констант для @ref lip_udp,
         которые используются только самим стеком.
  @author Borisov Alexey <borisov@lcard.ru>
  @date   05.08.2011
 ******************************************************************************/

#ifndef LIP_UDP_PRIVATE_H_
#define LIP_UDP_PRIVATE_H_

#include "lip/transport/udp/lip_udp.h"
#include "lip/net/ipv4/lip_ip_private.h"

/** @internal Инициализация udp-модуля */
void lip_udp_init(void);
/** @internal Обработка UDP-заголовка принятого пакета */
t_lip_errs lip_udp_process_packet(const t_lip_ip_rx_packet *ip_packet);

#endif /* LIP_UDP_PRIVATE_H_ */

/** @}*/
