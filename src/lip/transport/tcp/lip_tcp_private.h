/*
 * lip_tcp_private.h
 *
 *  Created on: 05.08.2011
 *      Author: Borisov
 */

#ifndef LIP_TCP_PRIVATE_H_
#define LIP_TCP_PRIVATE_H_

#include "lip_config.h"
#include "lip_tcp.h"
#include "lip/net/ipv4/lip_ip_private.h"


//функции, которые использует стек
void lip_tcp_init(void);
void lip_tcp_close(unsigned tout);
t_lip_errs lip_tcp_process_packet(const t_lip_ip_rx_packet *ip_packet);
void lip_tcp_pull(void);
void lip_tcp_ip_addr_changed(t_lip_ip_type type,
                             const uint8_t* old_addr, int len);

#endif /* LIP_TCP_PRIVATE_H_ */
