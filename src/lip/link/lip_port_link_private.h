#ifndef LIP_PORT_LINK_PRIVATE_H
#define LIP_PORT_LINK_PRIVATE_H

#include "lip/lip_defs.h"
#include "lip_port_link_cfg_defs.h"

/** @brief Информация для стека, что изменилось состояние link-up для заданного порта */
void lip_port_link_status_changed(int port_idx, bool link_up);

#if LIP_PORT_SELFLOOP_DETECTION
void lip_port_set_selfloop_detected(int port_idx);
#endif

#endif // LIP_PORT_LINK_PRIVATE_H
