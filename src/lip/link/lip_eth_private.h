#ifndef LIP_ETH_PRIVATE_H
#define LIP_ETH_PRIVATE_H

#include "lip_eth.h"
#include "lip/lip_defs.h"
#include "lip/phy/lip_phy_cfg_defs.h"
#include "lip_mac_tx.h"




t_lip_errs lip_eth_process_packet(t_lip_rx_frame *rx_frame, t_lip_eth_rx_packet *eth_packet);




#endif // LIP_ETH_PRIVATE_H
