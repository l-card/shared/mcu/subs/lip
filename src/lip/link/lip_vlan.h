#ifndef LIP_VLAN_H
#define LIP_VLAN_H

#include "lbitfield.h"
#include <stdint.h>

/* Поля из VLAN Tag Control Information (TCI), 9.6 IEEE 802.1Q-2018 */
#define LIP_VLAN_TCI_FLD_VID      (0x0FFFUL <<  0U) /**< VLAN ID */
#define LIP_VLAN_TCI_FLD_DEI      (0x0001UL << 12U) /**< Drop eligible */
#define LIP_VLAN_TCI_FLD_PCP      (0x0007UL << 13U) /**< Priority code point */


/* Зарезервированные значения VID, IEEE 802.1Q-2018  table 9-2 */
#define LIP_VLAN_VID_NULL               0x000 /**< No VLAN Information */
#define LIP_VLAN_VID_DEFAULT_PVID       0x001 /**< Default port VID */
#define LIP_VLAN_VID_DEFAULT_SR_PVID    0x002 /**< Default port stream related traffic VID */
#define LIP_VLAN_VID_RESERVED           0xFFF /**< Default port stream related traffic VID */

#define LIP_VLAN_TCI(vid, prio, dei) (LBITFIELD_SET(LIP_VLAN_TCI_FLD_VID, (vid)) | \
                                      LBITFIELD_SET(LIP_VLAN_TCI_FLD_DEI, (dei)) | \
                                      LBITFIELD_SET(LIP_VLAN_TCI_FLD_PCP, (prio)))
#define LIP_VLAN_TCI_PRIO(prio)      LIP_VLAN_TCI(0, prio, 0)


#include "lcspec_pack_start.h"
typedef struct  {
    uint16_t type;
    uint16_t tci;
} LATTRIBUTE_PACKED t_lip_vlan_hdr;

#include "lcspec_pack_restore.h"



#endif // LIP_VLAN_H
