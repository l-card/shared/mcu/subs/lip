/*
 * lip_arp.h
 *
 *  Created on: 30.07.2010
 *      Author: borisov
 */

#ifndef __LIP_ARP_H__
#define __LIP_ARP_H__

#include "lip_config.h"
#include "lip/lip_defs.h"
#include "lip_eth_private.h"
#include "lip/net/lip_ip.h"


//hardware address space types
#define LIP_HRD_ETHERNET  1




void lip_arp_init(void);
void lip_arp_pull(void);
t_lip_errs lip_arp_process_packet(t_lip_payload *pl, const t_lip_eth_rx_packet *eth_packet);
uint8_t *lip_arp_resolve(t_lip_ip_type type, const uint8_t *ip_addr);
void lip_arp_invalidate(t_lip_ip_type type, const uint8_t *ip_addr);

uint8_t lip_arp_upd_ip(const uint8_t *ip_addr, const uint8_t *hrd_addr);
t_lip_errs lip_arp_send_req(t_lip_ip_type type, const uint8_t *local_ip, const uint8_t *ip, uint8_t len);

static LINLINE t_lip_errs lip_arp_send_probe(const uint8_t *ip_addr) {
    return lip_arp_send_req(LIP_IPTYPE_IPv4, 0, ip_addr, LIP_IPV4_ADDR_SIZE);
}

static LINLINE t_lip_errs lip_arp_send_announce(const uint8_t *ip_addr) {
    return lip_arp_send_req(LIP_IPTYPE_IPv4, ip_addr, ip_addr, LIP_IPV4_ADDR_SIZE);
}





#endif /* LIP_ARP_H_ */
