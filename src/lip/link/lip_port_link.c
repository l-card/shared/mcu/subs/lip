#include "lip_port_link_private.h"
#include "lip_port_link_cfg_defs.h"
#include "lip_mac_cfg_defs.h"
#include "lip/app/ptp/lip_ptp_cfg_defs.h"
#if LIP_PTP_ENABLE
#include "lip/app/ptp/lip_ptp_private.h"
#endif
#include "lip/lip.h"

/** @todo в будущем сделать поддержку петли через MSTP.
 *  Также сделать соответствие порта и свича через контекст порта, в
 *  который входят доп. функции, как установить MSTP состояние порта.
 *  Сделать, чтобы номер порта в стеке мог быть не зависим от номера порта
 *  в свиче, а также чтобы у каждого порта могли быть свои функции управления
 *  (если более одного свича и т.п.)
 */

static bool f_link_state[LIP_PORT_MAX_CNT];
#if LIP_PORT_SELFLOOP_DETECTION
static bool f_link_self_loop[LIP_PORT_MAX_CNT];
#endif


bool lip_port_link_is_up(int port_idx) {
    return f_link_state[port_idx];
}

#if LIP_PORT_SELFLOOP_DETECTION
void lip_port_set_seetloop_state(int port_idx, bool detected) {
    if (f_link_self_loop[port_idx] != detected) {
        f_link_self_loop[port_idx] = detected;
        lip_user_port_selfloop_detection_cb(port_idx, detected);
    }
}

void lip_port_set_selfloop_detected(int port_idx) {
    lip_port_set_seetloop_state(port_idx, true);
}
#endif

void lip_port_link_status_changed(int port_idx, bool link_up) {
    if (f_link_state[port_idx] != link_up) {
        f_link_state[port_idx] = link_up;
#if LIP_PORT_SELFLOOP_DETECTION
        lip_port_set_seetloop_state(port_idx, false);
#endif
#if LIP_PTP_ENABLE
        if ((lip_state() == LIP_STATE_GET_IP) || (lip_state() == LIP_STATE_CONFIGURED)) {
            lip_ptp_port_link_changed(port_idx, link_up);
        }
#endif
    }
}
