#ifndef LIP_VLAN_CFG_DEFS_H
#define LIP_VLAN_CFG_DEFS_H

#include "lip_config.h"
#include "lip_mac_rx_cfg_defs.h"
#include "lip_mac_tx_cfg_defs.h"

#ifdef LIP_CFG_VLAN_ENABLE
#define LIP_VLAN_ENABLE         LIP_CFG_VLAN_ENABLE
#else
#define LIP_VLAN_ENABLE         0
#endif

#ifdef LIP_CFG_VLAN_RX_QUEUE_NUM
    #define LIP_VLAN_RX_QUEUE_NUM            LIP_CFG_VLAN_RX_QUEUE_NUM
#else
    #define LIP_VLAN_RX_QUEUE_NUM(pcp)       (LIP_MAC_RX_QUEUE_CNT - ((pcp) * LIP_MAC_RX_QUEUE_CNT / 8 + 1))
#endif

#ifdef LIP_CFG_VLAN_TX_QUEUE_NUM
    #define LIP_VLAN_TX_QUEUE_NUM            LIP_CFG_VLAN_TX_QUEUE_NUM
#else
    #define LIP_VLAN_TX_QUEUE_NUM(pcp)       (LIP_MAC_TX_QUEUE_CNT - ((pcp) * LIP_MAC_TX_QUEUE_CNT / 8 + 1))
#endif

#endif // LIP_VLAN_CFG_DEFS_H
