#ifndef LIP_MSTP_DEFS_H
#define LIP_MSTP_DEFS_H

typedef enum {
    LIP_MSTP_PORT_STATE_FORWARDING = 0,
    LIP_MSTP_PORT_STATE_DISABLED,
    LIP_MSTP_PORT_STATE_LEARNING,
} t_lip_mstp_port_state;

#endif // LIP_MSTP_DEFS_H
