#ifndef LIP_MAC_RX_CFG_DEFS_H
#define LIP_MAC_RX_CFG_DEFS_H

#include "lip_config.h"


#ifdef LIP_CFG_MAC_RX_FRAME_ALIGN
    #define LIP_MAC_RX_FRAME_ALIGN          LIP_CFG_MAC_RX_FRAME_ALIGN
#else
    #define LIP_MAC_RX_FRAME_ALIGN          4
#endif


#ifdef LIP_CFG_MAC_RX_FRAME_PREPROC_CB
#define LIP_MAC_RX_FRAME_PREPROC_CB    LIP_CFG_MAC_RX_FRAME_PREPROC_CB
#else
#define LIP_MAC_RX_FRAME_PREPROC_CB    0
#endif


#ifdef LIP_CFG_MAC_RX_QUEUE_CNT
    #define LIP_MAC_RX_QUEUE_CNT            LIP_CFG_MAC_RX_QUEUE_CNT
#else
    #define LIP_MAC_RX_QUEUE_CNT            1
#endif

#if LIP_MAC_RX_QUEUE_CNT < 1 || LIP_MAC_RX_QUEUE_CNT > 4
    #error unsupported rx queue count
#endif

#ifdef LIP_CFG_MAC_RX_QUEUE0_SIZE
    #define LIP_MAC_RX_QUEUE0_SIZE          LIP_CFG_MAC_RX_QUEUE0_SIZE
#else
    #define LIP_MAC_RX_QUEUE0_SIZE          8192
#endif

#ifdef LIP_CFG_MAC_RX_QUEUE0_FRAME_CNT
    #define LIP_MAC_RX_QUEUE0_FRAME_CNT     LIP_CFG_MAC_RX_QUEUE0_FRAME_CNT
#else
    #define LIP_MAC_RX_QUEUE0_FRAME_CNT     32
#endif

#if (LIP_MAC_RX_QUEUE0_FRAME_CNT != 8) \
    && (LIP_MAC_RX_QUEUE0_FRAME_CNT != 16) \
    && (LIP_MAC_RX_QUEUE0_FRAME_CNT != 32) \
    && (LIP_MAC_RX_QUEUE0_FRAME_CNT != 64) \
    && (LIP_MAC_RX_QUEUE0_FRAME_CNT != 128)
    #error unsupported rx queue fame cnt value
#endif

#ifdef LIP_CFG_MAC_RX_QUEUE0_BUF_MEM
    #define LIP_MAC_RX_QUEUE0_BUF_MEM       LIP_CFG_MAC_RX_QUEUE0_BUF_MEM
#else
    #define LIP_MAC_RX_QUEUE0_BUF_MEM(var)  var
#endif


#if LIP_MAC_RX_QUEUE_CNT >= 2
    #ifdef LIP_CFG_MAC_RX_QUEUE1_SIZE
        #define LIP_MAC_RX_QUEUE1_SIZE          LIP_CFG_MAC_RX_QUEUE1_SIZE
    #else
        #define LIP_MAC_RX_QUEUE1_SIZE          8192
    #endif

    #ifdef LIP_CFG_MAC_RX_QUEUE1_FRAME_CNT
        #define LIP_MAC_RX_QUEUE1_FRAME_CNT     LIP_CFG_MAC_RX_QUEUE1_FRAME_CNT
    #else
        #define LIP_MAC_RX_QUEUE1_FRAME_CNT     32
    #endif

    #if (LIP_MAC_RX_QUEUE1_FRAME_CNT != 8) \
        && (LIP_MAC_RX_QUEUE1_FRAME_CNT != 16) \
        && (LIP_MAC_RX_QUEUE1_FRAME_CNT != 32) \
        && (LIP_MAC_RX_QUEUE1_FRAME_CNT != 64) \
        && (LIP_MAC_RX_QUEUE1_FRAME_CNT != 128)
        #error unsupported rx queue fame cnt value
    #endif

    #ifdef LIP_CFG_MAC_RX_QUEUE1_BUF_MEM
        #define LIP_MAC_RX_QUEUE1_BUF_MEM       LIP_CFG_MAC_RX_QUEUE1_BUF_MEM
    #else
        #define LIP_MAC_RX_QUEUE1_BUF_MEM(var)  var
    #endif
#endif

#if LIP_MAC_RX_QUEUE_CNT >= 3
    #ifdef LIP_CFG_MAC_RX_QUEUE2_SIZE
        #define LIP_MAC_RX_QUEUE2_SIZE          LIP_CFG_MAC_RX_QUEUE2_SIZE
    #else
        #define LIP_MAC_RX_QUEUE2_SIZE          8192
    #endif

    #ifdef LIP_CFG_MAC_RX_QUEUE2_FRAME_CNT
        #define LIP_MAC_RX_QUEUE2_FRAME_CNT          LIP_CFG_MAC_RX_QUEUE2_FRAME_CNT
    #else
        #define LIP_MAC_RX_QUEUE2_FRAME_CNT          32
    #endif

    #if (LIP_MAC_RX_QUEUE2_FRAME_CNT != 8) \
        && (LIP_MAC_RX_QUEUE2_FRAME_CNT != 16) \
        && (LIP_MAC_RX_QUEUE2_FRAME_CNT != 32) \
        && (LIP_MAC_RX_QUEUE2_FRAME_CNT != 64) \
        && (LIP_MAC_RX_QUEUE2_FRAME_CNT != 128)
        #error unsupported rx queue fame cnt value
    #endif

    #ifdef LIP_CFG_MAC_RX_QUEUE2_BUF_MEM
        #define LIP_MAC_RX_QUEUE2_BUF_MEM       LIP_CFG_MAC_RX_QUEUE2_BUF_MEM
    #else
        #define LIP_MAC_RX_QUEUE2_BUF_MEM(var)  var
    #endif
#endif

#if LIP_MAC_RX_QUEUE_CNT >= 4
    #ifdef LIP_CFG_MAC_RX_QUEUE3_SIZE
        #define LIP_MAC_RX_QUEUE3_SIZE          LIP_CFG_MAC_RX_QUEUE3_SIZE
    #else
        #define LIP_MAC_RX_QUEUE3_SIZE          8192
    #endif

    #ifdef LIP_CFG_MAC_RX_QUEUE3_FRAME_CNT
        #define LIP_MAC_RX_QUEUE3_FRAME_CNT          LIP_CFG_MAC_RX_QUEUE3_FRAME_CNT
    #else
        #define LIP_MAC_RX_QUEUE3_FRAME_CNT          32
    #endif

    #if (LIP_MAC_RX_QUEUE3_FRAME_CNT != 8) \
        && (LIP_MAC_RX_QUEUE3_FRAME_CNT != 16) \
        && (LIP_MAC_RX_QUEUE3_FRAME_CNT != 32) \
        && (LIP_MAC_RX_QUEUE3_FRAME_CNT != 64) \
        && (LIP_MAC_RX_QUEUE3_FRAME_CNT != 128)
        #error unsupported rx queue fame cnt value
    #endif

    #ifdef LIP_CFG_MAC_RX_QUEUE3_BUF_MEM
        #define LIP_MAC_RX_QUEUE3_BUF_MEM       LIP_CFG_MAC_RX_QUEUE3_BUF_MEM
    #else
        #define LIP_MAC_RX_QUEUE3_BUF_MEM(var)  var
    #endif
#endif

#endif // LIP_MAC_RX_CFG_DEFS_H
