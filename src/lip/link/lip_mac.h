#ifndef LIP_MAC_PHY_H
#define LIP_MAC_PHY_H

#include "lip/phy/lip_phy_cfg_defs.h"
#include "lip/phy/lip_phy_defs.h"
#include "lip/phy/lip_phy_stats_defs.h"
#include "lip_mac_cfg_defs.h"

void lip_mac_phy_set_cfg(const t_lip_phy_cfg *cfg);
const t_lip_phy_mode *lip_mac_phy_mode(void);
const t_lip_phy_ctx *lip_mac_phy_ctx(void);
#if LIP_PHY_USE_STATS
const t_lip_phy_stats_results *lip_mac_phy_stats_results(void);
#endif


bool lip_mac_is_initialized(void);
bool lip_mac_in_work_state(void);
t_lip_errs lip_mac_init(unsigned cfg_flags);
t_lip_errs lip_mac_pull(void);
void lip_mac_close(void);

#if LIP_MAC_USER_PULL_CB
    void lip_mac_user_pull(void);
#endif


#endif // LIP_MAC_PHY_H
