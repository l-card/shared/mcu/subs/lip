#ifndef LIP_MAC_TX_CFG_DEFS_H
#define LIP_MAC_TX_CFG_DEFS_H

#include "lip_config.h"
#include "lip_eth_defs.h"

#define LIP_MAC_TX_FRAME_MAX_SIZE           LIP_ETH_MAX_PACKET_SIZE

#ifdef LIP_CFG_MAC_TX_FRAME_ALIGN
    #define LIP_MAC_TX_FRAME_ALIGN          LIP_CFG_MAC_TX_FRAME_ALIGN
#else
    #define LIP_MAC_TX_FRAME_ALIGN          4
#endif

#ifdef LIP_CFG_MAC_TX_QUEUE_CNT
    #define LIP_MAC_TX_QUEUE_CNT            LIP_CFG_MAC_TX_QUEUE_CNT
#else
    #define LIP_MAC_TX_QUEUE_CNT            1
#endif

#if LIP_MAC_TX_QUEUE_CNT < 1 || LIP_MAC_TX_QUEUE_CNT > 4
    #error unsupported tx queue count
#endif

#ifdef LIP_CFG_MAC_TX_QUEUE0_SIZE
    #define LIP_MAC_TX_QUEUE0_SIZE          LIP_CFG_MAC_TX_QUEUE0_SIZE
#else
    #define LIP_MAC_TX_QUEUE0_SIZE          8192
#endif

#ifdef LIP_CFG_MAC_TX_QUEUE0_FRAME_CNT
    #define LIP_MAC_TX_QUEUE0_FRAME_CNT     LIP_CFG_MAC_TX_QUEUE0_FRAME_CNT
#else
    #define LIP_MAC_TX_QUEUE0_FRAME_CNT     32
#endif

#if (LIP_MAC_TX_QUEUE0_FRAME_CNT != 8) \
    && (LIP_MAC_TX_QUEUE0_FRAME_CNT != 16) \
    && (LIP_MAC_TX_QUEUE0_FRAME_CNT != 32) \
    && (LIP_MAC_TX_QUEUE0_FRAME_CNT != 64) \
    && (LIP_MAC_TX_QUEUE0_FRAME_CNT != 128)
    #error unsupported tx queue fame cnt value
#endif

#ifdef LIP_CFG_MAC_TX_QUEUE0_BUF_MEM
    #define LIP_MAC_TX_QUEUE0_BUF_MEM       LIP_CFG_MAC_TX_QUEUE0_BUF_MEM
#else
    #define LIP_MAC_TX_QUEUE0_BUF_MEM(var)  var
#endif


#if LIP_MAC_TX_QUEUE_CNT >= 2
    #ifdef LIP_CFG_MAC_TX_QUEUE1_SIZE
        #define LIP_MAC_TX_QUEUE1_SIZE          LIP_CFG_MAC_TX_QUEUE1_SIZE
    #else
        #define LIP_MAC_TX_QUEUE1_SIZE          8192
    #endif

    #ifdef LIP_CFG_MAC_TX_QUEUE1_FRAME_CNT
        #define LIP_MAC_TX_QUEUE1_FRAME_CNT     LIP_CFG_MAC_TX_QUEUE1_FRAME_CNT
    #else
        #define LIP_MAC_TX_QUEUE1_FRAME_CNT     32
    #endif

    #if (LIP_MAC_TX_QUEUE1_FRAME_CNT != 8) \
        && (LIP_MAC_TX_QUEUE1_FRAME_CNT != 16) \
        && (LIP_MAC_TX_QUEUE1_FRAME_CNT != 32) \
        && (LIP_MAC_TX_QUEUE1_FRAME_CNT != 64) \
        && (LIP_MAC_TX_QUEUE1_FRAME_CNT != 128)
        #error unsupported tx queue fame cnt value
    #endif

    #ifdef LIP_CFG_MAC_TX_QUEUE1_BUF_MEM
        #define LIP_MAC_TX_QUEUE1_BUF_MEM       LIP_CFG_MAC_TX_QUEUE1_BUF_MEM
    #else
        #define LIP_MAC_TX_QUEUE1_BUF_MEM(var)  var
    #endif
#endif

#if LIP_MAC_TX_QUEUE_CNT >= 3
    #ifdef LIP_CFG_MAC_TX_QUEUE2_SIZE
        #define LIP_MAC_TX_QUEUE2_SIZE          LIP_CFG_MAC_TX_QUEUE2_SIZE
    #else
        #define LIP_MAC_TX_QUEUE2_SIZE          8192
    #endif

    #ifdef LIP_CFG_MAC_TX_QUEUE2_FRAME_CNT
        #define LIP_MAC_TX_QUEUE2_FRAME_CNT          LIP_CFG_MAC_TX_QUEUE2_FRAME_CNT
    #else
        #define LIP_MAC_TX_QUEUE2_FRAME_CNT          32
    #endif

    #if (LIP_MAC_TX_QUEUE2_FRAME_CNT != 8) \
        && (LIP_MAC_TX_QUEUE2_FRAME_CNT != 16) \
        && (LIP_MAC_TX_QUEUE2_FRAME_CNT != 32) \
        && (LIP_MAC_TX_QUEUE2_FRAME_CNT != 64) \
        && (LIP_MAC_TX_QUEUE2_FRAME_CNT != 128)
        #error unsupported tx queue fame cnt value
    #endif
    #ifdef LIP_CFG_MAC_TX_QUEUE2_BUF_MEM
        #define LIP_MAC_TX_QUEUE2_BUF_MEM       LIP_CFG_MAC_TX_QUEUE2_BUF_MEM
    #else
        #define LIP_MAC_TX_QUEUE2_BUF_MEM(var)  var
    #endif
#endif

#if LIP_MAC_TX_QUEUE_CNT >= 4
    #ifdef LIP_CFG_MAC_TX_QUEUE3_SIZE
        #define LIP_MAC_TX_QUEUE3_SIZE          LIP_CFG_MAC_TX_QUEUE3_SIZE
    #else
        #define LIP_MAC_TX_QUEUE3_SIZE          8192
    #endif

    #ifdef LIP_CFG_MAC_TX_QUEUE3_FRAME_CNT
        #define LIP_MAC_TX_QUEUE3_FRAME_CNT          LIP_CFG_MAC_TX_QUEUE3_FRAME_CNT
    #else
        #define LIP_MAC_TX_QUEUE3_FRAME_CNT          32
    #endif

    #if (LIP_MAC_TX_QUEUE3_FRAME_CNT != 8) \
        && (LIP_MAC_TX_QUEUE3_FRAME_CNT != 16) \
        && (LIP_MAC_TX_QUEUE3_FRAME_CNT != 32) \
        && (LIP_MAC_TX_QUEUE3_FRAME_CNT != 64) \
        && (LIP_MAC_TX_QUEUE3_FRAME_CNT != 128)
        #error unsupported tx queue fame cnt value
    #endif

    #ifdef LIP_CFG_MAC_TX_QUEUE3_BUF_MEM
        #define LIP_MAC_TX_QUEUE3_BUF_MEM       LIP_CFG_MAC_TX_QUEUE3_BUF_MEM
    #else
        #define LIP_MAC_TX_QUEUE3_BUF_MEM(var)  var
    #endif
#endif

#ifdef LIP_CFG_MAC_TX_QUEUE_NUM
    #define LIP_MAC_TX_QUEUE_NUM    LIP_CFG_MAC_TX_QUEUE_NUM
#else
    #if LIP_MAC_TX_QUEUE_CNT == 1
        #define LIP_MAC_TX_QUEUE_NUM(prio)      0
    #elif LIP_MAC_TX_QUEUE_CNT == 2
        #define LIP_MAC_TX_QUEUE_NUM(prio)      ((prio) >= LIP_PKT_PRIO_VHIGH ? 0 : 1)
    #elif LIP_MAC_TX_QUEUE_CNT == 3
        #define LIP_MAC_TX_QUEUE_NUM(prio)      ((prio) >= LIP_PKT_PRIO_CRITICAL ? 0 : (prio) >= LIP_PKT_PRIO_VHIGH ? 1 : 2)
    #elif LIP_MAC_TX_QUEUE_CNT == 4
        #define LIP_MAC_TX_QUEUE_NUM(prio)      ((prio) >= LIP_PKT_PRIO_CRITICAL ? 0 : (prio) >= LIP_PKT_PRIO_VHIGH ? 1 : (prio) >= LIP_PKT_PRIO_HIGH ? 2 : 3)
    #endif
#endif

#endif // LIP_MAC_TX_CFG_DEFS_H
