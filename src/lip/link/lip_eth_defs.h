#ifndef LIP_ETH_DEFS_H
#define LIP_ETH_DEFS_H

/** Размер MAC-адреса */
#define LIP_MAC_ADDR_SIZE    6

/** Максимальный размер пакета по Ethernet (без CRC) */
#define LIP_ETH_MAX_PACKET_SIZE             1514
/** Максимальный размер пакета по Ethernet, включая CRC */
#define LIP_ETH_MAX_FULL_PACKET_SIZE        1518
/** Максимальный размер пакета по Ethernet с VLAN tag, включая CRC */
#define LIP_ETH_MAX_VLAN_FULL_PACKET_SIZE   1522
/** максимальный размер данных в кадре Ethernet */
#define LIP_ETH_MTU_SIZE                    1500
/** Минимальный размер пакета по Ethernet (без CRC) */
#define LIP_ETH_MIN_PACKET_SIZE             60

#endif // LIP_ETH_DEFS_H
