#ifndef LIP_PORT_LINK_H
#define LIP_PORT_LINK_H

#include "lip/lip_defs.h"
#include "lip_port_link_cfg_defs.h"

typedef enum {
    LIP_PORT_STATE_FLAG_LEARN_EN = 1 << 0,
    LIP_PORT_STATE_FLAG_TX_EN    = 1 << 1,
    LIP_PORT_STATE_FLAG_RX_EN    = 1 << 2,
    LIP_PORT_STATE_FLAG_ALL_EN   = LIP_PORT_STATE_FLAG_LEARN_EN | LIP_PORT_STATE_FLAG_TX_EN | LIP_PORT_STATE_FLAG_RX_EN
} t_lip_port_state_flags;

bool lip_port_link_is_up(int port_idx);

#if LIP_PORT_SELFLOOP_DETECTION
    void lip_user_port_selfloop_detection_cb(int port_idx, bool detected);
#endif

#endif // LIP_PORT_LINK_H
