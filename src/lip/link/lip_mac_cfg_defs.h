#ifndef LIP_MAC_CFG_DEFS_H
#define LIP_MAC_CFG_DEFS_H

#include "lip_config.h"
#include "lip/phy/lip_phy_cfg_defs.h"


#if LIP_SW_SUPPORT_KSZ8567
    #define LIP_MULTIPORT_ENABLE        1
#else
    #define LIP_MULTIPORT_ENABLE        0
#endif

#if LIP_MULTIPORT_ENABLE
    #ifdef LIP_CFG_PORT_MAX_CNT
        #define LIP_PORT_MAX_CNT        LIP_CFG_PORT_MAX_CNT
    #else
        #define LIP_PORT_MAX_CNT        1
    #endif
#else
    #define LIP_PORT_MAX_CNT            1
#endif

#ifdef LIP_CFG_MAC_USER_PULL_CB
    #define LIP_MAC_USER_PULL_CB        LIP_CFG_MAC_USER_PULL_CB
#else
    #define LIP_MAC_USER_PULL_CB        0
#endif

#ifdef LIP_CFG_ETH_RX_UNSUP_TYPE_CB
    #define LIP_ETH_RX_UNSUP_TYPE_CB    LIP_CFG_ETH_RX_UNSUP_TYPE_CB
#else
    #define LIP_ETH_RX_UNSUP_TYPE_CB    0
#endif

#endif // LIP_MAC_CFG_DEFS_H
