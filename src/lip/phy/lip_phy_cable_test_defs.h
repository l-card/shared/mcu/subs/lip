#ifndef LIP_PHY_CABLE_TEST_DEFS_H
#define LIP_PHY_CABLE_TEST_DEFS_H

#include "lip/lip_defs.h"
#include "lip_phy_mdio.h"
#include "lip_phy_cfg_defs.h"

/** @brief Результат выполнения теста кабеля */
typedef enum {
    LIP_PHY_CABLE_CHECK_STATUS_NORMAL = 0, /**< Normal, no fault has been detected */
    LIP_PHY_CABLE_CHECK_STATUS_OPEN   = 1, /**< Open Fault has been detected */
    LIP_PHY_CABLE_CHECK_STATUS_SHORT  = 2, /**< Short Fault has been detected */
    LIP_PHY_CABLE_CHECK_STATUS_FAILED = 3, /**<  Cable diagnostic test failed */
} t_lip_phy_cable_check_status;

typedef struct {
    uint8_t fsm;
    uint8_t pair_num;
    uint8_t pair_msk;
    uint8_t res_cntr;
    uint16_t bctl;
} t_lip_phy_cable_test_ctx;

typedef struct {
    uint8_t valid;
    uint8_t status;
    uint16_t fail_code;
    float   distance;
} t_lip_phy_cable_test_pair_result;

typedef struct {
    bool finished;
    t_lip_phy_cable_test_pair_result pair[LIP_PHY_MAX_PAIR_CNT];
} t_lip_phy_cable_test_result;


typedef t_lip_errs (* t_lip_phy_cable_test_start)(const t_lip_phy_mdio_acc *regs_acc, t_lip_phy_cable_test_ctx *state, uint32_t pair);
typedef t_lip_errs (* t_lip_phy_cable_test_check)(const t_lip_phy_mdio_acc *regs_acc, t_lip_phy_cable_test_ctx *state, t_lip_phy_cable_test_result *result);

typedef struct {
    t_lip_phy_cable_test_start start; /**< Запуск теста проверки кабеля */
    t_lip_phy_cable_test_check check; /**< Проверка результатов теста */
} t_lip_phy_cable_test_check_info;

typedef struct {
    t_lip_phy_cable_test_ctx ctx;
    t_lip_phy_cable_test_result result;
} t_lip_phy_cable_test_data;

#endif // LIP_PHY_CABLE_TEST_DEFS_H
