#ifndef KSZ9131_REGS_H
#define KSZ9131_REGS_H

#include "../lip_phy_mdio_regs.h"

#define KSZ9131_ID_VENDOR_H  0x0022
#define KSZ9131_ID_VENDOR_L  0x0005
#define KSZ9131_ID_VENDOR    LIP_PHY_VENDOR_ID(KSZ9131_ID_VENDOR_H, KSZ9131_ID_VENDOR_L)
#define KSZ9131_ID_MODEL     0x0024
#define KSZ9131_ID_REV_A0    0
#define KSZ9131_ID_REV_B0    1
#define KSZ9131_ID_REV_C0    2


#define KSZ9131_REGADDR_REM_LOOP                17 /* Remote Loopback Register */
#define KSZ9131_REGADDR_LINKMD                  18 /* LinkMD Cable Diagnostic Register */
#define KSZ9131_REGADDR_PCS_STAT                19 /* Digital PMA/PCS Status Register */
#define KSZ9131_REGADDR_RXERR_CNT               21 /* Receive Error Counter Register */
#define KSZ9131_REGADDR_LED_MODE                22 /* LED Mode Select Register */
#define KSZ9131_REGADDR_LED_BHV                 23 /* LED Behavior Register */
#define KSZ9131_REGADDR_MDIO_DRIVE              25 /* MDIO Drive Register */
#define KSZ9131_REGADDR_LED_KSZ9031_MODE        26 /* KSZ9031 LED Mode Register */
#define KSZ9131_REGADDR_INT_CTL_STAT            27 /* Interrupt Control/Status Register */
#define KSZ9131_REGADDR_AUTO_MDIX               28 /* Auto MDI/MDI-X Register */
#define KSZ9131_REGADDR_SPD_CTL                 29 /* Software Power Down Control Register */
#define KSZ9131_REGADDR_EXT_LOOP                30 /* External Loopback Register */
#define KSZ9131_REGADDR_CTL                     31 /* Control Register */
/* ------------------------------- MMD ---------------------------------------*/
#define KSZ9131_REGADDR_MEAN_SLICER_ERR         LIP_PHY_REGADDR_MMD(1, 225) /* Mean Slicer Error Register */
#define KSZ9131_REGADDR_DCQ_MSE                 LIP_PHY_REGADDR_MMD(1, 226) /* DCQ Mean Square Error Register */
#define KSZ9131_REGADDR_DCQ_MSE_WORST           LIP_PHY_REGADDR_MMD(1, 227) /* DCQ Mean Square Error Worst Case Register */
#define KSZ9131_REGADDR_DCQ_SQI                 LIP_PHY_REGADDR_MMD(1, 228) /* DCQ SQI Register */
#define KSZ9131_REGADDR_DCQ_MSE_PEAK            LIP_PHY_REGADDR_MMD(1, 229) /* DCQ Peak MSE Register */
#define KSZ9131_REGADDR_DCQ_CTL                 LIP_PHY_REGADDR_MMD(1, 230) /* DCQ Control Register */
#define KSZ9131_REGADDR_DCQ_CFG                 LIP_PHY_REGADDR_MMD(1, 231) /* DCQ Configuration Register */
#define KSZ9131_REGADDR_SQI_TABLE(r)            LIP_PHY_REGADDR_MMD(1, 232 + (r)) /* DCQ SQI Table Registers */
#define KSZ9131_REGCNT_SQI_TABLE                7
#define KSZ9131_REGADDR_COMM_CTL                LIP_PHY_REGADDR_MMD(2,   0) /* Common Control Register */
#define KSZ9131_REGADDR_STRAP_STAT              LIP_PHY_REGADDR_MMD(2,   1) /* Strap Status Register */
#define KSZ9131_REGADDR_MODE_STRAP_OVRD         LIP_PHY_REGADDR_MMD(2,   2) /* Operation Mode Strap Override Register */
#define KSZ9131_REGADDR_MODE_STRAP              LIP_PHY_REGADDR_MMD(2,   3) /* Operation Mode Strap Register */
#define KSZ9131_REGADDR_CLK_INV_CTL_SKEW        LIP_PHY_REGADDR_MMD(2,   4) /* Clock Invert and Control Signal Pad Skew Register */
#define KSZ9131_REGADDR_RXD_SKEW                LIP_PHY_REGADDR_MMD(2,   5) /* RX Data Pad Skew Register */
#define KSZ9131_REGADDR_TXD_SKEW                LIP_PHY_REGADDR_MMD(2,   6) /* TX Data Pad Skew Register */
#define KSZ9131_REGADDR_CLK_SKEW                LIP_PHY_REGADDR_MMD(2,   8) /* Clock Pad Skew Register */
#define KSZ9131_REGADDR_SELFTEST_PKT_CNTL       LIP_PHY_REGADDR_MMD(2,   9) /* Self-Test Packet Count LO Register */
#define KSZ9131_REGADDR_SELFTEST_PKT_CNTH       LIP_PHY_REGADDR_MMD(2,  10) /* Self-Test Packet Count HI Register */
#define KSZ9131_REGADDR_SELFTEST_STAT           LIP_PHY_REGADDR_MMD(2,  11) /* Self-Test Status Register */
#define KSZ9131_REGADDR_SELFTEST_FRAME_CNT_EN   LIP_PHY_REGADDR_MMD(2,  12) /* Self-Test Frame Count Enbale Register */
#define KSZ9131_REGADDR_SELFTEST_PGEN_EN        LIP_PHY_REGADDR_MMD(2,  13) /* Self-Test PGEN Enbale Register */
#define KSZ9131_REGADDR_SELFTEST_EN             LIP_PHY_REGADDR_MMD(2,  14) /* Self-Test Enbale Register */
#define KSZ9131_REGADDR_WOL_CTL                 LIP_PHY_REGADDR_MMD(2,  16) /* Wake-On-LAN Control Register */
#define KSZ9131_REGADDR_WOL_MAC_L               LIP_PHY_REGADDR_MMD(2,  17) /* MAC-Address[15:0] of magic packet */
#define KSZ9131_REGADDR_WOL_MAC_M               LIP_PHY_REGADDR_MMD(2,  18) /* MAC-Address[31:16] of magic packet */
#define KSZ9131_REGADDR_WOL_MAC_H               LIP_PHY_REGADDR_MMD(2,  19) /* MAC-Address[47:32] of magic packet */
#define KSZ9131_RECCNT_WOL_CPKT_FLT             4
#define KSZ9131_REGADDR_WOL_CPKT_CRC_L(n)       LIP_PHY_REGADDR_MMD(2,  20 + 2*(n)) /* Customized frame filter n CRC[15:0] */
#define KSZ9131_REGADDR_WOL_CPKT_CRC_H(n)       LIP_PHY_REGADDR_MMD(2,  21 + 2*(n)) /* Customized frame filter n CRC[31:16] */
#define KSZ9131_REGADDR_WOL_CPKT_MSK_LL(n)      LIP_PHY_REGADDR_MMD(2,  28 + 4*(n)) /* Customized frame filter n mask[15:0] */
#define KSZ9131_REGADDR_WOL_CPKT_MSK_LH(n)      LIP_PHY_REGADDR_MMD(2,  29 + 4*(n)) /* Customized frame filter n mask[31:16] */
#define KSZ9131_REGADDR_WOL_CPKT_MSK_HL(n)      LIP_PHY_REGADDR_MMD(2,  30 + 4*(n)) /* Customized frame filter n mask[47:32] */
#define KSZ9131_REGADDR_WOL_CPKT_MSK_HH(n)      LIP_PHY_REGADDR_MMD(2,  31 + 4*(n)) /* Customized frame filter n mask[63:48] */
#define KSZ9131_REGADDR_WOL_CTRL_STAT           LIP_PHY_REGADDR_MMD(2,  44) /* Wake-on-LAN Control Status Register */
#define KSZ9131_REGADDR_WOL_CPKT_RXSTAT         LIP_PHY_REGADDR_MMD(2,  45) /* Wake-on-LAN Custom Packet Receive Status Register */
#define KSZ9131_REGADDR_WOL_MPKT_RXSTAT         LIP_PHY_REGADDR_MMD(2,  46) /* Wake-on-LAN Magic Packet Receive Status Register */
#define KSZ9131_REGADDR_WOL_DATA_STAT           LIP_PHY_REGADDR_MMD(2,  47) /* Wake-on-LAN Data Module Status Register */
#define KSZ9131_REGADDR_WOL_CPKT_RCRC_L(n)      LIP_PHY_REGADDR_MMD(2,  48 + 2*(n)) /* Customized Packet n Received CRC [15:0] */
#define KSZ9131_REGADDR_WOL_CPKT_RCRC_H(n)      LIP_PHY_REGADDR_MMD(2,  49 + 2*(n)) /* Customized Packet n Received CRC [31:16] */
#define KSZ9131_REGADDR_SELFTEST_COR_CNT_L      LIP_PHY_REGADDR_MMD(2,  60) /* Self-Test Correct Count Low Register [15:0] */
#define KSZ9131_REGADDR_SELFTEST_COR_CNT_H      LIP_PHY_REGADDR_MMD(2,  61) /* Self-Test Correct Count High Register [31:16] */
#define KSZ9131_REGADDR_SELFTEST_ERR_CNT_L      LIP_PHY_REGADDR_MMD(2,  62) /* Self-Test Error Count Low Register [15:0] */
#define KSZ9131_REGADDR_SELFTEST_ERR_CNT_H      LIP_PHY_REGADDR_MMD(2,  63) /* Self-Test Error Count High Register [31:16] */
#define KSZ9131_REGADDR_SELFTEST_BAD_SFD_L      LIP_PHY_REGADDR_MMD(2,  64) /* Self-Test Bad SFD Count Low Register [15:0] */
#define KSZ9131_REGADDR_RXDLL_CTL               LIP_PHY_REGADDR_MMD(2,  76) /* RX DLL Control Register */
#define KSZ9131_REGADDR_TXDLL_CTL               LIP_PHY_REGADDR_MMD(2,  77) /* TX DLL Control Register */

#define KSZ9131_REGADDR_PCS_CTL1                LIP_PHY_REGADDR_MMD(3,   0) /* --> LIP_PHY_REGADDR_PCS_CTL1 */
#define KSZ9131_REGADDR_PCS_STAT1               LIP_PHY_REGADDR_MMD(3,   1) /* --> LIP_PHY_REGADDR_PCS_STAT1 */
#define KSZ9131_REGADDR_EEE_QUIET_TMR           LIP_PHY_REGADDR_MMD(3,   8) /* EEE Quiet Timer Register */
#define KSZ9131_REGADDR_EEE_UPD_TMR             LIP_PHY_REGADDR_MMD(3,   9) /* EEE Update Timer Register */
#define KSZ9131_REGADDR_EEE_LINK_FAIL_TMR       LIP_PHY_REGADDR_MMD(3,  10) /* EEE Link-Fail Timer Register */
#define KSZ9131_REGADDR_EEE_POST_UPD_TMR        LIP_PHY_REGADDR_MMD(3,  11) /* EEE Post-Update Timer Register */
#define KSZ9131_REGADDR_EEE_WAITWQ_TMR          LIP_PHY_REGADDR_MMD(3,  12) /* EEE WaitWQ Timer Register */
#define KSZ9131_REGADDR_EEE_WAKE_TMR            LIP_PHY_REGADDR_MMD(3,  13) /* EEE Wait Timer Register */
#define KSZ9131_REGADDR_EEE_WAKETX_TMR          LIP_PHY_REGADDR_MMD(3,  14) /* EEE WaitTX Timer Register */
#define KSZ9131_REGADDR_EEE_WAKEMZ_TMR          LIP_PHY_REGADDR_MMD(3,  15) /* EEE WaitMz Timer Register */
#define KSZ9131_REGADDR_EEE_CTLCAP1             LIP_PHY_REGADDR_MMD(3,  20) /* --> LIP_PHY_REGADDR_PCS_EEE_CTLCAP1 */
#define KSZ9131_REGADDR_EEE_WAKE_ERR_CNTR       LIP_PHY_REGADDR_MMD(3,  22) /* --> LIP_PHY_REGADDR_PCS_EEE_WAKE_ERR_CNTR */
#define KSZ9131_REGADDR_EEE_100_TIMER0          LIP_PHY_REGADDR_MMD(3,  24) /* EEE 100 Timer-0 Register */
#define KSZ9131_REGADDR_EEE_100_TIMER1          LIP_PHY_REGADDR_MMD(3,  25) /* EEE 100 Timer-1 Register */
#define KSZ9131_REGADDR_EEE_100_TIMER2          LIP_PHY_REGADDR_MMD(3,  26) /* EEE 100 Timer-2 Register */
#define KSZ9131_REGADDR_EEE_100_TIMER3          LIP_PHY_REGADDR_MMD(3,  27) /* EEE 100 Timer-3 Register */

#define KSZ9131_REGADDR_EEE_ADV                 LIP_PHY_REGADDR_MMD(7,  60) /* --> LIP_PHY_REGADDR_EEE_ADV1 */
#define KSZ9131_REGADDR_EEE_LP_ABL              LIP_PHY_REGADDR_MMD(7,  61) /* --> LIP_PHY_REGADDR_AN_EEE_LP_ABL1 */
#define KSZ9131_REGADDR_EEE_LP_ABL_OVRD         LIP_PHY_REGADDR_MMD(7,  62) /* EEE Link Partner Ability Override Register */
#define KSZ9131_REGADDR_EEE_MSG_CODE            LIP_PHY_REGADDR_MMD(7,  63) /* EEE Message Code Register */

#define KSZ9131_REGADDR_XTAL_CTL                LIP_PHY_REGADDR_MMD(28,  1) /* XTAL Control Register */
#define KSZ9131_REGADDR_AFED_CTL                LIP_PHY_REGADDR_MMD(28,  9) /* AFED Control Register */
#define KSZ9131_REGADDR_LDO_CTL                 LIP_PHY_REGADDR_MMD(28, 14) /* LDO Control Register */
#define KSZ9131_REGADDR_LOOPBACK_CFG1           LIP_PHY_REGADDR_MMD(28, 21)
#define KSZ9131_REGADDR_LOOPBACK_CFG2           LIP_PHY_REGADDR_MMD(28, 22)
#define KSZ9131_REGADDR_LOOPBACK_CFG3           LIP_PHY_REGADDR_MMD(28, 24)
#define KSZ9131_REGADDR_LOOPBACK_CFG4           LIP_PHY_REGADDR_MMD(28, 27)
#define KSZ9131_REGADDR_EDPD_CTL                LIP_PHY_REGADDR_MMD(28, 36) /* Eenergy Detected Power Down Control Register */
#define KSZ9131_REGADDR_EMITX_CTL               LIP_PHY_REGADDR_MMD(28, 37) /* EMITX Control Register */
#define KSZ9131_REGADDR_EMITX_COEF(n)           LIP_PHY_REGADDR_MMD(28, 38 + (n)) /* EMITX Coefficient n Register */
#define KSZ9131_REGCNT_EMITX_COEF               15
#define KSZ9131_REGADDR_QW                      LIP_PHY_REGADDR_MMD(31, 19) /* Quiet-WIRE Register */



/****** Remote Loopback Register (Register 17, REM_LOOP) **********************/
#define KSZ9131_REGFLD_REM_LOOP_EN                  (0x0001UL <<  8U) /* (RW)    Enable remote loopback */

/****** LinkMD Cable Diagnostic Register (Register 18, LINKMD) ****************/
#define KSZ9131_REGFLD_LINKMD_VCT_EN                (0x0001UL << 15U) /* (RW SC) Cable Diagnostics Test Enable  */
#define KSZ9131_REGFLD_LINKMD_VCT_DIS_TX            (0x0001UL << 14U) /* (RW)    Cable Diagnostic Disable Transmitter */
#define KSZ9131_REGFLD_LINKMD_VCT_PAIR              (0x0003UL << 12U) /* (RW)    Cable Diagnostics Test Pair */
#define KSZ9131_REGFLD_LINKMD_VCT_STAT              (0x0003UL <<  8U) /* (RO)    Cable Diagnostics Status */
#define KSZ9131_REGFLD_LINKMD_VCT_DATA              (0x00FFUL <<  0U) /* (RO)    Cable Diagnostics Data or Threshold */
#define KSZ9131_REGMSK_LINKMD_RESERVED              (0x0C00UL)

#define KSZ9131_REGFLDVAL_LINKMD_VCT_STAT_NORM      0
#define KSZ9131_REGFLDVAL_LINKMD_VCT_STAT_OPEN      1
#define KSZ9131_REGFLDVAL_LINKMD_VCT_STAT_SHORT     2
#define KSZ9131_REGFLDVAL_LINKMD_VCT_STAT_FAILED    3
#define KSZ9131_REGFLDVAL_LINKMD_VCT_DATA_ERR_HPULSE_CNT            (0x3 << 0)
#define KSZ9131_REGFLDVAL_LINKMD_VCT_DATA_ERR_LPULSE_CNT            (0x3 << 2)
#define KSZ9131_REGFLDVAL_LINKMD_VCT_DATA_ERR_NFLP_NOT_SILENT       (0x1 << 4)
#define KSZ9131_REGFLDVAL_LINKMD_VCT_DATA_ERR_100M_NOT_SILENT       (0x1 << 5)
#define KSZ9131_REGFLDVAL_LINKMD_VCT_DATA_ERR_WAIT_TOO_LONG         (0x1 << 6)
#define KSZ9131_REGFLDVAL_LINKMD_VCT_DATA_ERR_PULSE_WIDTH           (0x1 << 7)



/****** Digital PMA/PCS Status Register (Register 19, PCS_STAT) ***************/
#define KSZ9131_REGFLD_PCS_STAT_1000BT_OK           (0x0001UL <<  1U) /* (RO)    1000BT link status OK */
#define KSZ9131_REGFLD_PCS_STAT_100BT_OK            (0x0001UL <<  0U) /* (RO)    100BT link status OK */
#define KSZ9131_REGMSK_PCS_STAT_RESERVED            (0xFFFCUL)

/****** Receive Error Counter Register (Register 21, RXERR_CNT) ***************/
#define KSZ9131_REGFLD_RXERR_CNT_VAL                (0xFFFFUL <<  0U) /* (RC)    RX Error counter for the RX_ER signal */


/****** LED Mode Select Register (Register 22, LED_MODE) **********************/
#define KSZ9131_REGFLD_LED_MODE_CFG(l)              (0x000FUL <<  (4U * l)) /* (RW) LED l (0 - LED1, 1 - LED2)  Configuration */
#define KSZ9131_REGMSK_LED_MODE_RESERVED            (0xFF00UL)

#define KSZ9131_REGFLDVAL_LED_MODE_LINK_ACT         0 /* Valid link at any speed on any media interface + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_LINK1000_ACT     1 /* Valid link at 1000BASE-T + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_LINK100_ACT      2 /* Valid link at 100BASE-TX + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_LINK10_ACT       3 /* Valid link at 10BASE-T + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_LINK100_1000_ACT 4 /* Valid link at 100BASE-TX or 1000BASE-T + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_LINK10_1000_ACT  5 /* Valid link at 10BASE-T or 1000BASE-T  + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_LINK10_100_ACT   6 /* Valid link at 10BASE-T or 100BASE-TX  + Activity */
#define KSZ9131_REGFLDVAL_LED_MODE_DUPLEX_COL       8 /* Full Duplex + Collision */
#define KSZ9131_REGFLDVAL_LED_MODE_COL              9 /* Collision detected */
#define KSZ9131_REGFLDVAL_LED_MODE_ACT              10 /* Activity detected */
#define KSZ9131_REGFLDVAL_LED_MODE_AN_FLT           12 /* Auto-NegotiationFault */
#define KSZ9131_REGFLDVAL_LED_MODE_OFF              14 /* De-asserts the LED (1) */
#define KSZ9131_REGFLDVAL_LED_MODE_ON               15 /* Asserts the LED (0) */


#define KSZ9131_LEDNUM_LED1                         0
#define KSZ9131_LEDNUM_LED2                         1

/****** LED Behavior Register (Register 23, LED_BHV) **************************/
#define KSZ9131_REGFLD_LED_BHV_EXT_MODE_FIX         (0x0001UL << 15U) /* (RW)   Необходим для корректной работы LED в Extended Mode! (в регистрах Reserved, но есть главе по LED) */
#define KSZ9131_REGFLD_LED_BHV_ACTIVITY_OUT         (0x0001UL << 14U) /* (RW)    LED Activity Output Select */
#define KSZ9131_REGFLD_LED_BHV_PULSING_EN           (0x0001UL << 12U) /* (RW)    LED Pulsing Enable */
#define KSZ9131_REGFLD_LED_BHV_RATE                 (0x0003UL << 10U) /* (RW)    LED Blink / Pulse-Stretch Rate */
#define KSZ9131_REGFLD_LED_BHV_PULSE_STRETCH_EN(l)  (0x0001UL << (5U + (l))) /* (RW) LED l Pulse Stretch Enables */
#define KSZ9131_REGFLD_LED_BHV_COMBINE_DIS(l)       (0x0001UL << (0U + (l))) /* (RW) LED l Combination Disables */
#define KSZ9131_REGMSK_LED_BHV_RESERVED             (0x239CUL)


#define KSZ9131_REGFLDVAL_LED_BHV_RATE_2_5HZ        0
#define KSZ9131_REGFLDVAL_LED_BHV_RATE_5HZ          1
#define KSZ9131_REGFLDVAL_LED_BHV_RATE_10HZ         2
#define KSZ9131_REGFLDVAL_LED_BHV_RATE_20HZ         3

/****** MDIO Drive Register (Register 25, MDIO_DRIVE) *************************/
#define KSZ9131_REGFLD_MDIO_DRIVE_VAL               (0x0001UL <<  1U) /* (RW)    LED Activity Output Select */
#define KSZ9131_REGMSK_MDIO_RESERVED                (0xFFFDUL)

/****** KSZ9031 LED Mode Register  (Register 26, LED_KSZ9031_MODE) ************/
#define KSZ9131_REGFLD_LED_KSZ9031_MODE_EN          (0x0001UL << 14U) /* (RW)    KSZ9031 Compatible LED Mode */
#define KSZ9131_REGMSK_MDIO_RESERVED                (0xFFFDUL)

/****** Interrupt Control/Status Register  (Register 27, INT_CTL_STAT) ********/
#define KSZ9131_REGFLD_INT_CTL_STAT_IE(i)           (0x0001UL << (8U + (i))) /* (RW) Interrupt (i) Enable */
#define KSZ9131_REGFLD_INT_CTL_STAT_INT(i)          (0x0001UL << (0U + (i))) /* (RC) Interrupt (i) Flag */

#define KSZ9131_INTNUM_JABBER                       7 /* Jabber Interrupt */
#define KSZ9131_INTNUM_RXERR                        6 /* Receive error Interrupt */
#define KSZ9131_INTNUM_AN_PG_RCVD                   5 /* Page receive interrupt */
#define KSZ9131_INTNUM_PARA_DET_FLT                 4 /* Parallel Detect Fault Interrupt */
#define KSZ9131_INTNUM_AN_LP_ACK                    3 /* Link Partner Acknowledge Interrupt */
#define KSZ9131_INTNUM_LINK_DOWN                    2 /* Link Down Interrupt */
#define KSZ9131_INTNUM_REM_FLT                      1 /* Remote Fault Interrupt */
#define KSZ9131_INTNUM_LINK_UP                      0 /* Link Up Interrupt */

/****** Auto MDI/MDI-X Register  (Register 28, AUTO_MDIX) *********************/
#define KSZ9131_REGFLD_AUTO_MDIX_MAN_CFG            (0x0001UL <<  7U) /* (RW)    MDI/MDI-X Mode Manual Config Value  (0 - MDI-X, 1 - MDI) */
#define KSZ9131_REGFLD_AUTO_MDIX_MAN_EN             (0x0001UL <<  6U) /* (RW)    MDI/MDI-X Mode Manual Config Enable (diable Auto-MDI/MDI-X) */
#define KSZ9131_REGMSK_AUTO_MDIX_RESERVED           (0xFF3FUL)

/****** Software Power Down Control Register (Register 29, SPD_CTL) ***********/
#define KSZ9131_REGFLD_SPD_CTL_CLKGATE_OVD          (0x0001UL << 11U) /* (RW)    Internal clock gating is overridden during the SPD mode */
#define KSZ9131_REGFLD_SPD_CTL_PLL_DIS              (0x0001UL << 10U) /* (RW)    PLL is disabled during the SPD mode */
#define KSZ9131_REGFLD_SPD_CTL_IO_DC_TEST_EN        (0x0001UL <<  7U) /* (RW)    enable IO test */
#define KSZ9131_REGFLD_SPD_CTL_VOH                  (0x0001UL <<  6U) /* (RW)    output IO to GND (0) or VDD (1) */
#define KSZ9131_REGMSK_SPD_CTL_RESERVED             (0xF33FUL)

/****** External Loopback Register (Register 30, EXT_LOOP) ********************/
#define KSZ9131_REGFLD_EXT_LOOP_SINGLE_LED_FIX      (0x0001UL <<  9U) /* (RW)    Согласно Errata, требуется установить этот бит для корректной работы Signle-LED Mode */
#define KSZ9131_REGFLD_EXT_LOOP_EN                  (0x0001UL <<  3U) /* (RW)    External loopback enable */
#define KSZ9131_REGMSK_RESERVED                     (0xFFD7UL)

/****** Control Register (Register 31, CTL) ***********************************/
#define KSZ9131_REGFLD_CTL_INT_POL_INVERT           (0x0001UL << 14U) /* (RW)    Interrupt Polarity Invert */
#define KSZ9131_REGFLD_CTL_JBR_EN                   (0x0001UL <<  9U) /* (RW)    Enable jabber counter */
#define KSZ9131_REGFLD_CTL_SQE_TEST_EN              (0x0001UL <<  8U) /* (RW)    Enable SQE Test */
#define KSZ9131_REGFLD_CTL_STAT_SPEED_1000_T        (0x0001UL <<  6U) /* (RO)    Speed status 1000BASE-T */
#define KSZ9131_REGFLD_CTL_STAT_SPEED_100_TX        (0x0001UL <<  5U) /* (RO)    Speed status 100BASE-TX */
#define KSZ9131_REGFLD_CTL_STAT_SPEED_10_T          (0x0001UL <<  4U) /* (RO)    Speed status 10BASE-T */
#define KSZ9131_REGFLD_CTL_STAT_DUPLEX              (0x0001UL <<  3U) /* (RO)    Duplex status */
#define KSZ9131_REGFLD_CTL_STAT_MS                  (0x0001UL <<  2U) /* (RO)    1000BASE-T Mater/Slave status */
#define KSZ9131_REGFLD_CTL_SW_RST                   (0x0001UL <<  1U) /* (W1S RC) Software Reset (Reset PHY except all registers) */
#define KSZ9131_REGFLD_CTL_STAT_LINK_CHK_FAIL       (0x0001UL <<  1U) /* (RC)    Link Status Check Fail */



/****** MMD Mean Slicer Error Register (Register 1.225, MEAN_SLICER_ERR) ******/
#define KSZ9131_REGFLD_MEAN_SLICER_ERR_VAL          (0xFFFFUL <<  0U) /* (RO)    Mean Slicer Error Value */

/* MMD DCQ Mean Square Error/Worst Case Register (Register 1.226/1.127, DCQ_MSE/DCQ_MSE_WORST) */
#define KSZ9131_REGFLD_DCQ_MSE_INVALID              (0x0001UL <<  9U) /* (RO)    MSE Value Valid */
#define KSZ9131_REGFLD_DCQ_MSE_VALUE                (0x01FFUL <<  0U) /* (RO)    MSE Value */
#define KSZ9131_REGMSK_DCQ_MSE_RESERVED             (0xFC00UL)

/****** MMD DCQ SQI Register (Register 1.228, DCQ_SQI) ************************/
#define KSZ9131_REGFLD_DCQ_SQI_WORST                (0x0007UL <<  5U) /* (RO)    SQI Worst Case */
#define KSZ9131_REGFLD_DCQ_SQI_VALUE                (0x0007UL <<  1U) /* (RO)    Current SQI value */
#define KSZ9131_REGMSK_DCQ_SQI_RESERVED             (0xFF11UL)

/****** MMD DCQ Peak MSE Register (Register 1.229, DCQ_MSE_PEAK) **************/
#define KSZ9131_REGFLD_DCQ_MSE_PEAK_WORST           (0x00FFUL <<  8U) /* (RO)    Peak MSE Worst Case */
#define KSZ9131_REGFLD_DCQ_MSE_PEAK_VALUE           (0x00FFUL <<  0U) /* (RO)    Current peak MSE value. */

#define KSZ9131_REGFLDVAL_DCQ_MSE_PEAK_MAX          63  /* Максимальное действительное значение параметров PEAK_WORDS/PEAK_VALUE */
#define KSZ9131_REGFLDVAL_DCQ_MSE_PEAK_NOT_RDY      0xFF /* Признак, что PEAK_WORDS/PEAK_VALUE еще не готовы */

/****** MMD Control Register (Register 1.230, DCQ_CTL) ************************/
#define KSZ9131_REGFLD_DCQ_CTL_RD_CAPTURE           (0x0001UL <<  15U) /* (RW SC) DCQ Read Capture */
#define KSZ9131_REGFLD_DCQ_CTL_CHNUM                (0x0003UL <<   0U) /* (RW)    DCQ Channel Number */
#define KSZ9131_REGMSK_DCQ_CTL_RESERVED             (0x7FFCUL)

#define KSZ9131_REGFLDVAL_DCQ_CTL_CHNUM_A           0
#define KSZ9131_REGFLDVAL_DCQ_CTL_CHNUM_B           1
#define KSZ9131_REGFLDVAL_DCQ_CTL_CHNUM_C           2
#define KSZ9131_REGFLDVAL_DCQ_CTL_CHNUM_D           3

/****** MMD Configuration Register (Register 1.231, DCQ_CFG) ******************/
#define KSZ9131_REGFLD_DCQ_CFG_SCALE613             (0x0003UL <<  14U) /* (RW)   Scaling factor for SQI method 5 (TC1 peak MSE) */
#define KSZ9131_REGFLD_DCQ_CFG_SQI_KP3              (0x000FUL <<  10U) /* (RW)   LPF bandwidth control.for SQI method 5 (TC1 peak MSE) */
#define KSZ9131_REGFLD_DCQ_CFG_SCALE611             (0x0003UL <<   8U) /* (RW)   Scaling factor for SQI methods 3 (TC1 MSE) and 4 (TC1 SQI) */
#define KSZ9131_REGFLD_DCQ_CFG_SQI_RST              (0x0001UL <<   7U) /* (RW)   Reset SQI logic */
#define KSZ9131_REGFLD_DCQ_CFG_SQI_SQU_MODE_EN      (0x0001UL <<   6U) /* (RW)   Square mode enable */
#define KSZ9131_REGFLD_DCQ_CFG_SQI_EN               (0x0001UL <<   5U) /* (RW)   SQI enable */
#define KSZ9131_REGFLD_DCQ_CFG_SQI_KP               (0x001FUL <<   0U) /* (RW)   LPF bandwidth control.for SQI methods 2 (non TC1 LPF mean), 3 (TC1 MSE) and 4 (TC1 SQI) */

/****** MMD SQI Table Registers (Registers 1.232-238, SQI_TABLE) **************/
#define KSZ9131_REGFLD_SQI_TABLE_VALUE              (0x00FFUL <<  0U) /* (RW)    threshold value to map the error value to a SQI level. */
#define KSZ9131_REGMSK_SQI_TABLE_RESERVED           (0x00FFUL <<  8U)

/* SQI Table Default Values */
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_1         0xA3
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_2         0x82
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_3         0x67
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_4         0x52
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_5         0x41
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_6         0x34
#define KSZ9131_REGFLDVAL_SQI_TABLE_VALUE_7         0x29

/****** MMD Common Control Register (Register 2.0, COMM_CTL) ******************/
#define KSZ9131_REGFLD_COMM_CTL_SINGLE_LED_MODE     (0x0001UL <<  4U) /* (RW)   Individual-LED Mode */
#define KSZ9131_REGFLD_COMM_CTL_CLK125_EN           (0x0001UL <<  1U) /* (RW)   Enables the 125 MHz clock output onto the CLK125_NDO pin */
#define KSZ9131_REGFLD_COMM_CTL_ALL_PHYADDR_EN      (0x0001UL <<  0U) /* (RW)   Enable response to PHY address 0 as well as it’s assigned PHY address. */
#define KSZ9131_REGMSK_COMM_CTL_RESERVED            (0xFFECUL)

/****** MMD Strap Status Register (Register 2.1, STRAP_STAT) ******************/
#define KSZ9131_REGFLD_STRAP_STAT_SINGLE_LED_MODE   (0x0001UL <<  7U) /* (RO)   LED_MODE Strap-In Status */
#define KSZ9131_REGFLD_STRAP_STAT_CLK125_EN         (0x0001UL <<  5U) /* (RO)   CLK125_EN strap-in is enabled */
#define KSZ9131_REGFLD_STRAP_STAT_PHYADDR           (0x001FUL <<  0U) /* (RO)   PHY Address Strap-In Status */
#define KSZ9131_REGMSK_STRAP_STAT_RESERVED          (0xFF40UL)

/* MMD Operation Mode Strap (Override) Register (Register 2.2/2.3, MODE_STRAP (OVRD)) */
#define KSZ9131_REGFLD_MODE_STRAP_AN_ALL_N1000_HD   (0x0001UL << 14U) /* (RW)    Advertise all capabilities except 1000BASE-T half duplex */
#define KSZ9131_REGFLD_MODE_STRAP_AN_1000_FD        (0x0001UL << 12U) /* (RW)    Advertise 1000BASE-T full duplex only */
#define KSZ9131_REGFLD_MODE_STRAP_PME_ON_INT        (0x0001UL << 10U) /* (RW)    PME_N2 mapped onto INT_N */
#define KSZ9131_REGFLD_MODE_STRAP_PME_ON_LED1       (0x0001UL <<  8U) /* (RW)    PME_N1 mapped onto LED1 */
#define KSZ9131_REGFLD_MODE_STRAP_IDDQ_PD           (0x0001UL <<  7U) /* (RW)    Chip IDDQ Power-Down */
#define KSZ9131_REGFLD_MODE_STRAP_NTREE             (0x0001UL <<  4U) /* (RW)    NAND Tree mode */
#define KSZ9131_REGMSK_MODE_STRAP_RESERVED          (0xAA6FUL)

/* MMD Clock Invert and Control Signal Pad Skew Register (Register 2.4, CLK_INV_CTL_SKEW) */
#define KSZ9131_REGFLD_CLK_INV_CTL_SKEW_RXC_INV     (0x0001UL <<  9U) /* (RW)    Inverse RGMII RXC Output */
#define KSZ9131_REGFLD_CLK_INV_CTL_SKEW_TXC_INV     (0x0001UL <<  8U) /* (RW)    Inverse RGMII TXC Input */
#define KSZ9131_REGFLD_CLK_INV_CTL_SKEW_RX_CTL_SKEW (0x000FUL <<  4U) /* (RW)    RX_CTL Output Skew Control (~28 min to ~73 max ps/step) */
#define KSZ9131_REGFLD_CLK_INV_CTL_SKEW_TX_CTL_SKEW (0x000FUL <<  0U) /* (RW)    TX_CTL Input Skew Control (~28 min to ~73 max ps/step) */
#define KSZ9131_REGMSK_CLK_INV_CTL_SKEW_RESERVED    (0xFC00UL)

/****** MMD RX/TX Data Pad Skew Register (Register 2.5/2.6, RXD_SKEW/TXD) *****/
#define KSZ9131_REGFLD_DATA_PAD_SKEW(n)             (0x000FUL <<  (4U * (n))) /* (RO) RXDn/TXDn Pad Skew Control (~28 min to ~73 max ps/step) */

/****** MMD Clock Pad Skew Register (Register 2.8, CLK_SKEW) ******************/
#define KSZ9131_REGFLD_CLK_SKEW_TXC                 (0x001FUL <<  5U) /* (RW)    TXC Pad Inut Skew Control  (~24 min to ~58 max ps/step) */
#define KSZ9131_REGFLD_CLK_SKEW_RXC                 (0x001FUL <<  0U) /* (RW)    RXC Pad Inut Skew Control  (~24 min to ~58 max ps/step) */
#define KSZ9131_REGMSK_CLK_SKEW_RESERVED            (0xFFC0UL)

/****** MMD Self-Test Packet Count LO Register (Register 2.9, SELFTEST_PKT_CNTL) */
#define KSZ9131_REGFLD_SELFTEST_PKT_CNTL_VAL        (0xFFFFUL <<  0U) /* (RW)    Self-Test frame count low word */
/****** MMD Self-Test Packet Count HI Register (Register 2.10, SELFTEST_PKT_CNTH) */
#define KSZ9131_REGFLD_SELFTEST_PKT_CNTH_VAL        (0xFFFFUL <<  0U) /* (RW)    Self-Test frame count high word */
/****** MMD Self-Test Status Register (Register 2.11, SELFTEST_STAT) **********/
#define KSZ9131_REGFLD_SELFTEST_STAT_DONE           (0x0001UL <<  0U) /* (RO)    Self-Test Finished */
/****** MMD Self-Test Frame Count Enbale Register (Register 2.12, SELFTEST_FRAME_CNT_EN) */
#define KSZ9131_REGFLD_SELFTEST_FRAME_CNT_EN        (0x0001UL <<  0U) /* (RW)    Self-Test Frame Count Enbale */
/****** MMD Self-Test PGEN Enbale Register (Register 2.13, SELFTEST_PGEN_EN) */
#define KSZ9131_REGFLD_SELFTEST_PGEN_EN             (0x0001UL <<  0U) /* (RW)    Self-Test PGEN Enbale */
/****** MMD Self-Test Enbale Register (Register 2.14, SELFTEST_EN) ************/
#define KSZ9131_REGFLD_SELFTEST_EN_EXTCLK_SEL       (0x0001UL << 15U) /* (RW)    Self-Test Requires a clock to be supplied onto GTX_CLK */
#define KSZ9131_REGFLD_SELFTEST_EN                  (0x0001UL <<  0U) /* (RW)    Self-Test Enbale */
#define KSZ9131_REGMSK_SELFTEST_EN_RESERVED         (0x7FFEUL)

/****** MMD Wake-On-LAN Control Register (Register 2.16, WOL_CTL) *************/
#define KSZ9131_REGFLD_WOL_CTL_PME_OUTSEL           (0x0003UL << 14U) /* (RW)    PME Output Select*/
#define KSZ9131_REGFLD_WOL_CTL_WOL_RST              (0x0001UL <<  7U) /* (RW)    Wake-on-LAN Reset */
#define KSZ9131_REGFLD_WOL_CTL_MPKT_WK_EN           (0x0001UL <<  6U) /* (RW)    Enable Magic Packet Detecton Wake Event */
#define KSZ9131_REGFLD_WOL_CTL_CPKT_WK_EN(n)        (0x0001UL <<  (2U + (n))) /* (RW)    Enable Customized Frame Filter n Wake Event */
#define KSZ9131_REGFLD_WOL_CTL_LINK_DOWN_WK_EN      (0x0001UL <<  1U) /* (RW)    Enable Link Down Wake Event */
#define KSZ9131_REGFLD_WOL_CTL_LINK_UP_WK_EN        (0x0001UL <<  0U) /* (RW)    Enable Link Up Wake Event */

/* MMD Wake-on-LAN Custom Packet Receive Status Register (Register 2.45, WOL_CPKT_RXSTAT) */
#define KSZ9131_REGFLD_WOL_CPKT_RXSTAT_PMEN         (0x0001UL << 15U) /* (RO)    Custom Packet enabled and found*/
#define KSZ9131_REGFLD_WOL_CPKT_RXSTAT_MSMATCH      (0x0007UL << 12U) /* (RO)    Mismatch Code */
#define KSZ9131_REGFLD_WOL_CPKT_RXSTAT_GOOD_CRC     (0x0001UL << 11U) /* (RO)    Good Packet CRC */
#define KSZ9131_REGFLD_WOL_CPKT_RXSTAT_CRC_MATCH(n) (0x0001UL <<  7U) /* (RO)    CRC matched for custom packet n */
#define KSZ9131_REGFLD_WOL_CPKT_RXSTAT_FOUND(n)     (0x0001UL <<  3U) /* (RO)    Custom packet n found */
#define KSZ9131_REGFLD_WOL_CPKT_RXSTAT_CPKT_STATE   (0x0007UL <<  0U) /* (RO)    Custom packet detection state */

/* MMD Wake-on-LAN Magic Packet Receive Status Register (Register 2.45, WOL_MPKT_RXSTAT) */
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_PMEN         (0x0001UL << 15U) /* (RO)    Magic Packet enabled and found*/
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_BYTE_CNT     (0x0007UL << 12U) /* (RO)    Byte Count */
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_MSMATCH      (0x0007UL <<  9U) /* (RO)    Mismatch Code */
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_MACDA_MCNT   (0x000FUL <<  5U) /* (RO)    MAC DA Match Count */
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_GOOD_CRC     (0x0001UL <<  4U) /* (RO)    Good Packet CRC */
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_MPKT_FOUND   (0x0001UL <<  3U) /* (RO)    Magic Packet Found */
#define KSZ9131_REGFLD_WOL_MPKT_RXSTAT_MPKT_STATE   (0x0001UL <<  0U) /* (RO)    Magic packet detection state */

/* MMD RX/TX DLL Control Register (Register 2.76/2.77, RXDLL_CTL/TXDLL_CTL) ***/
#define KSZ9131_REGFLD_DLL_CTL_TUNDE_DIS            (0x0001UL << 14U) /* (RW)    Disable dynamically DLL tune */
#define KSZ9131_REGFLD_DLL_CTL_RST                  (0x0001UL << 13U) /* (RW)    DLL Reset */
#define KSZ9131_REGFLD_DLL_CTL_BYPASS               (0x0001UL << 12U) /* (RW)    DLL delay is not used */
#define KSZ9131_REGFLD_DLL_TAP_SEL                  (0x003FUL <<  6U) /* (RW)    initial DLL ta settings (default 0x1B) */
#define KSZ9131_REGFLD_DLL_TAP_ADJ                  (0x003FUL <<  0U) /* (RW)    statically account in delay chain (default 0x11) */

/****** MMD EEE 100 Timer-0 Register (Register 3.24, EEE_100_TIMER0) **********/
#define KSZ9131_REGFLD_EEE_100_TIMER0_TX_SLEEP      (0x00FFUL <<  8U) /* (RW)    tx_sleep_time = (5250 + TX_SLEEP_TIMER_ADD * 32) * 40ns */
#define KSZ9131_REGFLD_EEE_100_TIMER0_TX_WAKE       (0x007FUL <<  1U) /* (RW)    tx_wake_time = (512 + TX_WAKE_TIMER_ADD * 32) * 40ns */
#define KSZ9131_REGMSK_EEE_100_TIMER0_RESERVED      (0x0001UL)
/****** MMD EEE 100 Timer-1 Register (Register 3.25, EEE_100_TIMER1) **********/
#define KSZ9131_REGFLD_EEE_100_TIMER1_RX_SLEEP      (0x00FFUL <<  8U) /* (RW)    rx_sleep_time = (6250 + RX_SLEEP_TIMER_ADD * 32) * 40ns */
#define KSZ9131_REGFLD_EEE_100_TIMER1_TX_QUIET      (0x007FUL <<  1U) /* (RW)    tx_quiet_time = (525000 + TX_QUIET_TIMER_ADD * 8192) * 40ns */
#define KSZ9131_REGFLD_EEE_100_TIMER1_EEE_TEST      (0x0001UL <<  0U) /* (RW)    force TX LPI */
/****** MMD EEE 100 Timer-2 Register (Register 3.26, EEE_100_TIMER2) **********/
#define KSZ9131_REGFLD_EEE_100_TIMER2_RX_IDLE_EXIT  (0x000FUL << 12U) /* (RW)    rx_wait_idle_exit_time = (16 + RX_WAIT_IDLE_EXIT_TIMER_ADD * 2) * 40ns */
#define KSZ9131_REGFLD_EEE_100_TIMER2_RX_IDLE_WAIT  (0x000FUL <<  8U) /* (RW)    rx_idle_wait_time = (20 + RX_IDLE_WAIT_TIMER_ADD * 2) * 40ns */
#define KSZ9131_REGFLD_EEE_100_TIMER2_RX_QUIET      (0x00FFUL <<  0U) /* (RW)    rx_quiet_time = (625000 + RX_QUIET_TIMER_ADD * 4096) * 40ns */
/****** MMD EEE 100 Timer-3 Register (Register 3.27, EEE_100_TIMER3) **********/
#define KSZ9131_REGFLD_EEE_100_TIMER3_RX_WAKE       (0x00FFUL <<  8U) /* (RW)    rx_wake_time = (512 + RX_WAKE_TIMER_ADD * 4) * 40ns */
#define KSZ9131_REGFLD_EEE_100_TIMER3_RX_LINK_FAIL  (0x00FFUL <<  0U) /* (RW)    rx_link_fail_time = (2500 + RX_LINK_FAIL_TIMER_ADD * 16) * 40ns */


/* MMD EEE Link Partner Ability Override Register (Register 7.62, EEE_LP_ABL_OVRD) */
#define KSZ9131_REGFLD_EEE_LP_ABL_OVRD_EN           (0x0001UL << 15U) /* (RW)    Enable LP AN Override */
#define KSZ9131_REGFLD_EEE_LP_ABL_OVRD_1000_T       (0x0001UL <<  2U) /* (RW)    1000BASE-T EEE */
#define KSZ9131_REGFLD_EEE_LP_ABL_OVRD_100_TX       (0x0001UL <<  1U) /* (RW)    100BASE-TX EEE */
#define KSZ9131_REGMSK_EEE_LP_ABL_OVRD_RESERVED     (0x7FF9UL)

/****** MMD EEE Message Code Register (Register 7.63, EEE_MSG_CODE) ***********/
#define KSZ9131_REGFLD_EEE_MSG_CODE_VAL             (0x03FFUL <<  0U) /* (RW)    Programmable EEE specific message code for AN */
#define KSZ9131_REGMSK_EEE_MSG_CODE_RESERVED        (0xFC00UL)

/****** MMD XTAL Control Register (Register 28.1, XTAL_CTL) *******************/
#define KSZ9131_REGFLD_XTAL_CTL_XTAL_DIS            (0x0001UL << 13U) /* (RW)    Crystal oscillator disable */
#define KSZ9131_REGMSK_XTAL_CTL_RESERVED            (0xDFFFUL)

/****** MMD AFED Control Register (Register 28.9, AFED_CTL) *******************/
#define KSZ9131_REGFLD_AFED_CTL_CAT3                (0x0001UL <<  9U) /* (RW)    cat3 parameter for 10 Base-T TX */
#define KSZ9131_REGMSK_AFED_CTL_RESERVED            (0xFDFFUL)

/****** MMD LDO Control Register (Register 28.14, LDO_CTL) ********************/
#define KSZ9131_REGFLD_LDO_CTL_LDE_EN               (0x0001UL << 15U) /* (RW)    turn off VDD regulator by software */
#define KSZ9131_REGMSK_LDO_CTL_RESERVED             (0x7FFFUL)

/****** MMD EPDP Control Register (Register 28.36, EDPD_CTL) ******************/
#define KSZ9131_REGFLD_EDPD_CTL_MASK_TMR            (0x0003UL <<  4U) /* (RW)    EPDP Mask Timer */
#define KSZ9131_REGFLD_EDPD_CTL_TMR                 (0x0003UL <<  2U) /* (RW)    EDPD pulse separation timer */
#define KSZ9131_REGFLD_EDPD_CTL_RND_DIS             (0x0001UL <<  1U) /* (RW)    Use pulse separation timer rather random seed */
#define KSZ9131_REGFLD_EDPD_CTL_EN                  (0x0001UL <<  0U) /* (RW)    EDPD Mode Enable */
#define KSZ9131_REGMSK_EDPD_CTL_RESERVED            (0xFFC0UL)

#define KSZ9131_REGFLDVAL_EDPD_CTL_MASK_TMR_2_6US   0
#define KSZ9131_REGFLDVAL_EDPD_CTL_MASK_TMR_3_2US   1
#define KSZ9131_REGFLDVAL_EDPD_CTL_MASK_TMR_4_0US   2
#define KSZ9131_REGFLDVAL_EDPD_CTL_MASK_TMR_5_0US   3

#define KSZ9131_REGFLDVAL_EDPD_CTL_TMR_1_0S         0
#define KSZ9131_REGFLDVAL_EDPD_CTL_TMR_1_3S         1
#define KSZ9131_REGFLDVAL_EDPD_CTL_TMR_1_6S         2
#define KSZ9131_REGFLDVAL_EDPD_CTL_TMR_1_9S         3

/****** MMD EMITX Control Register (Register 28.37, EMITX_CTL) ************d****/
#define KSZ9131_REGFLD_EMITX_CTL_SCALE              (0x0003UL <<  0U) /* (RW) */
#define KSZ9131_REGMSK_EMITX_CTL_RESERVED           (0xFFFCUL)

/****** MMD EMITX Coefficient Registers (Register 28.38-52, EMITX_COEF) *******/
#define KSZ9131_REGFLD_EMITX_COEF1                  (0x001FUL <<  8U) /* (RW) */
#define KSZ9131_REGFLD_EMITX_COEF0                  (0x001FUL <<  0U) /* (RW) */
#define KSZ9131_REGMSK_EMITX_COEF_RESERVED          (0x8080UL)

#define KSZ9131_REGVAL_EMITX_COEF(c1,c0)            (LBITFIELD_SET(KSZ9131_REGFLD_EMITX_COEF1, c1) | LBITFIELD_SET(KSZ9131_REGFLD_EMITX_COEF0, c0))

/****** MMD Quiet-WIRE Register (Register 31.19, QW) ******************/
#define KSZ9131_REGFLD_QW_EN                        (0x0001UL << 10U) /* (RW) */
#define KSZ9131_REGMSK_QW_RESERVED                  (0xFBFFUL)

#endif // KSZ9131_REGS_H
