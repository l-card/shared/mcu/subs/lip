#ifndef LIP_PHY_MDIO_REGS_MMD_PCS_H
#define LIP_PHY_MDIO_REGS_MMD_PCS_H

#include "lip_phy_mdio_regs_mmd.h"

#define LIP_PHY_REGADDR_PCS_CTL1                    LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_PCS,   0) /* PCS control 1 register */
#define LIP_PHY_REGADDR_PCS_STAT1                   LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_PCS,   1) /* PCS status 1 register */

#define LIP_PHY_REGADDR_PCS_EEE_CTLCAP1             LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_PCS,  20) /* EEE Control and Capability 1 Register */
#define LIP_PHY_REGADDR_PCS_EEE_CTLCAP2             LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_PCS,  21) /* EEE Control and Capability 2 Register */
#define LIP_PHY_REGADDR_PCS_EEE_WAKE_ERR_CNTR       LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_PCS,  22) /* EEE Wake Error Counter Register */


/****** PCS control 1 register (Register 3.0, PCS_CTL1) ***********************/
#define LIP_PHY_REGFLD_PCS_CTL1_RST                 (0x0001UL << 15) /* (RW SC) PCS Reset */
#define LIP_PHY_REGFLD_PCS_CTL1_LOOP                (0x0001UL << 14) /* (RW)    Enable PCS loopback mode */
#define LIP_PHY_REGFLD_PCS_CTL1_SPEED_HH            (0x0001UL << 13) /* (RW)    Speed Selection bit 5 (must be 1) */
#define LIP_PHY_REGFLD_PCS_CTL1_LP                  (0x0001UL << 11) /* (RW)    Low Power Mode */
#define LIP_PHY_REGFLD_PCS_CTL1_CLK_STOP_EN         (0x0001UL << 10) /* (RW)    Enable PHY stop the clock during LPI */
#define LIP_PHY_REGFLD_PCS_CTL1_SPEED_HL            (0x0001UL <<  6) /* (RW)    Speed Selection bit 4 (must be 1) */
#define LIP_PHY_REGFLD_PCS_CTL1_SPEED               (0x000FUL <<  2) /* (RW)    Speed Selection [3:0] */
#define LIP_PHY_REGMSK_PCS_CTL1_RESERVED            (0x1383UL)

#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_400G       0xA /* 400 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_200G       0x9 /* 200 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_5G         0x8 /* 5 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_2_5G       0x7 /* 2.5 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_25G        0x5 /* 25 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_100G       0x4 /* 100 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_40G        0x3 /* 40 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_10_1G      0x2 /* 10/1 Gb/s */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_10TS_2TL   0x1 /* 10PASS-TS/2BASE-TL */
#define LIP_PHY_REGFLDVAL_PCS_CTL1_SPEED_10G        0x0 /* 10 Gb/s */

/****** PCS status 1 register (Register 3.1, PCS_STAT1) ***********************/
#define LIP_PHY_REGFLD_PCS_STAT1_TX_LPI_RCVD        (0x0001UL << 11) /* (RO LH) Tx LPI Received */
#define LIP_PHY_REGFLD_PCS_STAT1_RX_LPI_RCVD        (0x0001UL << 10) /* (RO LH) Rx LPI Received */
#define LIP_PHY_REGFLD_PCS_STAT1_TX_LPI_IND         (0x0001UL <<  9) /* (RO)    Tx PCS is currently receiving LPI */
#define LIP_PHY_REGFLD_PCS_STAT1_RX_LPI_IND         (0x0001UL <<  8) /* (RO)    Rx PCS is currently receiving LPI */
#define LIP_PHY_REGFLD_PCS_STAT1_FAULT              (0x0001UL <<  7) /* (RO)    Fault condition detected */
#define LIP_PHY_REGFLD_PCS_STAT1_CLK_STOP_CAP       (0x0001UL <<  6) /* (RO)    The MAC may stop the clock during LPI */
#define LIP_PHY_REGFLD_PCS_STAT1_RX_LINK_UP         (0x0001UL <<  2) /* (RO LL) PCS Receive link up */
#define LIP_PHY_REGFLD_PCS_STAT1_LPWR_ABL           (0x0001UL <<  1) /* (RO)    PCS Supports Low-Power Mode */
#define LIP_PHY_REGMSK_PCS_STAT1_RESERVED           (0xF039UL)


/****** EEE Control and Capability 1 Register (Register 3.20, PCS_EEE_CTLCAP1) */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_100G_R_DSLP  (0x0001UL << 13) /* (RO)    100GBASE-R deep sleep */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_100G_R_FWAKE (0x0001UL << 12) /* (RO)    100GBASE-R fast wake */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_25G_R_DSLP   (0x0001UL << 11) /* (RO)    25GBASE-R deep sleep */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_25G_R_FWAKE  (0x0001UL << 10) /* (RO)    25GBASE-R fast wake */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_40G_R_DSLP   (0x0001UL <<  9) /* (RO)    40GBASE-R deep sleep */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_40G_R_FWAKE  (0x0001UL <<  8) /* (RO)    40GBASE-R fast wake */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_40G_T_EEE    (0x0001UL <<  7) /* (RO)    40GBASE-T EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_10G_KR_EEE   (0x0001UL <<  6) /* (RO)    10GBASE-KR EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_10G_KX4_EEE  (0x0001UL <<  5) /* (RO)    10GBASE-KX4 EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_10G_KX_EEE   (0x0001UL <<  4) /* (RO)    10GBASE-KX EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_10G_T_EEE    (0x0001UL <<  3) /* (RO)    10GBASE-T EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_1000_T_EEE   (0x0001UL <<  2) /* (RO)    1000BASE-T EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_100_TX_EEE   (0x0001UL <<  1) /* (RO)    100BASE-TX EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP1_LPI_FW       (0x0001UL <<  0) /* (RW)    Fast wake or Deep Sleep for LPI */
#define LIP_PHY_REGMSK_PCS_EEE_CTLCAP1_RESERVED     (0xC000UL)

/****** EEE Control and Capability 2 Register (Register 3.20, PCS_EEE_CTLCAP1) */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP2_400G_R_FW    (0x0001UL <<  5) /* (RO)    400GBASE-R fast wake */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP2_200G_R_FW    (0x0001UL <<  3) /* (RO)    200GBASE-R fast wake */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP2_25G_T_EEE    (0x0001UL <<  2) /* (RO)    25GBASE-T EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP2_2G_T_EEE     (0x0001UL <<  1) /* (RO)    5GBASE-T EEE */
#define LIP_PHY_REGFLD_PCS_EEE_CTLCAP2_2_5G_T_EEE   (0x0001UL <<  0) /* (RO)    2.5GBASE-T EEE */
#define LIP_PHY_REGMSK_PCS_EEE_CTLCAP2_RESERVED     (0xFFE0UL)


#endif // LIP_PHY_MDIO_REGS_MMD_PCS_H
