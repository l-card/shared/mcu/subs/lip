﻿#ifndef LIP_PHY_REGS_H
#define LIP_PHY_REGS_H

#include "lbitfield.h"
/* Описание стандартных регистров PHY из IEEE 802.3, доступных через MDIO/MDC интерфейс. */
#include "lip_phy_mdio_regs_std.h"
#include "lip_phy_mdio_regs_mmd_pcs.h"
#include "lip_phy_mdio_regs_mmd_an.h"

#endif // LIP_PHY_REGS_H


