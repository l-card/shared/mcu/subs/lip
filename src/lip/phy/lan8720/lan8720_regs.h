#ifndef LAN8720_REGS_H_
#define LAN8720_REGS_H_


#define LAN8720_ID_VENDOR_H  0x0007
#define LAN8720_ID_VENDOR_L  0x0030
#define LAN8720_ID_VENDOR    LIP_PHY_VENDOR_ID(LAN8720_ID_VENDOR_H, LAN8720_ID_VENDOR_L)
#define LAN8720_ID_MODEL     0x000F


#define LAN8720_REGADDR_MODE_CTL_STAT          17 /* Mode Control/Status Register */
#define LAN8720_REGADDR_SPECMODES              18 /* Special Modes Register */
#define LAN8720_REGADDR_SYMERR_CNT             26 /* Symbol Error Counter Register */
#define LAN8720_REGADDR_SPECCTL_STAT_IND       27 /* Special Control/Status Indication Register */
#define LAN8720_REGADDR_INT_SRC                29 /* Interrupt Source Flag Register */
#define LAN8720_REGADDR_INT_MSK                30 /* Interrupt Mask Register */
#define LAN8720_REGADDR_PHY_SPECCTL_STAT       31 /* PHY Special Control/Status Register */



/****** Mode Control/Status Register (Register 17, MODE_CTL_STAT) *************/
#define LAN8720_REGFLD_MODE_CTL_STAT_ED_PD          (0x0001UL <<  13U) /* (RW)    Enable the Energy Detect Power-Down mode */
#define LAN8720_REGFLD_MODE_CTL_STAT_FAR_LOOP       (0x0001UL <<   9U) /* (RW)    Enables far loopback mode  */
#define LAN8720_REGFLD_MODE_CTL_STAT_ALT_INT        (0x0001UL <<   6U) /* (RW)    Alternate Interrupt Mode */
#define LAN8720_REGFLD_MODE_CTL_STAT_ENERGY_ON      (0x0001UL <<   1U) /* (RO)    Indicates whether energy is detected */
#define LAN8720_REGMSK_MODE_CTL_STAT_RESERVED       (0xDDBDUL)

/****** Special Modes Register (Register 18, SPECMODES) ***********************/
#define LAN8720_REGFLD_SPECMODES_RES_WR1           (0x0001UL <<  14U) /* (RW)    Reserved. Write as 1! */
#define LAN8720_REGFLD_SPECMODES_MODE              (0x0007UL <<   5U) /* (RW)    Transceiver mode of operation */
#define LAN8720_REGFLD_SPECMODES_PHYADDR           (0x001FUL <<   0U) /* (RW)    PHY Address. */
#define LAN8720_REGMSK_SPECMODES_RESERVED          (0xFF00UL)

#define LAN8720_REGFLD_SPECMODES_MODE_10_T_HD            0 /* 10Base-T Half Duplex. Auto-negotiation disabled. */
#define LAN8720_REGFLD_SPECMODES_MODE_10_T_FD            1 /* 10Base-T Full Duplex. Auto-negotiation disabled. */
#define LAN8720_REGFLD_SPECMODES_MODE_100_TX_HD          2 /* 100Base-TX Half Duplex. Auto-negotiation disabled. CRS is active during Transmit & Receive. */
#define LAN8720_REGFLD_SPECMODES_MODE_100_TX_FD          3 /* 100Base-TX Full Duplex. Auto-negotiation disabled. CRS is active during Receive. */
#define LAN8720_REGFLD_SPECMODES_MODE_AN_100_TX_HD       4 /* 100Base-TX Full Duplex. Auto-negotiation disabled. CRS is active during Receive. */
#define LAN8720_REGFLD_SPECMODES_MODE_AN_REP_100_TX_HD   5 /* Repeater mode. Auto-negotiation enabled. 100Base-TX Half Duplex is advertised. CRS is active during Receive. */
#define LAN8720_REGFLD_SPECMODES_MODE_PD                 6 /* Power Down mode. */
#define LAN8720_REGFLD_SPECMODES_MODE_AN_ALL             7 /* All capable. Auto-negotiation enabled. */


/****** Symbol Error Counter Register (Register 26, SYMERR_CNT) ***************/
#define LAN8720_REGFLD_SYMERR_CNT_VAL               (0xFFFFUL <<  0U) /* (RO)    The symbol error counter */

/****** Special Control/Status Indication Register (Register 27, SPECCTL_STAT_IND) */
#define LAN8720_REGFLD_SPECCTL_STAT_IND_AUTO_MDIX   (0x0001UL << 15U) /* (RW)    Enable Auto-MDIX */
#define LAN8720_REGFLD_SPECCTL_STAT_IND_CH_SELECT   (0x0001UL << 13U) /* (RW)    Manual MDI/MDI-X Select */
#define LAN8720_REGFLD_SPECCTL_STAT_IND_SQE_OFF     (0x0001UL << 11U) /* (RW)    Disable the SQE test (Heartbeat) */
#define LAN8720_REGFLD_SPECCTL_STAT_IND_XPOL        (0x0001UL <<  4U) /* (RO)    Polarity state of the 10BASE-T */
#define LAN8720_REGMSK_SPECCTL_STAT_IND_RESERVED    (0x57EFUL)


/****** Interrupt Source Flag/Mask Register (Register 29/30, INT_SRC/INT_MSK) */
#define LAN8720_INTNUM_ENERGY_ON                    7 /* ENERGY_ON Generated */
#define LAN8720_INTNUM_AN_CPL                       6 /* Auto-Negotiation complete */
#define LAN8720_INTNUM_REM_FLT                      5 /* Remote Fault Detected */
#define LAN8720_INTNUM_LINK_DOWN                    4 /* Link Down */
#define LAN8720_INTNUM_AN_LP_ACK                    3 /* Auto-Negotiation Link Partner Acknowledge*/
#define LAN8720_INTNUM_PARA_DET_FLT                 2 /* Parallel Detection Fault */
#define LAN8720_INTNUM_AN_PG_RCVD                   1 /* Auto-Negotiation Page Received */

/****** PHY Special Control/Status Register (Register 31, PHY_SPECCTL_STAT) ***/
#define LAN8720_REGFLD_PHY_SPECCTL_STAT_AN_DONE     (0x0001UL << 12U) /* (RO)    Auto-negotiation done indication */
#define LAN8720_REGFLD_PHY_SPECCTL_STAT_DUPLEX      (0x0001UL <<  4U) /* (RO)    Half or Full Duplex */
#define LAN8720_REGFLD_PHY_SPECCTL_STAT_SPEED_100_TX (0x0001UL <<  3U) /* (RO)    Speed status 100BASE-TX */
#define LAN8720_REGFLD_PHY_SPECCTL_STAT_SPEED_10_T   (0x0001UL <<  2U) /* (RO)    Speed status 10BASE-T */


#endif //LAN8720_REGS_H_
