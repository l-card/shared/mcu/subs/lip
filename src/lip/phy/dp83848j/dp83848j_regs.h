#ifndef DP83848J_REGS_H_
#define DP83848J_REGS_H_

/* В данном файле определены Device Specific регистры для
 * PHY DP83848J и часть их бит */
#define DP83848J_REG_PHYSTS     0x10 /* Phy Status */
#define DP83848J_REG_FCSCR      0x14 /* False Carrier Sense Counter */
#define DP83848J_REG_RECR       0x15 /* Receive Error Counter */
#define DP83848J_REG_PCSR       0x16 /* 100 Mb/s PCS Configuration and Status*/
#define DP83848J_REG_RBR        0x17 /* RMII and Bypass */
#define DP83848J_REG_LEDCR      0x18 /* LED Direct Control */
#define DP83848J_REG_PHYCR      0x19 /* Phy Control Register */
#define DP83848J_REG_10BTSCR    0x1A /* 10Base-T Status/Control */
#define DP83848J_REG_CDCTRL1    0x1B /* CD Test and BIST Extensions */
#define DP83848J_REG_EDCR       0x1D /* Energy Detect Control */




#define DP83848J_BIT_PHYSTS_LINK_ST         (1UL << 0)
#define DP83848J_BIT_PHYSTS_SPEED_ST        (1UL << 1)
#define DP83848J_BIT_PHYSTS_DUPLEX_ST       (1UL << 2)
#define DP83848J_BIT_PHYSTS_LOOPBACK_ST     (1UL << 3)
#define DP83848J_BIT_PHYSTS_AUTONEG_CPL     (1UL << 4)




#define DP83848J_BIT_RBR_RMII   (1UL << 5)


#endif
