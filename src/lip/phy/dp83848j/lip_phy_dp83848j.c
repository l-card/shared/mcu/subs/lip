#include "lip/phy/lip_phy_cfg_defs.h"

#if LIP_PHY_SUPPORT_DP83848J || LIP_PHY_SUPPORT_DP83848C
#include "phy/lip_phy_port.h"
#include "dp83848j_regs.h"

#define VENDOR_OUI_TI 0x080017UL


static t_lip_errs f_init_dp83848(uint8_t devaddr, int flags);
static t_lip_errs f_get_mode_dp83848(uint8_t devaddr, e_LIP_PHY_SPEED *speed,
                                     e_LIP_PHY_DUPLEX_MODE *mode);


#if LIP_PHY_SUPPORT_DP83848J
    t_lip_phy_info lip_phy_dp83848j = {
        "DP83848J",
        (VENDOR_OUI_TI >> 6) & 0xFFFF,
        ((VENDOR_OUI_TI & 0x3F)<<10) | (9<<4),
        0xFFF0,
        f_init_dp83848,
        f_get_mode_dp83848
    };
#endif
#if LIP_PHY_SUPPORT_DP83848C
    t_lip_phy_info lip_phy_dp83848c = {
        "DP83848C",
        (VENDOR_OUI_TI >> 6) & 0xFFFF,
        ((VENDOR_OUI_TI & 0x3F)<<10) | (10<<4) | 2,
        0xFFF0,
        f_init_dp83848,
        f_get_mode_dp83848
    };
#endif



static t_lip_errs f_init_dp83848(uint8_t devaddr, int flags) {
    t_lip_errs err = LIP_ERR_SUCCESS;
    /* разрешаем режим RMII */
    err = lip_phy_reg_write(devaddr, DP83848J_REG_RBR, DP83848J_BIT_RBR_RMII);
    return err;
}

static t_lip_errs f_get_mode_dp83848(uint8_t devaddr, e_LIP_PHY_SPEED *speed, e_LIP_PHY_DUPLEX_MODE *mode) {
    t_lip_errs err = LIP_ERR_SUCCESS;
    uint16_t phy_reg;

    err = lip_phy_reg_read(devaddr, DP83848J_REG_PHYSTS, &phy_reg);
    if (err == LIP_ERR_SUCCESS) {
        *mode = phy_reg & DP83848J_BIT_PHYSTS_DUPLEX_ST ? LIP_PHY_DUPLEX_MODE_FULL :
                                                          LIP_PHY_DUPLEX_MODE_HALF;
        *speed = phy_reg & DP83848J_BIT_PHYSTS_SPEED_ST ? LIP_PHY_SPEED_10 :
                                                          LIP_PHY_SPEED_100;
    }
    return err;
}

#endif
