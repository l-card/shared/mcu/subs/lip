#ifndef YT8521S_REGS_H
#define YT8521S_REGS_H

#include "../lip_phy_mdio_regs.h"
#include "../lip_phy_mdio_regs_ext.h"

#define YT8521S_ID_VENDOR_H  0x0000
#define YT8521S_ID_VENDOR_L  0x0000
#define YT8521S_ID_VENDOR    LIP_PHY_VENDOR_ID(YT8521S_ID_VENDOR_H, YT8521S_ID_VENDOR_L)
#define YT8521S_ID_MODEL     0x0011
#define YT8521S_ID_REVISION  0x000A






#define YT8521S_REGADDR_SPEC_CTRL                   0x10 /* PHY Specific Function Control Register */
#define YT8521S_REGADDR_SPEC_STAT                   0x11 /* PHY Specific Status Register */
#define YT8521S_REGADDR_INT_MASK                    0x12 /* Interrupt Mask Register */
#define YT8521S_REGADDR_INT_STAT                    0x13 /* Interrupt Status Register */
#define YT8521S_REGADDR_SPEED_DOWNGR_CTL            0x14 /* Speed Auto Downgrade Control Register */
#define YT8521S_REGADDR_RX_ERR_CNTR                 0x15 /* Rx Error Counter Register */



/*------------------------- Extended Registers ------------------------------*/
/* UTP registers */
#define YT8521S_REGADDR_UTP_PKGEN_CFG1              LIP_PHY_REGADDR_EXT(0x0038)
#define YT8521S_REGADDR_UTP_PKGEN_CFG3              LIP_PHY_REGADDR_EXT(0x003A)
#define YT8521S_REGADDR_UTP_PKG_CFG0                LIP_PHY_REGADDR_EXT(0x00A0)
#define YT8521S_REGADDR_UTP_PKG_CFG1                LIP_PHY_REGADDR_EXT(0x00A1)
#define YT8521S_REGADDR_UTP_PKG_CFG2                LIP_PHY_REGADDR_EXT(0x00A2)
#define YT8521S_REGADDR_UTP_PKG_RX_VALID0           LIP_PHY_REGADDR_EXT(0x00A3) /* RO RC, Pkg_ib_valid[31:16], RX packets count with good CRC and 64 <= len <=  1518  */
#define YT8521S_REGADDR_UTP_PKG_RX_VALID1           LIP_PHY_REGADDR_EXT(0x00A4) /* RO RC, Pkg_ib_valid[15:0], RX packets count with good CRC and 64 <= len <=  1518  */
#define YT8521S_REGADDR_UTP_PKG_RX_OS0              LIP_PHY_REGADDR_EXT(0x00A5) /* RO RC, Pkg_ib_os_good[31:16], RX packets count with good CRC and len > 1518 */
#define YT8521S_REGADDR_UTP_PKG_RX_OS1              LIP_PHY_REGADDR_EXT(0x00A6) /* RO RC, Pkg_ib_os_good[15:0], RX packets count with good CRC and len > 1518 */
#define YT8521S_REGADDR_UTP_PKG_RX_US0              LIP_PHY_REGADDR_EXT(0x00A7) /* RO RC, Pkg_ib_us_good[31:16], RX packets count with good CRC and len < 64 */
#define YT8521S_REGADDR_UTP_PKG_RX_US1              LIP_PHY_REGADDR_EXT(0x00A8) /* RO RC, Pkg_ib_us_good[15:0], RX packets count with good CRC and len < 64 */
#define YT8521S_REGADDR_UTP_PKG_RX_ERR              LIP_PHY_REGADDR_EXT(0x00A9) /* RO RC, Pkg_ib_err, RX packets count with bad CRC and 64 <= len <=  1518  */
#define YT8521S_REGADDR_UTP_PKG_RX_OS_BAD           LIP_PHY_REGADDR_EXT(0x00AA) /* RO RC, Pkg_ib_os_bad, RX packets count with bad CRC and len > 1518 */
#define YT8521S_REGADDR_UTP_PKG_RX_FRAG             LIP_PHY_REGADDR_EXT(0x00AB) /* RO RC, pkg_ib_frag, RX packets count with bad CRC and len < 64 */
#define YT8521S_REGADDR_UTP_PKG_RX_NOSFD            LIP_PHY_REGADDR_EXT(0x00AC) /* RO RC, pkg_ib_nosfd, RX packets count with missed SFD */
#define YT8521S_REGADDR_UTP_PKG_TX_VALID0           LIP_PHY_REGADDR_EXT(0x00AD) /* RO RC, Pkg_ob_valid[31:16], TX packets count with good CRC and 64 <= len <=  1518  */
#define YT8521S_REGADDR_UTP_PKG_TX_VALID1           LIP_PHY_REGADDR_EXT(0x00AE) /* RO RC, Pkg_ob_valid[15:0], TX packets count with good CRC and 64 <= len <=  1518  */
#define YT8521S_REGADDR_UTP_PKG_TX_OS0              LIP_PHY_REGADDR_EXT(0x00AF) /* RO RC, Pkg_ob_os_good[31:16], TX packets count with good CRC and len > 1518 */
#define YT8521S_REGADDR_UTP_PKG_TX_OS1              LIP_PHY_REGADDR_EXT(0x00B0) /* RO RC, Pkg_ob_os_good[15:0], TX packets count with good CRC and len > 1518 */
#define YT8521S_REGADDR_UTP_PKG_TX_US0              LIP_PHY_REGADDR_EXT(0x00B1) /* RO RC, Pkg_ob_us_good[31:16], TX packets count with good CRC and len < 64 */
#define YT8521S_REGADDR_UTP_PKG_TX_US1              LIP_PHY_REGADDR_EXT(0x00B2) /* RO RC, Pkg_ob_us_good[15:0], TX packets count with good CRC and len < 64 */
#define YT8521S_REGADDR_UTP_PKG_TX_ERR              LIP_PHY_REGADDR_EXT(0x00B3) /* RO RC, Pkg_ob_err, TX packets count with bad CRC and 64 <= len <=  1518  */
#define YT8521S_REGADDR_UTP_PKG_TX_OS_BAD           LIP_PHY_REGADDR_EXT(0x00B4) /* RO RC, Pkg_ob_os_bad, TX packets count with bad CRC and len > 1518 */
#define YT8521S_REGADDR_UTP_PKG_TX_FRAG             LIP_PHY_REGADDR_EXT(0x00B5) /* RO RC, pkg_ob_frag, TX packets count with bad CRC and len < 64 */
#define YT8521S_REGADDR_UTP_PKG_TX_NOSFD            LIP_PHY_REGADDR_EXT(0x00B6) /* RO RC, pkg_ob_nosfd, TX packets count with missed SFD */

/* Common registers */
#define YT8521S_REGADDR_SMI_SDS_PHY                 LIP_PHY_REGADDR_EXT(0xA000)
#define YT8521S_REGADDR_CHIP_CONFIG                 LIP_PHY_REGADDR_EXT(0xA001)
#define YT8521S_REGADDR_SDS_CONFIG                  LIP_PHY_REGADDR_EXT(0xA002)
#define YT8521S_REGADDR_RGMII_CONFIG1               LIP_PHY_REGADDR_EXT(0xA003)




/****** PHY Specific Function Control Register (Register 0x10, SPEC_CTRL) *******/
#define YT8521S_REGFLD_SPEC_CTRL_CROSS_MD               (0x0003UL <<  5U) /* (RW) MDI/MDIX configuration. Require soft reeset after change */
#define YT8521S_REGFLD_SPEC_CTRL_CRS_ON_TX              (0x0001UL <<  3U) /* (RW) Assert CRS on transmitting or receiving (0 - only on receive) */
#define YT8521S_REGFLD_SPEC_CTRL_EN_SQE_TEST            (0x0001UL <<  2U) /* (RW) SQE test enable (only in half-duplex mode) */
#define YT8521S_REGFLD_SPEC_CTRL_EN_POL_INV             (0x0001UL <<  1U) /* (RW) Automatic Polarity Reversal Enabled (if 0 -> force normal polarity) */
#define YT8521S_REGFLD_SPEC_CTRL_DIS_JAB                (0x0001UL <<  0U) /* (RW) Disable 10BASE-Te jabber detection function */
#define YT8521S_REGMSK_SPEC_CTRL_RESERVED               (0xFF90UL)

#define YT8521S_REGFLDVAL_SPEC_CTRL_CROSS_MD_MDI        0 /* Manual MDI configuration  */
#define YT8521S_REGFLDVAL_SPEC_CTRL_CROSS_MD_MDIX       1 /* Manual MDIX configuratin */
#define YT8521S_REGFLDVAL_SPEC_CTRL_CROSS_MD_AUTO       3 /* Enable automatic crossover for all modes */


/****** PHY Specific Status Register (Register 0x11, SPEC_STAT) *****************/
#define YT8521S_REGFLD_SPEC_STAT_SPEED                  (0x0003UL <<  14U) /* (RO) Current speed */
#define YT8521S_REGFLD_SPEC_STAT_DUPLEX                 (0x0001UL <<  13U) /* (RO) Full-Duplex (1) or Half-duplex (0) */
#define YT8521S_REGFLD_SPEC_STAT_PAGE_RECVD_RT          (0x0001UL <<  12U) /* (RO) Page Received real-time */
#define YT8521S_REGFLD_SPEC_STAT_SPEED_DUPLEX_RSLVD     (0x0001UL <<  11U) /* (RO) Speed and Duplex Resolved. Set when Auto-Negotiation is completed or Auto-Negotiation is disabled. */
#define YT8521S_REGFLD_SPEC_STAT_LINK_UP_RT             (0x0001UL <<  10U) /* (RO) Link status real-time */
#define YT8521S_REGFLD_SPEC_STAT_MDI_CROSS              (0x0001UL <<   6U) /* (RO) MDI Crossover Status */
#define YT8521S_REGFLD_SPEC_STAT_WIRESPEED_DOWNGRD      (0x0001UL <<   5U) /* (RO) Wirespeed downgrade */
#define YT8521S_REGFLD_SPEC_STAT_TX_PAUSE_EN            (0x0001UL <<   3U) /* (RO) Transmit Pause Enabled */
#define YT8521S_REGFLD_SPEC_STAT_RX_PAUSE_EN            (0x0001UL <<   2U) /* (RO) Receive Pause Enabled */
#define YT8521S_REGFLD_SPEC_STAT_POLARITY_RT            (0x0001UL <<   1U) /* (RO) Polarity Real Time (1 - Reverted, 0 - Normal) */
#define YT8521S_REGFLD_SPEC_STAT_JABBER_RT              (0x0001UL <<   0U) /* (RO) Jabber Real Time */
#define YT8521S_REGFLD_SPEC_STAT_RESERVED               (0x0390UL)

#define YT8521S_REGFLDVAL_SPEC_STAT_SPEED_1000          2
#define YT8521S_REGFLDVAL_SPEC_STAT_SPEED_100           1
#define YT8521S_REGFLDVAL_SPEC_STAT_SPEED_10            0

#define YT8521S_REGFLDVAL_SPEC_STAT_DUPLEX_FULL         1
#define YT8521S_REGFLDVAL_SPEC_STAT_DUPLEX_HALF         0


/****** Interrupt Mask/Status Register (Register 0x12/0x13, INT_MASK/INT_STAT) */
#define YT8521S_REGFLD_INT_AN_ERR                       (0x0001UL <<  15U) /* (RW/RC) Auto-Negotiation Error  */
#define YT8521S_REGFLD_INT_SPEED_CHANGE                 (0x0001UL <<  14U) /* (RW/RC) Speed changed */
#define YT8521S_REGFLD_INT_DUPLEX_CHANGE                (0x0001UL <<  13U) /* (RW/RC) Duplex changed */
#define YT8521S_REGFLD_INT_PAGE_RECVD                   (0x0001UL <<  12U) /* (RW/RC) Page received */
#define YT8521S_REGFLD_INT_LINK_FAIL                    (0x0001UL <<  11U) /* (RW/RC) PHY link down takes place */
#define YT8521S_REGFLD_INT_LINK_SUCCEES                 (0x0001UL <<  10U) /* (RW/RC) PHY link up takes place */
#define YT8521S_REGFLD_INT_WOL                          (0x0001UL <<   6U) /* (RW/RC) PHY received WOL magic frame */
#define YT8521S_REGFLD_INT_SPEED_DOWNGRD                (0x0001UL <<   5U) /* (RW/RC) Wirespeed downgraded */
#define YT8521S_REGFLD_INT_SERDES_LINK_FAIL             (0x0001UL <<   3U) /* (RW/RC) Sds link down takes place */
#define YT8521S_REGFLD_INT_SERDES_LINK_SUCCESS          (0x0001UL <<   2U) /* (RW/RC) Sds link up takes place */
#define YT8521S_REGFLD_INT_POLARITY_CHANGE              (0x0001UL <<   1U) /* (RW/RC) PHY revered MDI polarity */
#define YT8521S_REGFLD_INT_JABBER                       (0x0001UL <<   0U) /* (RW/RC) 10BASE-Te TX jabber happened */
#define YT8521S_REGMSK_INT_RESERVED                     (0x0390UL)


/****** Speed Auto Downgrade Control Register (Register 0x14, SPEED_DOWNGR_CTL) */
#define YT8521S_REGFLD_SPEED_DOWNGR_CTL_EN              (0x0001UL <<   5U) /* (RW POS)  Enable PHY smart-speed function (change req soft reset)  */
#define YT8521S_REGFLD_SPEED_DOWNGR_CTL_RETRY_LIM       (0x0007UL <<   2U) /* (RW)  Attempt count = 2 + value (default 2 + 3)  */
#define YT8521S_REGMSK_SPEED_DOWNGR_CTL_RESERVED        (0xFFC3UL)


/****** Rx Error Counter Register (Register 0x15, RX_ERR_CNTR) ****************/
#define YT8521S_REGFLD_RX_ERR_CNTR_VAL                  (0xFFFFUL <<   0U) /* (RO SWC) Increase on 1st rising of RX_ERR when RX_DV is 1 (hld on 0xFFFF)  */



/****** UTP_PKGEN_CFG1 (EXT_0x0038) *******************************************/
#define YT8521S_REGFLD_UTP_PKGEN_CFG1_EN_DA_SA          (0x0001UL <<  12U) /* (RW) Set the DA/SA of the packet generated by pkg_gen */
#define YT8521S_REGFLD_UTP_PKGEN_CFG1_BRDCST            (0x0001UL <<  11U) /* (RW) Set the DA to broadcase address (if EN_DA_SA = 1) */
#define YT8521S_REGFLD_UTP_PKGEN_CFG1_CHK_TXSRC_SEL     (0x0001UL <<  10U) /* (RW) Check the tx data generated by pkg_gen (1) or tx data of UTP GMII/MII (0) */
#define YT8521S_REGFLD_UTP_PKGEN_CFG1_RESERVED          (0xE3FFUL)


/****** UTP_PKGEN_CFG3 (EXT_0x003A) *******************************************/
#define YT8521S_REGFLD_UTP_PKGEN_CFG3_DA                (0x00FFUL <<   8U) /* (RW) Lowest 8 bits of DA (if EN_DA_SA = 1 and BRDCST = 0) */
#define YT8521S_REGFLD_UTP_PKGEN_CFG3_SA                (0x00FFUL <<   0U) /* (RW) Lowest 8 bits of SA (if EN_DA_SA = 1) */


/****** UTP_PKG_CFG0   (EXT_0x00A0) *******************************************/
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_CHK_EN          (0x0001UL <<  15U) /* (RW) Enable UTP RX/TX packet checker */
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_EN_GATE         (0x0001UL <<  14U) /* (RW) Enable gate packet self-test clock if PKG_CHK_EN=0 & PKG_GEN_EN=0) */
#define YT8521S_REGFLD_UTP_PKG_CFG0_BP_PKG_GEN          (0x0001UL <<  13U) /* (RW) 0 -> test mode to send UTP pkg-gen generated packets */
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_GEN_EN          (0x0001UL <<  12U) /* (RW SC)  enable pkg_gen generating packets (if BP_PKG_GEN = 0) */
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_PRM_LEN         (0x000FUL <<   8U) /* (RW) Preamble length of the generated packets (>=2) */
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_IPG_LEN         (0x000FUL <<   4U) /* (RW) IPG of the generated packets (2-12 - bytes, 13 - 2ms, 14 - 20 ms, 15 - 400 ms) */
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_CURRUPT_CRC     (0x0001UL <<   2U) /* (RW) Send packets with CRC error */
#define YT8521S_REGFLD_UTP_PKG_CFG0_PKG_PAYLOAD         (0x0003UL <<   0U) /* (RW) Control the payload of the generated packets */
#define YT8521S_REGMSK_UTP_PKG_CFG0_RESERVED            (0x0008UL)

#define YT8521S_REGFLDVAL_UTP_PKG_CFG0_PKG_PAYLOAD_INC  0 /* Increased Byte payload */
#define YT8521S_REGFLDVAL_UTP_PKG_CFG0_PKG_PAYLOAD_RNS  1 /* Random payload */
#define YT8521S_REGFLDVAL_UTP_PKG_CFG0_PKG_PAYLOAD_FIX  2 /* Fixed pattern 0x5AA5 */


/****** UTP_PKG_CFG1   (EXT_0x00A1) *******************************************/
#define YT8521S_REGFLD_UTP_PKG_CFG1_PKG_LEN             (0xFFFFUL <<  0U) /* (RW) Length of generated packets */

/****** UTP_PKG_CFG2   (EXT_0x00A2) *******************************************/
#define YT8521S_REGFLD_UTP_PKG_CFG2_PKG_BURST_SIZE      (0xFFFFUL <<  0U) /* (RW) Number of packets in generated a burst (0 - until stop) */





/****** SMI_SDS_PHY (EXT_0xA000) **********************************************/
#define YT8521S_REGFLD_SMI_SDS_PHY                      (0x0001UL <<   1U) /* (RW POS) Control wheter UTP or SDS registers accessed */
#define YT8521S_REGMSK_SMI_SDS_PHY_RESERVED             (0xFFFDUL)

#define YT8521S_REGFLDVAL_SMI_SDS_PHY_SDS               1 /* Access SDS */
#define YT8521S_REGFLDVAL_SMI_SDS_PHY_UTP               1 /* Access UTP */


/****** Chip_Config (EXT_0xA001) **********************************************/
#define YT8521S_REGFLD_CHIP_CONFIG_SW_RST_N_MODE        (0x0001UL <<  15U) /* (RW SC) A whole chip software reset, 0 - active  */
#define YT8521S_REGFLD_CHIP_CONFIG_IDDQ_MODE            (0x0001UL <<  11U) /* (RW) Iddq test mode */
#define YT8521S_REGFLD_CHIP_CONFIG_RXC_DLY_EN           (0x0001UL <<   8U) /* (RW POS) RGMII clk 2ns delay control */
#define YT8521S_REGFLD_CHIP_CONFIG_EN_LDO               (0x0001UL <<   6U) /* (RW) RGMII LDO enable */
#define YT8521S_REGFLD_CHIP_CONFIG_CFG_LDO              (0x0003UL <<   4U) /* (RW POS) RGMII LDO voltage control */
#define YT8521S_REGFLD_CHIP_CONFIG_MODE_SEL             (0x0007UL <<   0U) /* (RW POS) RGMII LDO voltage control */
#define YT8521S_REGMSK_CHIP_CONFIG_RESERVED             (0x7688UL)


#define YT8521S_REGFLDVAL_CHIP_CONFIG_CFG_LDO_3_3V      0
#define YT8521S_REGFLDVAL_CHIP_CONFIG_CFG_LDO_2_5V      1
#define YT8521S_REGFLDVAL_CHIP_CONFIG_CFG_LDO_1_8V      2

#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_UTP_TO_RGMII         0
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_FIBER_TO_RGMII       1
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_UTP_FIBER_TO_RGMII   2
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_UTP_TO_SGMII         3
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_SGPHY_TO_RGMAC       4
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_SGMAC_TO_RGPHY       5
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_UTP_TO_FIBER_AUTO    6
#define YT8521S_REGFLDVAL_CHIP_CONFIG_MODE_SEL_UTP_TO_FIBER_FORCE   7

/****** SDS_Config (EXT_0xA002) ***********************************************/
#define YT8521S_REGFLD_SDS_CONFIG_EN_SUPPRESS_TXERR     (0x0001UL <<  12U) /* (RW) surppress the RX_ER generated by the serdes */
#define YT8521S_REGMSK_SDS_CONFIG_RESERVED              (0xEFFFUL)

/****** RGMII_Config1 (EXT_0xA003) ********************************************/
#define YT8521S_REGFLD_RGMII_CONFIG1_RGMAC_CFG_MODE     (0x0001UL <<  15U) /* (RW) For SGPHY_TO_RGMAC mode select source of RGMII speed, duplex, link status */
#define YT8521S_REGFLD_RGMII_CONFIG1_TX_CLK_SEL         (0x0001UL <<  14U) /* (RW) Use inverted RGMII TX_CLK (for debug) */
#define YT8521S_REGFLD_RGMII_CONFIG1_RX_DELAY_SEL       (0x000FUL <<  10U) /* (RW) RGMII RX_CLK delay train configuration (~150ps per step) */
#define YT8521S_REGFLD_RGMII_CONFIG1_EN_RGMII_FD_CRS    (0x0001UL <<   9U) /* (RW) enable encode GMII/MII CRS in fd mode (if RGMII_CRS = 1)*/
#define YT8521S_REGFLD_RGMII_CONFIG1_EN_RGMII_CRS       (0x0001UL <<   8U) /* (RW) encode GMII/MII CRS into RGMII OOB in hd or if RGMII_FD_CRS set */
#define YT8521S_REGFLD_RGMII_CONFIG1_TX_DELAY_SEL_FE    (0x000FUL <<   4U) /* (RW) RGMII TX_CLK delay train configuration for 100/10 Mbps (150ps per step) */
#define YT8521S_REGFLD_RGMII_CONFIG1_TX_DELAY_SEL       (0x000FUL <<   0U) /* (RW) RGMII TX_CLK delay train configuration for 1000 Mbps (150ps per step) */


#define YT8521S_REGFLDVAL_RGMII_CONFIG1_RGMAC_CFG_MODE_REG      1 /* from EXT 0xA004 */
#define YT8521S_REGFLDVAL_RGMII_CONFIG1_RGMAC_CFG_MODE_OOB      0 /* from RGMII OOB */



#endif // YT8521S_REGS_H
