#ifndef KSZSW_REGS_H
#define KSZSW_REGS_H

/* KSZ switch register and bitfield definitions */

#define KSZSW_CHIP_ID_8567R  0x8567
#define KSZSW_CHIP_ID_9567R  0x9567

#define KSZSW_GLOBAL_REG_ADDR(reg)     ((reg) & 0x0FFF)
#define KSZSW_PORT_REG_ADDR(port, reg) (((((port) + 1) & 0xF) << 12) | ((reg) & 0x0FFF))


/* -------------- GLOBAL REGISTER ADDRESS MAP --------------------------------*/
/*  GLOBAL OPERATION CONTROL REGISTERS */
#define KSZSW_REGADDR_CHIP_ID0                        0x0000 /*  8 bit */
#define KSZSW_REGADDR_CHIP_ID1                        0x0001 /*  8 bit */
#define KSZSW_REGADDR_CHIP_ID2                        0x0002 /*  8 bit */
#define KSZSW_REGADDR_CHIP_ID3                        0x0003 /*  8 bit */
#define KSZSW_REGADDR_PME_PINCTL                      0x0006 /*  8 bit. PME Pin Control Register */
#define KSZSW_REGADDR_GINT_STAT                       0x0010 /* 32 bit. Global Interrupt Status Register */
#define KSZSW_REGADDR_GINT_MASK                       0x0014 /* 32 bit. Global Interrupt Mask Register */
#define KSZSW_REGADDR_PORTS_INT_STAT                  0x0018 /* 32 bit. Global Port Interrupt Status Register */
#define KSZSW_REGADDR_PORTS_INT_MASK                  0x001C /* 32 bit. Global Port Interrupt Mask Register (1 - disable)*/
/* GLOBAL I/O CONTROL REGISTERS  */
#define KSZSW_REGADDR_SERIO_CTL                       0x0100 /*  8 bit. Serial I/O Control Register */
#define KSZSW_REGADDR_CLKO_CTL                        0x0103 /*  8 bit. Output Clock Control Register */
#define KSZSW_REGADDR_IBA_CTL                         0x0104 /* 32 bit. In-Band Management (IBA) Control Register */
#define KSZSW_REGADDR_IODRIVE_STRENGTH                0x010D /*  8 bit. I/O Drive Strength Register */
#define KSZSW_REGADDR_IBA_OPSTAT1                     0x0110 /* 32 bit. In-Band Management (IBA) Operation Status 1 Register */
#define KSZSW_REGADDR_LED_OVERRIDE                    0x0120 /* 32 bit. LED Override Register */
#define KSZSW_REGADDR_LED_OUT                         0x0124 /* 32 bit. LED Output Register */
#define KSZSW_REGADDR_LED_SRC                         0x0128 /* 32 bit. LED2_0/LED2_1 Source Register */
/* GLOBAL PHY CONTROL AND STATUS REGISTERS  */
#define KSZSW_REGADDR_PD_CTL                          0x0201 /*  8 bit. Power Down Control 0 Register */
#define KSZSW_REGADDR_LED_CFGSTRAP_STATE              0x0210 /* 32 bit. LED Configuration Strap Register */
/* GLOBAL SWITCH CONTROL REGISTERS */
#define KSZSW_REGADDR_SWITCH_OP                       0x0300 /*  8 bit. Switch Operation Register */
#define KSZSW_REGADDR_MAC_ADDR0                       0x0302 /*  8 bit. MAC Address [47:40] */
#define KSZSW_REGADDR_MAC_ADDR1                       0x0303 /*  8 bit. MAC Address [39:32] */
#define KSZSW_REGADDR_MAC_ADDR2                       0x0304 /*  8 bit. MAC Address [31:24] */
#define KSZSW_REGADDR_MAC_ADDR3                       0x0305 /*  8 bit. MAC Address [23:16] */
#define KSZSW_REGADDR_MAC_ADDR4                       0x0306 /*  8 bit. MAC Address [15:8] */
#define KSZSW_REGADDR_MAC_ADDR5                       0x0307 /*  8 bit. MAC Address [7:0] */
#define KSZSW_REGADDR_SWITCH_MTU                      0x0308 /* 16 bit. Switch Maximum Transmit Unit Register */
#define KSZSW_REGADDR_SWITCH_ISP_TPID                 0x030A /* 16 bit. Switch ISP TPID Register */
#define KSZSW_REGADDR_AVB_CBS_STRATEGY                0x030E /* 16 bit. AVB Credit Based Shaper Strategy Register */
#define KSZSW_REGADDR_LUE_CTL0                        0x0310 /*  8 bit. Switch Lookup Engine Control 0 Register */
#define KSZSW_REGADDR_LUE_CTL1                        0x0311 /*  8 bit. Switch Lookup Engine Control 1 Register */
#define KSZSW_REGADDR_LUE_CTL2                        0x0312 /*  8 bit. Switch Lookup Engine Control 2 Register */
#define KSZSW_REGADDR_LUE_CTL3                        0x0313 /*  8 bit. Switch Lookup Engine Control 3 Register */
#define KSZSW_REGADDR_ALU_TBL_INT_STAT                0x0314 /*  8 bit. Address Lookup Table Interrupt Register */
#define KSZSW_REGADDR_ALU_TBL_INT_MASK                0x0315 /*  8 bit. Address Lookup Table Mask Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY_IDX0              0x0316 /* 16 bit. Address Lookup Table Entry Index 0 Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY_IDX1              0x0318 /* 16 bit. Address Lookup Table Entry Index 1 Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY_IDX2              0x031A /* 16 bit. Address Lookup Table Entry Index 2 Register */
#define KSZSW_REGADDR_UNK_UCAST_CTL                   0x0320 /* 32 bit. Unknown Unicast Control Register */
#define KSZSW_REGADDR_UNK_MCAST_CTL                   0x0324 /* 32 bit. Unknown Multicast Control Register */
#define KSZSW_REGADDR_UNK_VLANID_CTL                  0x0328 /* 32 bit. Unknown VLAN ID Control Register */
#define KSZSW_REGADDR_MAC_CTL0                        0x0330 /*  8 bit. Switch MAC Control 0 Register */
#define KSZSW_REGADDR_MAC_CTL1                        0x0331 /*  8 bit. Switch MAC Control 1 Register */
#define KSZSW_REGADDR_MAC_CTL2                        0x0332 /*  8 bit. Switch MAC Control 2 Register */
#define KSZSW_REGADDR_MAC_CTL3                        0x0333 /*  8 bit. Switch MAC Control 3 Register */
#define KSZSW_REGADDR_MAC_CTL4                        0x0334 /*  8 bit. Switch MAC Control 4 Register */
#define KSZSW_REGADDR_MAC_CTL5                        0x0335 /*  8 bit. Switch MAC Control 5 Register */
#define KSZSW_REGADDR_MIB_CTL                         0x0336 /*  8 bit. Switch MIB Control Register */
#define KSZSW_REGADDR_802_1P_PRI_MAP0                 0x0338 /*  8 bit. 802.1p Priority Mapping 0 Register */
#define KSZSW_REGADDR_802_1P_PRI_MAP1                 0x0339 /*  8 bit. 802.1p Priority Mapping 1 Register */
#define KSZSW_REGADDR_802_1P_PRI_MAP2                 0x033A /*  8 bit. 802.1p Priority Mapping 2 Register */
#define KSZSW_REGADDR_802_1P_PRI_MAP3                 0x033B /*  8 bit. 802.1p Priority Mapping 3 Register */
#define KSZSW_REGADDR_DSCP_PRI_EN                     0x033E /*  8 bit. IP DiffServ Priority Enable Register */
#define KSZSW_REGADDR_DSCP_PRI_MAP(i)                 (0x0340 + (i)) /* 8 bit. IP DiffServ Priority Mapping i (0-31) Register */
#define KSZSW_REGADDR_SNOOPING_CTL                    0x0370 /*  8 bit. Global Port Mirroring and Snooping Control Register */
#define KSZSW_REGADDR_DSCP_COLOR_MAP                  0x0378 /*  8 bit. WRED DiffServ Color Mapping Register */
#define KSZSW_REGADDR_PTP_EVT_MSG_PRI                 0x037C /*  8 bit. PTP Event Message Priority Register */
#define KSZSW_REGADDR_PTP_NONEVT_MSG_PRI              0x037D /*  8 bit. PTP Non-Event Message Priority Register */
#define KSZSW_REGADDR_QMGMT_CTL                       0x0390 /* 32 bit. Queue Management Control 0 Register */
#define KSZSW_REGADDR_GLOB_PM_AV                      0x03AC /* 32 bit. Global PM Available Register */
/*  GLOBAL SWITCH LOOK UP ENGINE (LUE) CONTROL REGISTERS */
#define KSZSW_REGADDR_VLAN_TBL_ENTRY0                 0x0400 /* 32 bit. VLAN Table Entry 0 Register */
#define KSZSW_REGADDR_VLAN_TBL_ENTRY1                 0x0404 /* 32 bit. VLAN Table Entry 1 Register */
#define KSZSW_REGADDR_VLAN_TBL_ENTRY2                 0x0408 /* 32 bit. VLAN Table Entry 2 Register */
#define KSZSW_REGADDR_VLAN_TBL_IDX                    0x040C /* 16 bit. VLAN Table Index Register */
#define KSZSW_REGADDR_VLAN_TBL_ACC_CTL                0x040E /*  8 bit. VLAN Table Access Control Register */
#define KSZSW_REGADDR_ALU_TBL_IDX0                    0x0410 /* 32 bit. ALU Table Index 0 Register */
#define KSZSW_REGADDR_ALU_TBL_IDX1                    0x0414 /* 32 bit. ALU Table Index 1 Register */
#define KSZSW_REGADDR_ALU_TBL_ACC_CTL                 0x0418 /* 32 bit. ALU Table Access Control Register */
#define KSZSW_REGADDR_STADDR_RMCAST_TBL_CTL           0x041C /* 32 bit. Static Address and Reserved Multicast Table Control Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY1                  0x0420 /* 32 bit. ALU Table Entry 1 Register */
#define KSZSW_REGADDR_STADDR_TBL_ENTRY1               0x0420 /* 32 bit. Static Address Table Entry 1 Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY2                  0x0424 /* 32 bit. ALU Table Entry 2 Register */
#define KSZSW_REGADDR_STADDR_TBL_ENTRY2               0x0424 /* 32 bit. Static Address Table Entry 2 Register */
#define KSZSW_REGADDR_RMCAST_TBL_ENTRY2               0x0424 /* 32 bit. Reserved Multicast Table Entry 2 Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY3                  0x0428 /* 32 bit. ALU Table Entry 2 Register */
#define KSZSW_REGADDR_STADDR_TBL_ENTRY3               0x0428 /* 32 bit. Static Address Table Entry 3 Register */
#define KSZSW_REGADDR_ALU_TBL_ENTRY4                  0x042C /* 32 bit. ALU Table Entry 4 Register */
#define KSZSW_REGADDR_STADDR_TBL_ENTRY4               0x042C /* 32 bit. Static Address Table Entry 4 Register */
/* GLOBAL SWITCH PTP CONTROL REGISTERS */
#define KSZSW_REGADDR_PTP_CLK_CTL                     0x0500 /* 16 bit. Global PTP Clock Control Register */
#define KSZSW_REGADDR_PTP_RTC_CLK_PHASE               0x0502 /* 16 bit. Global PTP RTC Clock Phase Register */
#define KSZSW_REGADDR_PTP_NS_H                        0x0504 /* 16 bit. Global PTP RTC Clock Nanosecond High Word Register [31:16] */
#define KSZSW_REGADDR_PTP_NS_L                        0x0506 /* 16 bit. Global PTP RTC Clock Nanosecond Low Word Register  [15:0] */
#define KSZSW_REGADDR_PTP_SEC_H                       0x0508 /* 16 bit. Global PTP RTC Clock Second High Word Register [31:16] */
#define KSZSW_REGADDR_PTP_SEC_L                       0x050A /* 16 bit. Global PTP RTC Clock Second Low Word Register  [15:0] */
#define KSZSW_REGADDR_PTP_SUBNS_RATE_H                0x050C /* 16 bit. Global PTP Clock Sub-Nanosecond Rate High Word Register */
#define KSZSW_REGADDR_PTP_SUBNS_RATE_L                0x050E /* 16 bit. Global PTP Clock Sub-Nanosecond Rate Low Word Register */
#define KSZSW_REGADDR_PTP_CLKADJ_DURATION_H           0x0510 /* 16 bit. Global PTP Clock Temp Adjustment Duration High Word Register */
#define KSZSW_REGADDR_PTP_CLKADJ_DURATION_L           0x0512 /* 16 bit. Global PTP Clock Temp Adjustment Duration Low Word Register */
#define KSZSW_REGADDR_PTP_MSG_CFG1                    0x0514 /* 16 bit. Global PTP Message Config 1 Register */
#define KSZSW_REGADDR_PTP_MSG_CFG2                    0x0516 /* 16 bit. Global PTP Message Config 2 Register */
#define KSZSW_REGADDR_PTP_DOMAIN_VER                  0x0518 /* 16 bit. Global PTP Domain and Version Register */
#define KSZSW_REGADDR_PTP_UNIT_IDX                    0x0520 /* 32 bit. Global PTP Unit Index Register */
#define KSZSW_REGADDR_PTP_GPIO_MON0                   0x0524 /* 32 bit. GPIO Status Monitor 0 Register */
#define KSZSW_REGADDR_PTP_GPIO_MON1                   0x0528 /* 32 bit. GPIO Status Monitor 1 Register */
#define KSZSW_REGADDR_PTP_TSTMP_CTLSTAT               0x052C /* 32 bit. Timestamp Control and Status Register */
#define KSZSW_REGADDR_PTP_TRIG_TIME_NS                0x0530 /* 32 bit. Trigger Output Unit Target Time Nanosecond Register */
#define KSZSW_REGADDR_PTP_TRIG_TIME_SEC               0x0534 /* 32 bit. Trigger Output Unit Target Time Second Register */
#define KSZSW_REGADDR_PTP_TRIG_CTL1                   0x0538 /* 32 bit. Trigger Output Unit Control 1 Register */
#define KSZSW_REGADDR_PTP_TRIG_CTL2                   0x053C /* 32 bit. Trigger Output Unit Control 2 Register */
#define KSZSW_REGADDR_PTP_TRIG_CTL3                   0x0540 /* 32 bit. Trigger Output Unit Control 3 Register */
#define KSZSW_REGADDR_PTP_TRIG_CTL4                   0x0544 /* 32 bit. Trigger Output Unit Control 4 Register */
#define KSZSW_REGADDR_PTP_TRIG_CTL5                   0x0548 /* 32 bit. Trigger Output Unit Control 5 Register */
#define KSZSW_REGADDR_PTP_TSTMP_STATCTL               0x0550 /* 32 bit. Timestamp Status and Control Register */
#define KSZSW_REGADDR_PTP_TSTMP_ST_NS(n)              (0x0554 + 12*n) /* 32 bit. Timestamp Nth Sample Time Nanoseconds Register (n = 0..7) */
#define KSZSW_REGADDR_PTP_TSTMP_ST_SEC(n)             (0x0558 + 12*n) /* 32 bit. Timestamp Nth Sample Time Seconds Register (n = 0..7)*/
#define KSZSW_REGADDR_PTP_TSTMP_ST_PHASE(n)           (0x055C + 12*n) /* 32 bit. Timestamp Nth Sample Time Phase Register (n = 0..7)*/

/* --------- Port Registers ------------------------*/
/* PORT OPERATION CONTROL REGISTERS */
#define KSZSW_REGADDR_PORT_DEFAULT_TAG_H              0x0000 /*  8 bit. Port Default Tag 0 Register (high byte) */
#define KSZSW_REGADDR_PORT_DEFAULT_TAG_L              0x0001 /*  8 bit. Port Default Tag 1 Register (low byte) */
#define KSZSW_REGADDR_PORT_PME_WOL_EVT                0x0013 /*  8 bit. Port PME_WoL Event Register */
#define KSZSW_REGADDR_PORT_PME_WOL_EN                 0x0017 /*  8 bit. Port PME_WoL Enable Register */
#define KSZSW_REGADDR_PORT_INT_STAT                   0x001B /*  8 bit. Port Interrupt Status Register */
#define KSZSW_REGADDR_PORT_INT_MASK                   0x001F /*  8 bit. Port Interrupt Mask Register */
#define KSZSW_REGADDR_PORT_OP_CTL0                    0x0020 /*  8 bit. Port Operation Control 0 Register */
#define KSZSW_REGADDR_PORT_STAT                       0x0030 /*  8 bit. Port Status Register */
/*  PORT ETHERNET PHY REGISTERS */
#define KSZSW_REGADDR_PORT_PHY(phyreg_addr)           (0x100 + 2*(phyreg_addr)) /* Трансляция адресов регистров PHY на адреса регистров свича.
                                                                                   Описание регистров phy в phy_regs.h */
/* PORT RGMII/MII/RMII CONTROL REGISTERS  */
#define KSZSW_REGADDR_PORT_XMII_CTL0                  0x0300 /*  8 bit. XMII Port Control 0 Register */
#define KSZSW_REGADDR_PORT_XMII_CTL1                  0x0301 /*  8 bit. XMII Port Control 1 Register */
/*  PORT SWITCH MAC CONTROL REGISTERS  */
#define KSZSW_REGADDR_PORT_MAC_CTL0                   0x0400 /*  8 bit. Port MAC Control 0 Register */
#define KSZSW_REGADDR_PORT_MAC_CTL1                   0x0401 /*  8 bit. Port MAC Control 1 Register */
#define KSZSW_REGADDR_PORT_INGR_RATELIM_CTL           0x0403 /*  8 bit. Port Ingress Rate Limit Control Register */
#define KSZSW_REGADDR_PORT_INGR_RATELIM_PRIOCTL(x)    (0x0410 + (x)) /*  8 bit. Port Priority x Ingress Limit Control Register (x=0..7) */
#define KSZSW_REGADDR_PORT_EGR_RATELIM_QCTL(x)        (0x0420 + (x)) /*  8 bit. Port Queue x Egress Limit Control Register (x=0..3) */
/* PORT SWITCH MIB COUNTERS REGISTERS */
#define KSZSW_REGADDR_PORT_MIB_CTL_STAT               0x0500 /* 32 bit. Port MIB Control and Status Register */
#define KSZSW_REGADDR_PORT_MIB_DATA                   0x0504 /* 32 bit. Port MIB Data Register */
/* PORT SWITCH ACL CONTROL REGISTERS  */
#define KSZSW_REGADDR_PORT_ACL_ACC_0                  0x0600 /*  8 bit. Port ACL Access 0 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_1                  0x0601 /*  8 bit. Port ACL Access 1 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_2                  0x0602 /*  8 bit. Port ACL Access 2 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_3                  0x0603 /*  8 bit. Port ACL Access 3 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_4                  0x0604 /*  8 bit. Port ACL Access 4 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_5                  0x0605 /*  8 bit. Port ACL Access 5 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_6                  0x0606 /*  8 bit. Port ACL Access 6 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_7                  0x0607 /*  8 bit. Port ACL Access 7 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_8                  0x0608 /*  8 bit. Port ACL Access 8 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_9                  0x0609 /*  8 bit. Port ACL Access 9 Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_A                  0x060A /*  8 bit. Port ACL Access A Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_B                  0x060B /*  8 bit. Port ACL Access B Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_C                  0x060C /*  8 bit. Port ACL Access C Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_D                  0x060D /*  8 bit. Port ACL Access D Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_E                  0x060E /*  8 bit. Port ACL Access E Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_F                  0x060F /*  8 bit. Port ACL Access F Register */
#define KSZSW_REGADDR_PORT_ACL_BYTE_EN_H              0x0610 /*  8 bit. Port ACL Byte Enable MSB Register */
#define KSZSW_REGADDR_PORT_ACL_BYTE_EN_L              0x0611 /*  8 bit. Port ACL Byte Enable LSB Register */
#define KSZSW_REGADDR_PORT_ACL_ACC_CTL0               0x0612 /*  8 bit. Port ACL Access Control 0 Register */
/* PORT SWITCH INGRESS CONTROL REGISTERS */
#define KSZSW_REGADDR_PORT_MIRRORING_CTL              0x0800 /*  8 bit. Port Mirroring Control Register */
#define KSZSW_REGADDR_PORT_PRIO_CTL                   0x0801 /*  8 bit. Port Priority Control Register */
#define KSZSW_REGADDR_PORT_INGR_MAC_CTL               0x0802 /*  8 bit. Port Ingress MAC Control Register */
#define KSZSW_REGADDR_PORT_AUTH_CTL                   0x0803 /*  8 bit. Port Authentication Control Register */
#define KSZSW_REGADDR_PORT_PTR                        0x0804 /* 32 bit. Port Pointer Register */
#define KSZSW_REGADDR_PORT_PRIO_QMAP                  0x0808 /* 32 bit. Port Priority to Queue Mapping Register */
#define KSZSW_REGADDR_PORT_POL_CTL                    0x080C /* 32 bit. Port Police Control Register */
#define KSZSW_REGADDR_PORT_POL_QRATE                  0x0820 /* 32 bit. Port Police Queue Rate Register */
#define KSZSW_REGADDR_PORT_POL_QBURST_SIZE            0x0824 /* 32 bit. Port Police Queue Burst Size Register */
#define KSZSW_REGADDR_PORT_WRED_PKT_MEM_CTL0          0x0830 /* 32 bit. Port WRED Packet Memory Control Register 0 */
#define KSZSW_REGADDR_PORT_WRED_PKT_MEM_CTL1          0x0834 /* 32 bit. Port WRED Packet Memory Control Register 1 */
#define KSZSW_REGADDR_PORT_WRED_QCTL0                 0x0840 /* 32 bit. Port WRED Queue Control Register 0 */
#define KSZSW_REGADDR_PORT_WRED_QCTL1                 0x0844 /* 32 bit. Port WRED Queue Control Register 1 */
#define KSZSW_REGADDR_PORT_WRED_QPERF_MON_CTL         0x0848 /* 32 bit. Port WRED Queue Performance Monitor Control Register */
/* PORT SWITCH EGRESS CONTROL REGISTERS */
#define KSZSW_REGADDR_PORT_TXQ_IDX                    0x0900 /* 32 bit. Port Transmit Queue Index Registe */
#define KSZSW_REGADDR_PORT_TXQ_PVID                   0x0904 /* 32 bit. Port Transmit Queue PVID Register */
#define KSZSW_REGADDR_PORT_TXQ_CTL0                   0x0914 /*  8 bit. Port Transmit Queue Control 0 Register */
#define KSZSW_REGADDR_PORT_TXQ_CTL1                   0x0915 /*  8 bit. Port Transmit Queue Control 1 Register */
#define KSZSW_REGADDR_PORT_TXQ_CREDIT_SHAPER_CTL0     0x0916 /* 16 bit. Port Transmit Credit Shaper Control 0 Register */
#define KSZSW_REGADDR_PORT_TXQ_CREDIT_SHAPER_CTL1     0x0918 /* 16 bit. Port Transmit Credit Shaper Control 1 Register */
#define KSZSW_REGADDR_PORT_TXQ_CREDIT_SHAPER_CTL2     0x091A /* 16 bit. Port Transmit Credit Shaper Control 2 Register */
#define KSZSW_REGADDR_PORT_TXQ_TAS_CTL                0x0920 /*  8 bit. Port Time Aware Shaper Control Register */
#define KSZSW_REGADDR_PORT_TXQ_TAS_EVT_IDX            0x0923 /*  8 bit. Port Time Aware Shaper Event Index Register */
#define KSZSW_REGADDR_PORT_TXQ_TAS_EVT                0x0924 /* 32 bit. Port Time Aware Shaper Event Register */
/* PORT SWITCH QUEUE MANAGEMENT CONTROL REGISTERS */
#define KSZSW_REGADDR_PORT_CTL0                       0x0A00 /* 32 bit. Port Control 0 Register */
#define KSZSW_REGADDR_PORT_CTL1                       0x0A04 /* 32 bit. Port Control 1 Register */
#define KSZSW_REGADDR_PORT_TXQ_MEM                    0x0A10 /* 32 bit. Port Transmit Queue Memory Register */
/* PORT SWITCH ADDRESS LOOKUP CONTROL REGISTERS */
#define KSZSW_REGADDR_PORT_CTL2                       0x0B00 /*  8 bit. Port Control 2 Register */
#define KSZSW_REGADDR_PORT_MSTP_PTR                   0x0B01 /*  8 bit. Port MSTP Pointer Register */
#define KSZSW_REGADDR_PORT_MSTP_STATE                 0x0B04 /*  8 bit. Port MSTP State Register */
/* PORT SWITCH PTP CONTROL REGISTERS */
#define KSZSW_REGADDR_PORT_PTP_RX_LATENCY             0x0C00 /* 16 bit. Port PTP Receive Latency Register */
#define KSZSW_REGADDR_PORT_PTP_TX_LATENCY             0x0C02 /* 16 bit. Port PTP Transmit Latency Register */
#define KSZSW_REGADDR_PORT_PTP_ASYM_COR               0x0C04 /* 16 bit. Port PTP Asymmetry Correction Register */
#define KSZSW_REGADDR_PORT_PTP_EGR_TSTMP_DLY_REQ_H    0x0C08 /* 16 bit. Port PTP Egress Timestamp for Pdelay_Req and Delay_Req High Word Register */
#define KSZSW_REGADDR_PORT_PTP_EGR_TSTMP_DLY_REQ_L    0x0C0A /* 16 bit. Port PTP Egress Timestamp for Pdelay_Req and Delay_Req Low Word Register */
#define KSZSW_REGADDR_PORT_PTP_EGR_TSTMP_SYNC_H       0x0C0C /* 16 bit. Port PTP Egress Timestamp for Sync High Word Register */
#define KSZSW_REGADDR_PORT_PTP_EGR_TSTMP_SYNC_L       0x0C0E /* 16 bit. Port PTP Egress Timestamp for Sync Low Word Register */
#define KSZSW_REGADDR_PORT_PTP_EGR_TSTMP_DLY_RESP_H   0x0C10 /* 16 bit. Port PTP Egress Timestamp for Pdelay_Resp High Word Register */
#define KSZSW_REGADDR_PORT_PTP_EGR_TSTMP_DLY_RESP_L   0x0C12 /* 16 bit. Port PTP Egress Timestamp for Pdelay_Resp Low Word Register */
#define KSZSW_REGADDR_PORT_PTP_INT_STAT               0x0C14 /* 16 bit. Port PTP Timestamp Interrupt Status Register */
#define KSZSW_REGADDR_PORT_PTP_INT_EN                 0x0C16 /* 16 bit. Port PTP Timestamp Interrupt Enable Register */
#define KSZSW_REGADDR_PORT_PTP_LINK_DLY               0x0C18 /* 32 bit. Port PTP Link Delay Register */


/* --------------- REGISTER BITFIELD DEFINITION -------------------------------*/

/************ Global Chip ID 3 Register (CHIP_ID3, 8 bit) ****************************/
#define KSZSW_REGFLD_CHIP_ID3_REVISION                (0x0000000FUL <<  4U) /* (RO) Revision ID */
#define KSZSW_REGFLD_CHIP_ID3_SW_RST                  (0x00000001UL <<  0U) /* (RW) Global Software Reset */
#define KSZSW_REGMSK_CHIP_ID3_RESERVED                (0x0EUL)
/************ PME Pin Control Register (PME_PIN_CTL, 8 bit) **************************/
#define KSZSW_REGFLD_PME_PINCTL_EN                    (0x00000001UL <<  1U) /* (RW) PME Pin Output Enable */
#define KSZSW_REGFLD_PME_PINCTL_POLARITY              (0x00000001UL <<  0U) /* (RW) PME Pin Output Polarity (0 - Low) */
#define KSZSW_REGMSK_PME_PINCTL_RESERVED              (0xFCUL)
/************ Global Interrupt Status/Mask Register (GINT_STAT / GINT_MASK, 32 bit) ***/
#define KSZSW_REGFLD_GINT_LUE                         (0x00000001UL << 31U) /* (RO/RW) Lookup Engine (LUE) Interrupt */
#define KSZSW_REGFLD_GINT_GPIO_TSTAMP                 (0x00000001UL << 30U) /* (RO/RW) GPIO Pin Output Trigger and Timestamp Unit Interrupt */
#define KSZSW_REGFLD_GINT_APB_TOUT                    (0x00000001UL << 29U) /* (RO/RW) APB Timout Interrupt Status */
#define KSZSW_REGMSK_GINT_RESERVED                    (0x1FFFFFFFUL)
/************ Global Port Interrupt Status/Mask Register (PORT_INT_STAT / PORT_INT_MASK, 32 bit) */
#define KSZSW_REGFLD_PORT_INT(n)                      (0x00000001UL << (n)) /* (RO/RW) Port n global interrupt  */
#define KSZSW_REGMSK_PORT_RESERVED                    (0xFFFFFF80UL)
/************ Serial I/O Control Register (SERIO_CTL, 8 bit) *************************/
#define KSZSW_REGFLD_SERIO_CTL_MDIO_PRE_SUPR          (0x00000001UL <<  2U) /* (RW) MIIM Preamble Suppression */
#define KSZSW_REGFLD_SERIO_CTL_SPI_DOUT_AUTO          (0x00000001UL <<  1U) /* (RW) Automatic SPI Data Out Edge Select */
#define KSZSW_REGFLD_SERIO_CTL_SPI_DOUT_EDGE          (0x00000001UL <<  0U) /* (RW) SPI Data Out Edge Select (0 - falling, 1 - rising; SPI_DOUT_AUTO = 0) */
#define KSZSW_REGMSK_SERIO_CTL_RESERVED               (0xF8UL)
/************ Output Clock Control Register (CLKO_CTL, 8 bit) ************************/
#define KSZSW_REGFLD_CLKO_CTL_REC_CLK_RDY             (0x00000001UL <<  7U) /* (RO) Recovered Clock Ready */
#define KSZSW_REGFLD_CLKO_CTL_SYNCLKO_SRC             (0x00000007UL <<  2U) /* (RW) SYNCLKO Source (0 - XI, 1-5 - port) */
#define KSZSW_REGFLD_CLKO_CTL_SYNCLKO_OUT_EN          (0x00000001UL <<  1U) /* (RW) SYNCLKO Output Pin Enable */
#define KSZSW_REGFLD_CLKO_CTL_SYNCLKO_FREQ            (0x00000001UL <<  0U) /* (RW) SYNCLKO Frequency (0 - 25 MHz, 1 - 125 MHz) */
#define KSZSW_REGMSK_CLKO_CTL_RESERVED                (0x60UL)

#define KSZSW_REGFLDVAL_CLKO_CTL_SYNCLKO_FREQ_25MHZ   0
#define KSZSW_REGFLDVAL_CLKO_CTL_SYNCLKO_FREQ_125MHZ  1
/************ In-Band Management (IBA) Control Register (IBA_CTL, 8 bit) *************/
#define KSZSW_REGFLD_IBA_CTL_EN                       (0x00000001UL << 31U) /* (RW) IBA Enable (RX_DV7 Strap) */
#define KSZSW_REGFLD_IBA_CTL_DEST_MAC_MATCH           (0x00000001UL << 30U) /* (RW) IBA Destination MAC Address Match Enable */
#define KSZSW_REGFLD_IBA_CTL_RST                      (0x00000001UL << 29U) /* (RW SC) IBA Reset (self cleared) */
#define KSZSW_REGFLD_IBA_CTL_RESP_PQUEUE              (0x00000003UL << 22U) /* (RW) Priority Queue for IBA response (default = 1) */
#define KSZSW_REGFLD_IBA_CTL_PORT_NUM                 (0x00000007UL << 16U) /* (RW) Port used for IBA communication */
#define KSZSW_REGFLD_IBA_CTL_TPID                     (0x0000FFFFUL <<  0U) /* (RW) TPID (EtherType) value for IBA frame header */
#define KSZSW_REGMSK_IBA_CTL_RESERVED                 (0x1F380000UL)
/************ I/O Drive Strength Register (IODRIVE_STRENGTH, 8 bit) ******************/
#define KSZSW_REGFLD_IODRIVE_STRENGTH_HS              (0x00000007UL <<  4U) /* (RW) High Speed Drive Strength (24mA) -
                                                                                    RGMII / MII / RMII (except TX_CLK/REFCLKI,
                                                                                    COL and CRS) and SYNCLKO. */
#define KSZSW_REGFLD_IODRIVE_STRENGTH_LS              (0x00000007UL <<  0U) /* (RW) Low Speed Drive Strength (8mA) -
                                                                                    TX_CLK/REFCLKI, COL, CRS, LEDs, PME_N,
                                                                                    INTRP_N, SDO and SDI/SDA/MDIO */
/************ In-Band Management (IBA) Operation Status 1 Register (IBA_OPSTAT1, 32 bit) */
#define KSZSW_REGFLD_IBA_OPSTAT1_PKT_DETECT           (0x00000001UL << 31U) /* (RO) Good IBA Packet Detect */
#define KSZSW_REGFLD_IBA_OPSTAT1_RESP_DONE            (0x00000001UL << 30U) /* (RO) IBA Response Packet Transmit Done */
#define KSZSW_REGFLD_IBA_OPSTAT1_EXEC_DONE            (0x00000001UL << 29U) /* (RO) IBA Execution Done */
#define KSZSW_REGFLD_IBA_OPSTAT1_MAC_ERR              (0x00000001UL << 14U) /* (RO) IBA MAC Address Mismatch Error */
#define KSZSW_REGFLD_IBA_OPSTAT1_ACC_FMT_ERR          (0x00000001UL << 13U) /* (RO) IBA Access Format Error */
#define KSZSW_REGFLD_IBA_OPSTAT1_ACC_CODE_ERR         (0x00000001UL << 12U) /* (RO) IBA Access Code Error */
#define KSZSW_REGFLD_IBA_OPSTAT1_ACC_CMD_ERR          (0x00000001UL << 11U) /* (RO) IBA Access Command Error */
#define KSZSW_REGFLD_IBA_OPSTAT1_OVSZ_PKT_ERR         (0x00000001UL << 10U) /* (RO) IBA Oversize Packet Error */
#define KSZSW_REGFLD_IBA_OPSTAT1_ERR_LOC              (0x0000007FUL <<  0U) /* (RO) IBA Access Code Error Location */
#define KSZSW_REGMSK_IBA_OPSTAT1_RESERVED             (0x1FFF8380UL)

/* LED Override/Output/Source/Configuration Strap Registers (LED_OVERRIDE / LED_OUTPUT / LED_SRC (2_0 and 2_1 only) / LED_CFGSTRAP_STATE 32 bit) */
#define KSZSW_REGFLD_LED_1_0                          (0x00000001UL <<  0U)
#define KSZSW_REGFLD_LED_1_1                          (0x00000001UL <<  1U)
#define KSZSW_REGFLD_LED_2_0                          (0x00000001UL <<  2U)
#define KSZSW_REGFLD_LED_2_1                          (0x00000001UL <<  3U)
#define KSZSW_REGFLD_LED_3_0                          (0x00000001UL <<  4U)
#define KSZSW_REGFLD_LED_3_1                          (0x00000001UL <<  5U)
#define KSZSW_REGFLD_LED_4_0                          (0x00000001UL <<  6U)
#define KSZSW_REGFLD_LED_4_1                          (0x00000001UL <<  7U)
#define KSZSW_REGFLD_LED_5_0                          (0x00000001UL <<  8U)
#define KSZSW_REGFLD_LED_5_1                          (0x00000001UL <<  9U)

#define KSZSW_REGFLD_LED(port, led)                   (0x00000001UL <<  (0 + 2 * (port) + (led)))
/************ Power Down Control 0 Register (PD_CTL, 8 bit) ************************/
#define KSZSW_REGFLD_PD_CTL_PLL_PD                    (0x00000001UL <<  5U) /* (RW) PLL Power Down */
#define KSZSW_REGFLD_PD_CTL_PM_MODE                   (0x00000003UL <<  3U) /* (RW) Power Management Mode */
#define KSZSW_REGMSK_PD_CTL_RESERVED                  (0xC7UL)

#define KSZSW_REGFLDVAL_PD_CTL_PM_MODE_NORMAL         0 /* Normal operation */
#define KSZSW_REGFLDVAL_PD_CTL_PM_MODE_EDPD           1 /* Energy Detect Power Down (EDPD) Mode */
#define KSZSW_REGFLDVAL_PD_CTL_PM_MODE_SWPD           2 /* Soft Power Down Mode */
/************ Switch Operation Register (SWITCH_OP, 8bit) **************************/
#define KSZSW_REGFLD_SWITCH_OP_DBLTAG_EN              (0x00000001UL <<  7U) /* (RW) Double Tag Enable */
#define KSZSW_REGFLD_SWITCH_OP_RST                    (0x00000001UL <<  1U) /* (RW SC) Soft Hardware Reset */
#define KSZSW_REGFLD_SWITCH_OP_START                  (0x00000001UL <<  0U) /* (RW) Start Switch (enable switch function) */
#define KSZSW_REGMSK_SWITCH_RESERVED                  (0x7CUL)
/************ Switch Maximum Transmit Unit Register (SWITCH_MTU, 16 bit) ***********/
#define KSZSW_REGFLD_SWITCH_MTU_VAL                   (0x00003FFFUL <<  0U) /* (RW) Maximum Frame Length (MTU) - up to 9000 (0x2328) */
#define KSZSW_REGMSK_SWITCH_MTU_RESERVED              (0xC000UL)
/************ Switch ISP TPID Register (SWITCH_MTU, 16 bit) ************************/
#define KSZSW_REGFLD_SWITCH_ISP_TPID_VAL              (0x0000FFFFUL <<  0U) /* (RW) Default tag TPID (EtherType) for untagged incoming frames */

/************ AVB Credit Based Shaper Strategy Register (AVB_SHAPER_STRATEGY, 16 bit) **/
#define KSZSW_REGFLD_AVB_CBS_STRATEGY_SHAPING         (0x00000001UL <<  1U) /* (RW) Shaping Credit Accounting (1 - data and IPG + preamble, 0 - data only)*/
#define KSZSW_REGFLD_AVB_CBS_STRATEGY_POLICING        (0x00000001UL <<  0U) /* (RW) Policing Credit Accounting (1 - data and IPG + preamble, 0 - data only) */
#define KSZSW_REGMSK_AVB_CBS_STRATEGY_RESERVED        (0xFFFCUL)
/************ Switch Lookup Engine Control 0 Register (LUE_CTL0, 8 bit) ****************/
#define KSZSW_REGFLD_LUE_CTL0_QVLAN_EN                (0x00000001UL <<  7U) /* (RW) 802.1Q VLAN Enable */
#define KSZSW_REGFLD_LUE_CTL0_DROP_INV_VLAN           (0x00000001UL <<  6U) /* (RW) Drop Invalid VID (1 - dropped, 0 - forward to host) */
#define KSZSW_REGFLD_LUE_CTL0_AGE_CNT                 (0x00000007UL <<  3U) /* (RW) Age Count (aging time of dynamic entries in Age Periods) */
#define KSZSW_REGFLD_LUE_CTL0_RES_MCAST_LU_EN         (0x00000001UL <<  2U) /* (RW) Reserved Multicast Lookup Enable */
#define KSZSW_REGFLD_LUE_CTL0_HASH_OPTION             (0x00000003UL <<  0U) /* (RW) Hashing option for mapping entries to the dynamic lookup table */

#define KSZSW_REGFLDVAL_LUE_CTL0_HASH_OPTION_LADDR    0 /* 10 least significant bits of the destination address */
#define KSZSW_REGFLDVAL_LUE_CTL0_HASH_OPTION_CRC      1 /* The CRC hashing function is used */
#define KSZSW_REGFLDVAL_LUE_CTL0_HASH_OPTION_XOR      2 /* The XOR hashing function is used */
/************ Switch Lookup Engine Control 1 Register (LUE_CTL1, 8 bit) ****************/
#define KSZSW_REGFLD_LUE_CTL1_UCAST_LEARN_DIS         (0x00000001UL <<  7U) /* (RW) Unicast Learning Disable */
#define KSZSW_REGFLD_LUE_CTL1_SELF_FLT_EN             (0x00000001UL <<  6U) /* (RW) Self-Address Filtering – Global Enable */
#define KSZSW_REGFLD_LUE_CTL1_FLUSH_ALU_TBL           (0x00000001UL <<  5U) /* (RW SC) Flush Address Lookup Table (self clear) */
#define KSZSW_REGFLD_LUE_CTL1_FLUSH_MSTP              (0x00000001UL <<  4U) /* (RW SC) Flush MSTP Address Entries (Address Lookup Table) */
#define KSZSW_REGFLD_LUE_CTL1_MCAST_SADDR_FLT         (0x00000001UL <<  3U) /* (RW) Multicast Source Address Filtering */
#define KSZSW_REGFLD_LUE_CTL1_AGING_EN                (0x00000001UL <<  2U) /* (RW) Aging Enable */
#define KSZSW_REGFLD_LUE_CTL1_FAST_AGING              (0x00000001UL <<  1U) /* (RW) Fast Aging */
#define KSZSW_REGFLD_LUE_CTL1_LINK_DOWN_FLUSH         (0x00000001UL <<  0U) /* (RW) Link Down Flush (flush tables on link down) */
/************ Switch Lookup Engine Control 2 Register (LUE_CTL2, 8 bit) ****************/
#define KSZSW_REGFLD_LUE_CTL2_DBLTAG_MCAST_TRAP       (0x00000001UL <<  6U) /* (RW) Double Tag Multicast Trap */
#define KSZSW_REGFLD_LUE_CTL2_DYN_EGR_VLAN_FLT        (0x00000001UL <<  5U) /* (RW) Dynamic Entry Egress VLAN Filtering (recomended!) */
#define KSZSW_REGFLD_LUE_CTL2_STAT_EGR_VLAN_FLT       (0x00000001UL <<  4U) /* (RW) Static Entry Egress VLAN Filtering (recomended!) */
#define KSZSW_REGFLD_LUE_CTL2_FLUSH_OPTION            (0x00000003UL <<  2U) /* (RW) Flush Option */
#define KSZSW_REGFLD_LUE_CTL2_MAC_ADDR_PRI            (0x00000003UL <<  0U) /* (RW) MAC Address Priority */
#define KSZSW_REGMSK_LUE_CTL2_RESERVED                (0x80UL)

#define KSZSW_REGFLDVAL_LUE_CTL2_FLUSH_OPTION_NOFLUSH 0 /* No flush or flush is done */
#define KSZSW_REGFLDVAL_LUE_CTL2_FLUSH_OPTION_DYNAMIC 1 /* Flush only dynamic table entries */
#define KSZSW_REGFLDVAL_LUE_CTL2_FLUSH_OPTION_STATIC  2 /* Flush only static table entries */
#define KSZSW_REGFLDVAL_LUE_CTL2_FLUSH_OPTION_ALL     3 /* Flush both static and dynamic table entries */

#define KSZSW_REGFLD_LUE_CTL2_MAC_ADDR_PRI_DA         0 /* MAC address priority for a packet is determined from the destination address (DA) lookup */
#define KSZSW_REGFLD_LUE_CTL2_MAC_ADDR_PRI_SA         1 /* MAC address priority for a packet is determined from the source address (SA) lookup */
#define KSZSW_REGFLD_LUE_CTL2_MAC_ADDR_PRI_HIGHER     2 /* MAC address priority for a packet is determined from the higher of the DA and SA lookups */
#define KSZSW_REGFLD_LUE_CTL2_MAC_ADDR_PRI_LOWER      3 /* MAC address priority for a packet is determined from the lower of the DA and SA lookups */
/************ Switch Lookup Engine Control 3 Register (LUE_CTL3, 8 bit) ****************/
#define KSZSW_REGFLD_LUE_CTL3_AGE_PERIOD              (0x000000FFUL <<  0U) /* (RW) Age Period in seconds */
/************ Address Lookup Table Interrupt/Mask Registers (ALU_TBL_INT_STAT / ALU_TBL_INT_MASK, 8 bit) ***/
#define KSZSW_REGFLD_ALU_TBL_INT_LEARN_FAIL           (0x00000001UL <<  2U) /* (RW1C) Learn Fail Interrupt Status */
#define KSZSW_REGFLD_ALU_TBL_INT_ALMOST_FULL          (0x00000001UL <<  1U) /* (RW1C) Almost Full Interrupt Status */
#define KSZSW_REGFLD_ALU_TBL_INT_WRITE_FAIL           (0x00000001UL <<  0U) /* (RW1C) Write Fail Interrupt Status */
#define KSZSW_REGMSK_ALU_TBL_INT_RESERVED             (0xF8UL)
/************ Address Lookup Table Entry Index 0/1/2 Registers (ALU_TBL_ENTRY_IDX0/1/2, 16 bit) **/
#define KSZSW_REGFLD_ALU_TBL_ENTRY_IDX0_ALMOST_FULL   (0x00000FFFUL <<  0U) /* (RO) Almost Full Entry Index [11:0] */
#define KSZSW_REGFLD_ALU_TBL_ENTRY_IDX0_FAIL_WRITE    (0x000003FFUL <<  0U) /* (RO) Fail Write Index [9:0] */
#define KSZSW_REGFLD_ALU_TBL_ENTRY_IDX1_FAIL_LEARN    (0x000003FFUL <<  0U) /* (RO) Fail Learn Index */
#define KSZSW_REGFLD_ALU_TBL_ENTRY_IDX2_CPU_ACCESS    (0x000003FFUL <<  0U) /* (RO) CPU Access Index */
/************ Unknown Unicast/Multicast/VID Control Register (UNK_UCAST_CTL/UNK_MCAST_CTL/UNK_VLANID_CTL, 32 bit) **/
#define KSZSW_REGFLD_UNK_CTL_FORWARD_EN               (0x00000001UL << 31U) /* (RW) Unknown Packet Forward Enable */
#define KSZSW_REGFLD_UNK_CTL_FORWARD_PORTS            (0x0000007FUL <<  0U) /* (RW) Unknown Forwarding Ports */
#define KSZSW_REGMSK_UNK_CTL_RESERVED                 (0x7FFFFF80UL)
/************ Switch MAC Control 0 Register (MAC_CTL0, 8 bit)  *********************/
#define KSZSW_REGFLD_MAC_CTL0_ALT_BACKOFF_MODE        (0x00000001UL <<  7U) /* (RW) Alternate Back-off Mode Enable (half-duplex) */
#define KSZSW_REGFLD_MAC_CTL0_FRAME_LEN_CHECK         (0x00000001UL <<  3U) /* (RW) Frame Length Field Check (for EtherType/Length < 1500) */
#define KSZSW_REGFLD_MAC_CTL0_FLOWCTL_DROP_MODE       (0x00000001UL <<  1U) /* (RW) Flow Control Packet Drop Mode (EtherType and DA check) */
#define KSZSW_REGFLD_MAC_CTL0_AGGRESSIVE_BACKOFF      (0x00000001UL <<  0U) /* (RW) Aggressive Back-off Enable (half-duplex, nonstd enchance perfomance) */
#define KSZSW_REGMSK_MAC_CTL0_RESERVED                (0x70UL)
/************ Switch MAC Control 1 Register (MAC_CTL1, 8 bit) **********************/
#define KSZSW_REGFLD_MAC_CTL1_MCAST_STORM_PROT_DIS    (0x00000001UL <<  6U) /* (RW) Multicast Storm Protection Disable */
#define KSZSW_REGFLD_MAC_CTL1_BACKPRESS_MODE          (0x00000001UL <<  5U) /* (RW) Back Pressure Mode (1 - carrier sense, 0 - collision based (recommended!)) */
#define KSZSW_REGFLD_MAC_CTL1_FLOWCTL_BACKPRESS_FAIR  (0x00000001UL <<  4U) /* (RW) Flow Control and Back Pressure Fair Mode (enable drop on non-flowcontrol) */
#define KSZSW_REGFLD_MAC_CTL1_NO_EXSC_COLLISION_DROP  (0x00000001UL <<  3U) /* (RW) No Excessive Collision Drop */
#define KSZSW_REGFLD_MAC_CTL1_JUMBO_PACK_SUPPORT      (0x00000001UL <<  2U) /* (RW) Jumbo packet support */
#define KSZSW_REGFLD_MAC_CTL1_MAX_PACK_SIZE_CHECK_DIS (0x00000001UL <<  1U) /* (RW) Legal Maximum Packet Size Check Disable */
#define KSZSW_REGFLD_MAC_CTL1_PASS_SHORT_PACKET       (0x00000001UL <<  0U) /* (RW) Pass Short Packet */
#define KSZSW_REGMSK_MAC_CTL1_RESERVED                (0x80UL)
/************ Switch MAC Control 2 Register (MAC_CTL2, 8 bit) **********************/
#define KSZSW_REGFLD_MAC_CTL2_NULL_VID_REPLACE        (0x00000001UL <<  3U) /* (RW) Null VID Replacement (Enable replace NULL VID to default VID */
#define KSZSW_REGFLD_MAC_CTL2_BCAST_STORM_PROTRATE_H  (0x00000007UL <<  0U) /* (RW) Broadcast Storm Protection Rate bits [10:8] */
#define KSZSW_REGMSK_MAC_CTL2_RESERVED                (0xF0UL)
/************ Switch MAC Control 3 Register (MAC_CTL3, 8 bit) **********************/
#define KSZSW_REGFLD_MAC_CTL3_BCAST_STORM_PROTRATE_L  (0x000000FFUL <<  0U) /* (RW) Broadcast Storm Protection Rate bits [7:0] */
/************ Switch MAC Control 3 Register (MAC_CTL4, 8 bit) **********************/
#define KSZSW_REGFLD_MAC_CTL4_PASS_FLOWCTL            (0x00000001UL <<  0U) /* (RW) Pass Flow Control Packets */
#define KSZSW_REGMSK_MAC_CTL4_RESERVED                (0xFEUL)
/************ Switch MAC Control 5 Register (MAC_CTL5, 8 bit) **********************/
#define KSZSW_REGFLD_MAC_CTL5_INGRESS_RATELIM_PERIOD  (0x00000003UL <<  4U) /* (RW) Ingress Rate Limit Period (16ms, 64ms, 256ms) */
#define KSZSW_REGFLD_MAC_CTL5_QBASED_EGRESS_RATELIM   (0x00000001UL <<  3U) /* (RW) Queue Based Egress Rate Limit Enable (1 - queue-based, 0 - port-based) */
#define KSZSW_REGMSK_MAC_CTL5_RESERVED                (0xC7UL)
/************ Switch MIB Control Register(MIB_CTL, 8 bit) **************************/
#define KSZSW_REGFLD_MIB_CTL_FLUSH_CNTRS              (0x00000001UL <<  7U) /* (RWSC) Flush MIB Counters (self clear) */
#define KSZSW_REGFLD_MIB_CTL_FREEZE_CNTRS             (0x00000001UL <<  6U) /* (RW) Freeze MIB Counters */
#define KSZSW_REGMSK_MIB_CTL_RESERVED                 (0x3FUL)
/************ 802.1p Priority Mapping 0-4 Registers (802_1P_PRI_MAP0-4, 8 bit) *****/
#define KSZSW_REGFLD_802_1P_PRI_MAP0_PCP1             (0x00000007UL <<  4U) /* (RW) Priority for PCP field = 1 */
#define KSZSW_REGFLD_802_1P_PRI_MAP0_PCP0             (0x00000007UL <<  0U) /* (RW) Priority for PCP field = 0 */
#define KSZSW_REGFLD_802_1P_PRI_MAP1_PCP3             (0x00000007UL <<  4U) /* (RW) Priority for PCP field = 3 */
#define KSZSW_REGFLD_802_1P_PRI_MAP1_PCP2             (0x00000007UL <<  0U) /* (RW) Priority for PCP field = 2 */
#define KSZSW_REGFLD_802_1P_PRI_MAP2_PCP5             (0x00000007UL <<  4U) /* (RW) Priority for PCP field = 5 */
#define KSZSW_REGFLD_802_1P_PRI_MAP2_PCP4             (0x00000007UL <<  0U) /* (RW) Priority for PCP field = 4 */
#define KSZSW_REGFLD_802_1P_PRI_MAP3_PCP7             (0x00000007UL <<  4U) /* (RW) Priority for PCP field = 7 */
#define KSZSW_REGFLD_802_1P_PRI_MAP3_PCP6             (0x00000007UL <<  0U) /* (RW) Priority for PCP field = 6 */
/************ IP DiffServ Priority Enable Register (DSCP_PRI_EN, 8 bit) *************/
#define KSZSW_REGFLD_DSCP_PRI_EN_REMAP_EN             (0x00000001UL <<  0U) /* (RW) DiffServ Priority Remap Enable (1 - from regs, 0 - from field) */
#define KSZSW_REGMSK_DSCP_PRI_EN_RESERVED             (0xFEUL)
/************ Global Port Mirroring and Snooping Control Register (SNOOPING_CTL, 8 bit) */
#define KSZSW_REGFLD_SNOOPING_CTL_IGMP_EN             (0x00000001UL <<  6U) /* (RW) IGMP Snooping Enable (IPv4) - forward to host */
#define KSZSW_REGFLD_SNOOPING_CTL_MLD_OPT             (0x00000001UL <<  3U) /* (RW) MLD Snooping Option  */
#define KSZSW_REGFLD_SNOOPING_CTL_MLD_EN              (0x00000001UL <<  2U) /* (RW) MLD Snooping Enable (IPv6) */
#define KSZSW_REGFLD_SNOOPING_CTL_SNIFF_MODE          (0x00000001UL <<  0U) /* (RW) Sniff Mode Select (1 - Rx AND Tx, 0 - Rx OR Tx) */
#define KSZSW_REGMSK_SNOOPING_CTL_RESERVED            (0xB2UL)
/************ WRED DiffServ Color Mapping Register (DSCP_COLOR_MAP, 8 bit) **************/
#define KSZSW_REGFLD_DSCP_COLOR_MAP_RED               (0x00000003UL <<  4U) /* (RW) red DSCP value */
#define KSZSW_REGFLD_DSCP_COLOR_MAP_YELLOW            (0x00000003UL <<  2U) /* (RW) yellow DSCP value */
#define KSZSW_REGFLD_DSCP_COLOR_MAP_GREEN             (0x00000003UL <<  0U) /* (RW) green DSCP value */
/************ PTP Event (Non-Event) Message Priority Register (PTP_EVT_MSG_PRI / PTP_NONEVT_MSG_PRI, 8 bit) **/
#define KSZSW_REGFLD_PTP_MSG_PRI_OVERRIDE             (0x00000001UL <<  7U) /* (RW) PTP Message Priority Override (0 - QoS, 1 - register value) */
#define KSZSW_REGFLD_PTP_MSG_PRI_VAL                  (0x0000000FUL <<  0U) /* (RW) PTP Event Message Priority */
#define KSZSW_REGMSK_PTP_MSG_PRI_RESERVED             (0x70UL)
/************ Queue Management Control 0 Register (QMGMT_CTL, 32 bit) *******************/
#define KSZSW_REGFLD_QMGMT_CTL_PRI_2Q                 (0x00000003UL <<  6U) /* (RW) Priority_2Q - mapping 4-bit priority to 2 queue (if 2 queue enabled) */
#define KSZSW_REGFLD_QMGMT_CTL_RESTRICT_FWD           (0x00000001UL <<  1U) /* (RW) Restrict forward frames to ports from PORT_CTL1_FWD_PORTS */
#define KSZSW_REGMSK_QMGMT_CTL_RESERVED               (0xFFFFFF3CUL)
/************ Global PM Available Register (GLOB_PM_AV, 32 bit) *******************/
#define KSZSW_REGFLD_GLOB_PM_AV_BC                    (0x000007FFUL << 16U) /* (RO) Packet Memory Available Block Count */
#define KSZSW_REGMSK_GLOB_PM_AV_RESERVED              (0xF800FFFFUL)
/************ VLAN Table Entry 0 Register (VLAN_TBL_ENTRY0, 32 bit) *********************/
#define KSZSW_REGFLD_VLAN_TBL_ENTRY0_VALID            (0x00000001UL << 31U) /* (RW) VALID - if the table entry is valid */
#define KSZSW_REGFLD_VLAN_TBL_ENTRY0_FORWARD_OPT      (0x00000001UL << 27U) /* (RW) FORWARD OPTION - forward to the VLAN Table port map */
#define KSZSW_REGFLD_VLAN_TBL_ENTRY0_PRIORITY         (0x00000007UL << 24U) /* (RW) PRIORITY - priority level */
#define KSZSW_REGFLD_VLAN_TBL_ENTRY0_MSTP_IDX         (0x00000007UL << 12U) /* (RW) MSTP INDEX - Multiple Spanning Tree Protocol index */
#define KSZSW_REGFLD_VLAN_TBL_ENTRY0_FID              (0x0000007FUL <<  0U) /* (RW) FID - Filter ID (hashed with the destination address) */
#define KSZSW_REGMSK_VLAN_TBL_ENTRY0_RESERVED         (0x70FF8F80UL)
/************ VLAN Table Entry 1 Register (VLAN_TBL_ENTRY1, 32 bit) *********************/
#define KSZSW_REGFLD_VLAN_TBL_ENTRY1_PORT_UNTAG       (0x0000007FUL <<  0U) /* (RW) Untagging policy for each egress port */
/************ VLAN Table Entry 1 Register (VLAN_TBL_ENTRY2, 32 bit) *********************/
#define KSZSW_REGFLD_VLAN_TBL_ENTRY2_PORT_FWD         (0x0000007FUL <<  0U) /* (RW) Forwarding policy for each port (if FORWARD_OPT set) */
/************ VLAN Table Index Register (VLAN_TBL_IDX, 16 bit) **************************/
#define KSZSW_REGFLD_VLAN_TBL_IDX_VAL                 (0x00000FFFUL <<  0U) /* (RW) VLAN Index */
#define KSZSW_REGMSK_VLAN_TBL_IDX_RESERVED            (0xF000UL)
/************ VLAN Table Access Control Register (VLAN_TBL_ACC_CTL, 8 bit) **************/
#define KSZSW_REGFLD_VLAN_TBL_ACC_CTL_START           (0x00000001UL <<  7U) /* (RWSC) Start the specified action (self cleared) */
#define KSZSW_REGFLD_VLAN_TBL_ACC_CTL_ACTION          (0x00000003UL <<  0U) /* (RW) Action */
#define KSZSW_REGMSK_VLAN_TBL_ACC_CTL_RESERVED        (0x7CUL)

#define KSZSW_REGFLDVAL_VLAN_TBL_ACC_CTL_ACTION_NOOP  0
#define KSZSW_REGFLDVAL_VLAN_TBL_ACC_CTL_ACTION_WRITE 1
#define KSZSW_REGFLDVAL_VLAN_TBL_ACC_CTL_ACTION_READ  2
#define KSZSW_REGFLDVAL_VLAN_TBL_ACC_CTL_ACTION_CLEAR 3
/************ ALU Table Index 0 Register (ALU_TBL_IDX0, 32 bit) *********************************/
#define KSZSW_REGFLD_ALU_TBL_IDX0_FID                 (0x0000007FUL << 16U) /* (RW) FID value used to hash index the table */
#define KSZSW_REGFLD_ALU_TBL_IDX0_MAC_H               (0x0000FFFFUL <<  0U) /* (RW) MAC Address Index [47:32] */
#define KSZSW_REGMSK_ALU_TBL_IDX0_RESERVED            (0xFF800000UL)
/************ ALU Table Index 1 Register (ALU_TBL_IDX1, 32 bit) *********************************/
#define KSZSW_REGFLD_ALU_TBL_IDX1_MAC_L               (0xFFFFFFFFUL <<  0U) /* (RW) MAC Address Index [31:00] */
/************ ALU Table Access Control Register (ALU_TBL_ACC_CTL, 32 bit) ***********************/
#define KSZSW_REGFLD_ALU_TBL_ACC_CTL_VALID_CNT        (0x00003FFFUL << 16U) /* (RO) Indicates the total number of valid entries */
#define KSZSW_REGFLD_ALU_TBL_ACC_CTL_START            (0x00000001UL <<  7U) /* (RWSC) Start the action (self cleared) */
#define KSZSW_REGFLD_ALU_TBL_ACC_CTL_VALID            (0x00000001UL <<  6U) /* (RO) Indicates that the next valid entry is ready */
#define KSZSW_REGFLD_ALU_TBL_ACC_CTL_VALID_OR_FINISH  (0x00000001UL <<  5U) /* (RO) Indicates either that the next valid entry is ready, or that the search has ended */
#define KSZSW_REGFLD_ALU_TBL_ACC_CTL_DIRECT           (0x00000001UL <<  2U) /* (RW) Direct addressing the ALU Table (debugging only) */
#define KSZSW_REGFLD_ALU_TBL_ACC_CTL_ACTION           (0x00000003UL <<  0U) /* (RW) Specifies the action to be taken for the ALU Table entry access */
#define KSZSW_REGMSK_ALU_TBL_ACC_CTL_RESERVED         (0xC000FF18UL)

#define KSZSW_REGFLDVAL_ALU_TBL_ACC_CTL_ACTION_NOOP   0
#define KSZSW_REGFLDVAL_ALU_TBL_ACC_CTL_ACTION_WRITE  1
#define KSZSW_REGFLDVAL_ALU_TBL_ACC_CTL_ACTION_READ   2
#define KSZSW_REGFLDVAL_ALU_TBL_ACC_CTL_ACTION_SEARCH 3
/************ Static Address and Reserved Multicast Table Control Register (SADDR_RMCAST_TBL_CTL, 32 buit) *****/
#define KSZSW_REGFLD_STADDR_RMCAST_TBL_CTL_RMCAST_IDX (0x0000003FUL << 16U) /* (RW) index the Reserved Multicast Table */
#define KSZSW_REGFLD_STADDR_RMCAST_TBL_CTL_STADDR_IDX (0x0000000FUL << 16U) /* (RW) index the Static Address Table */
#define KSZSW_REGFLD_STADDR_RMCAST_TBL_CTL_START      (0x00000001UL <<  7U) /* (RWSC) Start Access */
#define KSZSW_REGFLD_STADDR_RMCAST_TBL_CTL_TBL_SEL    (0x00000001UL <<  1U) /* (RW) Table Select (1 - RMCAST, 0 - SADDR) */
#define KSZSW_REGFLD_STADDR_RMCAST_TBL_CTL_ACTION     (0x00000001UL <<  0U) /* (RW) Action (1 - Read, 0 - Write) */
#define KSZSW_REGMSK_STADDR_RMCAST_TBL_CTL_RESERVED   (0xFFC0FF7CUL)

#define KSZSW_REGFLDVAL_STADDR_RMCAST_TBL_CTL_TBL_SEL_RMCAST 1
#define KSZSW_REGFLDVAL_STADDR_RMCAST_TBL_CTL_TBL_SEL_SADDR  0

#define KSZSW_REGFLDVAL_STADDR_RMCAST_TBL_CTL_ACTION_READ    1
#define KSZSW_REGFLDVAL_STADDR_RMCAST_TBL_CTL_ACTION_WRITE   0

/************ ALU Table Entry 1 Register (ALU_TBL_ENTRY1, 32 bit) ********************/
#define KSZSW_REGFLD_ALU_TBL_ENTRY1_STATIC            (0x00000001UL << 31U) /* (RW) Static Entry (updated by host; will not aged out) */
#define KSZSW_REGFLD_ALU_TBL_ENTRY1_SRC_FLT           (0x00000001UL << 30U) /* (RW) Drop packet if source address match during source learning */
#define KSZSW_REGFLD_ALU_TBL_ENTRY1_DST_FLT           (0x00000001UL << 29U) /* (RW) Drop packet if destination address match during source learning */
#define KSZSW_REGFLD_ALU_TBL_ENTRY1_PRIO              (0x00000007UL << 26U) /* (RW) Priority (static entry) */
#define KSZSW_REGFLD_ALU_TBL_ENTRY1_AGE_CNT           (0x00000007UL << 26U) /* (RW) Age count (dynamic entry) */
#define KSZSW_REGFLD_ALU_TBL_ENTRY1_MSTP              (0x00000007UL <<  0U) /* (RW) Multiple Spanning Tree Protocol group ID for matching  */
#define KSZSW_REGMSK_ALU_TBL_ENTRY1_RESERVED          (0x03FFFFF8UL)
/************ ALU Table Entry 2 Register (ALU_TBL_ENTRY2, 32 bit) ********************/
#define KSZSW_REGFLD_ALU_TBL_ENTRY2_OVRD              (0x00000001UL << 31U) /* (RW)  Enable overriding of port state  */
#define KSZSW_REGFLD_ALU_TBL_ENTRY2_PORT_FWD          (0x0000007FUL <<  0U) /* (RW)  Forward to the corresponding ports  */
#define KSZSW_REGMSK_ALU_TBL_ENTRY2_RESERVED          (0x7FFFFF80UL)
/************ ALU Table Entry 3 Register (ALU_TBL_ENTRY3, 32 bit) ********************/
#define KSZSW_REGFLD_ALU_TBL_ENTRY3_FID               (0x0000007FUL << 16U) /* (RW)  VLAN group ID for matching */
#define KSZSW_REGFLD_ALU_TBL_ENTRY3_MAC_H             (0x0000FFFFUL << 16U) /* (RW)  MAC Address [47:32] */
/************ ALU Table Entry 4 Register (ALU_TBL_ENTRY4, 32 bit) ********************/
#define KSZSW_REGFLD_ALU_TBL_ENTRY4_MAC_L             (0xFFFFFFFFUL <<  0U) /* (RW)  MAC Address [31:00] */

/************ Static Address Table Entry 1 Register (STADDR_TBL_ENTRY1, 32 bit) ********************/
#define KSZSW_REGFLD_STADDR_TBL_ENTRY1_VALID          (0x00000001UL << 31U) /* (RW) Entry is valid */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY1_SRC_FLT        (0x00000001UL << 30U) /* (RW) Drop packet if source address match during source learning */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY1_DST_FLT        (0x00000001UL << 29U) /* (RW) Drop packet if destination address match during source learning */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY1_PRIO           (0x00000007UL << 26U) /* (RW) Priority (static entry) */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY1_MSTP           (0x00000007UL <<  0U) /* (RW) Multiple Spanning Tree Protocol group ID for matching  */
#define KSZSW_REGMSK_STADDR_TBL_ENTRY1_RESERVED       (0x03FFFFF8UL)
/************ Static Address Table Entry 2 Register (STADDR_TBL_ENTRY2, 32 bit) ********************/
#define KSZSW_REGFLD_STADDR_TBL_ENTRY2_OVRD           (0x00000001UL << 31U) /* (RW)  Enable overriding of port state */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY2_USE_FID        (0x00000001UL << 30U) /* (RW)  Use FID on multicast packets for matching */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY2_PORT_FWD       (0x0000007FUL <<  0U) /* (RW)  Forward to the corresponding ports  */
#define KSZSW_REGMSK_STADDR_TBL_ENTRY2_RESERVED       (0x3FFFFF80UL)
/************ Static Address Table Entry 3 Register (STADDR_TBL_ENTRY3, 32 bit) ********************/
#define KSZSW_REGFLD_STADDR_TBL_ENTRY3_FID            (0x0000007FUL << 16U) /* (RW)  VLAN group ID for matching */
#define KSZSW_REGFLD_STADDR_TBL_ENTRY3_MAC_H          (0x0000FFFFUL <<  0U) /* (RW)  MAC Address [47:32] */
/************ Static Address Table Entry 4 Register (STADDR_TBL_ENTRY4, 32 bit) ********************/
#define KSZSW_REGFLD_STADDR_TBL_ENTRY4_MAC_L          (0xFFFFFFFFUL <<  0U) /* (RW)  MAC Address [31:00] */

/************ Reserved Multicast Table Entry 2 Register (RMCAST_TBL_ENTRY2, 32 bit) ****************/
#define KSZSW_REGFLD_RMCAST_TBL_ENTRY2_PORT_FWD       (0x0000007FUL <<  0U) /* (RW)  Forward to the corresponding ports  */
#define KSZSW_REGMSK_RMCAST_TBL_ENTRY2_RESERVED       (0xFFFFFF80UL)


/************ Global PTP Clock Control Register (PTP_CLK_CTL, 16 bit) ****************************/
#define KSZSW_REGFLD_PTP_CLK_CTL_SWITCH_FREQADJ_DIS   (0x00000001UL << 15U) /* (RW) Disable Switch Frequency Adjustment */
#define KSZSW_REGFLD_PTP_CLK_CTL_STEP_ADJ             (0x00000001UL <<  6U) /* (RWSC) PTP Clock Step Adjustment */
#define KSZSW_REGFLD_PTP_CLK_CTL_STEP_DIR             (0x00000001UL <<  5U) /* (RW) PTP Step Direction (1 - Add, 0 - Substract) */
#define KSZSW_REGFLD_PTP_CLK_CTL_READ                 (0x00000001UL <<  4U) /* (RWSC) Current PTP clock read to registers 0x0502-0x050B (self cleared)*/
#define KSZSW_REGFLD_PTP_CLK_CTL_LOAD                 (0x00000001UL <<  3U) /* (RWSC) Load PTP clock from registers 0x0502-0x050B (self cleared) */
#define KSZSW_REGFLD_PTP_CLK_CTL_CONT_ADJ             (0x00000001UL <<  2U) /* (RW) Enable PTP Clock Continuous Adjustment (every 25MHz) */
#define KSZSW_REGFLD_PTP_CLK_CTL_EN                   (0x00000001UL <<  1U) /* (RW) Enable PTP Clock */
#define KSZSW_REGFLD_PTP_CLK_CTL_RST                  (0x00000001UL <<  0U) /* (RWSC) Reset PTP Clock */
#define KSZSW_REGMSK_PTP_CLK_RESERVED                 (0x7F80UL)
/************ Global PTP RTC Clock Phase Register (PTP_RTC_CLK_PHASE, 16 bit) ********************/
#define KSZSW_REGFLD_PTP_RTC_CLK_PHASE_VAL            (0x00000007UL <<  0U) /* (RW) PTP Real Time Clock 8ns Phase */
#define KSZSW_REGMSK_PTP_RTC_CLK_PHASE_RESERVED       (0xFFF8UL)

#define KSZSW_REGFLDVAL_PTP_RTC_CLK_PHASE_VAL_0NS     0
#define KSZSW_REGFLDVAL_PTP_RTC_CLK_PHASE_VAL_8NS     1
#define KSZSW_REGFLDVAL_PTP_RTC_CLK_PHASE_VAL_16NS    2
#define KSZSW_REGFLDVAL_PTP_RTC_CLK_PHASE_VAL_24NS    3
#define KSZSW_REGFLDVAL_PTP_RTC_CLK_PHASE_VAL_32NS    4
/************ Global PTP Clock Sub-Nanosecond Rate High Word Register (PTP_SUBNS_RATE_H, 16 bit) **/
#define KSZSW_REGFLD_PTP_SUBNS_RATE_H_DIR             (0x00000001UL << 15U) /* (RW) PTP Rate Direction (1 - added, 0 - substracted)*/
#define KSZSW_REGFLD_PTP_SUBNS_RATE_H_TEMPADJ_MODE    (0x00000001UL << 14U) /* (RW) PTP Temporary Adjustment Mode */
#define KSZSW_REGFLD_PTP_SUBNS_RATE_H_VAL             (0x00003FFFUL <<  0U) /* (RW) PTP Real Time Clock Sub-Nanosecond [29:16] */
/************ Global PTP Clock Sub-Nanosecond Rate Low Word Register (PTP_SUBNS_RATE_L, 16 bit) ***/
#define KSZSW_REGFLD_PTP_SUBNS_RATE_L_VAL             (0x0000FFFFUL <<  0U) /* (RW) PTP Real Time Clock Sub-Nanosecond [15:0] */
/************ Global PTP Message Config 1 Register (PTP_MSG_CFG1, 16 bit) *************************/
#define KSZSW_REGFLD_PTP_MSG_CFG1_802_1AS_MODE_EN     (0x00000001UL <<  7U) /* (RW) Enable IEEE 802.1AS Mode (forward to host) */
#define KSZSW_REGFLD_PTP_MSG_CFG1_PTP_MODE_EN         (0x00000001UL <<  6U) /* (RW) Enable IEEE 1588 PTP Mode (forward, modify, tail-tag) */
#define KSZSW_REGFLD_PTP_MSG_CFG1_DETECT_ETH_MSG_EN   (0x00000001UL <<  5U) /* (RW) Enable Detection of IEEE 802.3 Ethernet PTP Messages */
#define KSZSW_REGFLD_PTP_MSG_CFG1_DETECT_IPV4_MSG_EN  (0x00000001UL <<  4U) /* (RW) Enable Detection of IPv4/UDP PTP Messages */
#define KSZSW_REGFLD_PTP_MSG_CFG1_DETECT_IPV6_MSG_EN  (0x00000001UL <<  3U) /* (RW) Enable Detection of IPv6/UDP PTP Messages*/
#define KSZSW_REGFLD_PTP_MSG_CFG1_TC_P2P_MODE         (0x00000001UL <<  2U) /* (RW) Selection of P2P or E2E transparent clock mode*/
#define KSZSW_REGFLD_PTP_MSG_CFG1_HOST_MASTER_MODE    (0x00000001UL <<  1U) /* (RW) Selection of Master or Slave for host port */
#define KSZSW_REGFLD_PTP_MSG_CFG1_1STEP_MODE          (0x00000001UL <<  0U) /* (RW) Selection of One-step or Two-step Operation */
#define KSZSW_REGMSK_PTP_MSG_CFG1_RESERVED            (0xFF00UL)
/************ Global PTP Message Config 2 Register (PTP_MSG_CFG2, 16 bit)*************************/
#define KSZSW_REGFLD_PTP_MSG_CFG2_UCAST_EN            (0x00000001UL << 12U) /* (RW) Enable Unicast PTP */
#define KSZSW_REGFLD_PTP_MSG_CFG2_ALT_MASTER_EN       (0x00000001UL << 11U) /* (RW) Enable Alternate Master */
#define KSZSW_REGFLD_PTP_MSG_CFG2_MSG_PRI_TXQ         (0x00000001UL << 10U) /* (RW) PTP Messages Priority TX Queue (1 - All, 0 - only Events) */
#define KSZSW_REGFLD_PTP_MSG_CFG2_SYNC_FOLLOWUP_CHECK (0x00000001UL <<  9U) /* (RW) Enable Checking of Associated Sync and Follow_up PTP messages */
#define KSZSW_REGFLD_PTP_MSG_CFG2_DELAY_RESP_CHECK    (0x00000001UL <<  8U) /* (RW) Enable Checking of Associated Delay_Req and Delay Resp PTP Messages */
#define KSZSW_REGFLD_PTP_MSG_CFG2_PDELAY_PRESP_CHECK  (0x00000001UL <<  7U) /* (RW) Enable Checking of Associated Pdelay_Req and Pdelay_Resp PTP  Messages */
#define KSZSW_REGFLD_PTP_MSG_CFG2_SYNC_DELAY_DROP_EN  (0x00000001UL <<  5U) /* (RW) Enable Dropping of Sync/Follow_Up and Delay_Req PTP Messages */
#define KSZSW_REGFLD_PTP_MSG_CFG2_DOMAIN_CHECK        (0x00000001UL <<  4U) /* (RW) Enable Checking of Domain Field */
#define KSZSW_REGFLD_PTP_MSG_CFG2_EGR_CHECKSUM_CALC   (0x00000001UL <<  2U) /* (RW) Enable IPv4/UDP Checksum Calculation for Egress Packets */
#define KSZSW_REGMSK_PTP_MSG_CFG2_RESERVED            (0xE04BUL)
/************ Global PTP Domain and Version Register (PTP_DOMAIN_VER, 16 bit) *******************/
#define KSZSW_REGFLD_PTP_DOMAIN_VER_VERSION           (0x0000000FUL <<  8U) /* (RW) PTP Version */
#define KSZSW_REGFLD_PTP_DOMAIN_VER_DOMAIN            (0x000000FFUL <<  0U) /* (RW) PTP Domain */
#define KSZSW_REGMSK_PTP_DOMAIN_VER_RESERVED          (0xF000UL)
/************ Global PTP Unit Index Register (PTP_UNIT_IDX, 32 bit) *****************************/
#define KSZSW_REGFLD_PTP_UNIT_IDX_TSTMP_PTR           (0x00000001UL <<  8U) /* (RW) Timestamp Unit Index Pointer (Timestamp Unit 0-1) */
#define KSZSW_REGFLD_PTP_UNIT_IDX_TRIG_PTR            (0x00000003UL <<  0U) /* (RW) Trigger Unit Index Pointer (Trigger Unit 0-2) */
#define KSZSW_REGMSK_PTP_UNIT_IDX_RESERVED            (0xFFFFFEFCUL)
/************ GPIO Status Monitor 0 Register (PTP_GPIO_MON0, 32 bit) ****************************/
#define KSZSW_REGFLD_PTP_GPIO_MON0_EVT_TRIG_ERR(unt)  (0x00000001UL << (16U + (unit))) /* (RW1C) Event Trigger Output Error (Unit 0-2) */
#define KSZSW_REGFLD_PTP_GPIO_MON0_EVT_TRIG_DONE(unt) (0x00000001UL << ( 0U + (unit))) /* (RW1C) Event Trigger Output Done (Unit 0-2) */
#define KSZSW_REGMSK_PTP_GPIO_MON0_RESERVED           (0xFFF8FFF8UL)
/************ GPIO Status Monitor 1 Register (PTP_GPIO_MON1, 32 bit) ****************************/
#define KSZSW_REGFLD_PTP_GPIO_MON1_TRIG_INT(unt)      (0x00000001UL << (16U + (unit))) /* (RW1C) Trigger Output Interrupt Status (Unit 0-2) */
#define KSZSW_REGFLD_PTP_GPIO_MON1_TSTMP_INT          (0x00000001UL << ( 0U + (unit))) /* (RW1C) Timestamp Interrupt Status (GPIO input timestamp only) (Unit 0-1)*/
#define KSZSW_REGMSK_PTP_GPIO_MON1_RESERVED           (0xFFF8FFFCUL)
/************ Timestamp Control and Status Register (PTP_TSTMP_CTLSTAT, 32 bit) *****************/
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_GPIO_OUT_SEL   (0x00000001UL <<  8U) /* (RW) GPIO Output Source Select (1 - combinatorial result, 0 -  from flopped output) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_GPIO_IN        (0x00000001UL <<  7U) /* (RO) GPIO Inputs Monitor  (current value seen on the GOIO input) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_GPIO_OEN       (0x00000001UL <<  6U) /* (RW) GPIO Output Enable (1 - timestamp input, 0 - trigger output) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_TSTMP_INT_EN   (0x00000001UL <<  5U) /* (RW) Timestamp Unit Interrupt Enable (indexed) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_TRIG_ACTIVE    (0x00000001UL <<  4U) /* (RO) Event Trigger Output Unit Active (indexed) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_TRIG_EN        (0x00000001UL <<  3U) /* (RW) Event Trigger Output Unit Enable (indexed) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_TRIG_RST       (0x00000001UL <<  2U) /* (RW) Event Trigger Output Unit Software Reset (indexed) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_TSTM_IN_EN     (0x00000001UL <<  1U) /* (RW) Event Timestamp Input Unit Enable (indexed) */
#define KSZSW_REGFLD_PTP_TSTMP_CTLSTAT_TSTM_IN_RST    (0x00000001UL <<  0U) /* (RW) Event Timestamp Input Unit Software Reset (indexed) */
#define KSZSW_REGMSK_PTP_TSTMP_CTLSTAT_RESERVED       (0xFFFFFE00UL)
/************ Trigger Output Unit Target Time Nanosecond Register (PTP_TRIG_TIME_NS, 32 bit ****/
#define KSZSW_REGFLD_PTP_TRIG_TIME_NS_VAL             (0x3FFFFFFFUL <<  0U) /* (RW) Trigger Target Time (ns) (TRIGGER_TARGET_TIME_NS) */
#define KSZSW_REGMSK_PTP_TRIG_TIME_NS_RESERVED        (0xC0000000UL)
/************  Trigger Output Unit Target Time Second Register (PTP_TRIG_TIME_SEC, 32 bit) *****/
#define KSZSW_REGFLD_PTP_TRIG_TIME_SEC_VAL            (0xFFFFFFFFUL <<  0U) /* (RW) Trigger Target Time (s) (TRIGGER_TARGET_TIME_S) */
/************ Trigger Output Unit Control 1 Register (PTP_TRIG_CTL1, 32 bit) *******************/
#define KSZSW_REGFLD_PTP_TRIG_CTL1_CASCADE_EN         (0x00000001UL << 31U) /* (RW) Enable Event Trigger Output Unit in Cascade Mode */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_CASCADE_TAIL       (0x00000001UL << 30U) /* (RW) Cascade Mode Event Trigger Output Unit Tail Unit Indicator */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_CASCADE_UPSTR      (0x00000003UL << 26U) /* (RW) Cascade Mode Upstream Trigger Done Unit Select */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_NOW           (0x00000001UL << 25U) /* (RW) Trigger Now (immedeately trigger if trig time < systime) */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_NOTIFY        (0x00000001UL << 24U) /* (RW) Trigger Notify (enable reporting both TRIG_DONE and TRIGERR interrupt) */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_EDGE          (0x00000001UL << 23U) /* (RW) Trigger Edge - output on negative (1) or positive (0) edge of clock */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG           (0x00000007UL << 20U) /* (RW) Trigger Event Output Signal Pattern */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_ITER_CNT      (0x0000FFFFUL <<  0U) /* (RW) Trigger Output Iteration Count */
#define KSZSW_REGMSK_PTP_TRIG_CTL1_RESERVED           (0x300F0000UL)

#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_NEDGE      0 /* Generates negative edge */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_PEDGE      1 /* Generates positive edge */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_NPULSE     2 /* Generates negative pulse */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_PPULSE     3 /* Generates positive pulse */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_NPERIODIC  4 /* Generates negative periodic signal */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_PPERIODIC  5 /* Generates positive periodic signal */
#define KSZSW_REGFLD_PTP_TRIG_CTL1_TRIG_SIG_PATTERN    6 /* Pattern from CTL3 reg, MSB first */
/************ Trigger Output Unit Control 2 Register (PTP_TRIG_CTL2, 32 bit) *******************/
#define KSZSW_REGFLD_PTP_TRIG_CTL2_CYCLE_WIDTH         (0xFFFFFFFFUL <<  0U) /* (RW) Trigger Output Cycle Width or Bit Width (ns)*/
/************ Trigger Output Unit Control 3 Register (PTP_TRIG_CTL3, 32 bit) *******************/
#define KSZSW_REGFLD_PTP_TRIG_CTL3_CYCLE_COUNT         (0x0000FFFFUL << 16U) /* (RW) Trigger Output Cycle or Bit Count (0 - infinity)*/
#define KSZSW_REGFLD_PTP_TRIG_CTL3_BIT_PATTERN         (0x0000FFFFUL <<  0U) /* (RW) Trigger Output Bit Pattern */
/************ Trigger Output Unit Control 4 Register (PTP_TRIG_CTL4, 32 bit)********************/
#define KSZSW_REGFLD_PTP_TRIG_CTL4_CASCADE_CYCLE_TIME  (0xFFFFFFFFUL <<  0U) /* (RW) Trigger Output Iteration Cycle Time in Cascade Mode (ns)*/
/************ Trigger Output Unit Control 5 Register (PTP_TRIG_CTL5, 32 bit) *******************/
#define KSZSW_REGFLD_PTP_TRIG_CTL5_PPS_PULSE_WIDTH_H   (0x000000FFUL << 16U) /* (RW) Trigger Output Iteration Cycle Time in Cascade Mode (ns)*/
#define KSZSW_REGFLD_PTP_TRIG_CTL5_TRIG_PULSE_WIDTH    (0x0000FFFFUL <<  0U) /* (RW) Trigger Output Pulse Width */
#define KSZSW_REGMSK_PTP_TRIG_CTL5_RESERVED            (0xFF000000UL)
/************ Timestamp Status and Control Register (PTP_TSTMP_STATCTL, 32 bit) ****************/
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_EVT_CNT         (0x0000000FUL << 17U) /* (RO) Number of Detected Event Count for Timestamp Input Unit */
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_EVT_CNT_OV      (0x00000001UL << 16U) /* (RO) Detected Event Count Overflow */
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_RISE_DET_EN     (0x00000001UL <<  7U) /* (RW) Enable Rising Edge Detection */
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_FALL_DET_EN     (0x00000001UL <<  6U) /* (RW) Enable Falling Edge Detection */
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_CASCADE_TAIL    (0x00000001UL <<  5U) /* (RW) Tail Unit Indicator for Timestamp Input Unit Event in Cascade Mode */
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_CASCADE_UPSTR   (0x00000001UL <<  1U) /* (RW) Select Upstream Timestamp Done Unit in Cascade Mode */
#define KSZSW_REGFLD_PTP_TSTMP_STATCTL_CASCADE_EN      (0x00000001UL <<  0U) /* (RW) Enable This Event Timestamp Input Unit in Cascade Mode */
#define KSZSW_REGMSK_PTP_TSTMP_STATCTL_RESERVED        (0xFFE0FF1CUL)
/************ Timestamp n Sample Time Nanoseconds Register (PTP_TSTMP_ST_NS, 32 bit) ************/
#define KSZSW_REGFLD_PTP_TSTMP_ST_NS_EDGE              (0x00000001UL << 30U) /* (RO) Event Timestamp Input of the Sample Edge Indication (1 - rise, 0 - fall) */
#define KSZSW_REGFLD_PTP_TSTMP_ST_NS_TIME              (0x3FFFFFFFUL <<  0U) /* (RO) Event Timestamp Input of the Sample Time in Nanoseconds */
#define KSZSW_REGMSK_PTP_TSTMP_ST_NS_RESERVED          (0x80000000UL)
/************ Timestamp n Sample Time Seconds Register (PTP_TSTMP_ST_SEC, 32 bit) ***************/
#define KSZSW_REGFLD_PTP_TSTMP_ST_SEC_TIME             (0xFFFFFFFFUL <<  0U) /* Event Timestamp Input of the 1st Sample Time in Seconds */
/************ Timestamp n Sample Time Phase Register (PTP_TSTMP_ST_PHASE, 32 bit) ***************/
#define KSZSW_REGFLD_PTP_TSTMP_ST_PHASE_VAL            (0x00000007UL <<  0U) /* Event Timestamp Input of the 1st Sample Time in Sub 8ns */
#define KSZSW_REGMSK_PTP_TSTMP_ST_PHASE_RESERVED       (0xFFFFFFF8UL)

/************ Port PME_WoL Event/Enable Register (PORT_PME_WOL_EVT / PORT_PME_WOL_EN, 8 bit) ***/
#define KSZSW_REGFLD_PORT_PME_WOL_MAGIC_PKT_DET        (0x00000001UL <<  2U) /* (RW1C/RW) Magic Packet Detect */
#define KSZSW_REGFLD_PORT_PME_WOL_LINKUP_DET           (0x00000001UL <<  1U) /* (RW1C/RW) Link Up Detect (PHY Only) */
#define KSZSW_REGFLD_PORT_PME_WOL_ENERGY_DET           (0x00000001UL <<  0U) /* (RW1C/RW) Energy Detect */
#define KSZSW_REGMSK_PORT_PME_WOL_RESERVED             (0xF8UL)
/************ Port Interrupt Status/Mask Register (PORT_INT_STAT / PORT_INT_MASK, 8 bit) *******/
#define KSZSW_REGFLD_PORT_INT_PTP                      (0x00000001UL <<  2U) /* (RO/RW) PTP Interrupt */
#define KSZSW_REGFLD_PORT_INT_PHY                      (0x00000001UL <<  1U) /* (RO/RW) PHY Interrupt (PHY ports) */
#define KSZSW_REGFLD_PORT_INT_ACL                      (0x00000001UL <<  0U) /* (RO/RW) ACL Interrupt*/
#define KSZSW_REGMSK_PORT_INT_RESERVED                 (0xF8UL)
/************ Port Operation Control 0 Register (PORT_OP_CTL0, 8 bit) **************************/
#define KSZSW_REGFLD_PORT_OP_CTL0_MAC_LOCAL_LOOP       (0x00000001UL <<  7U) /* (RW) Local MAC Loopback */
#define KSZSW_REGFLD_PORT_OP_CTL0_MAC_REMOTE_LOOP      (0x00000001UL <<  6U) /* (RW) Remote MAC Loopback */
#define KSZSW_REGFLD_PORT_OP_CTL0_TAILTAG_EN           (0x00000001UL <<  2U) /* (RW) Tail Tag Enable */
#define KSZSW_REGFLD_PORT_OP_CTL0_EGR_QSPLIT           (0x00000003UL <<  0U) /* (RW) Egress Queue Split Enable */
#define KSZSW_REGMSK_PORT_OP_CTL0_RESERVED             (0x38UL)

#define KSZSW_REGFLDVAL_PORT_OP_CTL0_EGR_QSPLIT_4Q     2
#define KSZSW_REGFLDVAL_PORT_OP_CTL0_EGR_QSPLIT_2Q     1
#define KSZSW_REGFLDVAL_PORT_OP_CTL0_EGR_QSPLIT_1Q     0
/************ Port Status Register (PORT_STAT, 8 bit) ******************************************/
#define KSZSW_REGFLD_PORT_STAT_SPEED                   (0x00000003UL <<  3U) /* (RO) Port Speed Status */
#define KSZSW_REGFLD_PORT_STAT_DUPLEX                  (0x00000001UL <<  2U) /* (RO) Port Duplex Status */
#define KSZSW_REGFLD_PORT_STAT_TX_FLOWCTL_EN           (0x00000001UL <<  1U) /* (RO) Transmit Flow Control Enabled Status */
#define KSZSW_REGFLD_PORT_STAT_RX_FLOWCTL_EN           (0x00000001UL <<  0U) /* (RO) Receive Flow Control Enabled Status */
#define KSZSW_REGMSK_PORT_STAT_RESERVED                (0xE0UL)

#define KSZSW_REGFLDVAL_PORT_STAT_SPEED_10             0
#define KSZSW_REGFLDVAL_PORT_STAT_SPEED_100            1
#define KSZSW_REGFLDVAL_PORT_STAT_SPEED_1000           2

/************ XMII Port Control 0 Register (PORT_XMII_CTL0, 8 bit) ****************************/
#define KSZSW_REGFLD_PORT_XMII_CTL0_DUPLEX                    (0x00000001UL <<  6U) /* (RW) MAC Port Duplex (1 - Full, 0 - half) */
#define KSZSW_REGFLD_PORT_XMII_CTL0_TX_FLOWCTL_EN             (0x00000001UL <<  5U) /* (RW) MAC Port Transmit Flow Control Enable */
#define KSZSW_REGFLD_PORT_XMII_CTL0_SPEED_100                 (0x00000001UL <<  4U) /* (RW) MAC Port Speed 10/100 */
#define KSZSW_REGFLD_PORT_XMII_CTL0_RX_FLOWCTL_EN             (0x00000001UL <<  3U) /* (RW) MAC Port Receive Flow Control Enable */
#define KSZSW_REGMSK_PORT_XMII_CTL0_RESERVED                  (0x87UL)
/************ XMII Port Control 1 Register (PORT_XMII_CTL1, 8 bit) ****************************/
#define KSZSW_REGFLD_PORT_XMII_CTL1_SPEED_NOT_1000            (0x00000001UL <<  6U) /* (RW) Port Speed 1000 (inversed: 0 - 1000, 1 - 100) */
#define KSZSW_REGFLD_PORT_XMII_CTL1_RGMII_INGRESS_DELAY       (0x00000001UL <<  4U) /* (RW) RGMII Ingress Internal Delay (RGMII_ID_ig) (1 - >=1.5ns to clock) */
#define KSZSW_REGFLD_PORT_XMII_CTL1_RGMII_EGRESS_DELAY        (0x00000001UL <<  3U) /* (RW) RGMII Egress Internal Delay (RGMII_ID_eg) (1 - >=1.5ns to clock) */
#define KSZSW_REGFLD_PORT_XMII_CTL1_MODES                     (0x00000001UL <<  2U) /* (RW) MII / RMII Modes */
#define KSZSW_REGFLD_PORT_XMII_CTL1_IFACE_TYPE                (0x00000003UL <<  0U) /* (RW) Port Interface Type Select */
#define KSZSW_REGMSK_PORT_XMII_CTL1_RESERVED                  (0x80UL)

#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_MODES_MII_MAC          1 /* The MII interface operates as a MAC device (receives clocks, etc.) */
#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_MODES_MII_PHY          0 /* The MII interface operates as a PHY device (drives clocks, etc.) */
#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_MODES_RMII_REFCLK_IN   1 /* The 50 MHz RMII REFCLK is received at the RXC pin */
#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_MODES_RMII_REFCLK_OUT  0 /* A 50 MHz RMII REFCLK is generated from the RXC pin */

#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_IFACE_TYPE_RGMII       0
#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_IFACE_TYPE_RMII        1
#define KSZSW_REGFLDVAL_PORT_XMII_CTL1_IFACE_TYPE_MII         2
/************ Port MAC Control 0 Register (PORT_MAC_CTL0, 8 bit) ***************************/
#define KSZSW_REGFLD_PORT_MAC_CTL0_BCAST_STORM_PROT_EN        (0x00000001UL <<  1U) /* (RW) Broadcast Storm Protection Enable for ingress traffic */
#define KSZSW_REGMSK_PORT_MAC_CTL0_RESERVED                   (0xFDUL)
/************ Port MAC Control 1 Register (PORT_MAC_CTL1, 8 bit) ***************************/
#define KSZSW_REGFLD_PORT_MAC_CTL1_BACKPRESS_EN               (0x00000001UL <<  1U) /* (RW) Back pressure Enable */
#define KSZSW_REGFLD_PORT_MAC_CTL1_PASS_ALL                   (0x00000001UL <<  0U) /* (RW) Pass All Frames */
#define KSZSW_REGMSK_PORT_MAC_CTL1_RESERVED                   (0xF6UL)
/************ Port Ingress Rate Limit Control Register (PORT_INGR_RATELIM_CTL, 8 bit) ******/
#define KSZSW_REGFLD_PORT_INGR_RATELIM_CTL_PORT_PRIO          (0x00000001UL <<  6U) /* (RW) Port (1) or Priority (0) Based Ingress Rate Limiting */
#define KSZSW_REGFLD_PORT_INGR_RATELIM_CTL_PACKET_MODE        (0x00000001UL <<  5U) /* (RW) Ingress rate limiting is based on number of packets (1) or bits (0) */
#define KSZSW_REGFLD_PORT_INGR_RATELIM_CTL_FLOWCTL_EN         (0x00000001UL <<  4U) /* (RW) Ingress Rate Limit Flow Control Enable */
#define KSZSW_REGFLD_PORT_INGR_RATELIM_CTL_MODE               (0x00000003UL <<  2U) /* (RW) Ingress Limit Mode */
#define KSZSW_REGFLD_PORT_INGR_RATELIM_CTL_COUNT_IFG          (0x00000001UL <<  1U) /* (RW) inter-frame gap (IFG) bytes (12 per frame) are included in ingress rate limiting calculations */
#define KSZSW_REGFLD_PORT_INGR_RATELIM_CTL_COUNT_PREAMBLE     (0x00000001UL <<  0U) /* (RW) Each frame’s preamble bytes (8 per frame) are included in ingress rate limiting calculation */
#define KSZSW_REGMSK_PORT_INGR_RATELIM_CTL_RESERVED           (0x80UL)

#define KSZSW_REGFLDVAL_PORT_INGR_RATELIM_CTL_MODE_ALL        0 /* Count and limit all frames */
#define KSZSW_REGFLDVAL_PORT_INGR_RATELIM_CTL_MODE_BMUCAST    1 /* Count and limit broadcast, multicast and flooded unicast frames only */
#define KSZSW_REGFLDVAL_PORT_INGR_RATELIM_CTL_MODE_BMCAST     2 /* Count and limit broadcast and multicast frames only */
#define KSZSW_REGFLDVAL_PORT_INGR_RATELIM_CTL_MODE_BCAST      3 /* Count and limit broadcast frames only */
/************ Port Priority n Ingress Limit Control Register (PORT_INGR_RATELIM_PRIOCTL, 8 bit) **/
#define KSZSW_REGFLD_PORT_INGR_RATELIM_PRIOCTL_FRAMES         (0x0000007FUL <<  0U) /* (RW) Ingress Data Rate Limit for Priority x Frames */
#define KSZSW_REGMSK_PORT_INGR_RATELIM_PRIOCTL_RESERVED       (0x80UL)
/************  Port Queue n Egress Limit Control Register  (PORT_EGR_RATELIM_QCTL, 8 bit) ***/
#define KSZSW_REGFLD_PORT_EGR_RATELIM_QCTL_FRAMES             (0x0000007FUL <<  0U) /* (RW) Egress Data Rate Limit for Queue x Frames */
#define KSZSW_REGMSK_PORT_EGR_RATELIM_QCTL_FRAMES_RESERVED    (0x80UL)
/************ Port MIB Control and Status Register (PORT_MIB_CTL_STAT, 32 bit) **************/
#define KSZSW_REGFLD_PORT_MIB_CTL_STAT_CNTR_OV                (0x00000001UL << 31U) /* (RO) MIB Counter Overflow Indication */
#define KSZSW_REGFLD_PORT_MIB_CTL_STAT_RDEN_CNTVALID          (0x00000001UL << 25U) /* (RWSC) MIB Read Enable / Count Valid */
#define KSZSW_REGFLD_PORT_MIB_CTL_STAT_FLUSH_FREEZE           (0x00000001UL << 24U) /* (RW) Enable MIB counter flush and freeze function for this port */
#define KSZSW_REGFLD_PORT_MIB_CTL_STAT_IDX                    (0x000000FFUL << 16U) /* (RW) MIB Index */
#define KSZSW_REGFLD_PORT_MIB_CTL_STAT_CNTR_VAL_H             (0x0000000FUL <<  0U) /* (RO) MIB Counter Value [35:32] */
#define KSZSW_REGMSK_PORT_MIB_CTL_STAT_RESERVED               (0x7C00FFF0UL <<  0U)
/************ Port MIB Data Register (PORT_MIB_DATA, 32 bit) ********************************/
#define KSZSW_REGFLD_PORT_MIB_DATA_CNTR_VAL_L                 (0xFFFFFFFFUL <<  0U) /* (RO) MIB Counter Value [31:0] */
/************  Port ACL Access n Register (PORT_ACL_ACC_x, 8 bit) ***************************/
#define KSZSW_REGFLD_PORT_ACL_ACC_0_FRN                       (0x0000000FUL <<  0U) /* (RW) First Rule Number (FRN) */
#define KSZSW_REGMSK_PORT_ACL_ACC_0_RESERVED                  (0xF0UL)
#define KSZSW_REGFLD_PORT_ACL_ACC_1_MODE                      (0x00000003UL <<  4U) /* (RW) Mode */
#define KSZSW_REGFLD_PORT_ACL_ACC_1_EN                        (0x00000003UL <<  2U) /* (RW) Enable */
#define KSZSW_REGFLD_PORT_ACL_ACC_1_SRC_DEST                  (0x00000001UL <<  1U) /* (RW) Source (1) / Destination (0) */
#define KSZSW_REGFLD_PORT_ACL_ACC_1_COMP_EQ                   (0x00000001UL <<  0U) /* (RW) Match if the compared values are equal (1) / not equal (0) */
#define KSZSW_REGFLD_PORT_ACL_ACC_1_RESERVED                  (0xC0UL)
#define KSZSW_REGFLD_PORT_ACL_ACC_2_MAC_ADDR0                 (0x000000FFUL <<  0U) /* (RW) MAC Address [47:40] */
#define KSZSW_REGFLD_PORT_ACL_ACC_3_MAC_ADDR1                 (0x000000FFUL <<  0U) /* (RW) MAC Address [39:32] */
#define KSZSW_REGFLD_PORT_ACL_ACC_4_MAC_ADDR2                 (0x000000FFUL <<  0U) /* (RW) MAC Address [31:24] */
#define KSZSW_REGFLD_PORT_ACL_ACC_5_MAC_ADDR3                 (0x000000FFUL <<  0U) /* (RW) MAC Address [23:16] */
#define KSZSW_REGFLD_PORT_ACL_ACC_6_MAC_ADDR4                 (0x000000FFUL <<  0U) /* (RW) MAC Address [15:8] */
#define KSZSW_REGFLD_PORT_ACL_ACC_7_MAC_ADDR5                 (0x000000FFUL <<  0U) /* (RW) MAC Address [7:0] */
#define KSZSW_REGFLD_PORT_ACL_ACC_8_ETHTYPE_H                 (0x000000FFUL <<  0U) /* (RW) EtherType [15:8] */
#define KSZSW_REGFLD_PORT_ACL_ACC_9_ETHTYPE_L                 (0x000000FFUL <<  0U) /* (RW) EtherType [7:0] */
#define KSZSW_REGFLD_PORT_ACL_ACC_A_PRIO_MODE                 (0x00000003UL <<  6U) /* (RW) Priority Mode (PM) */
#define KSZSW_REGFLD_PORT_ACL_ACC_A_PRIO                      (0x00000007UL <<  3U) /* (RW) Priority Mode (PM) */
#define KSZSW_REGFLD_PORT_ACL_ACC_A_REMARK_PRIO_EN            (0x00000001UL <<  2U) /* (RW) Remark Priority Enable (RPE) */
#define KSZSW_REGFLD_PORT_ACL_ACC_A_REMARK_PRIO_H             (0x00000003UL <<  0U) /* (RW) Remark Priority [2:1] */
#define KSZSW_REGFLD_PORT_ACL_ACC_B_REMARK_PRIO_L             (0x00000001UL <<  7U) /* (RW) Remark Priority [0] */
#define KSZSW_REGFLD_PORT_ACL_ACC_B_MAP_MODE                  (0x00000003UL <<  5U) /* (RW)  Map Mode (MM) */
#define KSZSW_REGMSK_PORT_ACL_ACC_B_RESERVED                  (0x1FUL)
#define KSZSW_REGFLD_PORT_ACL_ACC_D_PORT_FWD_EN               (0x0000007FUL <<  0U) /* (RW) Port Forward Map */
#define KSZSW_REGMSK_PORT_ACL_ACC_D_RESERVED                  (0x80UL)
#define KSZSW_REGFLD_PORT_ACL_ACC_E_RULESET_H                 (0x000000FFUL <<  0U) /* (RW) Ruleset [15:8] */
#define KSZSW_REGFLD_PORT_ACL_ACC_F_RULESET_L                 (0x000000FFUL <<  0U) /* (RW) Ruleset [7:0] */

#define KSZSW_REGFLDVAL_PORT_ACL_ACC_1_MODE_NO_ACT            0 /* No action taken */
#define KSZSW_REGFLDVAL_PORT_ACL_ACC_1_MODE_L2_MAC_HDR        1 /* Layer 2 MAC header filtering */
#define KSZSW_REGFLDVAL_PORT_ACL_ACC_1_MODE_L3_IP_ADDR        2 /* Layer 3 IP address filtering */
#define KSZSW_REGFLDVAL_PORT_ACL_ACC_1_MODE_L4_TPORT_IPPROTO  3 /* Layer 4 TCP port number / IP protocol filtering */

/************ Port ACL Access Control 0 Register (PORT_ACL_ACC_CTL0, 8 bit) *******************/
#define KSZSW_REGFLD_PORT_ACL_ACC_CTL0_WR_STAT                (0x00000001UL <<  6U) /* (RO) Write operation is complete */
#define KSZSW_REGFLD_PORT_ACL_ACC_CTL0_RD_STAT                (0x00000001UL <<  5U) /* (RO) Read operation is complete */
#define KSZSW_REGFLD_PORT_ACL_ACC_CTL0_WR_RD                  (0x00000001UL <<  4U) /* (RW) Write (1) / Read (0) */
#define KSZSW_REGFLD_PORT_ACL_ACC_CTL0_ACL_IDX                (0x0000000FUL <<  0U) /* (RO) ACL Index */
/************ Port Mirroring Control Register (PORT_MIRRORING_CTL, 8 bit) *********************/
#define KSZSW_REGFLD_PORT_MIRRORING_CTL_RECV_SNIFF            (0x00000001UL <<  6U) /* (RW) Receive Sniff */
#define KSZSW_REGFLD_PORT_MIRRORING_CTL_TRANS_SNIFF           (0x00000001UL <<  5U) /* (RW) Transmit Sniff */
#define KSZSW_REGFLD_PORT_MIRRORING_CTL_SNIFFER_PORT          (0x00000001UL <<  1U) /* (RW) Sniffer port or normal operation */
#define KSZSW_REGMSK_PORT_MIRRORING_CTL_RESERVED              (0x9DUL)
/************ Port Priority Control Register (PORT_PRIO_CTL, 8 bit) ***************************/
#define KSZSW_REGFLD_PORT_PRIO_CTL_HIGHEST                    (0x00000001UL <<  7U) /* (RW) Highest priority selected */
#define KSZSW_REGFLD_PORT_PRIO_CTL_OR                         (0x00000001UL <<  6U) /* (RW) OR’ed Priority */
#define KSZSW_REGFLD_PORT_PRIO_CTL_MAC_ADDR                   (0x00000001UL <<  4U) /* (RW) Enable MAC Address Priority Classification for ingress packets on port */
#define KSZSW_REGFLD_PORT_PRIO_CTL_VLAN                       (0x00000001UL <<  3U) /* (RW) Enable VLAN priority classification for ingress packets on port */
#define KSZSW_REGFLD_PORT_PRIO_CTL_802_1P                     (0x00000001UL <<  2U) /* (RW) Enable 802.1p priority classification for ingress packets on port */
#define KSZSW_REGFLD_PORT_PRIO_CTL_DIFFSERV                   (0x00000001UL <<  1U) /* (RW) Enable Diffserv priority classification for ingress packets on port */
#define KSZSW_REGFLD_PORT_PRIO_CTL_ACL                        (0x00000001UL <<  0U) /* (RW) Enable ACL priority classification for ingress packets on port */
#define KSZSW_REGMSK_PORT_PRIO_CTL_RESERVED                   (0x20UL)
/************ Port Ingress MAC Control Register (PORT_INGR_MAC_CTL, 8 bit) ********************/
#define KSZSW_REGFLD_PORT_INGR_MAC_CTL_USER_PRIO_CEIL         (0x00000001UL <<  7U)  /* (RW) User Priority Ceiling (<= priority in default Tag) */
#define KSZSW_REGFLD_PORT_INGR_MAC_CTL_DISC_UNTAGGED          (0x00000001UL <<  4U)  /* (RW) Discard Untagged Packets */
#define KSZSW_REGFLD_PORT_INGR_MAC_CTL_DISC_TAGGED            (0x00000001UL <<  3U)  /* (RW) Discard Tagged Packets */
#define KSZSW_REGFLD_PORT_INGR_MAC_CTL_DEFAULT_PRIO           (0x00000007UL <<  0U)  /* (RW) Port Default Priority Classification */
#define KSZSW_REGMSK_PORT_INGR_MAC_CTL_RESERVED               (0x60UL)
/************ Port Authentication Control Register (PORT_AUTH_CTL, 8 bit) *********************/
#define KSZSW_REGFLD_PORT_AUTH_CTL_ACL_EN                     (0x00000001UL <<  2U)  /* (RW) Access Control List (ACL) Enable */
#define KSZSW_REGFLD_PORT_AUTH_CTL_MODE                       (0x00000003UL <<  0U)  /* (RW) Authentication Mode */
#define KSZSW_REGMSK_PORT_AUTH_CTL_RESERVED                   (0xF8UL)

#define KSZSW_REGFLD_PORT_AUTH_CTL_MODE_BLOCK                 1 /* Block Mode (drop all missed in ACL ) */
#define KSZSW_REGFLD_PORT_AUTH_CTL_MODE_PASS                  2 /* Pass Mode (forward all missed in ACL ) */
#define KSZSW_REGFLD_PORT_AUTH_CTL_MODE_TRAP                  3 /* Trap Mode (forward to host all missed in ACL ) */
/************ Port Pointer Register (PORT_PTR, 32 bit) ****************************************/
#define KSZSW_REGFLD_PORT_PTR_PORT_IDX                        (0x00000007UL << 16U) /* (RW) Port Index */
#define KSZSW_REGFLD_PORT_PTR_QPTR                            (0x00000003UL <<  0U) /* (RW) Queue Pointer */
#define KSZSW_REGMSK_PORT_PTR_RESERVED                        (0xFFF8FFFCUL)
/************ Port Priority to Queue Mapping Register (PORT_PRIO_QMAP, 32 bit) ****************/
#define KSZSW_REGFLD_PORT_PRIO_QMAP_REGVAL(p)                 (0x00000003UL <<  (4*p)) /* (RW) Regenerated priority (queue) value for  priority p */
/************ Port Police Control Register (PORT_POL_CTL, 32 bit)  ****************************/
#define KSZSW_REGFLD_PORT_POL_CTL_DROPPED_COL                 (0x00000001UL << 11U) /* (RW) Color packet PMON holds dropped packets of that color */
#define KSZSW_REGFLD_PORT_POL_CTL_DROP_ALL                    (0x00000001UL << 10U) /* (RW) All packets are dropped while max threshold is exceeded in PM WRED */
#define KSZSW_REGFLD_PORT_POL_CTL_PKT_TYPE                    (0x00000003UL <<  8U) /* (RW) PMON packet type to be read for the connection from Queue Pointer of the port pointed by Port Index */
#define KSZSW_REGFLD_PORT_POL_CTL_PORT_BASED_POL              (0x00000001UL <<  7U) /* (RW) Port Based Policing (per-port per-queue - 1, per-queue only - 0) */
#define KSZSW_REGFLD_PORT_POL_CTL_NONDSCP_COL                 (0x00000003UL <<  5U) /* (RW) Color of non-IP frame for color aware */
#define KSZSW_REGFLD_PORT_POL_CTL_COL_MARK_EN                 (0x00000001UL <<  4U) /* (RW) DSCP color mark enable */
#define KSZSW_REGFLD_PORT_POL_CTL_COL_REMAP_EN                (0x00000001UL <<  3U) /* (RW) DSCP color remap enable for color aware */
#define KSZSW_REGFLD_PORT_POL_CTL_DROP_SRP                    (0x00000001UL <<  2U) /* (RW) Allow drop SRP packets while WRED is enabled */
#define KSZSW_REGFLD_PORT_POL_CTL_POL_MODE                    (0x00000001UL <<  1U) /* (RW) Policing modes for the queue if policing is enabled (color blind = 1, color aware = 0) */
#define KSZSW_REGFLD_PORT_POL_CTL_POL_EN                      (0x00000001UL <<  0U) /* (RW) Enable policing and WRED */
#define KSZSW_REGMSK_PORT_POL_CTL_RESERVED                    (0xFFFFF000UL)

#define KSZSW_REGFLDVAL_PORT_POL_CTL_PKT_TYPE_RED             3 /* WRED_PMON holds the number of RED packets while read */
#define KSZSW_REGFLDVAL_PORT_POL_CTL_PKT_TYPE_YELLOW          2 /* WRED_PMON holds the number of YELLOW packets while read */
#define KSZSW_REGFLDVAL_PORT_POL_CTL_PKT_TYPE_GREEN           1 /* WRED_PMON holds the number of GREEN packets while read */
#define KSZSW_REGFLDVAL_PORT_POL_CTL_PKT_TYPE_DROPPED         0 /* WRED_PMON holds the number of dropped packets while read */

/************ Port Police Queue Rate Register (PORT_POL_QRATE, 32 bit) ************************/
#define KSZSW_REGFLD_PORT_POL_QRATE_COMMIT_INFO_RATE          (0x0000FFFFUL << 16U) /* (RW) Committed Information Rate */
#define KSZSW_REGFLD_PORT_POL_QRATE_PEAK_INFO_RATE            (0x0000FFFFUL <<  0U) /* (RW) Peak Information Rate */
/************ Port Police Queue Burst Size Register (PORT_POL_QBURST_SIZE, 32 bit)  ***********/
#define KSZSW_REGFLD_PORT_POL_QBURST_SIZE_COMMIT_SIZE         (0x0000FFFFUL << 16U) /* (RW) Committed Burst Size */
#define KSZSW_REGFLD_PORT_POL_QBURST_SIZE_PEAK_SIZE           (0x0000FFFFUL <<  0U) /* (RW) Peak Burst Size */
/************ Port WRED Packet Memory Control Register (PORT_WRED_PKT_MEM_CTL0/1, 32 bit) *****/
#define KSZSW_REGFLD_PORT_WRED_PKT_MEM_CTL0_MAX_THRESH        (0x000007FFUL << 16U) /* (RW) WRED Packet Memory Maximum Threshold */
#define KSZSW_REGFLD_PORT_WRED_PKT_MEM_CTL0_MIN_THRESH        (0x000007FFUL <<  0U) /* (RW) WRED Packet Memory Minimum Threshold */
#define KSZSW_REGFLD_PORT_WRED_PKT_MEM_CTL1_PROB_MULT         (0x000007FFUL << 16U) /* (RW) WRED Packet Memory Probability Multiplier */
#define KSZSW_REGFLD_PORT_WRED_PKT_MEM_CTL1_AVG_QSIZE         (0x000007FFUL <<  0U) /* (RO) WRED Packet Memory Average Queue Size */
/************ Port WRED Queue Control Register (PORT_WRED_QCTL0/1, 32 bit) ********************/
#define KSZSW_REGFLD_PORT_WRED_QCTL0_MAX_THRESH               (0x000007FFUL << 16U) /* (RW) WRED Maximum Queue Threshold */
#define KSZSW_REGFLD_PORT_WRED_QCTL0_MIN_THRESH               (0x000007FFUL <<  0U) /* (RW) WRED Minimum Queue Threshold */
#define KSZSW_REGFLD_PORT_WRED_QCTL1_PROB_MULT                (0x000007FFUL << 16U) /* (RW) WRED Queue Probability Multiplier */
#define KSZSW_REGFLD_PORT_WRED_QCTL1_AVG_QSIZE                (0x000007FFUL <<  0U) /* (RO) WRED Average Queue Size */
/************ Port WRED Queue Performance Monitor Control Register (PORT_WRED_QPERF_MON_CTL, 32 bit **/
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_RND_DROP_EN      (0x00000001UL << 31U) /* (RW) Random Drop Enable */
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_PMON_FLUSH       (0x00000001UL << 30U) /* (RW) PMON Counters Flush */
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_GYR_DROP_DIS     (0x00000001UL << 29U) /* (RW) Drop GREEN/YELLOW/RED is disabled */
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_YR_DROP_DIS      (0x00000001UL << 28U) /* (RW) Drop YELLOW/RED is disabled */
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_R_DROP_DIS       (0x00000001UL << 27U) /* (RW) Drop RED is disabled */
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_DROP_ALL         (0x00000001UL << 26U) /* (RW) Drop all packets wile the max threshold is exceeded */
#define KSZSW_REGFLD_PORT_WRED_QPERF_MON_CTL_PKT_EVT_CNTR     (0x00FFFFFFUL <<  0U) /* (RO) Packet Event Counter */
#define KSZSW_REGMSK_PORT_WRED_QPERF_MON_CTL_RESERVED         (0x03000000UL)
/************ Port Transmit Queue Index Register (PORT_TXQ_IDX, 32 bit) ***********************/
#define KSZSW_REGFLD_PORT_TXQ_IDX_VAL                         (0x00000003UL <<  0U) /* (RW) Queue Index (Points to the queue number for subsequent queue configuration registers) */
#define KSZSW_REGMSK_PORT_TXQ_IDX_RESERVED                    (0xFFFFFFFCUL)
/************ Port Transmit Queue PVID Register (PORT_TXQ_PVID, 32 bit) ***********************/
#define KSZSW_REGFLD_PORT_TXQ_PVID_REPL_EN                    (0x00000001UL <<  0U) /* Port VID Replacement Enable */
#define KSZSW_REGMSK_PORT_TXQ_PVID_RESERVED                   (0xFFFFFFFEUL)
/************ Port Transmit Queue Control 0 Register (PORT_TXQ_CTL0, 8 bit) *******************/
#define KSZSW_REGFLD_PORT_TXQ_CTL0_SHED_MODE                  (0x00000003UL <<  6U) /* (RW) Scheduler Mode */
#define KSZSW_REGFLD_PORT_TXQ_CTL0_SHAPER_MODE                (0x00000003UL <<  4U) /* (RW) Shaper Mode */
#define KSZSW_REGMSK_PORT_TXQ_CTL0_RESERVED                   (0x0FUL)

#define KSZSW_REGFLDVAL_PORT_TXQ_CTL0_SHED_MODE_STRICT        0 /* Strict Priority */
#define KSZSW_REGFLDVAL_PORT_TXQ_CTL0_SHED_MODE_WRR           2 /* Weighted Round Robin (WRR) */
#define KSZSW_REGFLDVAL_PORT_TXQ_CTL0_SHAPER_MODE_NONE        0 /* No shaping */
#define KSZSW_REGFLDVAL_PORT_TXQ_CTL0_SHAPER_MODE_CREDIT      1 /* Credit based shaper (CBS) as defined in IEEE 802.1Qav for AVB */
#define KSZSW_REGFLDVAL_PORT_TXQ_CTL0_SHAPER_MODE_TIME_AWARE  2 /* Time aware shaper (TAS) per IEEE 802.1Qbv for TSN */
/************ Port Transmit Queue Control 1 Register (PORT_TXQ_CTL1, 8 bit) *******************/
#define KSZSW_REGFLD_PORT_TXQ_CTL1_WRR_QWEIGHT                (0x0000007FUL <<  0U) /* (RW) Queue Weight for WRR Scheduling */
#define KSZSW_REGMSK_PORT_TXQ_CTL1_RESERVED                   (0x80UL)
/************ Port Transmit Credit Shaper Control 0 Register (PORT_TXQ_CREDIT_SHAPER_CTL0, 16 bit) **/
#define KSZSW_REGFLD_PORT_TXQ_CREDIT_SHAPER_CTL0_HIGH_WMARK   (0x0000FFFFUL <<  0U) /* (RW) Port Queue Credit High Water Mark */
/************ Port Transmit Credit Shaper Control 1 Register (PORT_TXQ_CREDIT_SHAPER_CTL1, 16 bit) **/
#define KSZSW_REGFLD_PORT_TXQ_CREDIT_SHAPER_CTL1_LOW_WMARK    (0x0000FFFFUL <<  0U) /* (RW) Port Queue Credit High Water Mark */
/************ Port Transmit Credit Shaper Control 2 Register (PORT_TXQ_CREDIT_SHAPER_CTL2, 16 bit) **/
#define KSZSW_REGFLD_PORT_TXQ_CREDIT_SHAPER_CTL2_CRED_INC     (0x0000FFFFUL <<  0U) /* (RW) Port Queue Credit Increment */
/************ Port Time Aware Shaper Control Register (PORT_TXQ_TAS_CTL, 8 bit) ***************/
#define KSZSW_REGFLD_PORT_TXQ_TAS_CTL_CUT_THROUGH_EN          (0x00000001UL <<  7U) /* (RW) Cut-Through Enable */
#define KSZSW_REGFLD_PORT_TXQ_TAS_CTL_RESTR_TAS_PKT           (0x00000001UL <<  6U) /* (RW) Restricted TAS: TAS packets will not be allowed to transmit out until the OPEN period */
#define KSZSW_REGFLD_PORT_TXQ_TAS_CTL_T0_REF                  (0x00000003UL <<  0U) /* (RW) Reference Time Select */
#define KSZSW_REGMSK_PORT_TXQ_TAS_CTL_RESERVED                (0x3CUL)

#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_CTL_T0_REF_REFTIME       0 /* Start t0 when reference time is crossed */
#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_CTL_T0_REF_PPS           1 /* Repeat t0 on PTP pps (pulse per second) */
#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_CTL_T0_REF_FREERUN       2 /* Free-running, repeats t0 based on internal 1 second pulse */
#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_CTL_T0_REF_NOREF         3 /* No reference */
/************* Port Time Aware Shaper Event Index Register (PORT_TXQ_TAS_EVT_IDX, 8 bit) ******/
#define KSZSW_REGFLD_PORT_TXQ_TAS_EVT_IDX_VAL                 (0x0000007FUL <<  0U) /* (RW) Event Index */
#define KSZSW_REGMSK_PORT_TXQ_TAS_EVT_IDX_RESERVED            (0x80UL)
/************ Port Time Aware Shaper Event Register (PORT_TXQ_TAS_EVT, 32 bit) ****************/
#define KSZSW_REGFLD_PORT_TXQ_TAS_EVT_CODE                    (0x00000007UL << 29U) /* (RW) Event code */
#define KSZSW_REGFLD_PORT_TXQ_TAS_EVT_TIME                    (0x1FFFFFFFUL <<  0U) /* (RW) Cycle count of system clock */

#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_EVT_CODE_REPEAT          7 /* Repeat event */
#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_EVT_CODE_SHED_OPEN       2 /* Scheduled open event */
#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_EVT_CODE_BAND_START      1 /* Guard band start event */
#define KSZSW_REGFLDVAL_PORT_TXQ_TAS_EVT_CODE_SHED_CLOSE      0 /* Scheduled closed event */
/************ Port Control 0 Register (PORT_CTL0, 32 bit) *************************************/
#define KSZSW_REGFLD_PORT_CTL0_DROP_MODE                      (0x00000003UL <<  0U) /* (RW) Drop Mode */
#define KSZSW_REGMSK_PORT_CTL0_RESERVED                       (0xFFFFFFFCUL)

#define KSZSW_REGFLDVAL_PORT_CTL0_DROP_MODE_NODROP            0 /* No drop, flow control enable */
#define KSZSW_REGFLDVAL_PORT_CTL0_DROP_MODE_PRIO0             1 /* Drop packets with priority 0 */
#define KSZSW_REGFLDVAL_PORT_CTL0_DROP_MODE_PRIO0_1           2 /* Drop packets with priority 0, 1 */
#define KSZSW_REGFLDVAL_PORT_CTL0_DROP_MODE_PRIO0_1_2         3 /* Drop packets with priority 0, 1, 2 */
/************ Port Control 1 Register (PORT_CTL1, 32 bit) *************************************/
#define KSZSW_REGFLD_PORT_CTL1_FWD_PORTS                      (0x0000007FUL <<  0U) /* (RW) Mask of enabled port to forward if KSZSW_REGFLD_QMGMT_CTL_RESTRICT_FWD on */
#define KSZSW_REGMSK_PORT_CTL1_RESERVED                       (0xFFFFFF80UL)
/************ Port Transmit Queue Memory Register(PORT_TXQ_MEM, 32 bit) ***********************/
#define KSZSW_REGFLD_PORT_TXQ_MEM_BU                          (0x000007FFUL <<  0U) /* (RO) Transmit Queue Blocks Used */
#define KSZSW_REGMSK_PORT_TXQ_MEM_RESERVED                    (0xFFFFF800UL)

/************ Port Control 2 Register (PORT_CTL2, 8 bit) **************************************/
#define KSZSW_REGFLD_PORT_CTL2_NULL_VID_LOOKUP_EN             (0x00000001UL <<  7U) /* (RW) Null VID Lookup Enable (1 - use null VID, 0 - default VID) */
#define KSZSW_REGFLD_PORT_CTL2_INGR_VLAN_FLT                  (0x00000001UL <<  6U) /* (RW) Ingress VLAN Filtering */
#define KSZSW_REGFLD_PORT_CTL2_DISC_NON_PVID                  (0x00000001UL <<  5U) /* (RW) Discard Non-PVID Packet (discard packet with VID != port default VID) */
#define KSZSW_REGFLD_PORT_CTL2_802_1X_AUTH_EN                 (0x00000001UL <<  4U) /* (RW) Enable MAC based 802.1X authentication in lookup engine */
#define KSZSW_REGFLD_PORT_CTL2_SELF_ADDR_FLT                  (0x00000001UL <<  3U) /* (RW) Self-Address Filtering */
#define KSZSW_REGMSK_PORT_CTL2_RESERVED                       (0x07UL)
/************ Port MSTP Pointer Register (PORT_MSTP_PTR, 8 bit) *******************************/
#define KSZSW_REGFLD_PORT_MSTP_PTR_VAL                        (0x00000007UL <<  0U) /* (RW) Points to one of the 8 MSTPs */
#define KSZSW_REGMSK_PORT_MSTP_PTR_RESERVED                   (0xF8UL)
/************ Port MSTP State Register (PORT_MSTP_STATE, 8 bit) *******************************/
#define KSZSW_REGFLD_PORT_MSTP_STATE_TX_EN                    (0x00000001UL <<  2U) /* (RW) Port Transmit Enable */
#define KSZSW_REGFLD_PORT_MSTP_STATE_RX_EN                    (0x00000001UL <<  1U) /* (RW) Port Receive Enable */
#define KSZSW_REGFLD_PORT_MSTP_STATE_LEARN_DIS                (0x00000001UL <<  0U) /* (RW) Port Learning Disable */
#define KSZSW_REGMSK_PORT_MSTP_STATE_RESERVED                 (0xF8UL)
/************ Port PTP Receive Latency Register (PORT_PTP_RX_LATENCY, 16 bit) *****************/
#define KSZSW_REGFLD_PORT_PTP_RX_LATENCY_NS                   (0x0000FFFFUL <<  0U) /* (RW) PTP Port N RX Latency in Nanoseconds */
/************ Port PTP Transmit Latency Register (PORT_PTP_TX_LATENCY, 16 bit) ****************/
#define KSZSW_REGFLD_PORT_PTP_TX_LATENCY_NS                   (0x0000FFFFUL <<  0U) /* (RW) PTP Port N TX Latency in Nanoseconds */
/************ Port PTP Asymmetry Correction Register (PORT_PTP_ASYM_COR, 16 bit) *******************************************/
#define KSZSW_REGGLD_PORT_PTP_ASYM_COR_SIGN                   (0x00000001UL << 15U) /* (RW) PTP Port N Asymmetry Correction Sign Bit */
#define KSZSW_REGGLD_PORT_PTP_ASYM_COR_NS                     (0x00007FFFUL <<  0U) /* (RW) PTP Port N Asymmetry Correction in Nanoseconds */
/************ Port PTP Timestamp Interrupt Status/Enable Register (PORT_PTP_INT_STAT / PORT_PTP_INT_EN, 16 bit) **/
#define KSZSW_REGFLD_PORT_PTP_INT_SYNC_TSTMP                  (0x00000001UL << 15U) /* (RW1C / RW) Port N Egress Timestamp for Sync Frame Interrupt */
#define KSZSW_REGFLD_PORT_PTP_INT_DELREQ_TSTMP                (0x00000001UL << 14U) /* (RW1C / RW) Port N Egress Timestamp for Pdelay_Req and Delay_Req Frames Interrupt */
#define KSZSW_REGFLD_PORT_PTP_INT_DELRESP_TSTMP               (0x00000001UL << 13U) /* (RW1C / RW) Port N Egress Timestamp for Pdelay_Resp Frame Interrupt */
#define KSZSW_REGMSK_PORT_PTP_INT_RESERVED                    (0x1FFFUL)



/************ MIB Counter Indexes *********************************************/
#define KSZSW_MIB_IDX_RX_HIPRI_BYTE_CNT                       0x00  /* RX high priority octet count, including bad packets. */
#define KSZSW_MIB_IDX_RX_UNDRSIZE_PKT_CNT                     0x01  /* RX undersize packets with good CRC. */
#define KSZSW_MIB_IDX_RX_BAD_FRAGMET_CNT                      0x02  /* RX fragment packets with bad CRC, symbol errors or alignment errors. */
#define KSZSW_MIB_IDX_RX_OVRSIZE_PKT_CNT                      0x03  /* RX oversize packets w/ good CRC (max: 1536 or 1522 bytes). */
#define KSZSW_MIB_IDX_RX_JABBER_CNT                           0x04  /* RX packets longer than 1522 bytes with either CRC errors, alignment errors or symbol errors (depends on max packet size setting); or RX packets longer than 1916 bytes only.. */
#define KSZSW_MIB_IDX_RX_SYMBOL_ERR_CNT                       0x05  /* RX packets with invalid data symbol; and legal preamble and packet size. */
#define KSZSW_MIB_IDX_RX_CRC_ERR_CNT                          0x06  /* RX packets between 64 and 1522 bytes in size, with an integral number of bytes and a bad CRC. */
#define KSZSW_MIB_IDX_RX_ALIGNMNT_ERR_CNT                     0x07  /* RX packets between 64 and 1522 bytes in size, with a non-integral number of bytes and a bad CRC */
#define KSZSW_MIB_IDX_RX_CTL_8808_PKT_CNT                     0x08  /* MAC control frames received with 0x8808 in the EtherType field. */
#define KSZSW_MIB_IDX_RX_PUASE_PKT_CNT                        0x09  /* PAUSE frames received. PAUSE is defined as EtherType (0x8808), DA, control opcode (0x0001), minimum 64 byte data length, and a valid CRC */
#define KSZSW_MIB_IDX_RX_BCAST_PKT_CNT                        0x0A  /* RX good broadcast packets. Does not include erred broadcast packets or valid multicast packets. */
#define KSZSW_MIB_IDX_RX_MCAST_PKT_CNT                        0x0B  /* RX good multicast packets. Does not include MAC control frames, erred multicast packets, or valid broadcast packets. */
#define KSZSW_MIB_IDX_RX_UCAST_PKT_CNT                        0x0C  /* RX good unicast packets. */
#define KSZSW_MIB_IDX_RX_64_SIZE_PKT_CNT                      0x0D  /* RX packets (bad packets included) that are 64 bytes in length. */
#define KSZSW_MIB_IDX_RX_65_127_SIZE_PKT_CNT                  0x0E  /* RX packets (bad packets included) that are 65 to 127 bytes in length. */
#define KSZSW_MIB_IDX_RX_128_255_SIZE_PKT_CNT                 0x0F  /* RX packets (bad packets included) that are 128 to 255 bytes in length. */
#define KSZSW_MIB_IDX_RX_256_511_SIZE_PKT_CNT                 0x10  /* RX packets (bad packets included) that are 256 to 511 bytes in length. */
#define KSZSW_MIB_IDX_RX_512_1023_SIZE_PKT_CNT                0x11  /* RX packets (bad packets included) that are 512 to 1023 bytes in length. */
#define KSZSW_MIB_IDX_RX_1024_1522_SIZE_PKT_CNT               0x12  /* RX packets (bad packets included) that are 1024 to 1522 bytes in length. */
#define KSZSW_MIB_IDX_RX_1523_2000_SIZE_PKT_CNT               0x13  /* RX packets (bad packets included) that are 1523 to 2000 bytes in length. */
#define KSZSW_MIB_IDX_RX_2001_SIZE_PKT_CNT                    0x14  /* RX packets (bad packets included) that are between 2001 bytes and the upper limit in length. */
#define KSZSW_MIB_IDX_TX_HIPRI_BYTE_CNT                       0x15  /* TX high priority good octet count, including PAUSE packets. */
#define KSZSW_MIB_IDX_TX_LATE_COL_CNT                         0x16  /* Collision is detected later than 512 bit times into the transmission of packet. */
#define KSZSW_MIB_IDX_TX_PAUSE_PKT_CNT                        0x17  /* PAUSE frames transmitted. PAUSE is EtherType (0x8808), DA, control opcode (0x0001), minimum 64 byte data length, and a valid CRC. */
#define KSZSW_MIB_IDX_TX_BCAST_PKT_CNT                        0x18  /* TX good broadcast packets. Does not include erred broadcast packets or valid multicast packets. */
#define KSZSW_MIB_IDX_TX_MCAST_PKT_CNT                        0x19  /* TX good multicast packets. Does not include MAC control frames, erred multicast packets, or valid broadcast packets.*/
#define KSZSW_MIB_IDX_TX_UCAST_PKT_CNT                        0x1A  /* TX good unicast packets.*/
#define KSZSW_MIB_IDX_TX_DEFFERED_PKT_CNT                     0x1B  /* TX packets where the first transmit attempt is delayed due to the busy medium. */
#define KSZSW_MIB_IDX_TX_TOTAL_COL_CNT                        0x1C  /* TX total collisions. Half duplex only. */
#define KSZSW_MIB_IDX_TX_EXCESSIVE_COL_CNT                    0x1D  /* TX fails due to excessive collisions. */
#define KSZSW_MIB_IDX_TX_SINGLE_COL_CNT                       0x1E  /* Successfully transmitted frames where transmission is inhibited by exactly one collision. */
#define KSZSW_MIB_IDX_TX_MULTIPLE_COL_CNT                     0x1F  /* Successfully transmitted frames where transmission is inhibited by more than one collision. */
#define KSZSW_MIB_IDX_RX_BYTE_CNT                             0x80  /* RX byte count. */
#define KSZSW_MIB_IDX_TX_BYTE_CNT                             0x81  /* TX byte count. */
#define KSZSW_MIB_IDX_RX_DROP_PKT_CNT                         0x82  /* RX packets dropped due to lack of resources. */
#define KSZSW_MIB_IDX_TX_DROP_PKT_CNT                         0x83  /* TX packets dropped due to lack of resources. */


/************ Tail Tag Word Format ********************************************/
#define KSZSW_TT_RXWRD_FLD_PORT              (0x7UL << 0) /* Receive port */
#define KSZSW_TT_RXWRD_FLD_PTP               (0x1UL << 7) /* PTP Timestamp present */

#define KSZSW_TT_TXWRD_FLD_LOOKUP            (0x01UL << 10) /* Lookup - 1 - standard switch forward, 0 - forward from TT[6..0] */
#define KSZSW_TT_TXWRD_FLD_PORTBL            (0x01UL <<  9) /* Port Blocking Override */
#define KSZSW_TT_TXWRD_FLD_PRIO              (0x03UL <<  7) /* Wgress priority */
#define KSZSW_TT_TXWRD_FLD_FWD_PORT(n)       (0x01UL << (n)) /* Forward to port n */
#define KSZSW_TT_TXWRD_FLD_FWD_PORTS         (0x7FUL <<  0)  /* Forward ports mask */



#endif // KSZSW_REGS_H

