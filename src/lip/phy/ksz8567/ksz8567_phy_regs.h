#ifndef KSZ8567_MDIO_REGS_H
#define KSZ8567_MDIO_REGS_H

#include "../lip_phy_mdio_regs.h"

#define KSZ8567_PHY_ID_VENDOR_H         0x0022
#define KSZ8567_PHY_ID_VENDOR_L         0x0005
#define KSZ8567_PHY_ID_VENDOR           LIP_PHY_VENDOR_ID(KSZ8567_PHY_ID_VENDOR_H, KSZ8567_PHY_ID_VENDOR_L)
#define KSZ8567_PHY_ID_MODEL            0x0023
#define KSZ8567_PHY_ID_REV              1


#define KSZ8567_PHY_REGADDR_REM_LOOP                    0x11 /* Remote Loopback Register */
#define KSZ8567_PHY_REGADDR_PCS_STAT                    0x13 /* Digital PMA/PCS Status Register */
#define KSZ8567_PHY_REGADDR_RXERR_CNT                   0x15 /* Receive Error Counter Register */
#define KSZ8567_PHY_REGADDR_INT_CTL_STAT                0x1B /* Interrupt Control/Status Register */
#define KSZ8567_PHY_REGADDR_AUTO_MDIX                   0x1C /* Auto MDI/MDI-X Register */
#define KSZ8567_PHY_REGADDR_SPEC                        0x1E /* Port Special Register */
#define KSZ8567_PHY_REGADDR_CTL                         0x1F /* Control Register */

#define KSZ8567_PHY_REGADDR_SQI                         LIP_PHY_REGADDR_MMD(1, 0xAC) /* Signal Quality Register */
#define KSZ8567_PHY_REGADDR_LED_MODE                    LIP_PHY_REGADDR_MMD(2, 0x00) /* LED Mode Register */
#define KSZ8567_PHY_REGADDR_EEE_ADV                     LIP_PHY_REGADDR_MMD(7, 0x3C) /* EEE Advertisement Register */
#define KSZ8567_PHY_REGADDR_QUIET_WIRE_CFG(n)           LIP_PHY_REGADDR_MMD(0x1C, 0x25 + (n)) /* Quiet-WIRE Configuration n (n = 0..15) */

#define KSZ8567_PHY_REGCNT_QUIET_WIRE_CFG               16



/****** Remote Loopback Register (Register 17, REM_LOOP) **********************/
#define KSZ8567_PHY_REGFLD_REM_LOOP_EN                  (0x0001UL <<  8U) /* (RW)    Enable remote loopback */


/****** Digital PMA/PCS Status Register (Register 19, PCS_STAT) ***************/
#define KSZ8567_PHY_REGFLD_PCS_STAT_100BT_OK            (0x0001UL <<  0U) /* (RO)    100BT link status OK */
#define KSZ8567_PHY_REGMSK_PCS_STAT_RESERVED            (0xFFFEUL)

/****** Receive Error Counter Register (Register 21, RXERR_CNT) ***************/
#define KSZ8567_PHY_REGFLD_RXERR_CNT_VAL                (0xFFFFUL <<  0U) /* (RC)    RX Error counter for the RX_ER signal */

/****** Interrupt Control/Status Register  (Register 27, INT_CTL_STAT) ********/
#define KSZ8567_PHY_REGFLD_INT_CTL_STAT_IE(i)           (0x0001UL << (8U + (i))) /* (RW) Interrupt (i) Enable */
#define KSZ8567_PHY_REGFLD_INT_CTL_STAT_INT(i)          (0x0001UL << (0U + (i))) /* (RC) Interrupt (i) Flag */

#define KSZ8567_PHY_INTNUM_JABBER                       7 /* Jabber Interrupt */
#define KSZ8567_PHY_INTNUM_RXERR                        6 /* Receive error Interrupt */
#define KSZ8567_PHY_INTNUM_AN_PG_RCVD                   5 /* Page receive interrupt */
#define KSZ8567_PHY_INTNUM_PARA_DET_FLT                 4 /* Parallel Detect Fault Interrupt */
#define KSZ8567_PHY_INTNUM_AN_LP_ACK                    3 /* Link Partner Acknowledge Interrupt */
#define KSZ8567_PHY_INTNUM_LINK_DOWN                    2 /* Link Down Interrupt */
#define KSZ8567_PHY_INTNUM_REM_FLT                      1 /* Remote Fault Interrupt */
#define KSZ8567_PHY_INTNUM_LINK_UP                      0 /* Link Up Interrupt */

/****** Auto MDI/MDI-X Register  (Register 28, AUTO_MDIX) *********************/
#define KSZ8567_PHY_REGFLD_AUTO_MDIX_MAN_CFG            (0x0001UL <<  7U) /* (RW)    MDI/MDI-X Mode Manual Config Value  (0 - MDI-X, 1 - MDI) */
#define KSZ8567_PHY_REGFLD_AUTO_MDIX_MAN_EN             (0x0001UL <<  6U) /* (RW)    MDI/MDI-X Mode Manual Config Enable (diable Auto-MDI/MDI-X) */
#define KSZ8567_PHY_REGMSK_AUTO_MDIX_RESERVED           (0xFF3FUL)

/****** Port Special Register (Register 30, SPEC) *****************************/
#define KSZ8567_PHY_REGFLD_SPEC_SINGLE_LED_WA           (0x0001UL <<  9U) /* (RW)    Single-LED Mode Workaround Bit */
#define KSZ8567_PHY_REGMSK_SPEC_RESERVED                (0xFDFFUL)

/****** Control Register (Register 31, CTL) ***********************************/
#define KSZ8567_PHY_REGFLD_CTL_JBR_EN                   (0x0001UL <<  9U) /* (RW)    Enable jabber counter */
#define KSZ8567_PHY_REGFLD_CTL_STAT_SPEED_100_TX        (0x0001UL <<  5U) /* (RO)    Speed status 100BASE-TX */
#define KSZ8567_PHY_REGFLD_CTL_STAT_SPEED_10_T          (0x0001UL <<  4U) /* (RO)    Speed status 10BASE-T */
#define KSZ8567_PHY_REGFLD_CTL_STAT_DUPLEX              (0x0001UL <<  3U) /* (RO)    Duplex status */
#define KSZ8567_PHY_REGMSK_RESERVED                     (0xFDC7UL)



/******* MMD SIGNAL_QUALITY ***************************************************/
#define KSZ8567_PHY_REGFLD_SQI_VAL                      (0x007FUL <<  8U) /* (RO)    SQI indicates relative quality of the signal. A lower value indicates better signal quality. */
#define KSZ8567_PHY_REGMSK_SQI_RESERVED                 (0x80FFUL)
/******* MMD LED_MODE *********************************************************/
#define KSZ8567_PHY_REGFLD_LED_MODE                     (0x0001UL <<  4U) /* (RW)    LED Mode */
#define KSZ8567_PHY_REGMSK_LED_MODE_RESERVED            (0xFFEFUL)

#define KSZ8567_PHY_REGFLDVAL_LED_MODE_SINGLE           1 /* Single-LED Mode */
#define KSZ8567_PHY_REGFLDVAL_LED_MODE_TRICOLOR         0 /* Tri-color Dual-LED Mode */
/****** MMD EEE_ADVERT ********************************************************/
#define KSZ8567_PHY_REGFLD_EEE_ADV_100_T_EN             (0x0001UL <<  1U) /* (RW) 100BASE-T EEE Enable */
#define KSZ8567_PHY_REGMSK_EEE_ADV_RESERVED             (0xFFFDUL)

#endif // KSZ8567_MDIO_REGS_H
