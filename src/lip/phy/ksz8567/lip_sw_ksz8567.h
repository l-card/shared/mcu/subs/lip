#ifndef LIP_SW_KSZ8567_H
#define LIP_SW_KSZ8567_H

#include "lip/lip_defs.h"
#include "lip_config.h"
#include "kszsw_reg_acc.h"
#include "lip/ll/lip_ll.h"
#include "lip/app/ptp/lip_ptp_cfg_defs.h"
#include "lip/link/lip_port_link.h"


#define KSZ8567_DEF_EGRESS_LATENCY  45
#define KSZ8567_DEF_INGRESS_LATENCY 415

t_lip_errs lip_sw_ksz8567_init(t_kszsw_reg_acc *regs_acc);
t_lip_errs lip_sw_ksz8567_pull(void);

/* Разрешение обработки tail-tag при приеме и добавления при передаче.
 * Должно соответсвовать настройкам tail-tag (для соответствующего порта) в регистрах свича */
t_lip_errs lip_sw_ksz8567_tailtag_set_en(bool en);

#if LIP_PTP_ENABLE
/* Обновление текущего состояния разрешения PTP в свиче. Влияет на обработку tail-tag
 * и фоновое обновление отметки времени.
 * Должно вызываться после настройки PTP в регистрах свича и после этого
 * KSZSW_REGADDR_PTP_CLK_CTL меняться не должен, т.к. запоминается для дальнейшего
 * управления отметками времени без предварительного чтения */
t_lip_errs lip_sw_ksz8567_ptp_set_en(bool ptp_en);
bool lip_sw_ksz8567_ptp_is_en(void);
t_lip_errs lip_sw_ksz8567_ptp_proc_irq(uint8_t port_num);

/* Функция фиксации метки времени, которая может быть вычитана с помощью lip_sw_ksz8567_tstmp_read_finish().
 * Вынесено отдельно для возможности более точно соотнести момент времени свича и внешнего источника.
 * Момент захвата ниболее близок к моменту завершения функции */
t_lip_errs lip_sw_ksz8567_tstmp_read_req(void);
/* Чтение ранее захваченной с помощью lip_sw_ksz8567_tstmp_read_req() метки времени */
t_lip_errs lip_sw_ksz8567_tstmp_read_finish(t_lip_ptp_tstmp *tstmp);
/* Чтение текущего времени свича (аналогично последовательному вызову lip_sw_ksz8567_tstmp_read_req() и lip_sw_ksz8567_tstmp_read_finish()) */
t_lip_errs lip_sw_ksz8567_tstmp_read(t_lip_ptp_tstmp *tstmp);
#endif

t_lip_errs lip_sw_ksz8567_set_port_state_flags(uint8_t port_num, unsigned int flags);

t_lip_errs lip_sw_ksz8567_preproc_frame(t_lip_rx_ll_frame *ll_frame);
/* обработка принятого пакета. Если разрешен tail-tag, то обработка соответствующего
 * байта и заполнение информации о принятом пакете */
t_lip_errs lip_sw_ksz8567_process_packet(t_lip_rx_frame *pkt);

/* подготовка и передача пакета. Если разрешн tail-tag, то он добавляется в
 * конец пакета */
t_lip_tx_frame *lip_sw_ksz8567_tx_prepare(const t_lip_tx_frame_info *info);
t_lip_errs lip_sw_ksz8567_tx_flush(t_lip_tx_frame *frame);





#endif // LIP_SW_KSZ8567_H
