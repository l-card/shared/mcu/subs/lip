#ifndef LIP_PHY_MDIO_REGS_MMD_AN_H
#define LIP_PHY_MDIO_REGS_MMD_AN_H

#include "lip_phy_mdio_regs_mmd.h"

#define LIP_PHY_REGADDR_AN_EEE_ADV1                 LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_AN, 60) /* EEE advertisement 1 (45.2.7.13) */
#define LIP_PHY_REGADDR_AN_EEE_LP_ABL1              LIP_PHY_REGADDR_MMD(LIP_PHY_MMD_DEVADDR_AN, 61) /* EEE link partner ability 1 (45.2.7.14) */


/****** EEE advertisement 1 (Register 7.60, EEE_ADV1) *************************/
#define LIP_PHY_REGFLD_AN_EEE_ADV1_25G_R            (0x0001UL << 14) /* (RW) 25GBASE-R PHY EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_100G_CR4         (0x0001UL << 13) /* (RW) 100GBASE-CR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_100G_KR4         (0x0001UL << 12) /* (RW) 100GBASE-KR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_100G_KP4         (0x0001UL << 11) /* (RW) 100GBASE-KP4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_100G_CR10        (0x0001UL << 10) /* (RW) 100GBASE-CR10 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_40G_T            (0x0001UL <<  9) /* (RW) 10GBASE-T EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_40G_CR4          (0x0001UL <<  8) /* (RW) 40GBASE-CR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_40G_KR4          (0x0001UL <<  7) /* (RW) 40GBASE-KR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_10G_KR           (0x0001UL <<  6) /* (RW) 10GBASE-KR EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_10G_KX4          (0x0001UL <<  5) /* (RW) 10GBASE-KX4 EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_1000KX           (0x0001UL <<  4) /* (RW) 1000BASE-KX EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_10G_T            (0x0001UL <<  3) /* (RW) 10GBASE-T EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_1000_T           (0x0001UL <<  2) /* (RW) 1000BASE-T EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_100_TX           (0x0001UL <<  1) /* (RW) 100BASE-TX EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_ADV1_25G_T            (0x0001UL <<  0) /* (RW) 25GBASE-T EEE capability */

/****** EEE link partner ability 1 (Register 7.61, EEE_LP_ABL1) ***************/
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_25G_R         (0x0001UL << 14) /* (RO) 25GBASE-R PHY EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_100G_CR4      (0x0001UL << 13) /* (RO) 100GBASE-CR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_100G_KR4      (0x0001UL << 12) /* (RO) 100GBASE-KR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_100G_KP4      (0x0001UL << 11) /* (RO) 100GBASE-KP4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_100G_CR10     (0x0001UL << 10) /* (RO) 100GBASE-CR10 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_40G_T         (0x0001UL <<  9) /* (RO) 10GBASE-T EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_40G_CR4       (0x0001UL <<  8) /* (RO) 40GBASE-CR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_40G_KR4       (0x0001UL <<  7) /* (RO) 40GBASE-KR4 EEE deep sleep capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_10G_KR        (0x0001UL <<  6) /* (RO) 10GBASE-KR EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_10G_KX4       (0x0001UL <<  5) /* (RO) 10GBASE-KX4 EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_1000KX        (0x0001UL <<  4) /* (RO) 1000BASE-KX EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_10G_T         (0x0001UL <<  3) /* (RO) 10GBASE-T EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_1000_T        (0x0001UL <<  2) /* (RO) 1000BASE-T EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_100_TX        (0x0001UL <<  1) /* (RO) 100BASE-TX EEE capability */
#define LIP_PHY_REGFLD_AN_EEE_LP_ABL1_25G_T         (0x0001UL <<  0) /* (RO) 25GBASE-T EEE capability */


#endif // LIP_PHY_MDIO_REGS_MMD_AN_H
