#ifndef LIP_PHY_MDIO_REGS_EXT_H
#define LIP_PHY_MDIO_REGS_EXT_H

/* Некоторые PHY имеют механизм доступа к extended регистрам, появившийся до MMD
 * и не описанный в стандарте. Для него запись адреса идет в регистр 0x1E, а затем
 * уже в регистр 0x1F идет запись или чтение данных из extended регистров с заданнм
 * адресом */

#define LIP_PHY_REGADDR_EXT_FLAG                    (1UL << 30)
#define LIP_PHY_REGADDR_EXT(addr)                   ((addr) | LIP_PHY_REGADDR_EXT_FLAG)
#define LIP_PHY_REGADDR_IS_EXT(addr)                ((addr & LIP_PHY_REGADDR_EXT_FLAG) != 0)
#define LIP_PHY_REGADDR_GET_EXT_REGADDR(reg)        (reg & 0xFFFF)


#define LIP_PHY_REGADDR_EXT_REG_ADDR                0x1E /* Extended Registers Address Offset Register */
#define LIP_PHY_REGADDR_EXT_REG_DATA                0x1F /* Extended Registers Data Register */

#endif // LIP_PHY_MDIO_REGS_EXT_H
