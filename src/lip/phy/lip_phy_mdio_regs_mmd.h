#ifndef LIP_PHY_MMD_REGS_H
#define LIP_PHY_MMD_REGS_H

/* ------------------------------- MMD ---------------------------------------*/
#define LIP_PHY_REGADDR_MMD_FLAG                    (1UL << 31)
#define LIP_PHY_REGADDR_IS_MMD(reg)                 ((reg & LIP_PHY_REGADDR_MMD_FLAG) != 0)
#define LIP_PHY_REGADDR_GET_STD_REGADDR(reg)        (reg & 0x1F)
#define LIP_PHY_REGADDR_GET_MMD_REGADDR(mmdreg)     ((mmdreg) & 0xFFFF)
#define LIP_PHY_REGADDR_GET_MMD_DEVADDR(mmdreg)     (((mmdreg) >> 16) & 0x1F)

#define LIP_PHY_REGADDR_MMD(devaddr, regaddr)       (LIP_PHY_REGADDR_MMD_FLAG | (((devaddr) & 0x1F) << 16) | ((regaddr) & 0xFFFF))


/* Table 45–1—MDIO Manageable Device addresses */
#define LIP_PHY_MMD_DEVADDR_PMA                     1
#define LIP_PHY_MMD_DEVADDR_WIS                     2
#define LIP_PHY_MMD_DEVADDR_PCS                     3
#define LIP_PHY_MMD_DEVADDR_PHY_XS                  4
#define LIP_PHY_MMD_DEVADDR_DTE_XS                  5
#define LIP_PHY_MMD_DEVADDR_TC                      6
#define LIP_PHY_MMD_DEVADDR_AN                      7
#define LIP_PHY_MMD_DEVADDR_SEP_PMA1                8
#define LIP_PHY_MMD_DEVADDR_SEP_PMA2                9
#define LIP_PHY_MMD_DEVADDR_SEP_PMA3                10
#define LIP_PHY_MMD_DEVADDR_SEP_PMA4                11
#define LIP_PHY_MMD_DEVADDR_OFDM_PMA                12
#define LIP_PHY_MMD_DEVADDR_POW                     13 /* Power Unit */
#define LIP_PHY_MMD_DEVADDR_C22_EXT                 29 /* Clause 29 extension */
#define LIP_PHY_MMD_DEVADDR_VND1                    30
#define LIP_PHY_MMD_DEVADDR_VND2                    31

#endif // LIP_PHY_MMD_REGS_H
