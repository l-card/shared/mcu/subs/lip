#ifndef LAN8742_REGS_H_
#define LAN8742_REGS_H_

// WRONG
//#define LAN8742_REG_SILICON_REV 0x10
//#define LAN8742_REG_MODECS      0x11 /* Mode Control/Status */
//#define LAN8742_REG_SPECMODES   0x12 /* Special Mode */
//#define LAN8742_REG_SYMERRCNT   0x1A /* Symbol Error Counter  */
//#define LAN8742_REG_CSIND       0x1B /* Control / Status Indication */
//#define LAN8742_REG_TEST_CTL    0x1C /* Special internal testability controls */
//#define LAN8742_REG_INTSRC      0x1D /* Interrupt source */
//#define LAN8742_REG_INTMSK      0x1E /* Interrupt mask */
#define LAN8742_REG_PHYSCSR     0x1F /* PHY Special Control/Status */


/* PHY Special Control/Status */
#define LAN8742_BIT_PHYSCSR_SPEED_IND_Pos     2
#define LAN8742_BIT_PHYSCSR_AUTODONE_Pos      12

#define LAN8742_BIT_PHYSCSR_SPEED_IND_Mask     (0x07UL << LAN8742_BIT_PHYSCSR_SPEED_IND_Pos)
#define LAN8742_BIT_PHYSCSR_AUTODONE_Mask      (0x01UL << LAN8742_BIT_PHYSCSR_AUTODONE_Pos)



#endif //LAN8742_REGS_H_
