#include "lip/phy/lip_phy_cfg_defs.h"
#if LIP_PHY_SUPPORT_LAN8742

#include "phy/lip_phy_port.h"
#include "lan8742_regs.h"


static t_lip_errs f_get_mode_lan8742(uint8_t devaddr, e_LIP_PHY_SPEED *speed,
                                     e_LIP_PHY_DUPLEX_MODE *mode);

t_lip_phy_info lip_phy_lan8742 = {
    "LAN8742",
    0x0007,
    0xC130,
    0xFFF0,
    NULL,
    f_get_mode_lan8742
};


static t_lip_errs f_get_mode_lan8742(uint8_t devaddr, e_LIP_PHY_SPEED *speed,
                                     e_LIP_PHY_DUPLEX_MODE *mode) {
    t_lip_errs err = LIP_ERR_SUCCESS;
    uint16_t phy_reg;

    err = lip_phy_reg_read( devaddr, LAN8742_REG_PHYSCSR, &phy_reg );
    if (err == LIP_ERR_SUCCESS) {
        switch ((phy_reg & LAN8742_BIT_PHYSCSR_SPEED_IND_Mask)>>LAN8742_BIT_PHYSCSR_SPEED_IND_Pos) {
            case 1ul:  // 10 BASE T Half-duplex
                *mode = LIP_PHY_DUPLEX_MODE_HALF;
                *speed = LIP_PHY_SPEED_10;
                break;
            case 2ul:  // 100 BASE TX Half-duplex
                *mode = LIP_PHY_DUPLEX_MODE_HALF;
                *speed = LIP_PHY_SPEED_100;
                break;
            case 5ul:  // 10 BASE T Full-duplex
                *mode = LIP_PHY_DUPLEX_MODE_FULL;
                *speed = LIP_PHY_SPEED_10;
                break;
            case 6ul:  // 100 BASE TX Full-duplex
                *mode = LIP_PHY_DUPLEX_MODE_FULL;
                *speed = LIP_PHY_SPEED_100;
                break;
            default:
                err = LIP_ERR_INIT_PHY_MODE;
                break;
        }
    }
    return err;

}

#endif
