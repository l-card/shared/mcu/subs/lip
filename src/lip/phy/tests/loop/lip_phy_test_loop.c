#include "lip_phy_test_loop.h"
#include "lip/ll/lip_ll.h"
#include "lip/link/lip_mac_tx.h"
#include "lip/link/lip_mac_rx.h"
#include "lip/phy/lip_phy.h"
#include "lip/link/lip_mac.h"

#include "ltimer.h"

static int f_init_done = 0;
static uint32_t f_tx_cntr = 0;
static uint32_t f_rx_cntr = 0;
/* настройки phy для режима loopback */
static const t_lip_phy_cfg f_loopback_phy_cfg = {
    .opmode = LIP_PHY_OPMODE_LOCAL_LOOP,
    .flags = LIP_PHY_CFG_FLAG_AN_DISABLED,
    .manual_mode = {
        .speed = LIP_PHY_SPEED_100,
        .flags = LIP_PHY_MODE_FLAG_DUPLEX_FULL,
    }
};
/* информация о передаваемых кадрах */
static t_lip_tx_frame_info f_tx_frame_info = {
    .flags = LIP_FRAME_TXINFO_FLAG_RAW  /* используем признак, что кадр не должен быть дополнительно обрабатываться */
};



void lip_phy_test_loop_init(void) {
    /* инициализация стека */
    f_init_done = 0;
    f_tx_cntr = f_rx_cntr = 0;

    /* инициализация MAC с запретом любой обработки входных пакетов и
     * фильтрации пакетов, чтобы можно было сравнить исходные данные с посылаемыми */
    lip_mac_init(LIP_INIT_FLAGS_MAC_RX_HWPROC_DIS | LIP_INIT_FLAGS_MAC_RX_FILTER_DIS | LIP_INIT_FLAGS_MAC_LOOPBACK);
    /* настройка phy на режим loopback */
    lip_mac_phy_set_cfg(&f_loopback_phy_cfg);
}

int lip_phy_test_loop_pull(t_lip_phy_test_loop_stats *stats) {
    int err = 0;
    int out = 0;

    err = lip_mac_pull();



    if (!err) {
        if (lip_mac_phy_ctx()->devinfo != NULL) {
            stats->flags |= LIP_PHY_TEST_LOOP_STAT_FLAG_PHY_DETECTED;
        } else {
            stats->flags &= ~LIP_PHY_TEST_LOOP_STAT_FLAG_PHY_DETECTED;
        }

        if (lip_mac_phy_ctx()->state.flags & LIP_PHY_STATE_FLAG_LINK_UP) {
            /* сперва вычитываем все принятые пакеты и проверяем на счетчик */
            while (!out) {
                t_lip_rx_frame *rx_frame = lip_mac_rx_get_frame();
                if (rx_frame != NULL) {
                    size_t rx_size = rx_frame->pl.size;
                    uint32_t *rx_buf = (uint32_t*)rx_frame->pl.data;
                    size_t i;

                    for (i = 0; i < rx_size/4; ++i) {
                        if (rx_buf[i] != f_rx_cntr) {
                            stats->last_err.exp_wrd = f_rx_cntr;
                            stats->last_err.rcv_wrd = rx_buf[i];
                            stats->last_err.word_num = i;
                            stats->last_err.packet_num = stats->rcv_packet_cnt;
                            if (stats->rcv_err_cnt == 0) {
                                stats->first_err = stats->last_err;
                            }
                            stats->rcv_err_cnt++;
                            f_rx_cntr = rx_buf[rx_size/4 - 1];
                            out = 1;
                            break;
                        }
                        f_rx_cntr++;
                    }
                    stats->rcv_packet_cnt++;
                    stats->rcv_cntr = f_rx_cntr;
                    lip_mac_rx_get_frame_finish(rx_frame);

                } else {
                    out = 1;
                }
            }
            /* затем посылаем пакет, если есть место */
            t_lip_tx_frame *frame = lip_mac_tx_frame_prepare(&f_tx_frame_info);
            if (frame != NULL) {
                uint32_t *tx_buf = (uint32_t*)frame->buf.start_ptr;
                int i;
                int max_size = (frame->buf.end_ptr - frame->buf.start_ptr) / 4;
                for (i = 0; i < max_size; i++) {
                    *tx_buf++ = f_tx_cntr++;
                }
                frame->buf.cur_ptr = (uint8_t*)tx_buf;
                /* передача сформированного пакета */
                lip_mac_tx_frame_flush(frame);
                stats->tx_packet_cnt++;
            }
        }
    }
    return err;
}


void lip_phy_test_loop_finish(void) {
    lip_mac_close();
}
