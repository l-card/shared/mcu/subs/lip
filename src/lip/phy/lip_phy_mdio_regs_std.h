#ifndef LIP_PHY_MDIO_REGS_STD_H
#define LIP_PHY_MDIO_REGS_STD_H


/* Стандартные регистры (раздел 22.2.4). Адреса 16-31 - Vendor Specific */
#define LIP_PHY_REGADDR_BASIC_CTL       0   /* Basic Control Register (22.2.4.1) */
#define LIP_PHY_REGADDR_BASIC_STAT      1   /* Basic Status Register (22.2.4.2) */
#define LIP_PHY_REGADDR_PHYID_H         2   /* PHY Identifier (High word) (22.2.4.3.1) */
#define LIP_PHY_REGADDR_PHYID_L         3   /* PHY Identifier (Low word)  (22.2.4.3.1) */
#define LIP_PHY_REGADDR_AN_ADV          4   /* Auto-Negotiation Advertisement (28.2.4.1.3, 37.2.5.1.3) */
#define LIP_PHY_REGADDR_AN_LP_ABL       5   /* Auto-Negotiation Link Partner Base Page Ability (28.2.4.1.4, 37.2.5.1) */
#define LIP_PHY_REGADDR_AN_EXP          6   /* Auto-Negotiation Expansion (28.2.4.1.5, 37.2.5.1) */
#define LIP_PHY_REGADDR_AN_NP_TX        7   /* Auto-Negotiation Next Page Transmit (28.2.4.1.6, 37.2.5.1) */
#define LIP_PHY_REGADDR_AN_LP_NP_RX     8   /* Auto-Negotiation Link Partner Received Next Page  (28.2.4.1.7, 37.2.5.1) */
#define LIP_PHY_REGADDR_MS_CTL          9   /* MASTER-SLAVE Control Register (32.5, 40.5) */
#define LIP_PHY_REGADDR_MS_STAT         10  /* MASTER-SLAVE Status Register  (32.5, 40.5) */
#define LIP_PHY_REGADDR_PSE_CTL         11  /* PSE Control register (33.5.1.1) */
#define LIP_PHY_REGADDR_PSE_STAT        12  /* PSE Status register  (33.5.1.2) */
#define LIP_PHY_REGADDR_MMD_CTL         13  /* MMD Access Control Register (22.2.4.3.11) */
#define LIP_PHY_REGADDR_MMD_AD          14  /* MMD Access Address Data Register (22.2.4.3.12) */
#define LIP_PHY_REGADDR_EXT_STAT        15  /* Extended Status (22.2.4.4) */


#define LIP_PHY_STD_REG_MAX_CNT         32

/****** Basic Control Register (Register 0, BASIC_CTL) ************************/
#define LIP_PHY_REGFLD_BASIC_CTL_RST                (0x0001UL << 15U) /* (RW SC) PHY Soft Reset */
#define LIP_PHY_REGFLD_BASIC_CTL_LOOPBACK           (0x0001UL << 14U) /* (RW)    Enable loopback mode */
#define LIP_PHY_REGFLD_BASIC_CTL_SPEED_L            (0x0001UL << 13U) /* (RW)    Speed Selection Low Bit */
#define LIP_PHY_REGFLD_BASIC_CTL_AN_EN              (0x0001UL << 12U) /* (RW)    Auto-Negotiation Enable */
#define LIP_PHY_REGFLD_BASIC_CTL_PD                 (0x0001UL << 11U) /* (RW)    Power Down */
#define LIP_PHY_REGFLD_BASIC_CTL_ISOLATE            (0x0001UL << 10U) /* (RW)    Isolate PHY from MII or GMII */
#define LIP_PHY_REGFLD_BASIC_CTL_AN_RSTART          (0x0001UL <<  9U) /* (RW SC) Restart Auto-Negotiation */
#define LIP_PHY_REGFLD_BASIC_CTL_DUPLEX             (0x0001UL <<  8U) /* (RW)    Duplex Mode */
#define LIP_PHY_REGFLD_BASIC_CTL_COL_TEST           (0x0001UL <<  7U) /* (RW)    Collision Test */
#define LIP_PHY_REGFLD_BASIC_CTL_SPEED_H            (0x0001UL <<  6U) /* (RW)    Speed Selection Low Bit */
#define LIP_PHY_REGFLD_BASIC_CTL_UNIDIR_EN          (0x0001UL <<  5U) /* (RW)    Unidirectional enable */
#define LIP_PHY_REGMSK_BASIC_CTL_RESERVED           (0x001FUL)


#define LIP_PHY_REGFLDVAL_BASIC_CTL_DUPLEX_FULL     1
#define LIP_PHY_REGFLDVAL_BASIC_CTL_DUPLEX_HALF     0

#define LIP_PHY_REGFLDMSK_BASIC_CTL_SPPED_1000      (LIP_PHY_REGFLD_BASIC_CTL_SPEED_H)
#define LIP_PHY_REGFLDMSK_BASIC_CTL_SPPED_100       (LIP_PHY_REGFLD_BASIC_CTL_SPEED_L)
#define LIP_PHY_REGFLDMSK_BASIC_CTL_SPPED_10        (0)

/****** Basic Status Register (Register 1, BASIC_STAT) ************************/
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_100_T4        (0x0001UL << 15U) /* (RO)    100BASE-T4 ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_100X_FD       (0x0001UL << 14U) /* (RO)    100BASE-X Full Duplex ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_100X_HD       (0x0001UL << 13U) /* (RO)    100BASE-X Half Duplex ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_10_FD         (0x0001UL << 12U) /* (RO)    10 Mb/s full duplex ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_10_HD         (0x0001UL << 11U) /* (RO)    10 Mb/s half duplex ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_100_T2_FD     (0x0001UL << 10U) /* (RO)    100BASE-T2 full duplex ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_100_T2_HD     (0x0001UL <<  9U) /* (RO)    100BASE-T2 half duplex ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_EXT_STAT      (0x0001UL <<  8U) /* (RO)    Extended status information in Register 15 */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_UNIDIR        (0x0001UL <<  7U) /* (RO)    Unidirectional ability */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_PRE_SUPR      (0x0001UL <<  6U) /* (RO)    Accept management frames with preamble suppressed */
#define LIP_PHY_REGFLD_BASIC_STAT_AN_CPL            (0x0001UL <<  5U) /* (RO)    Auto-Negotiation Complete */
#define LIP_PHY_REGFLD_BASIC_STAT_REM_FLT           (0x0001UL <<  4U) /* (RO LH) Remote fault condition detected */
#define LIP_PHY_REGFLD_BASIC_STAT_ABL_AN            (0x0001UL <<  3U) /* (RO)    Auto-Negotiation Ability */
#define LIP_PHY_REGFLD_BASIC_STAT_LINK_UP           (0x0001UL <<  2U) /* (RO LL) Link Status */
#define LIP_PHY_REGFLD_BASIC_STAT_JBR_DET           (0x0001UL <<  1U) /* (RO LH) Jabber condition detected */
#define LIP_PHY_REGFLD_BASIC_STAT_EXT_CAP           (0x0001UL <<  0U) /* (RO)    Extended register capabilities */


/****** PHY Identifier (Registers 2 & 3, PHYID_H & PHYID_L) *******************/
#define LIP_PHY_REGFLD_PHYID_REV                    (0x0000000F <<  0) /* (RO) Revision Number */
#define LIP_PHY_REGFLD_PHYID_MODEL                  (0x0000003F <<  4) /* (RO) Model Number */
#define LIP_PHY_REGFLD_PHYID_VENDOR_OUI             (0x003FFFFF << 10) /* (RO) Vendor OUI [24:3] */

#define LIP_PHY_VENDOR_ID(h,l)                      (((h & 0xFFFF) << 6UL) | (l & 0x3F))
#define LIP_PHY_MODEL_ID(vendor, model)             (LBITFIELD_SET(LIP_PHY_REGFLD_PHYID_VENDOR_OUI, vendor) \
                                                        | LBITFIELD_SET(LIP_PHY_REGFLD_PHYID_MODEL, model))
#define LIP_PHY_MODEL_CHECK(m1, m2)                 (((m1) & (LIP_PHY_REGFLD_PHYID_MODEL | LIP_PHY_REGFLD_PHYID_VENDOR_OUI)) \
                                                        == ((m2) & (LIP_PHY_REGFLD_PHYID_MODEL | LIP_PHY_REGFLD_PHYID_VENDOR_OUI)))

/****** Auto-Negotiation advertisement register (Register 4, AN_ADV) **********/
#define LIP_PHY_REGFLD_AN_ADV_NP                    (0x0001UL << 15U) /* (RW)    Next Page */
#define LIP_PHY_REGFLD_AN_ADV_REM_FLT               (0x0001UL << 13U) /* (RW)    Remote Fault */
#define LIP_PHY_REGFLD_AN_ADV_EXT_NP                (0x0001UL << 12U) /* (RW)    Extended Next Page */
#define LIP_PHY_REGFLD_AN_ADV_TA_PAUSE_ASYM         (0x0001UL << 11U) /* (RW)    Asymmetric PAUSE operation */
#define LIP_PHY_REGFLD_AN_ADV_TA_PAUSE_SYM          (0x0001UL << 10U) /* (RW)    Symmetric PAUSE operation */
#define LIP_PHY_REGFLD_AN_ADV_TA_100_T4             (0x0001UL <<  9U) /* (RW)    100BASE-T4 */
#define LIP_PHY_REGFLD_AN_ADV_TA_100_TX_FD          (0x0001UL <<  8U) /* (RW)    100BASE-TX full duplex */
#define LIP_PHY_REGFLD_AN_ADV_TA_100_TX_HD          (0x0001UL <<  7U) /* (RW)    100BASE-TX half duplex */
#define LIP_PHY_REGFLD_AN_ADV_TA_10_T_FD            (0x0001UL <<  6U) /* (RW)    10BASE-T full duplex */
#define LIP_PHY_REGFLD_AN_ADV_TA_10_T_HD            (0x0001UL <<  5U) /* (RW)    10BASE-T half duplex */
#define LIP_PHY_REGFLD_AN_ADV_TA                    (0x007FUL <<  5U) /* (RW)    Technology Ability Field */
#define LIP_PHY_REGFLD_AN_ADV_SEL                   (0x001FUL <<  0U) /* (RW)    Selector Field */
#define LIP_PHY_REGMSK_AN_ADV_RESERVED              (0x4000UL)

#define LIP_PHY_REGFLDVAL_AN_ADV_SEL_IEEE_802_3     1
#define LIP_PHY_REGFLDVAL_AN_ADV_SEL_IEEE_1394      4
#define LIP_PHY_REGFLDVAL_AN_ADV_SEL_INCITS         5

/****** Auto-Negotiation Link Partner ability register (Register 5, AN_LP_ABL) */
#define LIP_PHY_REGFLD_AN_LP_ABL_NP                 (0x0001UL << 15U) /* (RO)    Next Page */
#define LIP_PHY_REGFLD_AN_LP_ABL_ACK                (0x0001UL << 14U) /* (RO)    Acknowledge */
#define LIP_PHY_REGFLD_AN_LP_ABL_REM_FLT            (0x0001UL << 13U) /* (RO)    Remote Fault */
#define LIP_PHY_REGFLD_AN_LP_ABL_EXT_NP             (0x0001UL << 12U) /* (RO)    Extended Next Page */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_PAUSE_ASYM      (0x0001UL << 11U) /* (RO)    Asymmetric PAUSE operation */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_PAUSE_SYM       (0x0001UL << 10U) /* (RO)    Symmetric PAUSE operation */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_100_T4          (0x0001UL <<  9U) /* (RO)    100BASE-T4 */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_100_TX_FD       (0x0001UL <<  8U) /* (RO)    100BASE-TX full duplex */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_100_TX_HD       (0x0001UL <<  7U) /* (RO)    100BASE-TX half duplex */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_10_T_FD         (0x0001UL <<  6U) /* (RO)    10BASE-T full duplex */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA_10_T_HD         (0x0001UL <<  5U) /* (RO)    10BASE-T half duplex */
#define LIP_PHY_REGFLD_AN_LP_ABL_TA                 (0x007FUL <<  5U) /* (RO)    Technology Ability Field */
#define LIP_PHY_REGFLD_AN_LP_ABL_SEL                (0x001FUL <<  0U) /* (RO)    Selector Field */


/****** Auto-Negotiation expansion register (Register 6, AN_EXP) **************/
#define LIP_PHY_REGFLD_AN_EXP_NP_RX_LOC_ABL         (0x0001UL <<  6U) /* (RO)    Receive Next Page Location Able (Support NP_RX_LOC) */
#define LIP_PHY_REGFLD_AN_EXP_NP_RX_LOC             (0x0001UL <<  5U) /* (RO)    Received Next Page Storage Location (NP Stored in AN_LP_NP_RX reg) */
#define LIP_PHY_REGFLD_AN_EXP_PARA_DET_FLT          (0x0001UL <<  4U) /* (RO LH) Parallel Detection Fault */
#define LIP_PHY_REGFLD_AN_EXP_LP_NP_ABL             (0x0001UL <<  3U) /* (RO)    Link Partner Next Page Able */
#define LIP_PHY_REGFLD_AN_EXP_NP_ABL                (0x0001UL <<  2U) /* (RO)    Local Device Next Page Able */
#define LIP_PHY_REGFLD_AN_EXP_NP_RCVD               (0x0001UL <<  1U) /* (RO LH) New Page has been received */
#define LIP_PHY_REGFLD_AN_EXP_LP_AN_ABL             (0x0001UL <<  0U) /* (RO)    Link Partner is Auto-Negotiation Able */
#define LIP_PHY_REGMSK_AN_EXP_RESERVED              (0xFF80UL)


/****** Auto-Negotiation Next Page transmit register (Register 7, AN_NP_TX) ***/
#define LIP_PHY_REGFLD_AN_NP_TX_NP                  (0x0001UL << 15U) /* (RW)    Next Page */
#define LIP_PHY_REGFLD_AN_NP_TX_MSG_PG              (0x0001UL << 13U) /* (RW)    Message Page */
#define LIP_PHY_REGFLD_AN_NP_TX_ACK2                (0x0001UL << 12U) /* (RW)    Acknowledge 2 */
#define LIP_PHY_REGFLD_AN_NP_TX_TGL                 (0x0001UL << 11U) /* (RW)    Toggle */
#define LIP_PHY_REGFLD_AN_NP_TX_MSG                 (0x07FFUL <<  0U) /* (RW)    Message/Unformatted Code field */
#define LIP_PHY_REGMSK_AN_NP_TX_RESERVED            (0x4000UL)

/****** Auto-Negotiation Link Partner Received Next Page register (Register 8, AN_LP_NP_RX) */
#define LIP_PHY_REGFLD_AN_LP_NP_RX_NP               (0x0001UL << 15U) /* (RW)    Next Page */
#define LIP_PHY_REGFLD_AN_LP_NP_RX_ACK              (0x0001UL << 14U) /* (RW)    Acknowledge (Received) */
#define LIP_PHY_REGFLD_AN_LP_NP_RX_MSG_PG           (0x0001UL << 13U) /* (RW)    Message Page */
#define LIP_PHY_REGFLD_AN_LP_NP_RX_ACK2             (0x0001UL << 12U) /* (RW)    Acknowledge 2 (Accepted) */
#define LIP_PHY_REGFLD_AN_LP_NP_RX_TGL              (0x0001UL << 11U) /* (RW)    Toggle */
#define LIP_PHY_REGFLD_AN_LP_NP_RX_MSG              (0x07FFUL <<  0U) /* (RW)    Message/Unformatted Code field */


/****** MASTER-SLAVE Control Register (Register 9, MS_CTL) ******************/
#define LIP_PHY_REGFLD_MS_CTL_TEST_MODE             (0x0007UL << 13U) /* (RW)    Transmitter test mode (40.6.1.1.2) */
#define LIP_PHY_REGFLD_MS_CTL_MS_MAN_EN             (0x0001UL << 12U) /* (RW)    MASTER-SLAVE Manual Config Enable */
#define LIP_PHY_REGFLD_MS_CTL_MS_MAN_CFG            (0x0001UL << 11U) /* (RW)    MASTER-SLAVE Manual Config Value */
#define LIP_PHY_REGFLD_MS_CTL_PORT_TYPE             (0x0001UL << 10U) /* (RW)    Port type (multiport/signleport) */
#define LIP_PHY_REGFLD_MS_CTL_ADV_1000_T_FD         (0x0001UL <<  9U) /* (RW)    Advertise PHY is 1000BASE-T full duplex capable */
#define LIP_PHY_REGFLD_MS_CTL_ADV_1000_T_HD         (0x0001UL <<  8U) /* (RW)    Advertise PHY is 1000BASE-T half duplex capable */
#define LIP_PHY_REGMSK_MS_CTL_RESERVED              (0x00FFUL)

#define LIP_PHY_REGFLDVAL_MS_CTL_TEST_MODE_NORMAL   0
#define LIP_PHY_REGFLDVAL_MS_CTL_TEST_MODE_WF       1 /* Test mode 1 - Transmit waveform test */
#define LIP_PHY_REGFLDVAL_MS_CTL_TEST_MODE_JTR_MST  2 /* Test mode 2 - Transmit jitter test in MASTER mode */
#define LIP_PHY_REGFLDVAL_MS_CTL_TEST_MODE_JTR_SLV  3 /* Test mode 3 - Transmit jitter test in SLAVE mode */
#define LIP_PHY_REGFLDVAL_MS_CTL_TEST_MODE_DIST     4 /* Test mode 4 - Transmitter distortion test */

#define LIP_PHY_REGFLDVAL_MS_CTL_PORT_TYPE_MULTI    1
#define LIP_PHY_REGFLDVAL_MS_CTL_PORT_TYPE_SINGLE   0

/* LIP_PHY_REGFLD_MS_CTL_MSCFG_MAN_VAL & LIP_PHY_REGFLD_MS_STAT_MSCFG_RES */
#define LIP_PHY_REGFLDVAL_MS_CFG_MASTER             1
#define LIP_PHY_REGFLDVAL_MS_CFG_SLAVE              0


/****** MASTER-SLAVE Status Register (Register 10, MS_STAT) *******************/
#define LIP_PHY_REGFLD_MS_STAT_MSCFG_FLT            (0x0001UL << 15U) /* (RO LH) MASTER-SLAVE Configuration Fault */
#define LIP_PHY_REGFLD_MS_STAT_MSCFG_RES            (0x0001UL << 14U) /* (RO)    MASTER-SLAVE Configuration Resolution */
#define LIP_PHY_REGFLD_MS_STAT_LOC_RCV_OK           (0x0001UL << 13U) /* (RO)    Local Receiver Status OK */
#define LIP_PHY_REGFLD_MS_STAT_REM_RCV_OK           (0x0001UL << 12U) /* (RO)    Remote Receiver Status OK */
#define LIP_PHY_REGFLD_MS_STAT_LP_ABL_1000_TFD      (0x0001UL << 11U) /* (RO)    Link Partner is capable of 1000BASE-T full duplex */
#define LIP_PHY_REGFLD_MS_STAT_LP_ABL_1000_THD      (0x0001UL << 10U) /* (RO)    Link Partner is capable of 1000BASE-T half duplex */
#define LIP_PHY_REGFLD_MS_STAT_IDLE_ERR_CNT         (0x00FFUL <<  0U) /* (RO)    Idle Error Count */
#define LIP_PHY_REGMSK_MS_STAT_RESERVED             (0x0300UL)


/****** PSE Control register (Register 11, PSE_CTL) ***************************/
#define LIP_PHY_REGFLD_PSE_CTL_DLL_CLASS_CAP        (0x0001UL <<  5U) /* (R/W)   Data Link Layer classification capability enabled */
#define LIP_PHY_REGFLD_PSE_CTL_PHYL_CLASS           (0x0001UL <<  4U) /* (R/W)   Physical Layer classification enabled */
#define LIP_PHY_REGFLD_PSE_CTL_PAIR                 (0x0003UL <<  2U) /* (R/W)   Pair Control */
#define LIP_PHY_REGFLD_PSE_CTL_EN                   (0x0003UL <<  0U) /* (R/W)   PSE Enable */
#define LIP_PHY_REGMSK_PSE_CTL_RESERVED             (0xFFC0UL)

#define LIP_PHY_REGFLDVAL_PSE_CTL_PAIR_ALT_A        1 /* PSE pinout Alternative A */
#define LIP_PHY_REGFLDVAL_PSE_CTL_PAIR_ALT_B        2 /* PSE pinout Alternative B */

#define LIP_PHY_REGFLDVAL_PSE_CTL_EN_DISABLED       0 /* PSE Disabled */
#define LIP_PHY_REGFLDVAL_PSE_CTL_EN_ENABLED        1 /* PSE Enabled */
#define LIP_PHY_REGFLDVAL_PSE_CTL_EN_POWER_TEST     2 /* Force Power Test Mode */


/****** PSE Status register (Register 12, PSE_STAT) ***************************/
#define LIP_PHY_REGFLD_PSE_STAT_ELECTR_PARMS_TYPE   (0x0001UL << 15U)  /* (RO)    PSE Type Electrical Parameters */
#define LIP_PHY_REGFLD_PSE_STAT_DLL_CLASS_ENABLED   (0x0001UL << 14U)  /* (RO)    Data Link Layer Classification Enabled */
#define LIP_PHY_REGFLD_PSE_STAT_PHYL_CLASS_SUP      (0x0001UL << 13U)  /* (RO)    Physical Layer Classification Supported */
#define LIP_PHY_REGFLD_PSE_STAT_POW_DEN_REM         (0x0001UL << 12U)  /* (RO LH) Power Denied or Removed */
#define LIP_PHY_REGFLD_PSE_STAT_VALID_SIGN          (0x0001UL << 11U)  /* (RO LH) Valid PD signature detected */
#define LIP_PHY_REGFLD_PSE_STAT_INVALID_SIGN        (0x0001UL << 10U)  /* (RO LH) Invalid PD signature detected */
#define LIP_PHY_REGFLD_PSE_STAT_SHORT               (0x0001UL <<  9U)  /* (RO LH) Short circuit condition detected */
#define LIP_PHY_REGFLD_PSE_STAT_OVLD                (0x0001UL <<  8U)  /* (RO LH) Overload condition detected */
#define LIP_PHY_REGFLD_PSE_STAT_MPS_ABSENT          (0x0001UL <<  7U)  /* (RO LH) MPS absent condition detected */
#define LIP_PHY_REGFLD_PSE_STAT_PD_CLASS            (0x0007UL <<  4U)  /* (RO)    PD Class */
#define LIP_PHY_REGFLD_PSE_STAT_PSE_STATUS          (0x0007UL <<  1U)  /* (RO)    PSE Status */
#define LIP_PHY_REGFLD_PSE_STAT_PAIR_CTL_ABL        (0x0001UL <<  0U)  /* (RO)    Pair Control Ability */


#define LIP_PHY_REGFLDVAL_PSE_STATUS_DISABLED       0
#define LIP_PHY_REGFLDVAL_PSE_STATUS_SEARCHING      1
#define LIP_PHY_REGFLDVAL_PSE_STATUS_DELIVERY_PWR   2
#define LIP_PHY_REGFLDVAL_PSE_STATUS_TEST_MODE      3
#define LIP_PHY_REGFLDVAL_PSE_STATUS_TEST_ERROR     4
#define LIP_PHY_REGFLDVAL_PSE_STATUS_IMPL_SPEC_FLT  5



/****** MMD access control register (Register 13, MMD_CTL) ********************/
#define LIP_PHY_REGFLD_MMD_CTL_FUNC                 (0x0003UL << 14U) /* (RW)   MMD Function */
#define LIP_PHY_REGFLD_MMD_CTL_DEVADDR              (0x001FUL <<  0U) /* (RW)   MMD Device address */
#define LIP_PHY_REGMSK_MMD_CTL_RESERVED             (0x3FE0UL)

#define LIP_PHY_REGFLDVAL_MMD_CTL_FUNC_ADDR         0 /* address */
#define LIP_PHY_REGFLDVAL_MMD_CTL_FUNC_DATA         1 /* data, no post increment */
#define LIP_PHY_REGFLDVAL_MMD_CTL_FUNC_DATA_INC_RW  2 /* data, post increment on reads and writes */
#define LIP_PHY_REGFLDVAL_MMD_CTL_FUNC_DATA_INC_WO  3 /* data, post increment on writes only */

/****** MMD Access Address Data Register (Register 14, MMD_AD) ****************/
#define LIP_PHY_REGFLD_MMD_AD_VALUE                 (0xFFFFUL <<  0U) /* (RW)   MMD Register Address/Data Value */

/****** Extended Status register (Register 15, EXT_STAT) ***********************/
#define LIP_PHY_REGFLD_EXT_STAT_ABL_1000X_FD        (0x0001UL << 15U) /* (RO)    1000BASE-X full duplex ability */
#define LIP_PHY_REGFLD_EXT_STAT_ABL_1000X_HD        (0x0001UL << 14U) /* (RO)    1000BASE-X half duplex ability */
#define LIP_PHY_REGFLD_EXT_STAT_ABL_1000_T_FD       (0x0001UL << 13U) /* (RO)    1000BASE-T full duplex ability */
#define LIP_PHY_REGFLD_EXT_STAT_ABL_1000_T_HD       (0x0001UL << 12U) /* (RO)    1000BASE-T half duplex ability */
#define LIP_PHY_REGMSK_EXT_STAT_RESERVED            (0x0FFFUL)




/* -------------- Auto-Negotiation Next Page Message Codes -------------------*/
#define LIP_PHY_AN_NP_MSG_CODE_NULL                 1 /* Null Message Code */
#define LIP_PHY_AN_NP_MSG_CODE_TA_EXT1              2 /* Technology Ability extension code 1 */
#define LIP_PHY_AN_NP_MSG_CODE_TA_EXT2              3 /* Technology Ability extension code 2 */
#define LIP_PHY_AN_NP_MSG_CODE_REM_FLT              4 /* Remote fault number code */
#define LIP_PHY_AN_NP_MSG_CODE_OUI_TAG              5 /* Organizationally Unique Identifier (OUI) tag code */
#define LIP_PHY_AN_NP_MSG_CODE_PHY_ID               6 /* PHY identifier tag code */
#define LIP_PHY_AN_NP_MSG_CODE_100_T2               7 /* 100BASE-T2 technology message code (32.5.4.2) */
#define LIP_PHY_AN_NP_MSG_CODE_1000_T               8 /* 1000BASE-T technology message code (40.5.1.2) */
#define LIP_PHY_AN_NP_MSG_CODE_MULTIGT_EXT          9 /* MultiGBASE-T and 1000BASE-T technology message code (Extended Next Page) (55.6.1, 113.6.1, 126.6.1) */
#define LIP_PHY_AN_NP_MSG_CODE_EEE                  10 /* EEE technology message code (45.2.7.13) */
#define LIP_PHY_AN_NP_MSG_CODE_OUI_TAG_EXT          11 /* Organizationally Unique Identifier Tagged Message (Extended Next Page) */

/* -------------- AN NP Message Length in Unformmated Msg Count & Data bits --*/
#define LIP_PHY_AN_NP_MSG_LEN_NULL                  0

#define LIP_PHY_AN_NP_MSG_LEN_TA_EXT1               1

#define LIP_PHY_AN_NP_MSG_LEN_TA_EXT2               2

/****** Remote fault number code **********************************************/
#define LIP_PHY_AN_NP_MSG_LEN_REM_FLT               1
#define LIP_PHY_AN_NP_MSG_REM_FLT_UPG1_TEST         0 /* Remote fault test */
#define LIP_PHY_AN_NP_MSG_REM_FLT_UPG1_LINK_LOSS    1 /* Link Loss */
#define LIP_PHY_AN_NP_MSG_REM_FLT_UPG1_JBR          2 /* Jabber */
#define LIP_PHY_AN_NP_MSG_REM_FLT_UPG1_PARA_DET     3 /* Parallel Detection Fault */

#define LIP_PHY_AN_NP_MSG_LEN_OUI_TAG               4
#define LIP_PHY_AN_NP_MSG_LEN_PHY_ID                4
#define LIP_PHY_AN_NP_MSG_LEN_100_T2                2

/****** 1000BASE-T technology *************************************************/
#define LIP_PHY_AN_NP_MSG_LEN_1000_T                 2
#define LIP_PHY_AN_NP_MSG_1000_T_UPG1_FLD_HD         (0x001UL << 4) /* 1000BASE-T half duplex */
#define LIP_PHY_AN_NP_MSG_1000_T_UPG1_FLD_FD         (0x001UL << 3) /* 1000BASE-T full duplex */
#define LIP_PHY_AN_NP_MSG_1000_T_UPG1_FLD_PORT_TYPE  (0x001UL << 2) /* 1000BASE-T port type (1 = multiport device and 0 = single-port device) */
#define LIP_PHY_AN_NP_MSG_1000_T_UPG1_FLD_MS_MAN_CFG (0x001UL << 1) /* 1000BASE-T MASTER-SLAVE Manual Configuration value (1 = MASTER and 0 = SLAVE) */
#define LIP_PHY_AN_NP_MSG_1000_T_UPG1_FLD_MS_MAN_EN  (0x001UL << 0) /* 1000BASE-T MASTER-SLAVE Manual Configuration Enable */
#define LIP_PHY_AN_NP_MSG_1000_T_UPG2_FLD_SEED       (0x7FFUL << 0) /* MASTER-SLAVE Seed Value */

#define LIP_PHY_AN_NP_MSG_LEN_EEE                   1
#define LIP_PHY_AN_NP_MSG_EEE_UPG1_FLD_10GT         (0x001UL << 3) /* 10GBASE-T has EEE capability */
#define LIP_PHY_AN_NP_MSG_EEE_UPG1_FLD_1000_T       (0x001UL << 2) /* 1000BASE-T has EEE capability */
#define LIP_PHY_AN_NP_MSG_EEE_UPG1_FLD_100_TX       (0x001UL << 1) /* 100BASE-TX has EEE capability */
#endif // LIP_PHY_MDIO_REGS_STD_H
