#ifndef LIP_PHY_MDIO_DEFS_H
#define LIP_PHY_MDIO_DEFS_H

/* Определение размеров и значений полей кадра MDIO в соответствии с IEEE 802.3 22.2.4.5 */

#define LIP_PHY_MDIO_MAX_FREQ                   (2500*1000)

#define LIP_PHY_MDIO_FRAME_BITLEN_PREAMBLE      32
#define LIP_PHY_MDIO_FRAME_BITLEN_SOF            2
#define LIP_PHY_MDIO_FRAME_BITLEN_OP             2
#define LIP_PHY_MDIO_FRAME_BITLEN_PHY_ADDR       5
#define LIP_PHY_MDIO_FRAME_BITLEN_REG_ADDR       5
#define LIP_PHY_MDIO_FRAME_BITLEN_TA             2
#define LIP_PHY_MDIO_FRAME_BITLEN_DATA          16
#define LIP_PHY_MDIO_FRAME_BITLEN              (LIP_PHY_MDIO_FRAME_BITLEN_PREAMBLE \
                                                + LIP_PHY_MDIO_FRAME_BITLEN_SOF \
                                                + LIP_PHY_MDIO_FRAME_BITLEN_OP \
                                                + LIP_PHY_MDIO_FRAME_BITLEN_PHY_ADDR \
                                                + LIP_PHY_MDIO_FRAME_BITLEN_REG_ADDR \
                                                + LIP_PHY_MDIO_FRAME_BITLEN_TA \
                                                + LIP_PHY_MDIO_FRAME_BITLEN_DATA)

#define LIP_PHY_MDIO_FRAME_SOF                 0x1
#define LIP_PHY_MDIO_FRAME_OP_RD               0x2
#define LIP_PHY_MDIO_FRAME_OP_WR               0x1
#define LIP_PHY_MDIO_FRAME_TA_WR               0x2

#endif // LIP_PHY_MDIO_DEFS_H

