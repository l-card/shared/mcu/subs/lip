#ifndef LIP_MISC_STRING_H
#define LIP_MISC_STRING_H

#include <stdint.h>

char *lip_str_put_str(char *str, const char *text);
char *lip_str_put_str_s(char *str, const char *text, const char *endstr);
char *lip_str_put_hex_digit(char *str, uint8_t val);
char *lip_str_put_uint8(char *str, uint8_t val);
char *lip_str_put_uint16(char *str, uint16_t val);
char *lip_str_put_uint32(char *str, uint32_t val);
char *lip_str_put_ipv4_addr(char *str, const uint8_t *ipaddr);

#endif // LIP_MISC_STRING_H
