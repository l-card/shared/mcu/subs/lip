#ifndef LIP_RAND_CFG_DEFS_H
#define LIP_RAND_CFG_DEFS_H

#include "lip_config.h"

#ifdef LIP_CFG_RAND_ENABLE
    #define LIP_RAND_ENABLE                 LIP_CFG_RAND_ENABLE
#else
    #define LIP_RAND_ENABLE                 0
#endif

#ifdef LIP_CFG_RAND_UINT32_RANGE_SUPPORT
    #define LIP_RAND_UINT32_RANGE_SUPPORT   LIP_CFG_RAND_UINT32_RANGE_SUPPORT
#else
    #define LIP_RAND_UINT32_RANGE_SUPPORT   0
#endif



#endif // LIP_RAND_CFG_DEFS_H
