#ifndef LIP_MISC_DEFS_H
#define LIP_MISC_DEFS_H

#ifndef ABS
    #define ABS(x) (((x) < 0) ? -(x) : (x))
#endif

#ifndef MIN
    #define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
    #define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif


#endif // LIP_MISC_DEFS_H
