/* Пример реализации обмена данных по TCP.

   Для использования примера, необходимо из внешней программы, вызывать
   сперва lip_sample_tcp_cntr_init(), передав в него необходимые настройки,
   а заетем периодически вызывать lip_sample_tcp_cntr_pull().

   В примере создается сокет, прослушивающий указанный порт.
   После того, как удаленная машина установит соединение по этому порту,
   пример начинает с максимально возможной скоростью передавать
   поток данных с циклическим 32-битным счетчиком хосту.
   Передача завершается по разрыву соединения */

#ifndef LIP_SAMPLE_TPC_CNTR_H
#define LIP_SAMPLE_TCP_CNTR_H


void lip_sample_tcp_cntr_init(const uint8_t* mac, const uint8_t* ip,
                       const uint8_t *mask, const uint8_t* gate,
                       uint16_t port);

void lip_sample_tcp_cntr_pull(void) ;

#endif // LIP_SAMPLE_TCP_CNTR_H
