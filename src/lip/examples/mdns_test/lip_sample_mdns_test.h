#ifndef LIP_SAMPLE_MDNS_TEST_H
#define LIP_SAMPLE_MDNS_TEST_H

void lip_sample_mdns_test_init(void);

void lip_sample_mdns_test_pull(void) ;

#endif // LIP_SAMPLE_MDNS_TEST_H

