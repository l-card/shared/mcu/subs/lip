/* Пример реализации обмена данных по UDP.

   Для использования примера, необходимо из внешней программы, вызывать
   сперва lip_sample_udp_cntr_init(), передав в него необходимые настройки,
   а заетем периодически вызывать lip_sample_udp_cntr_pull().

   Пример ожидает по заданному порту прихода пакета со строкой "start",
   после чего начинает с максимально возможной скоростью передавать
   UDP-пакеты с циклическим 32-битным счетчиком хосту, от которого
   пришел пакет "start".
   Передача завершается по приходу пакета со строкой "stop" */

#ifndef LIP_SAMPLE_UDP_CNTR_H
#define LIP_SAMPLE_UDP_CNTR_H


void lip_sample_udp_cntr_init(const uint8_t* mac, const uint8_t* ip,
                       const uint8_t *mask, const uint8_t* gate,
                       uint16_t port);

void lip_sample_udp_cntr_pull(void) ;

#endif // LIP_SAMPLE_UDP_CNTR_H
