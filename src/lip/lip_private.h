#ifndef LIP_CORE_PRIV_H
#define LIP_CORE_PRIV_H

#include "lip.h"


typedef struct {
    uint8_t *addr; //текущий адрес - указывает на routable_addr или ll_addr
    unsigned routable_addr_flags;
    t_lip_ipv4_addr routable_addr; //статический адрес или полученный через DHCP
    t_lip_ipv4_addr previous_addr; //предыдущий адрес (чтобы при повторном коннекте сравнить)
    t_lip_ipv4_addr mask; //маска сети
    t_lip_ipv4_addr gate; //адрес шлюза по-умолчанию
#ifdef LIP_USE_LINK_LOCAL_ADDR
    t_lip_ipv4_addr ll_addr; //link-local адрес
    uint8_t ll_en; //разрешение получения link-local адреса
#endif
} t_lip_ip_state;


typedef struct {
    t_lip_ip_state ip;
} t_lip;


extern t_lip g_lip_st;

typedef enum {
    /* признак, что изменился предпочитаемый по данному интерфейсу
        адрес (например link-local на routable) */
    LIP_IP_ADDR_CHANGE_FLAG_PREF        = 1 << 0,
    LIP_IP_ADDR_CHANGE_FLAG_PREV_INV    = 1 << 1,
} t_lip_ip_addr_change_flags;

void lip_ip_addr_changed(t_lip_ip_type type, const uint8_t* prev_addr, unsigned flags, int len);


#endif // LIP_CORE_PRIV_H
