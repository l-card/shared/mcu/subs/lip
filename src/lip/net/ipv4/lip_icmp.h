/*
 * lip_icmp.h
 *
 *  Created on: 30.08.2010
 *      Author: borisov
 */

#ifndef LIP_ICMP_H_
#define LIP_ICMP_H_

#include "lip/lip_defs.h"
#include "lip/net/ipv4/lip_ip_private.h"

//коды для ICMP-пакета destination unreachable
#define LIP_ICMP_CODE_NET_UNREACHABLE                  0
#define LIP_ICMP_CODE_HOST_UNREACHABLE                 1
#define LIP_ICMP_CODE_PROTOCOL_UNREACHABLE             2
#define LIP_ICMP_CODE_PORT_UNREACHABLE                 3
#define LIP_ICMP_CODE_FRAGMENTATION_NEEDED_AND_DF_SET  4
#define LIP_ICMP_CODE_SOURCE_ROUTE_FAILD               5

t_lip_errs lip_icmp_process_packet(const t_lip_ip_rx_packet *ip_packet);
t_lip_errs lip_icmp_send_dest_unreachable(const uint8_t* dest_addr, const uint8_t* src_addr,
        uint8_t code, const uint8_t* data, uint16_t data_size);
t_lip_errs lip_icmp_send_frag_time_exceed(const uint8_t* dest_addr, const uint8_t* src_addr,
                                   const uint8_t* data, uint16_t data_size);

#endif /* LIP_ICMP_H_ */
