/*
 * lip_ip_link_local_private.h
 *
 *  Created on: 05.08.2011
 *      Author: Borisov
 */

#ifndef LIP_IP_LINK_LOCAL_PRIVATE_H_
#define LIP_IP_LINK_LOCAL_PRIVATE_H_

#include "lip/net/ipv4/lip_ip_link_local.h"

void lip_ip_link_local_init(void);
void lip_ip_link_local_pull(void);
void lip_ip_link_local_start(void);
void lip_ip_link_local_reconnect(void);
void lip_cb_link_local_recv_arp(const uint8_t *src_addr,
        const uint8_t *dst_addr, const uint8_t* src_mac);


#endif /* LIP_IP_LINK_LOCAL_PRIVATE_H_ */
