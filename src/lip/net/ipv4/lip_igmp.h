/*
 * lip_igmp.h
 *
 *  Created on: 13.06.2011
 *      Author: Melkor
 */

#ifndef LIP_IGMP_H_
#define LIP_IGMP_H_

#include "lip/lip_defs.h"
#include "lip/net/ipv4/lip_ip_private.h"

//тип igmp-сокет - используется адрес памяти
typedef const void* t_lip_igmp_sock;
#define LIP_IGMP_INVALID_SOCK   NULL

//ражим фильтра адресов источников
#define LIP_IGMP_FILTERMODE_INCLUDE 0
#define LIP_IGMP_FILTERMODE_EXCLUDE 1




t_lip_errs lip_igmp_listen(t_lip_igmp_sock sock, const uint8_t* addr,
                           uint8_t filter_mode, 
                           const uint8_t src_addrs[][LIP_IPV4_ADDR_SIZE],
                           uint8_t src_cnt);

//упрощенные функции стандартов v1 и v2 определяются через listen макросами
#define lip_igmp_join(sock, addr) lip_igmp_listen(sock, addr, \
        LIP_IGMP_FILTERMODE_EXCLUDE, NULL, 0)

#define lip_igmp_leave(sock, addr) lip_igmp_listen(sock, addr, \
        LIP_IGMP_FILTERMODE_INCLUDE, NULL, 0)

void lip_igmp_init(void);
void lip_igmp_pull(void);
t_lip_errs lip_igmp_process_packet(const t_lip_ip_rx_packet *ip_packet);
t_lip_errs lip_igmp_reconnect(void);
t_lip_errs lip_igmp_addr_changed(void);

int lip_igmp_check_mcast_addr(const uint8_t* mcast_addr, const uint8_t* src_addr);
t_lip_errs lip_igmp_check_sock(t_lip_igmp_sock sock, const uint8_t* mcast_addr, const uint8_t* src_addr);

#endif /* LIP_IGMP_H_ */
