/*
 * lip_ipv4_acd.h
 *
 *  Created on: 25.07.2011
 *      Author: Borisov
 */

#ifndef LIP_IPV4_ACD_H_
#define LIP_IPV4_ACD_H_

#include "lip/lip_defs.h"


/* dhcp и link-local используют модуль acd для проверки адресов
 *  => при использовании этих протоколов ACD должен быть всегда включен
 */
#if defined LIP_USE_DHCPC || defined LIP_USE_LINK_LOCAL_ADDR
    #ifndef LIP_USE_IPV4_ACD
        #define LIP_USE_IPV4_ACD
    #endif
#endif

/* коды событий, передаваемых в callback-е */
typedef enum {
    LIP_IPV4_ACD_EVENT_ADDR_ESTABLISHED   = 1,
    LIP_IPV4_ACD_EVENT_ADDR_CONFLICT      = 2
} t_lip_ipv4_acd_event;
/* callback функция, вызываемая при возникновении события, связанного с адресом */
typedef void (*t_lip_ipv4_acd_cb)(const uint8_t* addr, t_lip_ipv4_acd_event event);


typedef enum {
    /** При посылки ARP для проверки адреса используются более короткие
     *  интервалы в диапазоне от #LIP_IPV4_ACD_PROBE_FAST_INTERVAL_MIN до
     *  #LIP_IPV4_ACD_PROBE_FAST_INTERVAL_MAX, вместо диапазона от
     *  #LIP_IPV4_ACD_PROBE_INTERVAL_MIN до #LIP_IPV4_ACD_PROBE_INTERVAL_MAX */
    LIP_IPV4_ADC_FLAGS_FAST_PROBE = 1 << 0,
    /** Не требуется проверка адреса, а сразу выполняется аннонс */
    LIP_IPV4_ADC_FLAGS_SKIP_PROBE = 1 << 1,
    /** Признак, что адрес сохраняется при переподключении */
    LIP_IPV4_ADC_FLAGS_PERSIST    = 1 << 2,
} t_lip_ipv4_acd_flags;



/* запуск процедуры проверки ip-адреса */
t_lip_errs lip_ipv4_acd_add(const uint8_t *addr, t_lip_ipv4_acd_cb cb,
                            uint32_t init_tout, uint8_t flags);
/* освобождение отслеживания ip-адреса */
t_lip_errs lip_ipv4_acd_leave(const uint8_t *addr);
/* инициализация модуля  */
void lip_ipv4_acd_init(void);
void lip_ipv4_acd_reconnect(void);
/* продвижение модуля - фоновые задачи */
t_lip_errs lip_ipv4_acd_pull(void);
/* arp-callback на прием пакета */
void lip_ipv4_acd_arp_cb(const uint8_t *src_addr, const uint8_t *dst_addr, const uint8_t* src_mac);


#endif /* LIP_IPV4_ACD_H_ */

