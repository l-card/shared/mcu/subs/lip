#ifndef LIP_IP_REASSEMBLY_PRIVATE_H
#define LIP_IP_REASSEMBLY_PRIVATE_H

#include "lip_ip_private.h"


void lip_ipv4_reassembly_init(void);
void lip_ipv4_reassembly_pull(void);

t_lip_errs lip_ipv4_reassembly_process_packet(t_lip_ip_rx_packet *ip_packet);




#endif // LIP_IP_REASSEMBLY_PRIVATE_H
