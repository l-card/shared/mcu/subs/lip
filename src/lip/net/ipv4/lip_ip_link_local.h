/*
 * lip_ip_link_local.h
 *
 *  Created on: 11.06.2011
 *      Author: Melkor
 */

#ifndef LIP_IP_LINK_LOCAL_H_
#define LIP_IP_LINK_LOCAL_H_

#include "lip/lip_defs.h"

#define LIP_IP_LINK_LOCAL_ADDR1  169
#define LIP_IP_LINK_LOCAL_ADDR2  254


//проверка - является ли ip-адрес link-local адресом
#define lip_ipv4_is_link_local(ip) ((ip[0] == LIP_IP_LINK_LOCAL_ADDR1) \
                                    && (ip[1]==LIP_IP_LINK_LOCAL_ADDR2) ? 1 : 0)
//проверка, что данный ip-адрес - link-local broadcast
#define lip_ipv4_is_link_local_bcast(ip) (lip_ipv4_is_link_local(ip) && \
    (ip[2] == 0xFF) && (ip[3] == 0xFF) ? 1: 0)

#define lip_ip_link_local_is_en()   g_lip_st.ip.ll_en
#define lip_ip_link_local_addr() g_lip_st.ip.ll_addr

void lip_ip_link_local_enable(uint8_t en);




#endif /* LIP_IP_LINK_LOCAL_H_ */
