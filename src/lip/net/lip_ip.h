/***************************************************************************//**
 @defgroup lip_ip IP
 @ingroup  lip_lvl_network
 @brief  Модуль, реализующий общие функции для протоколов @ref lip_ipv4 и
         @ref lip_ipv6 (не реализован)

******************************************************************************/

/***************************************************************************//**
 @addtogroup lip_ip
 @{
 @file lip_ip.h 
 @brief Файл с объявлением функций и типов, общих для IP-протоколов

 @author Borisov Alexey <borisov@lcard.ru>
*******************************************************************************/


#ifndef LIP_IP_H_
#define LIP_IP_H_

#include "lip/lip_defs.h"
#include "lip/net/ipv4/lip_ipv4.h"

/** Размер адреса для протокола IPv6 */
#define LIP_IPV6_ADDR_SIZE  16

#ifdef LIP_USE_IPV6
    #define LIP_IP_ADDR_SIZE_MAX LIP_IPV6_ADDR_SIZE
#else
    /** Максимальный размер IP-адреса. Зависит от списка поддерживаемых версий проткола IP */
    #define LIP_IP_ADDR_SIZE_MAX LIP_IPV4_ADDR_SIZE
#endif


/** Тип IP-протокола, используемого на сетевом уровне */
typedef enum {
    LIP_IPTYPE_ANY = 0, /**< Может быть использован любой - при передаче */
    LIP_IPTYPE_IPv4,    /**< IPv4 */
    LIP_IPTYPE_IPv6     /**< IPv6 */
} t_lip_ip_type;

/** Структура, содержащая IP-адрес */
typedef  struct {
    uint8_t ip_type;  /** Тип адреса --- одно значение из #t_lip_ip_type */
    uint8_t addr_len; /** Длина адреса (количество действительных байт в массиве addr */
    uint8_t addr[LIP_IP_ADDR_SIZE_MAX]; /**< Адрес в виде массива байт */
} t_lip_ip_addr;


/** Флаги, задающие тип IP-адреса. Может быть несколько, объединенных по ИЛИ */
typedef enum {
    LIP_IP_ADDR_TYPE_INVALID    = 0x00, /**< Неверный тип адреса */
    LIP_IP_ADDR_TYPE_UNICAST    = 0x01, /**< Адрес для одного хоста */
    LIP_IP_ADDR_TYPE_MCAST      = 0x02, /**< Адрес группы */
    LIP_IP_ADDR_TYPE_BCAST      = 0x04, /**< Широковещательный адрес */
    LIP_IP_ADDR_TYPE_LOOPBACK   = 0x08, /**< Адрес петли (для посылке самому себе) */
    LIP_IP_ADDR_TYPE_LINKLOCAL  = 0x10, /**< Адрес действителен только в сети, где подключен хост */
    LIP_IP_ADDR_TYPE_LOCAL      = 0x20  /**< Адрес принадлежит той же сети, к которой подключен хост */
} t_lip_ip_addr_type;


void lip_ip_addr_fill(t_lip_ip_addr *addr, const uint8_t *addr_data, t_lip_ip_type type);
t_lip_ip_addr_type lip_ip_addr_type(const t_lip_ip_addr *addr);
t_lip_errs lip_ip_addr_check(const t_lip_ip_addr *addr, t_lip_ip_addr_type type);



#endif /* LIP_IP_H_ */

/** @}*/
