/***************************************************************************//**
  @addtogroup lip_tftp
  @{
  @internal @file lip_tftp_private.h
  @brief Файл содержит определения функций и констант для @ref lip_tftp,
         которые используются только самим стеком.
  @author Borisov Alexey <borisov@lcard.ru>
  @date   05.08.2011
 ******************************************************************************/

#ifndef LIP_TFTP_PRIVATE_H_
#define LIP_TFTP_PRIVATE_H_

#include "lip_tftp.h"

/** @internal Инициализация tftp-модуля */
void lip_tftp_init(void);
/** @internal Очистка ресурсов tftp-модуля */
void lip_tftp_close(void);
/** @internal Продвижение автомата tftp-модуля */
void lip_tftp_pull(void);

#endif /* LIP_TFTP_PRIVATE_H_ */
/** @}*/
