#ifndef LIP_PTP_ETH_SYNC_SEND_H
#define LIP_PTP_ETH_SYNC_SEND_H


#include "lip/app/ptp/lip_ptp_internal.h"

/* Реализация посылки Sync и FollowUp сообщений для передачи информации от времени
 * по GM через все порты в состоянии Master.
 * Используется всегда 2-step режим, как предпочтительный в gPTP (кроме того,
 * способ формирования timestamp и correction может не совпадать с аппаратной
 * реализацией коррекции one-step в контроллере, поэтому он далеко не всегда возможен).
 * Для gPTP originTimestamp содержит время GM отправки начального sync и не изменяется
 * при дальнейшей перессылки, а correction содержит разницу между меткой времени и непосредство
 * отправкой Sync. Correction меняется при каждой перессылке, каждый BC
 * добавляет разницу времени между посылкой SYNC предыдущим узлом и временем передачи SYNC дальше,
 * пересчитанный относительно клока GM. Время состоит из propagationDelay и residenceTime
 * Дополнительно с FollowUp передается TLV с информацией о rateRatio локального
 * клока узла, посылающего Sync, относительно GM, а также о последнем "разрыве" времени.
 * См. 11.1.3 из IEEE 802.1AS-202
 */

void lip_ptp_md_eth_sync_send_init(t_lip_ptp_port_ctx *p);
void lip_ptp_md_eth_send_sync_etstmp(t_lip_ptp_port_ctx *p, const t_lip_ptp_tstmp *etstmp);
void lip_ptp_md_eth_send_sync(t_lip_ptp_port_ctx *p, const t_lip_ptp_port_md_sync_info *info);

#endif // LIP_PTP_ETH_SYNC_SEND_H
