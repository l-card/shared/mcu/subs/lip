#ifndef LIP_PTP_ETH_SYNC_RECV_H
#define LIP_PTP_ETH_SYNC_RECV_H

#include "lip/app/ptp/lip_ptp_internal.h"

/* Автомат выполняет логику приема и обработки Sync/FollowUp от соседа для каждого порта
 * в случае работы по Ethernet.
 * При успешном приема выполняется расчет параметров структуры #t_lip_ptp_eth_sync_recv_ctx и
 * передача этой информации для дальнейшей обработки с помощью lip_ptp_prot_sync_process_rx().
 */

void lip_ptp_md_eth_sync_recv_init(t_lip_ptp_port_ctx *p);
void lip_ptp_md_eth_sync_recv_pull(t_lip_ptp_port_ctx *p);
/* обработка принятого сообщения sync */
void lip_ptp_md_eth_sync_recv_proc_sync(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);
/* обработка принятого сообщения FollowUp */
void lip_ptp_md_eth_sync_recv_proc_fup(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);



#endif // LIP_PTP_ETH_SYNC_RECV_H
