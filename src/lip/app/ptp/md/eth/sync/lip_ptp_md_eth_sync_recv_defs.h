#ifndef LIP_PTP_ETH_SYNC_RECV_DEFS_H
#define LIP_PTP_ETH_SYNC_RECV_DEFS_H


#include <stdint.h>
#include "ltimer.h"
#include "lip/lip_defs.h"
#include "lip/app/ptp/sync/lip_ptp_port_sync_defs.h"


typedef struct {
    bool            rx_wt_fup; /* признак, что был принят 2-step sync и ожидается followUp */
    t_lip_ptp_tstmp rx_sync_tstmp; /* метка времени приема sync */
    uint16_t        rx_sync_seqid_be; /* sequenceID принятого sync в be для сопоставления с followUp */
    t_ltimer        rx_fup_tout_tmr;  /* таймер для таймаута на ожидание followUp после sync */
    t_lip_ptp_port_identity     rx_src_port_id; /* sorucePortId для принятого sync для сравнения с followUp */
    t_lip_ptp_port_md_sync_info rx_info;        /* принятая и рассчитанная информация о времени синхронизации для передачи на обработку */
} t_lip_ptp_eth_sync_recv_ctx;

#endif // LIP_PTP_ETH_SYNC_RECV_DEFS_H
