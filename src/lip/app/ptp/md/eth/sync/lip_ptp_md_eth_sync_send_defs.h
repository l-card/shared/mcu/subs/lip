#ifndef LIP_PTP_ETH_SYNC_SEND_DEFS_H
#define LIP_PTP_ETH_SYNC_SEND_DEFS_H

#include "lip/app/ptp/sync/lip_ptp_port_sync_defs.h"


typedef struct {
    bool          tx_wt_fup;
    uint16_t      tx_sync_seqid;
    t_lip_fns     tx_cor;
    t_lip_ptp_port_md_sync_info tx_info;
} t_lip_ptp_eth_sync_send_ctx;

#endif // LIP_PTP_ETH_SYNC_SEND_DEFS_H
