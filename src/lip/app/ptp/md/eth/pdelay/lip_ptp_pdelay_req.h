#ifndef LIP_PTP_PDELAY_REQ_H
#define LIP_PTP_PDELAY_REQ_H

/* Реализая измерения задержки передачи с соседом.
 * Выполняется переиодическая посылка PDelayReq и обработка
 * принятых ответов в виде PDelayResp и PDelayFollowUp с вычислением meanLinkDelay.
 * Функция по умолчанию использует вариант реализации CMLDS с передачей полный меток времени,
 * но может работать и с раелизацией, использующей либо one-step, либо 2-step только с разницей t3-t2.
 *
 * Если соседняя сторона возвращает все метки времени (отдельно значения t2 и t3), то эти данные
 * также используются для вычисления nbRateRatio.
 * В противном случае nbRateRatio может быть вычислено только по sync в коде обработки приема sync и только
 *  если порт находится в состоянии slave */

#include "lip/app/ptp/lip_ptp_internal.h"

void lip_ptp_pdelay_req_init(t_lip_ptp_port_ctx *port);
void lip_ptp_pdelay_req_pull(t_lip_ptp_port_ctx *p);
void lip_ptp_pdelay_req_down(t_lip_ptp_port_ctx *p);
/* Обработка получения egress timestamp для переданного сообщения PDelayReq */
void lip_ptp_pdelay_req_etstmp(t_lip_ptp_port_ctx *p, const t_lip_ptp_tstmp *tsmp);
/* Обработка принятого сообщения PDelayResp */
void lip_ptp_pdelay_req_proc_resp(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);
/* Обработка принятого сообщения PDelayRespFollowUp */
void lip_ptp_pdelay_req_proc_follow_up(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);

#endif // LIP_PTP_PDELAY_REQ_H
