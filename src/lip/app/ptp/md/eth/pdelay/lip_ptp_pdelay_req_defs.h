#ifndef LIP_PTP_PDELAY_REQ_DEFS_H
#define LIP_PTP_PDELAY_REQ_DEFS_H

#include <stdint.h>
#include "ltimer.h"
#include "lip/app/ptp/lip_ptp_proto_defs.h"
#include "lip/app/ptp/lip_ptp_defs.h"
#include "lip/app/ptp/lip_ptp_cfg_defs.h"


typedef enum {
    LIP_PTP_PDELAY_REQ_STATE_INIT, /* начальная инициализация */
    LIP_PTP_PDELAY_REQ_STATE_WT_REQ_SEND,  /* ожидание истечения интервала для посылки нового PDelayReq (все операции по прошлому циклу завершены) */
    LIP_PTP_PDELAY_REQ_STATE_WT_REQ_TSTMP, /* ожидания вызова lip_ptp_pdelay_req_etstmp() после посылки PDelayReq */
    LIP_PTP_PDELAY_REQ_STATE_WT_RESP,      /* ожидание ответа на PdelayReq */
    LIP_PTP_PDELAY_REQ_STATE_WT_FOLLOW_UP, /* ожидание PdelayReqFollowUp после получения 2-step PDelayReq */
} t_lip_ptp_pdelay_req_state;

typedef struct {
    t_lip_ptp_pdelay_req_state state;
    t_ltimer interval_tmr; /* таймер для посылки PDelayReq */
    int8_t   log_interval; /* log по степени 2 для интервала передачи PDelayReq */
    uint8_t lost_resp; /* счетчик потерянных или некорректных ответов на PDelayReq */
    uint8_t faults_cnt; /* счетчик некорректных измерений meanLinkDelay */
    uint16_t seq_id; /* sequence id последнего переданного PDelay_Req */

    uint8_t   mean_link_delay_avg_cnt; /* текущее количество полученных измерений meanLinkDelay для вычисления среднего */

    t_lip_ptp_tstmp t1; /* время передачи pdelayReq */
    t_lip_ptp_tstmp t2; /* время приема другой стороной pdelayReq, высланное в Resp */
    t_lip_ptp_tstmp t3; /* время передачи другой стороной pdelayReq, высланное в FollowUp */
    t_lip_ptp_tstmp t4; /* время приема pdelayResp */
#if !LIP_PTP_PDELAY_REQ_2STEP_COR_DISABLE
    t_lip_fns       t2_cor; /* correctionField для t2 */
#endif

    t_lip_ptp_port_identity cur_presp_srcid; /* SourceID для принятого PDelayResp для проверки его в FollowUp */
} t_lip_ptp_pdelay_req_ctx;




#endif // LIP_PTP_PDELAY_REQ_DEFS_H
