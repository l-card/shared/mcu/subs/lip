#ifndef LIP_PTP_PDELAY_RESP_DEFS_H
#define LIP_PTP_PDELAY_RESP_DEFS_H

#include <stdint.h>
#include "lip/app/ptp/lip_ptp_proto_defs.h"


typedef enum {
    LIP_PTP_PDELAY_RESP_STATE_INIT,  /* начальная инициализация */
    LIP_PTP_PDELAY_RESP_STATE_WT_REQ, /* ожидание прихода следующего PDelayReq */
    LIP_PTP_PDELAY_RESP_STATE_WT_RESP_TSTMP, /* ожидания метки времени после передачи PDelayResp */
} t_lip_ptp_pdelay_resp_state;



typedef struct {
    t_lip_ptp_pdelay_resp_state state; /* Состояние автомата */
    uint16_t req_seq_id_be; /** sequenceId принятого PDelayReq в big-edian */
    t_lip_ptp_port_identity req_src_id; /** sourceId для принятого PDelayReq */
    uint64_t req_cor_be; /**< поле Correction от принятого PDelayReq в big-edian */
} t_lip_ptp_pdelay_resp_ctx;


#endif // LIP_PTP_PDELAY_RESP_DEFS_H
