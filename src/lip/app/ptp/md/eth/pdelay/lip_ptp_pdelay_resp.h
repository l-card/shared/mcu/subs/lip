#ifndef LIP_PTP_PDELAY_RESP_H
#define LIP_PTP_PDELAY_RESP_H

/* Раелизация ответов на PDelayReq удаленной стороны.
 * Работает в режиме 2-step с полными метками времени t2 и t3.
 * Поддерживает как CMLDS, так и обычный запрос (смотрит по SDOID) */

#include "lip/app/ptp/lip_ptp_internal.h"

void lip_ptp_pdelay_resp_init(t_lip_ptp_port_ctx *p);
/* Обработка egress timestamp для переданного PDelayResp */
void lip_ptp_pdelay_resp_etstmp(t_lip_ptp_port_ctx *p, const t_lip_ptp_tstmp *tstmp);
/* Обработка принятого PDelayReq */
void lip_ptp_pdelay_resp_proc_req(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);

#endif // LIP_PTP_PDELAY_RESP_H
