#ifndef LIP_PTP_SITE_SYNC_DEFS_H
#define LIP_PTP_SITE_SYNC_DEFS_H

#include "lip_ptp_port_sync_defs.h"
#include "ltimer.h"

/* Информация о принятом sync/followUp от MD = MDSyncReceive - 10.2.2.2 IEEE 802.1AS-2020 */
typedef struct {
    bool valid;
    void *port_ctx;
} t_lip_ptp_last_sync_info;

typedef struct {
    t_ltimer rx_tout_tmr;
    t_lip_ptp_last_sync_info last_info;    
} t_lip_ptp_site_sync;

#endif // LIP_PTP_SITE_SYNC_DEFS_H
