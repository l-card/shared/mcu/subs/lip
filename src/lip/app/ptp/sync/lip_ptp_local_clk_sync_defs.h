#ifndef LIP_PTP_LOCAL_CLK_SYNC_DEFS_H
#define LIP_PTP_LOCAL_CLK_SYNC_DEFS_H

#include "lip/app/ptp/lip_ptp_defs.h"
#include "lip/app/ptp/lip_ptp_cfg_defs.h"

typedef struct {
    t_lip_ptp_tstmp syn_rx_gm;
    t_lip_ptp_tstmp syn_rx_local;

    t_lip_ptp_tstmp local_clk_offs; /* начальная разница между локальным клоком и gm в виде полной метки */
    int      clk_offs_sign;       /* знак разницы clk_offs_init. Если > 0, то локальный клок опережает GM */
    //t_lip_fns   clk_offs_cor; /* усредненное текущее отклонение разницы локального клока и gm от clk_offs_init */
    //uint8_t     cur_avg_cnt; /* выполненное на данном этапе количество измерений для усреденнения */
    bool     clk_offs_valid;
} t_lip_ptp_local_clk_ctx;

#endif // LIP_PTP_LOCAL_CLK_SYNC_DEFS_H

