#include "lip/app/ptp/lip_ptp_cfg_defs.h"
#if LIP_PTP_ENABLE
#include "lip/app/ptp/lip_ptp_internal.h"
#include "lip/app/ptp/sync/lip_ptp_site_sync.h"
#include "lip/app/ptp/md/eth/sync/lip_ptp_md_eth_sync_send.h"


void lip_ptp_prot_sync_process_rx(t_lip_ptp_port_ctx *p, const t_lip_ptp_port_md_sync_info *rxinfo,
                                     const t_lip_ptp_tstmp *sync_rx_tstmp, const t_lip_ptp_port_identity *src_port_id) {
    /* обработка SYNC имеет смысл только для SLAVE порта */
    if (p->stats.state == LIP_PTP_PORT_STATE_SLAVE) {
        t_lip_ptp_inst_ctx *inst = f_ptp_port_inst_ctx(p);
        /* проверяем соответствие srcPortID текущему вектору */
        if (lip_ptp_gm_is_present(inst) && lip_ptp_port_id_is_equal(&inst->bmca.gm_prio.sourcePortIdentity, src_port_id)) {
            lip_ptp_site_sync_proc_rx(inst, p, rxinfo, sync_rx_tstmp);
        }
    }
}

void lip_ptp_prot_sync_tx(t_lip_ptp_port_ctx *p, const t_lip_ptp_port_md_sync_info *txinfo) {
    /* 10.2.12.2.1 setMDSync() */
    lip_ptp_md_eth_send_sync(p, txinfo);
}

#endif
