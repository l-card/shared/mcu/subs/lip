#ifndef LIP_PTP_LOCAL_CLK_SYNC_H
#define LIP_PTP_LOCAL_CLK_SYNC_H

/* Функции, отвечающие за работу localClock и его синхронизацию с GM. Сам local clock
   не выполняет подстройку, вместо этого расчитывается смещение и rateRatio относительно GM
   для пересчета времени относиетльно localClock во время GM */

#include "lip/app/ptp/lip_ptp_internal.h"



/* Сброс и инициализация синхронизации. Должно вызываться как вначале, так и при смене GM */
void lip_ptp_local_clk_sync_init(t_lip_ptp_inst_ctx *inst);
/* Обновление информации о синхронизации localClock и GM.
 * На основе меток времени одного события относительно локального клока и GM идет вычисление
 * смещения и rateRatio локального клока относительно GM, которое используется для
 * возможности пересчитать localClock в GM */
void lip_ptp_local_clk_sync_update(t_lip_ptp_inst_ctx *inst, const t_lip_ptp_tstmp *loc, const t_lip_ptp_tstmp *rem);

/* Функция возвращает значение MasterTime по времени локального клока */
void lip_ptp_local_clk_sync_to_gm(t_lip_ptp_inst_ctx *inst, const t_lip_ptp_tstmp *loc, t_lip_ptp_tstmp *res);
void lip_ptp_local_clk_sync_from_gm(t_lip_ptp_inst_ctx *inst, const t_lip_ptp_tstmp *gm, t_lip_ptp_tstmp *loc);

#endif // LIP_PTP_LOCAL_CLK_SYNC_H
