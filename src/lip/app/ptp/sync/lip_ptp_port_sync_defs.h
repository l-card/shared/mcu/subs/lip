#ifndef LIP_PTP_PORT_SYNC_DEFS_H
#define LIP_PTP_PORT_SYNC_DEFS_H

#include "lip/app/ptp/lip_ptp_proto_defs.h"
#include "lip/app/ptp/lip_ptp_defs.h"

/* Информация о принятом sync/followUp от MD = MDSyncReceive - 10.2.2.2 IEEE 802.1AS-2020 */
typedef struct {
    int8_t                  logMessageInterval; /* log2 интервала передачи SYNC сообщений, соответствует полю сообщения Sync */
    uint8_t                 rateRatioValid;     /* признак действительности rateRatio */
    t_lip_fns               fupCorrection;      /* поля коррекции времени в 1/2^16 мс (компенсирует разницу от момента посылки GM до момента посылки соседним узлом) */
    t_lip_ptp_pkt_tstmp     preciseOriginTimestamp; /* метка времени в сообщении sync. соответствует времени посылки Sync от GM */
    t_lip_ptp_tstmp         upstreamTxTime; /* вычисленное время передачи пакета соседним узлом пересчитанное на локальный клок текущего узла */
    double                  rateRatio;  /* отношение частоты GM к частоте локального клока узла, передающего SYNC */
    t_lip_ptp_last_time_change_info lastChange; /* информация о последнем разрыве времени */
} t_lip_ptp_port_md_sync_info;



#endif // LIP_PTP_PORT_SYNC_DEFS_H
