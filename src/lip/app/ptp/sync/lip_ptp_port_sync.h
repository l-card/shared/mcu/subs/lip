#ifndef LIP_PTP_PORT_SYN_H
#define LIP_PTP_PORT_SYN_H

#include "lip/app/ptp/lip_ptp_internal.h"

/* Логика обработки приема/передачи информации синхронизации в виде
 * структуры #t_lip_ptp_port_md_sync_info с Media-Dependent  уровнем для каждого порта.
 * Выполняет дополнительные проверки, не зависящие от используемого Media-Dependent
 * уровня и дальнейшую передачу для анализа на уровне узла в site_sync */

/* Обработка принятой информации синхронизации от Media-Dependent уровня
   @param[in] p             - контекст локального порта, по которому принята информация о синхронизации
   @param[in] rxinfo        - информация о синхронизации
   @param[in] sync_rx_tstmp - метка времени относительно local clock приема sync сообщения
   @param[in] src_port_id   - идентификатор порта уделенной строны, которая передала информацию sync
*/
void lip_ptp_prot_sync_process_rx(t_lip_ptp_port_ctx *p, const t_lip_ptp_port_md_sync_info *rxinfo,
                                     const t_lip_ptp_tstmp *sync_rx_tstmp, const t_lip_ptp_port_identity *src_port_id);
/* Запрос на посылку информации о синхронизации времени по порту. Выполняет
 * дальнейший запрос на MediaDependent уровень */
void lip_ptp_prot_sync_tx(t_lip_ptp_port_ctx *p, const t_lip_ptp_port_md_sync_info *txinfo);


#endif // LIP_PTP_PORT_SYN_H
