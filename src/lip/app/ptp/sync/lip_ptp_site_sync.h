#ifndef LIP_PTP_SITE_SYNC_H
#define LIP_PTP_SITE_SYNC_H

#include "lip/app/ptp/lip_ptp_internal.h"


void lip_ptp_site_sync_init(t_lip_ptp_inst_ctx *inst);
void lip_ptp_site_sync_pull(t_lip_ptp_inst_ctx *inst);
void lip_ptp_site_sync_tx(t_lip_ptp_inst_ctx *inst, const t_lip_ptp_port_md_sync_info *txinfo);
void lip_ptp_site_sync_send_master(t_lip_ptp_inst_ctx *inst, const t_lip_ptp_port_md_sync_info *txinfo);
void lip_ptp_site_sync_gm_change(t_lip_ptp_inst_ctx *inst);
void lip_ptp_site_sync_proc_rx(t_lip_ptp_inst_ctx *inst, t_lip_ptp_port_ctx *p,
                                  const t_lip_ptp_port_md_sync_info *rxinfo, const t_lip_ptp_tstmp *sync_rx_tstmp);

#endif // LIP_PTP_SITE_SYNC_H
