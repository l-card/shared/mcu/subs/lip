#ifndef LIP_PTP_MASTER_SYNC_SEND_H
#define LIP_PTP_MASTER_SYNC_SEND_H
#include "lip/app/ptp/lip_ptp_internal.h"

/* Автомат отвечает за запросы на посылку SYNC в случае, если текущий узел
 * является мастером.
 * В текущей реализации в этом случае источником времени всегда является
 * локальный клок (интерфейс для получения master time отличного от local clock
 * сейчас не реализован) */

void lip_ptp_master_sync_init(t_lip_ptp_inst_ctx *inst);
void lip_ptp_master_sync_pull(t_lip_ptp_inst_ctx *inst);
/* Разрешение/запрет передачи SYNC сообщений. Вызывается BMCA в зависимости
 * от результата разрешения кто является GM (теущий узел (true) или удаленный (false)) */
void lip_ptp_master_sync_send_enable(t_lip_ptp_inst_ctx *inst, bool en);


#endif // LIP_PTP_MASTER_SYNC_SEND_H
