#ifndef LIP_PTP_MASTER_SYNC_SEND_DEFS_H
#define LIP_PTP_MASTER_SYNC_SEND_DEFS_H

#include "ltimer.h"
#include <stdint.h>

typedef enum {
    LIP_PTP_MASTER_SYNC_SEND_STATE_OFF = 0,
    LIP_PTP_MASTER_SYNC_SEND_STATE_INIT,
    LIP_PTP_MASTER_SYNC_SEND_STATE_ACTIVE
} t_lip_ptp_master_sync_send_state;

typedef struct {
    t_lip_ptp_master_sync_send_state state;
    t_ltimer tx_interval_tmr;
    int8_t   tx_log_interval; /* log по степени 2 для интервала передачи signaling */
} t_lip_ptp_master_sync_ctx;

#endif // LIP_PTP_MASTER_SYNC_SEND_DEFS_H
