#ifndef LIP_PTP_PORT_GPTCAP_H
#define LIP_PTP_PORT_GPTCAP_H

#include "lip/app/ptp/lip_ptp_internal.h"

/* Автомат отвечающий за посылку и прием Signaling сообщений для порта, включающих
 * информацию с GPTP Capable TLV для проверки, поддерживает ли соседний узел
 * gPTP вариант реализации PTP */

void lip_ptp_port_gptp_cap_init(t_lip_ptp_port_ctx *p);
void lip_ptp_port_gptp_cap_pull(t_lip_ptp_port_ctx *p);
void lip_ptp_port_gptp_cap_down(t_lip_ptp_port_ctx *p);
/* обработка принятого signaling сообщения */
void lip_ptp_port_proc_signaling(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);

#endif // LIP_PTP_PORT_GPTCAP_H
