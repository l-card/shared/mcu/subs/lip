#ifndef LIP_PTP_PORT_GPTCAP_DEFS_H
#define LIP_PTP_PORT_GPTCAP_DEFS_H

#include "ltimer.h"
#include <stdint.h>

typedef struct {
    t_ltimer rcv_tout_tmr;
    t_ltimer tx_interval_tmr;
    int8_t   tx_log_interval; /* log по степени 2 для интервала передачи signaling */
    uint16_t tx_seq_id; /* sequence id последнего переданного signaling */
} t_lip_ptp_port_gptp_cap_ctx;

#endif // LIP_PTP_PORT_GPTCAP_DEFS_H
