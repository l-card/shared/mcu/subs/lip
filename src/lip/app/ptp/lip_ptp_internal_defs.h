#ifndef LIP_PTP_INTERNAL_DEFS_H
#define LIP_PTP_INTERNAL_DEFS_H

#include "lip_ptp_proto_defs.h"
#include "lip_ptp_cfg_defs.h"



typedef struct {
    uint16_t   steps_cnt;
    t_lip_ptp_clock_identity identities[LIP_PTP_PATH_TRACE_STEP_MAX];
} t_lip_ptp_path_trace_info;


#endif // LIP_PTP_INTERNAL_DEFS_H
