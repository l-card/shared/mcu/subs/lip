#ifndef LIP_PTP_CFG_DEFS_H
#define LIP_PTP_CFG_DEFS_H

#include "lip_config.h"

#ifdef LIP_CFG_PTP_ENABLE
    #define LIP_PTP_ENABLE          LIP_CFG_PTP_ENABLE
#else
    #define LIP_PTP_ENABLE          0
#endif

#ifdef LIP_CFG_PTP_RX_QUEUE_NUM
    #define LIP_PTP_RX_QUEUE_NUM    LIP_CFG_PTP_RX_QUEUE_NUM
#else
    #define LIP_PTP_RX_QUEUE_NUM    0
#endif

#ifdef LIP_CFG_PTP_DOMAIN_NUM
    #define LIP_PTP_DOMAIN_NUM      LIP_CFG_PTP_DOMAIN_NUM
#else
    #define LIP_PTP_DOMAIN_NUM      0
#endif

#define LIP_PTP_CLOCKID_DEFAULT_SUFFIX      0x0000


#define LIP_PTP_EVENT_PKT_PRIO  LIP_PKT_PRIO_VHIGH
#define LIP_PTP_GENERAL_PKT_PRIO LIP_PKT_PRIO_VHIGH

#define LIP_PTP_PATH_TRACE_STEP_MAX             179 /* Максимальное число узлов в TLV PATH_TRACE используемой в Announce. 179 - макс. для пакета Ethernet, т.к. 179*8+68 = 1500 */
#define LIP_PTP_ANNOUNCE_OPT_SIZE_MAX           1432

#define LIP_PTP_UTC_OFFSET_DEFAULT              37

#define LIP_PTP_SYSTEM_PRIO1_DEFAULT            LIP_PTP_SYSTEM_PRIO1_TRANSIENT

#define LIP_PTP_ANNOUNCE_STEPS_MAX              255 /* Определяет максимальную удаленность от GrandMaseter (255 по IEEE 802.1AS-2020) */

#define LIP_PTP_ANNOUNCE_RECV_INTERVALS_TOUT    3 /**< Количество Announce Interval, после которого происходит таймаут ожидания Announce, IEEE 802.1AS-2020 10.7.3.2 */
#define LIP_PTP_ANNOUNCE_LOG_INTERVAL           0 /**< IEEE 802.1AS-2020 10.7.2.2  */

#define LIP_PTP_GPTCAP_RECV_INTERVALS_TOUT      9 /**< Количество интервалов gPTP-capable без gPTP-capable-TLV, после которого считается что узел не поддерживает gPTP - IEEE 802.1AS-2020 10.7.3.3 */
#define LIP_PTP_GPTCAP_LOG_INTERVAL             0 /** IEEE 802.1AS-2020 10.7.2.5  */

#define LIP_PTP_SYNC_RECV_INTERVALS_TOUT        3 /**< Количество Sync Interval без SYNC, после чего считается что мастер больше недоступен, IEEE 802.1AS-2020 10.7.3.1 */
#define LIP_PTP_SYNC_LOG_INTERVAL               -3 /** Для ethernet - IEEE 802.1AS-2020 11.5.2.3 */
#define LIP_PTP_SYNC_GM_SEND_WAIT_TIME          3000 /**< Время ожидания после назначения GM до начала посылки sync в мс */

/** Запрет учета поля коррекции метки для PDelayReq при 2-step измерении.
    Используется из-за errata в KSZ8567, требующая включать 1-step режим
    даже при использовании 1-step, что приводит к изменению поля коррекции
    в PDelayReq, которое должно оставться 0 */
#define LIP_PTP_PDELAY_REQ_2STEP_COR_DISABLE    1

#define LIP_PTP_NB_RATE_RATIO_MAX_PPM           200

#define LIP_PTP_PDELAY_REQ_LOG_INTERVAL         0 /** IEEE 802.1AS-2020 11.5.2.2 */
#define LIP_PTP_PDELAY_ALLOW_LOST_RESP          9  /** Количество PDelayReq без ответа, после которого считается, что измерение peer delay невозможно (802.1AS-2020 11.5.2.4) */
#define LIP_PTP_PDELAY_ALLOW_FAULTS             9  /** Счетчик условий, что измеренная задержка больше порога, или нельзя вычислить neighborRation, после которого asCapable = 0 (802.1AS-2020 11.5.2.4) */

#define LIP_PTP_PDELAY_TIME_AVG_CNT             8  /** Количество измерений pdelay, используемых для усреднения */
#define LIP_PTP_PDELAY_TIME_AVG_APLPHA          (0.882496976787) /** коэффициент фильтра для усреденения pdelay (e ^ (-1/CNT)) */
#define LIP_PTP_PDELAY_TIME_THRESH_NS           8000 /** Порог действительного значения pdelay. Вообще должен быть 800 по IEEE 802.1AS-2020, но не укладывается... */
#define LIP_PTP_RATE_RATIO_INTERVAL_MIN_CNT     3 /** Минимальное колечество обменов для начала расчета rateRatio */
#define LIP_PTP_RATE_RATIO_INTERVAL_CNT         8 /** Длительность интервала (в количестве обменов) для измерения соотношения времен для rateRatio */


#endif // LIP_PTP_CFG_DEFS_H
