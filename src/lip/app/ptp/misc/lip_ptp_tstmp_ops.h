#ifndef LIP_PTP_TSTMP_OPS_H
#define LIP_PTP_TSTMP_OPS_H

#include "lip/app/ptp/lip_ptp_defs.h"
#include "lip/app/ptp/lip_ptp_proto_defs.h"
#include "lip/lip_defs.h"

static LINLINE void f_pkt_tstmp_to_host(const t_lip_ptp_pkt_tstmp *pkt_tstmp, t_lip_ptp_tstmp *host_tsmp) {
    host_tsmp->hsec = NTOH16(pkt_tstmp->sec_h);
    host_tsmp->sec  = NTOH32(pkt_tstmp->sec_l);
    host_tsmp->ns   = NTOH32(pkt_tstmp->ns);
    host_tsmp->fract_ns = 0;

}

static LINLINE void f_host_tstmp_to_pkt(const t_lip_ptp_tstmp *host_tsmp, t_lip_ptp_pkt_tstmp *pkt_tstmp) {
    pkt_tstmp->sec_h = HTON16(host_tsmp->hsec);
    pkt_tstmp->sec_l = HTON32(host_tsmp->sec);
    pkt_tstmp->ns = HTON32(host_tsmp->ns);
}

void lip_ptp_tstmp_add_ns(t_lip_ptp_tstmp *tsmp, int32_t ns);
t_lip_fns lip_ptp_tstmp_delta_fns(const t_lip_ptp_tstmp *t1, const t_lip_ptp_tstmp *t2);
void lip_ptp_tstmp_add_fns(t_lip_ptp_tstmp *tstmp, t_lip_fns fns);
int lip_ptp_tstmp_cmp(const t_lip_ptp_tstmp *t1, const t_lip_ptp_tstmp *t2);
void lip_ptp_tstmp_sub(t_lip_ptp_tstmp *t_res, const t_lip_ptp_tstmp *t1, const t_lip_ptp_tstmp *t2);
void lip_ptp_tstmp_add(t_lip_ptp_tstmp *t_res, const t_lip_ptp_tstmp *t1, const t_lip_ptp_tstmp *t2);


#endif // LIP_PTP_TSTMP_OPS_H
