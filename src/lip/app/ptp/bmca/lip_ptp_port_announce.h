#ifndef LIP_PTP_PORT_ANNOUNCE_H
#define LIP_PTP_PORT_ANNOUNCE_H

#include "lip/app/ptp/lip_ptp_internal.h"

/* Данный автомат отвечает за прием и передачу Announce сообщений отдельно для каждого порта.
   Для порта хранится priority-vector и сравнивается с msg-priority на основе принятого
   Announce. В случае более приоритетного вектора ставится запрос на обновление bmca
   с распределением ролей.
   Также обрабатываются TLV для принятого Announce и сохраняются для последующей
   передачи - сохряняются TLV, которые должны передаваться без изменений, а для
   PathTrace добавляется собственный ClockID. Для экономия места хранится только
   одна копия TLV для всех портов и сохряняется только если порт Slave или
   пришел более приоритетный priority-vector.

   Передача Announce выполняется только если порт в состоянии master. В Announce
   включается обновленный PathTrace TLV и возможно другие TLV для передачи без изменений.

 */


void lip_ptp_port_announce_init(t_lip_ptp_port_ctx *p);
void lip_ptp_port_announce_down(t_lip_ptp_port_ctx *p);
void lip_ptp_port_announce_pull(t_lip_ptp_port_ctx *p);
/* Обновление вектора для текущего порта. Вызывается при выполнении алгоритма BMCA */
void lip_ptp_port_announce_prio_update(t_lip_ptp_port_ctx *p, const t_lip_ptp_bmca_prio_vector *master_prio);
/* обработка пришедшего сообщения Announce */
void lip_ptp_port_proc_announce(t_lip_ptp_port_ctx *p, t_lip_payload *pl, const t_lip_ptp_rx_frame *frame);

#endif // LIP_PTP_PORT_ANNOUNCE_H
