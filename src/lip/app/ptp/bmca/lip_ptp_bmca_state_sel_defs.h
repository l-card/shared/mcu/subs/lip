#ifndef LIP_PTP_BMCA_STATE_SEL_DEFS_H
#define LIP_PTP_BMCA_STATE_SEL_DEFS_H

#include "lip/app/ptp/lip_ptp_internal_defs.h"

#define LIP_PTP_ANNONCE_SYS_OPT_SIZE  (4 + LIP_PTP_CLOCK_IDENTITY_SIZE)


typedef struct {
    bool update_req; /* Пользовательский запрос для перевычисления состояний по BMCA */
    t_lip_ptp_bmca_prio_vector gm_prio; /* вычисленный вектор для GM */
    uint16_t maseter_steps_removed_be; /* masterStepsRemoved (10.3.9.3) - шагов до мастера, включая себя
                                         (за счет последнего может отличаться от stepsRemoved порта) */

    /* Указатель на опции и их размер для передачи в Announce.
     * Могут указывать как на ann_sys_opts*, если сами gm, так и на
     * ann_rx_opts* - на информацию принятого ann по slave */
    uint16_t *ann_tx_opts_size_ptr;
    uint8_t  *ann_tx_opts_ptr;

    /* Опции в announce для случая, если сами являемся GM.
     * Включает path-trace из одного элемента */
    uint16_t ann_sys_opts_size;
    uint8_t  ann_sys_opts[LIP_PTP_ANNONCE_SYS_OPT_SIZE];

    /* Опции, созданные на основе принятых от annpunce удаленного мастера.
       Для экономия места используется только один буфер и сохраняются только
       от того порта, от которого вектор лучше */
    void     *ann_rx_opts_port;
    uint16_t ann_rx_opts_size;
    uint8_t  ann_rx_opts[LIP_PTP_ANNOUNCE_OPT_SIZE_MAX];
} t_lip_ptp_bcma_state_sel_ctx;

#endif // LIP_PTP_BMCA_STATE_SEL_DEFS_H
