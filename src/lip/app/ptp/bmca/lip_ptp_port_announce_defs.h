#ifndef LIP_PTP_PORT_ANNOUNCE_DEFS_H
#define LIP_PTP_PORT_ANNOUNCE_DEFS_H

#include "ltimer.h"
#include "lip/app/ptp/lip_ptp.h"

typedef enum {
    LIP_PTP_PORT_ANNOUNCE_INFO_STATE_DISABLED, /* Порт запрещен */
    LIP_PTP_PORT_ANNOUNCE_INFO_STATE_AGED, /* Произошел таймаут на Announce или Sync */
    LIP_PTP_PORT_ANNOUNCE_INFO_STATE_MINE, /* Информация от собственного slave-порта или GM */
    LIP_PTP_PORT_ANNOUNCE_INFO_STATE_RECEIVED, /* Информация принята от удаленного мастера */
} t_lip_ptp_port_announce_info_state;



typedef struct {
    t_ltimer rcv_ann_tout_tmr;
    t_ltimer tx_interval_tmr;
    int8_t   tx_log_interval; /* log по степени 2 для интервала передачи Announce */
    bool     tx_new_info; /*  признак, что обновилась информация и нужно послать новый Announce */        
    uint16_t seq_id; /* sequence id последнего переданного Announce */
    t_lip_ptp_port_announce_info_state info_state; /* =infoIs 10.3.10.4 IEEE 802.1AS-2020 */

    t_lip_ptp_bmca_prio_vector port_prio; /* port priority vector (10.3.10.11 IEEE 802.1AS-2020) */
    t_lip_ptp_bmca_prio_vector master_prio; /* port master priority vector ( IEEE 802.1AS-2020) */
    t_lip_ptp_time_info ann_time_info; /* информация о источнике времени принятого announce */
} t_lip_ptp_port_announce_ctx;

#endif // LIP_PTP_PORT_ANNOUNCE_DEFS_H
