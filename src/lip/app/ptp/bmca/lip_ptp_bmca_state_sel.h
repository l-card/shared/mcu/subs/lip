#ifndef LIP_PTP_BMCA_STATE_SEL_H
#define LIP_PTP_BMCA_STATE_SEL_H

#include "lip/app/ptp/lip_ptp_internal.h"

/* Реализация алгоритма определения лучшего кандидата на роль GM  в сети (BMCA).
 * На основе принятых от других портов сообщений Announce и своего системного
 * вектора приоритета, определяется режим работы каждого порта */

void lip_ptp_bmca_state_sel_init(t_lip_ptp_inst_ctx *inst);
void lip_ptp_bmca_state_sel_pull(t_lip_ptp_inst_ctx *inst);
/* Запрос на выполнение алгоритма BMCA с переопределением состояни портов.
 * Вызывается как при изменение данных, определящих приоритет текущего узла,
 * при разрешении/запрете портов и при изменении вектора приоритета у портов */
void lip_ptp_bmca_state_sel_update_req(t_lip_ptp_inst_ctx *inst);
/* Функция вызывается при смене clockid текущего узла */
void lip_ptp_bmca_state_sel_system_clkid_update(t_lip_ptp_inst_ctx *inst);


#endif // LIP_PTP_BMCA_STATE_SEL_H
