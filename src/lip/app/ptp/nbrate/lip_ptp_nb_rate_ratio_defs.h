#ifndef LIP_PTP_NB_RATE_RATIO_DEFS_H
#define LIP_PTP_NB_RATE_RATIO_DEFS_H

#include "lip/app/ptp/lip_ptp_defs.h"
#include "lip/app/ptp/lip_ptp_cfg_defs.h"

/** Источник данных для обновления отношения частот */
typedef enum {
    LIP_PTP_RATE_RATIO_SRC_PDELAY = 1, /**< Используются времена из pDelayResp/pDelayFup. Штатный вариант для IEEE 802.1AS-2020.
                                            Используется, если в Fup передаются полные метки (в противном случае информации недостаточно) */
    LIP_PTP_RATE_RATIO_SRC_SYNC   = 2  /**< Используются времена из Sync/FollowUp. Используется только если нельзя использовать
                                            pDelayResp/pDelayFup из-за того, что удаленная строна передает только разницу времен,
                                            а не сами метки. Возможно только для мастера клока */
} t_ptp_nb_rate_ratio_src;


/** Информация о одной точке измерения для обновление значения отношения частот (rateRatio) */
typedef struct {
    t_lip_ptp_tstmp t_rem; /**< время события по клоку удаленного узла */
    t_lip_ptp_tstmp t_loc; /**< время события по клоку локального узла */
    t_lip_fns t_rem_cor; /**< дополнительная коррекция времени t_rem в fract ns */
} t_ptp_nb_rate_ratio_tsmp;

/** Текущие данные по подсчету соотношения частот своего клока и соседа */
typedef struct {
    uint8_t tstms_cnt; /**< Количество действительных накопленных точек соотношения времен в массиве tstmps */
    uint8_t tstms_pos; /**< Позиция в массиве tstmps, куда будет сохранены следующие данные */
    uint8_t src;       /**< Признак из #t_ptp_nb_rate_ratio_src, что является источником для рассчета соотношения частот */
    t_ptp_nb_rate_ratio_tsmp tstmps[LIP_PTP_RATE_RATIO_INTERVAL_CNT]; /**< Сохраненный циклический буфер с соотношением меток времени */
} t_lip_ptp_nb_rate_ratio_calc_ctx;

#endif // LIP_PTP_NB_RATE_RATIO_DEFS_H
