#ifndef LIP_PTP_NB_RATE_RATIO_H
#define LIP_PTP_NB_RATE_RATIO_H

#include "lip/app/ptp/lip_ptp_internal.h"



void lip_ptp_nb_rate_ratio_init(t_lip_ptp_port_ctx *p);
void lip_ptp_nb_rate_ratio_calc(t_lip_ptp_port_ctx *p, const t_ptp_nb_rate_ratio_tsmp *tstmp);

#endif // LIP_PTP_NB_RATE_RATIO_H
