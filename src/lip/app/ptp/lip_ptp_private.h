 #ifndef LIP_PTP_PRIVATE_H
#define LIP_PTP_PRIVATE_H

/* Данный файл содержит функции, которые используются другими модулями стека и не
 * предназначены для прямого вызова из приложения */

#include "lip/lip_defs.h"
#include "lip_ptp_proto_defs.h"
#include "lip/link/lip_eth_private.h"
#include "lip/ll/lip_ll.h"

void lip_ptp_init(void);
void lip_ptp_close(void);
void lip_ptp_pull(void);
/* Функция должна вызываться при смене mac для того, чтобы стек обновил ClockID  */
void lip_ptp_mac_addr_update(const uint8_t *mac);
/* Фуенкция вызывается при изменении состояния подключения линка для порта с указанным
 * номером */
void lip_ptp_port_link_changed(uint8_t port_idx, bool on);

/* Обработка отметки времени по переданному PTP пакету. Должна вызываться при ее
 * появлении от соответствующей аппаратной части.
 * Для некоторых случаев она может быть доступна по завершению передачи пакета,
 * в этом случае порт возвращает ее в статусе завершенного пакета Т.к. в некоторых случаях доступна позже, чем покет ушел, нельзя
 * полагаться на статус пакета и lip_ptp_proc_egress_tstmp() вызывается автоматом при обработке завершения
 * в lip_mac_tx.
 * Однако для некоторой аппаратуры метки могут получаться по отдельному интерфейсу
 * (например при генерации меток во внешнем PHY или свиче) независимо от завершения
 * передачи и в этом случае данная функция должна вызываться явно по завершению
 * чтения метки */
void lip_ptp_proc_egress_tstmp(uint8_t port_idx, uint8_t msgtype, t_lip_ptp_tstmp *tstmp);
/* Обработка принятого пакета */
t_lip_errs lip_ptp_process_packet(t_lip_payload *pl, const t_lip_eth_rx_packet *eth_packet);



#endif // LIP_PTP_PRIVATE_H
