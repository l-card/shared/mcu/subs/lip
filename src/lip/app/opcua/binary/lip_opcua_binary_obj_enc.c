#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_binary_obj_enc.h"
#include "lip_opcua_binary_types_procbuf.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_struct.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_enum.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_server_status.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_buildinfo.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_range.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_complex_num.h"
#include "lip/app/opcua/server/tree/items/types/groups/lip_opcua_server_tree_std_types_da_axis.h"

static const t_lip_opcua_obj_enc_descr f_obj_enc_list[] = {
    {&g_lip_opcua_tree_std_item_datatype_struct_def.nodeid, &g_lip_opcua_tree_std_item_datatype_struct_def_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_struct_def},
    {&g_lip_opcua_tree_std_item_datatype_server_status.nodeid, &g_lip_opcua_tree_std_item_datatype_server_status_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_server_status},
    {&g_lip_opcua_tree_std_item_datatype_enum_value.nodeid, &g_lip_opcua_tree_std_item_datatype_enum_value_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_enum_value},
    {&g_lip_opcua_tree_std_item_datatype_build_info.nodeid, &g_lip_opcua_tree_std_item_datatype_build_info_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_build_info},
    {&g_lip_opcua_tree_std_item_datatype_range.nodeid, &g_lip_opcua_tree_std_item_datatype_range_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_da_range},
    {&g_lip_opcua_tree_std_item_datatype_eu_info.nodeid, &g_lip_opcua_tree_std_item_datatype_eu_info_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_da_eu_info},
    {&g_lip_opcua_tree_std_item_datatype_complex_num.nodeid, &g_lip_opcua_tree_std_item_datatype_complex_num_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_da_complex_num},
    {&g_lip_opcua_tree_std_item_datatype_double_complex_num.nodeid, &g_lip_opcua_tree_std_item_datatype_double_complex_num_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_da_double_complex_num},
    {&g_lip_opcua_tree_std_item_datatype_axis_info.nodeid, &g_lip_opcua_tree_std_item_datatype_axis_info_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_da_axis_info},
    {&g_lip_opcua_tree_std_item_datatype_xv.nodeid, &g_lip_opcua_tree_std_item_datatype_xv_enc_bin.nodeid, (t_lip_opcua_obj_enc_func_put)lip_opcua_procbuf_put_da_xv},
};

const t_lip_opcua_obj_enc_descr *lip_opcua_obj_get_enc(const t_lip_opcua_nodeid *objid) {
    const t_lip_opcua_obj_enc_descr *fnd_enc_descr = NULL;
    for (unsigned idx = 0; (idx < sizeof(f_obj_enc_list)/sizeof(f_obj_enc_list[0])) && !fnd_enc_descr; ++idx) {
        const t_lip_opcua_obj_enc_descr *enc_descr = &f_obj_enc_list[idx];
        if (lip_opcua_nodeid_is_eq(enc_descr->obj_id, objid)) {
            fnd_enc_descr = enc_descr;
        }
    }

    return fnd_enc_descr;
}

#endif
