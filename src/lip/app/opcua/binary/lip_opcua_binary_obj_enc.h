#ifndef LIP_OPCUA_BINARY_OBJ_ENC_H
#define LIP_OPCUA_BINARY_OBJ_ENC_H

#include "lip/app/opcua/types/lip_opcua_nodeid.h"
#include "lip/app/opcua/types/lip_opcua_base_types.h"

struct st_lip_proc_buf;
struct st_lip_opcua_session_ctx;

typedef t_lip_opcua_err (*t_lip_opcua_obj_enc_func_put)(struct st_lip_proc_buf *buf, const void *struct_def, struct st_lip_opcua_session_ctx *session);

typedef struct {
    const t_lip_opcua_nodeid *obj_id;
    const t_lip_opcua_nodeid *enc_id;
    t_lip_opcua_obj_enc_func_put put;
} t_lip_opcua_obj_enc_descr;

const t_lip_opcua_obj_enc_descr *lip_opcua_obj_get_enc(const t_lip_opcua_nodeid *objid);


#endif // LIP_OPCUA_BINARY_OBJ_ENC_H
