#ifndef LIP_OPCUA_BINARY_PROTO_DEFS_H
#define LIP_OPCUA_BINARY_PROTO_DEFS_H

/* Варианты кодирования nodeid */
#define LIP_OPCUA_NODEID_ENCTYPE_2BYTE                  0x00 /* A numeric value that fits into the two-byte representation. */
#define LIP_OPCUA_NODEID_ENCTYPE_4BYTE                  0x01 /* A numeric value that fits into the four-byte representation. */
#define LIP_OPCUA_NODEID_ENCTYPE_NUMERIC                0x02 /* A numeric value that does not fit into the two or four byte representations. */
#define LIP_OPCUA_NODEID_ENCTYPE_STRING                 0x03 /* A String value. */
#define LIP_OPCUA_NODEID_ENCTYPE_GUID                   0x04 /* A Guid value. */
#define LIP_OPCUA_NODEID_ENCTYPE_BYTESTRING             0x05 /* An opaque (ByteString) value. */
#define LIP_OPCUA_NODEID_ENCTYPE_FLAG_NAMESPACE_URI     0x80 /* An opaque (ByteString) value. */
#define LIP_OPCUA_NODEID_ENCTYPE_FLAG_SERVER_INDEX      0x40


/* Варианты кодирования тела для ExtensionObject */
#define LIP_OPCUA_EXTOBJ_BODYENC_NONE                   0x00 /* No body is encoded. */
#define LIP_OPCUA_EXTOBJ_BODYENC_BYTESTR                0x01 /* The body is encoded as a ByteString. */
#define LIP_OPCUA_EXTOBJ_BODYENC_XML                    0x02 /* The body is encoded as an XmlElement. */


/* Флаги наличия полей в теле LocalizedText */
#define LIP_OPCUA_LOCTEXT_ENCBIT_LOCALE                 0x01 /* The locale. */
#define LIP_OPCUA_LOCTEXT_ENCBIT_TEXT                   0x02 /* The text in the specified locale.. */

#define LIP_OPCUA_DATAVALUE_ENCMSK_VALUE                0x01 /* False if the Value is Null. */
#define LIP_OPCUA_DATAVALUE_ENCMSK_STATUS               0x02 /* False if the StatusCode is Good. */
#define LIP_OPCUA_DATAVALUE_ENCMSK_SRC_TSTMP            0x04 /* False if the SourceTimestamp is DateTime.MinValue. */
#define LIP_OPCUA_DATAVALUE_ENCMSK_SRV_TSTMP            0x08 /* False if the ServerTimestamp is DateTime.MinValue. */
#define LIP_OPCUA_DATAVALUE_ENCMSK_SRC_PICOSEC          0x10 /* False if the SourcePicoseconds is not present. */
#define LIP_OPCUA_DATAVALUE_ENCMSK_SRV_PICOSEC          0x20 /* False if the ServerPicoseconds is not present. */

#define LIP_OPCUA_VARIANT_ENCMSK_TYPE                   0x3F /* Built-in Type Id  */
#define LIP_OPCUA_VARIANT_ENCMSK_DIMENSIONS             0x40 /* True if the ArrayDimensions field is encoded.  */
#define LIP_OPCUA_VARIANT_ENCMSK_ARRAY                  0x80 /* True if an array of values is encoded.  */

#endif // LIP_OPCUA_BINARY_PROTO_DEFS_H
