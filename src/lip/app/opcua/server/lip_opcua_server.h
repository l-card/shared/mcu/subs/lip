#ifndef LIP_OPCUA_SERVER_H
#define LIP_OPCUA_SERVER_H

#include "lip/app/opcua/types/lip_opcua_types.h"
#include "lip/app/opcua/lip_opcua_svc_proto_defs.h"
#include "tree/lip_opcua_server_tree.h"
#include "tree/lip_opcua_server_tree_std_items.h"
#include "monitems/lip_opcua_server_monitem.h"

#define LIP_OPCUA_SERVER_TCP_DEFAULT_PORT   4840


struct st_lip_opcua_app_description;

/* issuedTokenType, issuerEndpointUrl, securityPolicyUri - not supported */
typedef struct {
    t_lip_opcua_user_token_type type;
    t_lip_opcua_string_ptr policyId;
} t_lip_opcua_user_token_policy;

typedef struct {
    t_lip_opcua_user_token_policy policy;
    union {
        struct {
            t_lip_opcua_string_ptr name;
            t_lip_opcua_string_ptr passwd;
            t_lip_opcua_string_ptr encAlg;
        } username;
    };
} t_lip_opcua_user_token;


typedef struct {
    bool is_discovery_ep;
    t_lip_opcua_string_ptr epUrl;
    struct st_lip_opcua_app_description *app;
    t_lip_opcua_msg_secmode securityMode;
    t_lip_opcua_string_ptr securityPolicyUri;
    t_lip_opcua_string_ptr transportProfileUri;
    uint8_t securityLevel;
    uint8_t userTokensCnt;
    const t_lip_opcua_user_token_policy *userTokens;
} t_lip_opcua_ep_description;



typedef struct st_lip_opcua_app_description {
    t_lip_opcua_app_type type;
    t_lip_opcua_string_ptr appUri;
    t_lip_opcua_string_ptr productUri;
    t_lip_opcua_loc_string_ptr_list name;
    const t_lip_opcua_ep_description *eps;
    int ep_cnt;
} t_lip_opcua_app_description;


void lip_opcua_server_enable(void);
void lip_opcua_server_disable(void);
bool lip_opcua_server_is_enabled(void);

uint16_t lip_opcua_server_port(void);
void lip_opcua_server_set_port(uint16_t port);

void lip_opcua_server_set_app_uri(const char *uri, int32_t len);
void lip_opcua_server_set_app_name(uint8_t locale_num, const char *name, int32_t len);
void lip_opcua_server_set_product_uri(const char *uri, int32_t len);

void lip_opcua_server_set_manufacturer(const char *name, int32_t len);
void lip_opcua_server_set_sw_name(const char *name, int32_t len);
void lip_opcua_server_set_sw_uri(const char *name, int32_t len);

const t_lip_opcua_app_description *lip_opcua_server_app_descr(void);
int lip_opcua_server_eps_cnt(void);
const t_lip_opcua_ep_description *lip_opcua_server_ep(int idx);


#endif // LIP_OPCUA_SERVER_H
