#ifndef LIP_OPCUA_SERVER_PRIVATE_H
#define LIP_OPCUA_SERVER_PRIVATE_H

#include "lip/net/lip_ip.h"

void lip_opcua_server_init(void);
void lip_opcua_server_pull(void);
void lip_opcua_server_close(void);

void lip_opcua_server_ip_addr_changed(t_lip_ip_type type);

#endif // LIP_OPCUA_SERVER_PRIVATE_H
