#ifndef LIP_OPCUA_SC_MSGPROC_H
#define LIP_OPCUA_SC_MSGPROC_H

#include "lip/app/opcua/types/lip_opcua_types.h"
#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"
#include "lip_opcua_sc.h"

typedef struct {
    uint32_t SequenceNumber;
    uint32_t RequestId;
} t_lip_opcua_sc_seq_hdr;

t_lip_opcua_err lip_opcua_open_hdr_parse(t_lip_proc_buf *buf, t_lip_opcua_sc_seq_hdr *sc_hdr, t_lip_opcua_sc_open_params *params);
t_lip_opcua_err lip_opcua_open_hdr_put(t_lip_proc_buf *buf, const t_lip_opcua_sc_seq_hdr *sc_hdr, const t_lip_opcua_sc_open_params *params, const t_lip_opcua_sc_token *token);

t_lip_opcua_err lip_opcua_sc_open_req_parse(t_lip_proc_buf *buf, t_lip_opcua_sc_open_params *params, uint32_t *proto_ver);
t_lip_opcua_err lip_opcua_sc_open_resp_put(t_lip_proc_buf *buf, uint32_t proto_ver, const t_lip_opcua_sc_token *token, const t_lip_opcua_string_ptr *server_nonce);


t_lip_opcua_err lip_opcua_sc_msg_hdr_parse(t_lip_proc_buf *buf, t_lip_opcua_sc_msg_info *msg);
t_lip_opcua_err lip_opcua_sc_msg_hdr_put(t_lip_proc_buf *buf, struct st_lip_opcua_sc_ctx *sc_ctx, uint32_t RequestId);

#endif // LIP_OPCUA_SC_MSGPROC_H
