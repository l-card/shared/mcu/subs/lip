#ifndef LIP_OPCUA_TREE_ITEM_ACCESS_H
#define LIP_OPCUA_TREE_ITEM_ACCESS_H

#include "lip/app/opcua/lip_opcua_svc_proto_defs.h"
#include "lip/app/opcua/types/lip_opcua_base_types.h"
#include "lip/app/opcua/types/lip_opcua_datavalue.h"

struct st_lip_opcua_tree_item;
struct st_lip_opcua_session_ctx;

typedef struct {
    t_lip_opcua_ret_tstmp_types tstmps;
} t_lip_opcua_item_read_params;

typedef struct {
    t_lip_opcua_string_ptr range;
} t_lip_opcua_item_write_params;


typedef t_lip_opcua_err (*t_lip_opcua_item_cb_read)(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_write)(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_write_params *params, const t_lip_opcua_data_value *val);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_get_access_lvl)(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, uint8_t *acc_level);

typedef struct {
    t_lip_opcua_item_cb_read value_read;
    t_lip_opcua_item_cb_write value_write;
    t_lip_opcua_item_cb_get_access_lvl get_access_lvl;
} t_lip_opcua_acc_cbs;


#endif // LIP_OPCUA_TREE_ITEM_ACCESS_H
