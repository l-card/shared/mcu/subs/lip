#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_enum.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip_opcua_server_tree_std_types_server_status.h"
#include "lip_opcua_server_tree_std_types_da_axis.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "../lip_opcua_server_tree_std_datatypes.h"
#include "../lip_opcua_server_tree_std_datatypes_descr.h"
#include "../lip_opcua_server_tree_reftypes.h"
#include "../lip_opcua_server_tree_std_vartypes.h"
#include "../lip_opcua_server_tree_std_objtypes.h"
#include "../lip_opcua_server_tree_std_names.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../lip_opcua_server_tree_std_var_descr.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"




/* Enumeration */
static const t_lip_opcua_tree_item *f_datatype_enum_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_struct_type,
    &g_lip_opcua_tree_std_item_datatype_server_state,
    &g_lip_opcua_tree_std_item_datatype_axis_scale,
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_enum_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_base) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_enum_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_enum = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ENUMERATION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Enumeration"),
    .refs = f_datatype_enum_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};


/* EnumDefinition */
static const t_lip_opcua_tree_item *f_datatype_enum_def_subtypes[] = {

};
static const t_lip_opcua_tree_item_ref_descr f_datatype_enum_def_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_datatype_def) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_enum_def_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_enum_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ENUM_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EnumDefinition"),
    .refs = f_datatype_enum_def_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};




/* EnumValueType */
static const t_lip_opcua_struct_field f_datatype_enum_value_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("Value", g_lip_opcua_tree_std_data_descr_int64),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("DisplayName", g_lip_opcua_tree_std_data_descr_localized_text),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("Description", g_lip_opcua_tree_std_data_descr_localized_text),
};

static const t_lip_opcua_struct_def f_datatype_enum_value_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_enum_value_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_enum_value_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_enum_value_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_enum_value_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_enum_value_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_enum_value_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_enum_value = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ENUM_VALUE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EnumValueType"),
    .refs = f_datatype_enum_value_refs,
    .class_descr_hdr = &f_datatype_enum_value_descr.hdr
};

/* EnumValueType Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_enum_value_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_enum_value)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_enum_value_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ENUM_VALUE_TYPE_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_enum_value_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};












/* StructureType EnumStrings */
static const t_lip_opcua_loc_string_ptr_list f_struct_type_enum_strings_val[] = {
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Structure")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("StructureWithOptionalFields")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Union")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("StructureWithSubtypedValues")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("UnionWithSubtypedValues")}
};


static const t_lip_opcua_tree_item_ref_descr f_struct_type_enum_strings_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_datatype_struct_type) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_struct_type_enum_strings = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE_TYPE_ENUM_STRINGS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_ENUM_STRINGS),
    .refs = f_struct_type_enum_strings_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_LOCTEXT_ARR(f_struct_type_enum_strings_val)
};


/* StructureType */
static const t_lip_opcua_tree_item_ref_descr f_datatype_struct_type_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_enum) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_struct_type_enum_strings)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_struct_type = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("StructureType"),
    .refs = f_datatype_struct_type_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* ServerState EnumStrings */
static const t_lip_opcua_loc_string_ptr_list f_server_state_enum_strings_val[] = {
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Running")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Failed")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("NoConfiguration")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Suspended")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Shutdown")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Test")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("CommunicationFault")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Unknown")},
    };


static const t_lip_opcua_tree_item_ref_descr f_server_state_enum_strings_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_datatype_server_state) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_server_state_enum_strings = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATE_ENUM_STRINGS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_ENUM_STRINGS),
    .refs = f_server_state_enum_strings_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_LOCTEXT_ARR(f_server_state_enum_strings_val)
};


/* ServerState */
static const t_lip_opcua_tree_item_ref_descr f_datatype_server_state_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_enum) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_datatype_server_state_enum_strings)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_server_state = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ServerState"),
    .refs = f_datatype_server_state_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};



#endif
