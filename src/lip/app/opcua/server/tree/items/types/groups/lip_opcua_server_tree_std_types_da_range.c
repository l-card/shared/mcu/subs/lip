#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_da_range.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_reftypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_objtypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_vartypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_names.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"



/* Range */
static const t_lip_opcua_struct_field f_datatype_range_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("low",                g_lip_opcua_tree_std_data_descr_double), /* Lowest value in the range. */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("high",               g_lip_opcua_tree_std_data_descr_double), /* Highest value in the range. */
};

static const t_lip_opcua_struct_def f_datatype_range_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_range_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_range_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_range_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_range_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_range_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_range_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_RANGE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Range"),
    .refs = f_datatype_range_refs,
    .class_descr_hdr = &f_datatype_range_descr.hdr
};

/* Range Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_range_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_range)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_range_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_RANGE_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_range_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};


/* EUInformation */
static const t_lip_opcua_struct_field f_datatype_eu_info_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("namespaceUri",       g_lip_opcua_tree_std_data_descr_str),   /* Identifies the organization (company, standards organization) that defines the EUInformation. */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("unitId",             g_lip_opcua_tree_std_data_descr_int32), /* Identifier for programmatic lookup (−1 - not available) */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("displayName",        g_lip_opcua_tree_std_data_descr_localized_text), /* Abbreviation of the engineering unit ("h" for hour or "m/s" for meter per second) */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("description",        g_lip_opcua_tree_std_data_descr_localized_text), /* Full name of the engineering unit such as "hour" or "meter per second" */
};

static const t_lip_opcua_struct_def f_datatype_eu_info_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_eu_info_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_eu_info_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_eu_info_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_eu_info_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_eu_info_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_eu_info_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_eu_info = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_EU_INFORMATION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EUInformation"),
    .refs = f_datatype_eu_info_refs,
    .class_descr_hdr = &f_datatype_eu_info_descr.hdr
};

/* EUInformation Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_eu_info_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_eu_info)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_eu_info_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_EU_INFORMATION_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_eu_info_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};





#endif
