#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "../lip_opcua_server_tree_decl_defs.h"
#include "lip_opcua_server_tree_std_vartypes.h"
#include "lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip_opcua_server_tree_reftypes.h"
#include "../objects/lip_opcua_server_tree_folders.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "groups/lip_opcua_server_tree_std_types_server_status.h"
#include "groups/lip_opcua_server_tree_std_types_buildinfo.h"
#include "groups/lip_opcua_server_tree_std_types_da_items.h"

/* --------------- BaseVariableType --------------------------------------------*/

const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_abstarct_any = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_any,
    .is_abstract = true
};

const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_abstarct_arr_anydim = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_arr_anydim,
    .is_abstract = true
};



const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_concrete_any = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_any,
    .is_abstract = false
};

const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_concrete_arr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_arr,
    .is_abstract = false
};

const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_concrete_arr_anydim = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_arr_anydim,
    .is_abstract = false
};

const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_concrete_arr_2dim = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_arr_2dim,
    .is_abstract = false
};

const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_concrete_arr_3dim = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_datadescr_arr_3dim,
    .is_abstract = false
};



const t_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_number_any = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_std_data_descr_number_any,
    .is_abstract = false
};

const struct st_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_bool_any = {
   .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
   .data_descr = &g_lip_opcua_tree_std_data_descr_bool_any,
   .is_abstract = false
};

const struct st_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_uinteger_any = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_std_data_descr_uinteger_any,
    .is_abstract = false
};

 const struct st_lip_opcua_tree_item_descr_vartype g_lip_opcua_tree_vartype_descr_xv_arr  = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_std_data_descr_xv_arr,
    .is_abstract = false
};




static const t_lip_opcua_tree_item *f_vartype_base_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_property,
    &g_lip_opcua_tree_std_item_vartype_base_data_variable
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_base_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_variable_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_base_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_base = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_VARIABLE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BaseVariableType"),
    .refs = f_vartype_base_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_abstarct_any.hdr
};

/* --------------- PropertyType --------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_vartype_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_property = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_PROPERTY_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("PropertyType"),
    .refs = f_vartype_property_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_any.hdr
};

/* --------------- BaseDataVariableType --------------------------------------------*/
static const t_lip_opcua_tree_item *f_vartype_base_data_variable_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_build_info,
    &g_lip_opcua_tree_std_item_vartype_server_status,
    &g_lip_opcua_tree_std_item_vartype_data_item,
};



static const t_lip_opcua_tree_item_ref_descr f_vartype_base_data_variable_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_base_data_variable_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_base_data_variable = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_DATA_VARIABLE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BaseDataVariableType"),
    .refs = f_vartype_base_data_variable_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_any.hdr
};

#endif
