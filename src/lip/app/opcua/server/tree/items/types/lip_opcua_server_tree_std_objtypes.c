#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "../lip_opcua_server_tree_decl_defs.h"
#include "lip_opcua_server_tree_std_objtypes.h"
#include "lip_opcua_server_tree_reftypes.h"
#include "../objects/lip_opcua_server_tree_folders.h"
#include "../objects/lip_opcua_server_tree_server_obj.h"
#include "../objects/lip_opcua_server_tree_modeling_rules.h"
#include "../types/groups/lip_opcua_server_tree_std_types_server.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"


const t_lip_opcua_tree_item_descr_objtype g_lip_opcua_tree_objtype_descr_abstract = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_OBJECT_TYPE,
    .is_abstract = true
};

const t_lip_opcua_tree_item_descr_objtype g_lip_opcua_tree_objtype_descr_concrete = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_OBJECT_TYPE,
    .is_abstract = false
};


const t_lip_opcua_tree_item_descr_object g_lip_opcua_tree_obj_descr_noevt = {
    LIP_OPCUA_TREE_ITEM_OBJECT_INIT
        .evt_notifier_flags = 0,
};




/* --------------- BaseObjectType --------------------------------------------*/
static const t_lip_opcua_tree_item *f_objtype_base_subtypes[] = {
    &g_lip_opcua_tree_std_item_objtype_folder,
    &g_lip_opcua_tree_std_item_server_type,
    &g_lip_opcua_tree_std_item_objtype_modeling_rule,
    &g_lip_opcua_tree_std_item_objtype_datatype_enc_type,
};
static const t_lip_opcua_tree_item_ref_descr f_objtype_base_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_object_types_folder)  LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_objtype_base_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_objtype_base = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_OBJECT_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BaseObjectType"),
    .refs = f_objtype_base_refs,
    .class_descr_hdr = &g_lip_opcua_tree_objtype_descr_concrete.hdr
};


/* --------------- FolderType ------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_objtype_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_objtype_base)
LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_objtype_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_FOLDER_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("FolderType"),
    .refs = f_objtype_folder_refs,
    .class_descr_hdr = &g_lip_opcua_tree_objtype_descr_concrete.hdr
};


/* --------------- DataTypeEncodingType ------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_objtype_datatype_enc_type_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_objtype_base)
LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_objtype_datatype_enc_type = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_TYPE_ENCODING_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DataTypeEncodingType"),
    .refs = f_objtype_datatype_enc_type_refs,
    .class_descr_hdr = &g_lip_opcua_tree_objtype_descr_concrete.hdr
};



#endif
