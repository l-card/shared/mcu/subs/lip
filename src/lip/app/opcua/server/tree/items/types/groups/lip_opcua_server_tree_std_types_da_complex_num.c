#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_da_complex_num.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_reftypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_objtypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_vartypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_names.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"


/* ComplexNumberType */
static const t_lip_opcua_struct_field f_datatype_complex_num_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("real",       g_lip_opcua_tree_std_data_descr_float), /* Value real part */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("imaginary",  g_lip_opcua_tree_std_data_descr_float), /* Value imaginary part */
};

static const t_lip_opcua_struct_def f_datatype_complex_num_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_complex_num_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_complex_num_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_complex_num_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_complex_num_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_complex_num_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_complex_num_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_complex_num = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_COMPLEX_NUMBER_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ComplexNumberType"),
    .refs = f_datatype_complex_num_refs,
    .class_descr_hdr = &f_datatype_complex_num_descr.hdr
};

/* ComplexNumberType Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_complex_num_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_complex_num)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_complex_num_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_COMPLEX_NUMBER_TYPE_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_complex_num_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};



/* DoubleComplexNumberType */
static const t_lip_opcua_struct_field f_datatype_double_complex_num_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("real",       g_lip_opcua_tree_std_data_descr_double), /* Value real part */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("imaginary",  g_lip_opcua_tree_std_data_descr_double), /* Value imaginary part */
};

static const t_lip_opcua_struct_def f_datatype_double_complex_num_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_double_complex_num_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_double_complex_num_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_double_complex_num_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_double_complex_num_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_double_complex_num_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_double_complex_num_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_double_complex_num = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DOUBLE_COMPLEX_NUMBER_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DoubleComplexNumberType"),
    .refs = f_datatype_double_complex_num_refs,
    .class_descr_hdr = &f_datatype_double_complex_num_descr.hdr
};

/* DoubleComplexNumberType Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_double_complex_num_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_double_complex_num)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_double_complex_num_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DOUBLE_COMPLEX_NUMBER_TYPE_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_double_complex_num_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};

#endif
