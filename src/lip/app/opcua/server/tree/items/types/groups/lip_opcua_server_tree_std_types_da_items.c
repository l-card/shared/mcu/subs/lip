#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_da_items.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_reftypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_objtypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_vartypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_names.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../lip_opcua_server_tree_std_var_descr.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "math.h"


/* DataItemType */
static const t_lip_opcua_tree_item *f_vartype_data_item_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_base_analog,
    &g_lip_opcua_tree_std_item_vartype_discrete_item,
    &g_lip_opcua_tree_std_item_vartype_array_item,
};

static const t_lip_opcua_tree_item *f_vartype_data_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_data_item_definition,
    &g_lip_opcua_tree_std_item_vardecl_data_item_value_precision,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_data_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base_data_variable) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_data_item_properties) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_data_item_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};



const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_data_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DataItemType"),
    .refs = f_vartype_data_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_any.hdr
};

/* DataItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_data_item_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_data_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_optional)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_data_item_definition = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_ITEM_TYPE_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Definition"),
    .refs = f_vartype_data_item_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_data_item_value_precision = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_ITEM_TYPE_VALUE_PRECISION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ValuePrecision"),
    .refs = f_vartype_data_item_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_double.hdr,
    .user.vdouble = NAN
};



/* BaseAnalogType */
static const t_lip_opcua_tree_item *f_vartype_base_analog_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_analog_item,
    &g_lip_opcua_tree_std_item_vartype_analog_unit,
};

static const t_lip_opcua_tree_item *f_vartype_base_analog_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_base_analog_instrument_range,
    &g_lip_opcua_tree_std_item_vardecl_base_analog_eu_range,
    &g_lip_opcua_tree_std_item_vardecl_base_analog_eu,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_base_analog_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_data_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_base_analog_properties) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_base_analog_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_base_analog = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BaseAnalogType"),
    .refs = f_vartype_base_analog_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_number_any.hdr
};

/* BaseAnalogType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_base_analog_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_base_analog) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_optional)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_base_analog_instrument_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE_INSTRUMENT_RANGE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("InstrumentRange"),
    .refs = f_vartype_base_analog_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_range.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_base_analog_eu_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE_EU_RANGE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EURange"),
    .refs = f_vartype_base_analog_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_range.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_base_analog_eu = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE_ENGINEERING_UNITS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EngineeringUnits"),
    .refs = f_vartype_base_analog_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_eu_info.hdr
};




/* AnalogItemType */
static const t_lip_opcua_tree_item *f_vartype_analog_item_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_analog_unit_range
};

static const t_lip_opcua_tree_item *f_vartype_analog_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_analog_item_eu_range,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_analog_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base_analog) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_analog_item_properties) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_analog_item_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_analog_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ANALOG_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AnalogItemType"),
    .refs = f_vartype_analog_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_number_any.hdr
};

/* AnalogItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_analog_item_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_analog_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_analog_item_eu_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ANALOG_ITEM_TYPE_EURANGE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EURange"),
    .refs = f_vartype_analog_item_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_range.hdr
};



/* AnalogUnitType */
static const t_lip_opcua_tree_item *f_vartype_analog_unit_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_analog_unit_eu,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_analog_unit_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base_analog) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_analog_unit_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_analog_unit = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ANALOG_UNIT_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AnalogUnitType"),
    .refs = f_vartype_analog_unit_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_number_any.hdr
};

/* AnalogUnitType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_analog_unit_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_analog_unit) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_analog_unit_eu = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ANALOG_UNIT_TYPE_ENGINEERING_UNITS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EngineeringUnits"),
    .refs = f_vartype_analog_unit_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_eu_info.hdr
};



/* AnalogUnitRangeType */
static const t_lip_opcua_tree_item *f_vartype_analog_unit_range_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_analog_unit_range_eu,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_analog_unit_range_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_analog_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_analog_unit_range_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_analog_unit_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ANALOG_UNIT_RANGE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AnalogUnitRangeType"),
    .refs = f_vartype_analog_unit_range_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_number_any.hdr
};

/* AnalogUnitRangeType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_analog_unit_range_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_analog_unit_range) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_analog_unit_range_eu = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ANALOG_UNIT_RANGE_TYPE_ENGINEERING_UNITS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EngineeringUnits"),
    .refs = f_vartype_analog_unit_range_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_eu_info.hdr
};



/* DiscreteItemType */
static const t_lip_opcua_tree_item *f_vartype_discrete_item_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_discrete_two_state,
    &g_lip_opcua_tree_std_item_vartype_discrete_multi_state,
    &g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val,
};


static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_data_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_discrete_item_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DISCRETE_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DiscreteItemType"),
    .refs = f_vartype_discrete_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_abstarct_any.hdr
};



/* TwoStateDiscreteType */
static const t_lip_opcua_tree_item *f_vartype_discrete_two_state_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_discrete_two_state_true,
    &g_lip_opcua_tree_std_item_vardecl_discrete_two_state_false,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_two_state_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_discrete_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_discrete_two_state_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_two_state = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_TWO_STATE_DISCRETE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("TwoStateDiscreteType"),
    .refs = f_vartype_discrete_two_state_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_bool_any.hdr
};

/* TwoStateDiscreteType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_two_state_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_discrete_two_state) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_discrete_two_state_false = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_TWO_STATE_DISCRETE_TYPE_FALSE_STATE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("FalseState"),
    .refs = f_vartype_discrete_two_state_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_localized_text.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_discrete_two_state_true = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_TWO_STATE_DISCRETE_TYPE_TRUE_STATE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("TrueState"),
    .refs = f_vartype_discrete_two_state_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_localized_text.hdr
};




/* MultiStateDiscreteType */
static const t_lip_opcua_tree_item *f_vartype_discrete_multi_state_properties[] = {
    &g_lip_opcua_tree_std_item_vartype_discrete_multi_state_enum_strings,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_multi_state_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_discrete_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_discrete_multi_state_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_multi_state = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MULTI_STATE_DISCRETE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("MultiStateDiscreteType"),
    .refs = f_vartype_discrete_multi_state_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_uinteger_any.hdr
};

/* TwoStateDiscreteType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_multi_state_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_discrete_multi_state) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_multi_state_enum_strings = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MULTI_STATE_DISCRETE_TYPE_ENUM_STRINGS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_ENUM_STRINGS),
    .refs = f_vartype_discrete_multi_state_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_localized_text_arr.hdr
};



/* MultiStateValueDiscreteType */
static const t_lip_opcua_tree_item *f_vartype_discrete_multi_state_val_properties[] = {
    &g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val_enum_vals,
    &g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val_value_as_text,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_multi_state_val_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_discrete_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_discrete_multi_state_val_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MULTI_STATE_VALUE_DISCRETE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("MultiStateValueDiscreteType"),
    .refs = f_vartype_discrete_multi_state_val_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_number_any.hdr
};

/* MultiStateValueDiscreteType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_discrete_multi_state_val_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val_enum_vals = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MULTI_STATE_VALUE_DISCRETE_TYPE_ENUM_VALUES),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EnumValues"),
    .refs = f_vartype_discrete_multi_state_val_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_enum_value_arr.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_discrete_multi_state_val_value_as_text = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MULTI_STATE_VALUE_DISCRETE_TYPE_VALUE_AS_TEXT),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ValueAsText"),
    .refs = f_vartype_discrete_multi_state_val_mandatory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_localized_text.hdr
};




/* ArrayItemType */
static const t_lip_opcua_tree_item *f_vartype_array_item_subtypes[] = {
    &g_lip_opcua_tree_std_item_vartype_y_array_item,
    &g_lip_opcua_tree_std_item_vartype_xy_array_item,
    &g_lip_opcua_tree_std_item_vartype_image_item,
    &g_lip_opcua_tree_std_item_vartype_cube_item,
    &g_lip_opcua_tree_std_item_vartype_ndim_array_item
};

static const t_lip_opcua_tree_item *f_vartype_array_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_array_item_instrument_range,
    &g_lip_opcua_tree_std_item_vardecl_array_item_eu_range,
    &g_lip_opcua_tree_std_item_vardecl_array_item_eu,
    &g_lip_opcua_tree_std_item_vardecl_array_item_title,
    &g_lip_opcua_tree_std_item_vardecl_array_item_axis_scale_type,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_array_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_data_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_array_item_properties) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_vartype_array_item_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_array_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ArrayItemType"),
    .refs = f_vartype_array_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_abstarct_arr_anydim.hdr
};

/* ArrayItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_array_item_optional_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_optional)
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_array_item_mandadory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_array_item_instrument_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_INSTRUMENT_RANGE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("InstrumentRange"),
    .refs = f_vartype_array_item_optional_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_range.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_array_item_eu_range = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_EU_RANGE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EURange"),
    .refs = f_vartype_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_range.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_array_item_eu = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_ENGINEERING_UNITS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EngineeringUnits"),
    .refs = f_vartype_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_eu_info.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_array_item_title = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_TITLE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Title"),
    .refs = f_vartype_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_localized_text.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_array_item_axis_scale_type = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_AXIS_SCALE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AxisScaleType"),
    .refs = f_vartype_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_scale.hdr
};



/* YArrayItemType */
static const t_lip_opcua_tree_item *f_vartype_y_array_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_y_array_item_x_axis_def,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_y_array_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_y_array_item_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_y_array_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_Y_ARRAY_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("YArrayItemType"),
    .refs = f_vartype_y_array_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_arr.hdr
};

/* YArrayItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_y_array_item_mandadory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_y_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_y_array_item_x_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_Y_ARRAY_ITEM_TYPE_X_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XAxisDefinition"),
    .refs = f_vartype_y_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};



/* XYArrayItemType */
static const t_lip_opcua_tree_item *f_vartype_xy_array_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_xy_array_item_x_axis_def,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_xy_array_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_xy_array_item_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_xy_array_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_XY_ARRAY_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XYArrayItemType"),
    .refs = f_vartype_xy_array_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_xv_arr.hdr
};

/* XYArrayItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_xy_array_item_mandadory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_xy_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_xy_array_item_x_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_XY_ARRAY_ITEM_TYPE_X_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XAxisDefinition"),
    .refs = f_vartype_xy_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};



/* ImageItemType */
static const t_lip_opcua_tree_item *f_vartype_image_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_image_item_x_axis_def,
    &g_lip_opcua_tree_std_item_vardecl_image_item_y_axis_def,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_image_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_image_item_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_image_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_IMAGE_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ImageItemType"),
    .refs = f_vartype_image_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_arr_2dim.hdr
};

/* ImageItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_image_item_mandadory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_image_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_image_item_x_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_IMAGE_ITEM_TYPE_X_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XAxisDefinition"),
    .refs = f_vartype_image_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_image_item_y_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_IMAGE_ITEM_TYPE_Y_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("YAxisDefinition"),
    .refs = f_vartype_image_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};



/* CubeItemType */
static const t_lip_opcua_tree_item *f_vartype_cube_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_cube_item_x_axis_def,
    &g_lip_opcua_tree_std_item_vardecl_cube_item_y_axis_def,
    &g_lip_opcua_tree_std_item_vardecl_cube_item_z_axis_def,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_cube_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_cube_item_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_cube_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("CubeItemType"),
    .refs = f_vartype_cube_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_arr_3dim.hdr
};

/* CubeItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_cube_item_mandadory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_cube_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_cube_item_x_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE_X_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XAxisDefinition"),
    .refs = f_vartype_cube_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_cube_item_y_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE_Y_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("YAxisDefinition"),
    .refs = f_vartype_cube_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_cube_item_z_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE_Z_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ZAxisDefinition"),
    .refs = f_vartype_cube_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info.hdr
};



/* NDimensionArrayItemType */
static const t_lip_opcua_tree_item *f_vartype_ndim_array_item_properties[] = {
    &g_lip_opcua_tree_std_item_vardecl_ndim_array_item_axis_def,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_ndim_array_item_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_vartype_ndim_array_item_properties)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_ndim_array_item = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_N_DIMENSION_ARRAY_ITEM_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("NDimensionArrayItemType"),
    .refs = f_vartype_ndim_array_item_refs,
    .class_descr_hdr = &g_lip_opcua_tree_vartype_descr_concrete_arr_anydim.hdr
};

/* NDimensionArrayItemType Properties */
static const t_lip_opcua_tree_item_ref_descr f_vartype_ndim_array_item_mandadory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_vartype_ndim_array_item) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_ndim_array_item_axis_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_N_DIMENSION_ARRAY_ITEM_TYPE_AXIS_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AxisDefinition"),
    .refs = f_vartype_ndim_array_item_mandadory_property_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_null_axis_info_arr.hdr
};






#endif
