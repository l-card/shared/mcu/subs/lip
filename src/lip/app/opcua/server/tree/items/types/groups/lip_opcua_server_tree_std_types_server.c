#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_server.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "../lip_opcua_server_tree_std_vartypes.h"
#include "../lip_opcua_server_tree_reftypes.h"
#include "../lip_opcua_server_tree_std_datatypes_descr.h"
#include "../lip_opcua_server_tree_std_objtypes.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../lip_opcua_server_tree_std_var_descr.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/server/lip_opcua_server_params.h"



/*----------- Server Type ----------------------------------------------------*/
static const t_lip_opcua_tree_item *f_server_type_property_refs[] = {
    &g_lip_opcua_tree_std_item_server_type_server_array,
    &g_lip_opcua_tree_std_item_server_type_ns_array,
    &g_lip_opcua_tree_std_item_server_type_uris_version,
    &g_lip_opcua_tree_std_item_server_type_service_level
};

static const t_lip_opcua_tree_item_ref_descr f_objtype_server_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_objtype_base) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_property, f_server_type_property_refs)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_server_type = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ServerType"),
    .refs = f_objtype_server_refs,
    .class_descr_hdr = &g_lip_opcua_tree_objtype_descr_concrete.hdr
};


static const t_lip_opcua_tree_item_ref_descr f_server_type_mandatory_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_server_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
            LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};
static const t_lip_opcua_tree_item_ref_descr f_server_type_optional_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_server_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property) LIP_OPCUA_TREE_REF_DECL_NEXT
            LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_optional)
    LIP_OPCUA_TREE_REF_DECL_END
};


/* ServerArray */
static const t_lip_opcua_tree_item_descr_variable f_server_type_server_array_descr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
        .hdr.refs = f_server_type_mandatory_property_refs,
    .data_descr = &g_lip_opcua_tree_std_data_descr_str_arr,
    .cbs.acc.value_read = lip_opcua_tree_std_item_cb_read_null
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_server_type_server_array = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_TYPE_SERVER_ARRAY),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ServerArray"),
    .class_descr_hdr = &f_server_type_server_array_descr.hdr
};

/* NamespaceArray */
static const t_lip_opcua_tree_item_descr_variable f_server_type_namespace_descr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
        .hdr.refs = f_server_type_mandatory_property_refs,
    .data_descr = &g_lip_opcua_tree_std_data_descr_str_arr,
    .cbs.acc.value_read = lip_opcua_tree_std_item_cb_read_null
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_server_type_ns_array = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_TYPE_NAMESPACE_ARRAY),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("NamespaceArray"),
    .class_descr_hdr = &f_server_type_namespace_descr.hdr
};

/* UrisVersion */
static const t_lip_opcua_tree_item_descr_variable f_server_type_uris_version_descr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
        .hdr.refs = f_server_type_optional_property_refs,
    .data_descr = &g_lip_opcua_tree_std_data_descr_version_time,
    .cbs.acc.value_read = lip_opcua_tree_std_item_cb_read_null
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_server_type_uris_version = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_TYPE_URIS_VERSION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("UrisVersion"),
    .class_descr_hdr = &f_server_type_uris_version_descr.hdr
};


/* ServiceLevel */
const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_server_type_service_level = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_TYPE_SERVICE_LEVEL),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ServiceLevel"),
    .refs = f_server_type_mandatory_property_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UINT8(LIP_OPCUA_SERVER_DEFAULT_SERVICE_LEVEL)
};

#endif
