#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "groups/lip_opcua_server_tree_std_types_buildinfo.h"
#include "groups/lip_opcua_server_tree_std_types_server_status.h"
#include "groups/lip_opcua_server_tree_std_types_enum.h"
#include "groups/lip_opcua_server_tree_std_types_da_range.h"
#include "groups/lip_opcua_server_tree_std_types_da_axis.h"


const t_lip_opcua_datatype_descr g_lip_opcua_tree_datadescr_any = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_base.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_ANY,
    .dimension.list = NULL
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_datadescr_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_base.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_datadescr_arr_anydim = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_base.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM1_OR_MORE,
    .dimension.list = NULL
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_datadescr_arr_2dim = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_base.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(2),
    .dimension.list = NULL
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_datadescr_arr_3dim = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_base.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(3),
    .dimension.list = NULL
};



const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_bool = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_boolean.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_bool_any = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_boolean.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_ANY,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_number = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_number.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_number_any = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_number.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_ANY,
    .dimension.list = NULL
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_uinteger_any = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_uinteger.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_ANY,
    .dimension.list = NULL
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_int32 = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_int32.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_int32_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_int32.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_int64 = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_int64.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_byte = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_byte.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_uint32 = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_uint32.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_uint32_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_uint32.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_float = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_float.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};


const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_double = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_double.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_double_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_double.nodeid,
    .rank =  LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_utc_time = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_utc_time.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_str = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_string.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_str_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_string.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_localized_text = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_localized_text.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_localized_text_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_localized_text.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};


const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_version_time = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_version_time.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_nodeid = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_nodeid.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_enum_value_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_enum_value.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_xv_arr  = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_xv.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

const t_lip_opcua_datatype_descr  g_lip_opcua_tree_std_data_descr_server_state = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_server_state.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_server_status = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_server_status.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const t_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_build_info = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_build_info.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};



const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_eu_info = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_eu_info.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};

const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_range = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_range.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};
const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_axis_scale = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_axis_scale.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};
const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_axis_info = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_axis_info.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
};
const struct st_lip_opcua_datatype_descr g_lip_opcua_tree_std_data_descr_axis_info_arr = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_axis_info.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};



#endif
