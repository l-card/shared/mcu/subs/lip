#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip_opcua_server_tree_std_types_enum.h"
#include "lip_opcua_server_tree_std_types_buildinfo.h"
#include "lip_opcua_server_tree_std_types_server_status.h"
#include "lip_opcua_server_tree_std_types_da_range.h"
#include "lip_opcua_server_tree_std_types_da_complex_num.h"
#include "lip_opcua_server_tree_std_types_da_axis.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "../lip_opcua_server_tree_std_datatypes.h"
#include "../lip_opcua_server_tree_reftypes.h"
#include "../lip_opcua_server_tree_std_datatypes_descr.h"
#include "../lip_opcua_server_tree_std_objtypes.h"
#include "../lip_opcua_server_tree_std_names.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"

/* Structure */
static const t_lip_opcua_tree_item *f_datatype_struct_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_datatype_def,
    &g_lip_opcua_tree_std_item_datatype_struct_field,
    &g_lip_opcua_tree_std_item_datatype_enum_value,
    &g_lip_opcua_tree_std_item_datatype_build_info,
    &g_lip_opcua_tree_std_item_datatype_server_status,
    &g_lip_opcua_tree_std_item_datatype_range,
    &g_lip_opcua_tree_std_item_datatype_eu_info,
    &g_lip_opcua_tree_std_item_datatype_complex_num,
    &g_lip_opcua_tree_std_item_datatype_double_complex_num,
    &g_lip_opcua_tree_std_item_datatype_axis_info,
    &g_lip_opcua_tree_std_item_datatype_xv,
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_struct_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_base) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_struct_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_struct = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Structure"),
    .refs = f_datatype_struct_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};


/* StructureField */
static const t_lip_opcua_struct_field f_datatype_struct_field_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("Name", g_lip_opcua_tree_std_data_descr_str),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("Description", g_lip_opcua_tree_std_data_descr_localized_text),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("DataType", g_lip_opcua_tree_std_data_descr_nodeid),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("ValueRank", g_lip_opcua_tree_std_data_descr_int32),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("ArrayDimensions", g_lip_opcua_tree_std_data_descr_uint32_arr),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("MaxStringLength", g_lip_opcua_tree_std_data_descr_uint32),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("IsOptional", g_lip_opcua_tree_std_data_descr_bool)
};

static const t_lip_opcua_struct_def f_datatype_struct_field_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_struct_field_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_struct_field_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_struct_field_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_struct_field_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_struct_field_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_struct_field_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_struct_field = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE_FIELD),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("StructureField"),
    .refs = f_datatype_struct_field_refs,
    .class_descr_hdr = &f_datatype_struct_field_descr.hdr
};

/* StructureField Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_struct_field_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_struct_field)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_struct_field_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE_FIELD_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_struct_field_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};


/* StructureDefinition */
static const t_lip_opcua_datatype_descr f_data_descr_struct_type = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_struct_type.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_SCALAR,
    .dimension.value = 0
};

static const t_lip_opcua_datatype_descr f_data_descr_struct_fields  = {
    .type_id = &g_lip_opcua_tree_std_item_datatype_struct_field.nodeid,
    .rank = LIP_OPCUA_VARIABLE_RANK_DIM(1),
    .dimension.value = 0
};

static const t_lip_opcua_struct_field f_datatype_struct_def_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("DefaultEncodingId", g_lip_opcua_tree_std_data_descr_nodeid),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("BaseDataType", g_lip_opcua_tree_std_data_descr_nodeid),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("StructureType", f_data_descr_struct_type),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("Fields", f_data_descr_struct_fields),
};

static const t_lip_opcua_struct_def f_datatype_struct_def_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_struct_def_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_struct_def_fields)
};



static const t_lip_opcua_tree_item *f_datatype_struct_def_subtypes[] = {

};
static const t_lip_opcua_tree_item_ref_descr f_datatype_struct_def_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_datatype_def) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_struct_def_enc_bin) LIP_OPCUA_TREE_REF_DECL_NEXT
            LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_struct_def_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};



static const t_lip_opcua_tree_item_descr_datatype f_datatype_struct_def_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_struct_def_typedef
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_struct_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("StructureDefinition"),
    .refs = f_datatype_struct_def_refs,
    .class_descr_hdr = &f_datatype_struct_def_descr.hdr
};


/* StructureDefinition Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_struct_def_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_struct_def)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_struct_def_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRUCTURE_DEFINITION_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_struct_def_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};


#endif
