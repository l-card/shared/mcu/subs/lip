#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_datatypes.h"
#include "lip_opcua_server_tree_reftypes.h"
#include "lip_opcua_server_tree_std_objtypes.h"
#include "lip_opcua_server_tree_std_vartypes.h"
#include "../lip_opcua_server_tree_decl_defs.h"
#include "../objects/lip_opcua_server_tree_folders.h"
#include "../objects/lip_opcua_server_tree_server_obj.h"
#include "../objects/lip_opcua_server_tree_modeling_rules.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "groups/lip_opcua_server_tree_std_types_struct.h"
#include "groups/lip_opcua_server_tree_std_types_enum.h"

/* --------------- BaseDataType --------------------------------------------*/
const t_lip_opcua_tree_item_descr_datatype g_lip_opcua_tree_datatype_descr_abstarct = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = true
};

const t_lip_opcua_tree_item_descr_datatype g_lip_opcua_tree_datatype_descr_concrete = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = true
};

static const t_lip_opcua_tree_item *f_datatype_base_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_boolean,
    &g_lip_opcua_tree_std_item_datatype_number,
    &g_lip_opcua_tree_std_item_datatype_string,
    &g_lip_opcua_tree_std_item_datatype_localized_text,
    &g_lip_opcua_tree_std_item_datatype_bytestring,
    &g_lip_opcua_tree_std_item_datatype_date_time,
    &g_lip_opcua_tree_std_item_datatype_nodeid,
    &g_lip_opcua_tree_std_item_datatype_expaneded_nodeid,
    &g_lip_opcua_tree_std_item_datatype_guid,
    &g_lip_opcua_tree_std_item_datatype_xml_element,
    &g_lip_opcua_tree_std_item_datatype_status_code,
    &g_lip_opcua_tree_std_item_datatype_qualified_name,
    &g_lip_opcua_tree_std_item_datatype_data_value,
    &g_lip_opcua_tree_std_item_datatype_diagnostic_info,
    &g_lip_opcua_tree_std_item_datatype_struct,
    &g_lip_opcua_tree_std_item_datatype_enum
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_base_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_data_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_base_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_base = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BASE_DATA_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BaseDataType"),
    .refs = f_datatype_base_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_base_subtype_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};

/* Boolean */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_boolean = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BOOLEAN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Boolean"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};



/* Number */
static const t_lip_opcua_tree_item *f_datatype_number_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_integer,
    &g_lip_opcua_tree_std_item_datatype_uinteger,
    &g_lip_opcua_tree_std_item_datatype_float,
    &g_lip_opcua_tree_std_item_datatype_double,
    &g_lip_opcua_tree_std_item_datatype_decimal
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_number_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_base) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_number_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_number = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_NUMBER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Number"),
    .refs = f_datatype_number_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};


/* Integer */
static const t_lip_opcua_tree_item *f_datatype_integer_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_sbyte,
    &g_lip_opcua_tree_std_item_datatype_int16,
    &g_lip_opcua_tree_std_item_datatype_int32,
    &g_lip_opcua_tree_std_item_datatype_int64,
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_integer_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_number) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype,  f_datatype_integer_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_integer = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INTEGER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Integer"),
    .refs = f_datatype_integer_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};

/* SByte */
static const t_lip_opcua_tree_item_ref_descr f_datatype_sbyte_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_integer)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_sbyte = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SBYTE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("SByte"),
    .refs = f_datatype_sbyte_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* Int16 */
static const t_lip_opcua_tree_item_ref_descr f_datatype_int16_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_integer)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_int16 = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INT16),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Int16"),
    .refs = f_datatype_int16_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* Int32 */
static const t_lip_opcua_tree_item_ref_descr f_datatype_int32_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_integer)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_int32 = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INT32),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Int32"),
    .refs = f_datatype_int32_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* Int64 */
static const t_lip_opcua_tree_item_ref_descr f_datatype_int64_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_integer)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_int64 = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INT64),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Int64"),
    .refs = f_datatype_int64_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* UInteger */
static const t_lip_opcua_tree_item *f_datatype_uinteger_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_byte,
    &g_lip_opcua_tree_std_item_datatype_uint16,
    &g_lip_opcua_tree_std_item_datatype_uint32,
    &g_lip_opcua_tree_std_item_datatype_uint64,
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_uinteger_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_number) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_uinteger_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_uinteger = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_UINTEGER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("UInteger"),
    .refs = f_datatype_uinteger_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};

/* SByte */
static const t_lip_opcua_tree_item_ref_descr f_datatype_byte_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_uinteger)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_byte = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BYTE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Byte"),
    .refs = f_datatype_byte_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* UInt16 */
static const t_lip_opcua_tree_item_ref_descr f_datatype_uint16_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_uinteger) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_access_restriction)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_uint16 = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_UINT16),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("UInt16"),
    .refs = f_datatype_uint16_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* UInt32 */
static const struct st_lip_opcua_tree_item *f_datatype_uint32_subtpes[] = {
    &g_lip_opcua_tree_std_item_datatype_counter,
    &g_lip_opcua_tree_std_item_datatype_integer_id,
    &g_lip_opcua_tree_std_item_datatype_index,
    &g_lip_opcua_tree_std_item_datatype_version_time,
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_uint32_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_uinteger) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_uint32_subtpes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_uint32 = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_UINT32),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("UInt32"),
    .refs = f_datatype_uint32_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_uint32_subtype_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_uint32)
    LIP_OPCUA_TREE_REF_DECL_END
};

/* UInt64 */
static const t_lip_opcua_tree_item_ref_descr f_datatype_uint64_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_uinteger)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_uint64 = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_UINT64),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("UInt64"),
    .refs = f_datatype_uint64_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* Counter */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_counter = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_COUNTER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Counter"),
    .refs = f_datatype_uint32_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* IntegerID */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_integer_id = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INTEGER_ID),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("IntegerId"),
    .refs = f_datatype_uint32_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* Index */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_index = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INDEX),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Index"),
    .refs = f_datatype_uint32_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};



/* VersionTime */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_version_time = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_VERSION_TIME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("VersionTime"),
    .refs = f_datatype_uint32_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* Float */
static const t_lip_opcua_tree_item_ref_descr f_datatype_float_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_number)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_float = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_FLOAT),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Float"),
    .refs = f_datatype_float_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* Double */
static const t_lip_opcua_tree_item_ref_descr f_datatype_double_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_number) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_duration)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_double = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DOUBLE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Double"),
    .refs = f_datatype_double_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* Decimal */
static const t_lip_opcua_tree_item_ref_descr f_datatype_decimal_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_number)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_decimal = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DECIMAL),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Decimal"),
    .refs = f_datatype_decimal_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* Duration */
static const t_lip_opcua_tree_item_ref_descr f_datatype_duration_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_double)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_duration = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DURATION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Duration"),
    .refs = f_datatype_duration_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};




/* AccessRestrictionType */
static const t_lip_opcua_tree_item_ref_descr f_datatype_access_restriction_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_uint16)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_access_restriction = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ACCESS_RESTRICTION_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AccessRestrictionType"),
    .refs = f_datatype_access_restriction_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* ByteString */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_bytestring = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BYTESTRING),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ByteString"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* String */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_string = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STRING),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("String"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* LocalizedText */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_localized_text = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_LOCALIZED_TEXT),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("LocalizedText"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};



/* DateTime */
static const t_lip_opcua_tree_item_ref_descr f_datatype_date_time_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_base) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_utc_time)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_date_time = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATETIME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DateTime"),
    .refs = f_datatype_date_time_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* UtcTime */
static const t_lip_opcua_tree_item_ref_descr f_datatype_utc_time_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_date_time)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_utc_time = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_UTC_TIME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("UtcTime"),
    .refs = f_datatype_utc_time_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* NodeId */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_nodeid = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_NODEID),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("NodeId"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* ExpandedNodeId */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_expaneded_nodeid = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_EXPANDED_NODEID),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ExpandedNodeId"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* StatusCode */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_status_code = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_STATUS_CODE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("StatusCode"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* QualifiedName */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_qualified_name = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_QUALIFIED_NAME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("QualifiedName"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* DataValue */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_data_value = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_VALUE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DataValue"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* DiagnosticInfo */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_diagnostic_info = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DIAGNOSTIC_INFO),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DiagnosticInfo"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};


/* Guid */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_guid = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_GUID),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Guid"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};

/* XmlElement */
const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_xml_element = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_XML_ELEMENT),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XmlElement"),
    .refs = f_datatype_base_subtype_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};
















/* DataTypeDefinition */
static const t_lip_opcua_tree_item *f_datatype_datatype_def_subtypes[] = {
    &g_lip_opcua_tree_std_item_datatype_enum_def,
    &g_lip_opcua_tree_std_item_datatype_struct_def
};
static const t_lip_opcua_tree_item_ref_descr f_datatype_datatype_def_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_datatype_datatype_def_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_datatype_def = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_TYPE_DEFINITION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DataTypeDefinition"),
    .refs = f_datatype_datatype_def_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_abstarct.hdr
};

















#endif
