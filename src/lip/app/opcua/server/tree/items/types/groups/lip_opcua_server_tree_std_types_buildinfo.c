#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_buildinfo.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_reftypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_objtypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_vartypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_names.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../lip_opcua_server_tree_std_var_descr.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"




/* BuildInfo */
static const t_lip_opcua_struct_field f_datatype_build_info_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("productUri",         g_lip_opcua_tree_std_data_descr_str), /* URI that identifies the software */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("manufacturerName",   g_lip_opcua_tree_std_data_descr_str), /* Name of the software manufacturer */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("productName",        g_lip_opcua_tree_std_data_descr_str), /* Name of the software */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("softwareVersion",    g_lip_opcua_tree_std_data_descr_str), /* Software version */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("buildNumber",        g_lip_opcua_tree_std_data_descr_str), /* Build number */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("buildDate",          g_lip_opcua_tree_std_data_descr_utc_time), /* Date and time of the build. */
};

static const t_lip_opcua_struct_def f_datatype_build_info_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_build_info_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_build_info_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_build_info_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_build_info_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_build_info_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_build_info_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_build_info = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILDINFO),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildInfo"),
    .refs = f_datatype_build_info_refs,
    .class_descr_hdr = &f_datatype_build_info_descr.hdr
};


/* BuildInfo Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_build_info_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_build_info)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_build_info_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILDINFO_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_build_info_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};




/* BuildInfoType */
static const t_lip_opcua_tree_item *f_vartype_build_info_components[] = {
    &g_lip_opcua_tree_std_item_vardecl_build_info_product_uri,
    &g_lip_opcua_tree_std_item_vardecl_build_info_manufacturer_name,
    &g_lip_opcua_tree_std_item_vardecl_build_info_product_name,
    &g_lip_opcua_tree_std_item_vardecl_build_info_software_version,
    &g_lip_opcua_tree_std_item_vardecl_build_info_build_number,
    &g_lip_opcua_tree_std_item_vardecl_build_info_build_date
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_build_info_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base_data_variable) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_component, f_vartype_build_info_components)
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_descr_vartype f_vartype_build_info_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_std_data_descr_build_info,
    .is_abstract = false
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_build_info = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildInfoType"),
    .refs = f_vartype_build_info_refs,
    .class_descr_hdr = &f_vartype_build_info_descr.hdr
};

/* ServerStatusType Components */
static const t_lip_opcua_tree_item_ref_descr f_build_info_type_comp_mandatory_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_component, g_lip_opcua_tree_std_item_vartype_build_info) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_base_data_variable) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_build_info_product_uri = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_PRODUCT_URI),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ProductUri"),
    .refs = f_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_build_info_manufacturer_name = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_MANUFACTURER_NAME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ManufacturerName"),
    .refs = f_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_build_info_product_name = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_PRODUCT_NAME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ProductName"),
    .refs = f_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_build_info_software_version = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_SOFTWARE_VERSION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("SoftwareVersion"),
    .refs = f_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_build_info_build_number = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_BUILD_NUMBER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildNumber"),
    .refs = f_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_build_info_build_date = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_BUILD_DATE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildDate"),
    .refs = f_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_utc_time.hdr
};




#endif
