#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_reftypes.h"
#include "../objects/lip_opcua_server_tree_folders.h"
#include "../lip_opcua_server_tree_decl_defs.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/lip_opcua_err_codes.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"

#define LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(namesuf)  (g_lip_opcua_tree_std_item_reftype_ ## namesuf )

#define LIP_OPCUA_TREE_STD_REFTYPE_DECL(varname, bname_val, invname_val, nideid, basevarname, refstable, nflags) \
static const t_lip_opcua_tree_item_descr_reftype f_item_reftype_ ## varname ## _descr = { \
    .hdr.node_class = LIP_OPCUA_NODECLASS_REFERENCE_TYPE, \
    .base_ref = &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(basevarname), \
    .flags = nflags, \
    .inverse_name.strlist[0] = { \
        .size = sizeof(invname_val) - 1, \
        .data = sizeof(invname_val) > 1 ? invname_val : NULL \
    } \
}; \
    const t_lip_opcua_tree_item LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(varname) = { \
        .nodeid = LIP_OPCUA_NODEID_STDNS_INT(nideid), \
        LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(bname_val), \
        .refs = refstable, \
       .class_descr_hdr = &f_item_reftype_ ## varname ## _descr.hdr \
};


static const t_lip_opcua_tree_item_descr_reftype f_item_reftype = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_REFERENCE_TYPE,
    .base_ref = NULL,
    .flags = LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_ABSTRACT | LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_SYMMETRIC
};

static const t_lip_opcua_tree_item *f_reftype_base_subtypes[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(hierarchical),
};
static const t_lip_opcua_tree_item_ref_descr f_reftype_base_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_ref_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_reftype_base_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

/* NonHierarchicalReferences refs */
static const t_lip_opcua_tree_item *f_reftype_non_hierarchical_subtypes[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_modelling_rule),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_type_definition),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_encoding),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(generates_event),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_interface),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(is_deprecated),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(associated_with),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_key_value_descr)
};
static const t_lip_opcua_tree_item_ref_descr f_reftype_non_hierarchical_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(base)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_reftype_non_hierarchical_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};

/* HierarchicalReferences refs */
static const t_lip_opcua_tree_item *f_reftype_hierarchical_subtypes[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_child),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(organizes),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_event_source),
};
static const t_lip_opcua_tree_item_ref_descr f_reftype_hierarchical_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(base)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_reftype_hierarchical_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


/* HasChild refs */
static const t_lip_opcua_tree_item *f_reftype_has_child_subtypes[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(aggregates),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_subtype)
};
static const t_lip_opcua_tree_item_ref_descr f_reftype_has_child_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(hierarchical)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_reftype_has_child_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


/* Aggregates refs */
static const t_lip_opcua_tree_item *f_reftype_aggregates_subtypes[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_component),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_property)
};
static const t_lip_opcua_tree_item_ref_descr f_reftype_aggregates_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_child)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_reftype_aggregates_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


/* Organizes refs */
static const t_lip_opcua_tree_item_ref_descr f_reftype_organizes_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};


/* HasComponent refs */
static const t_lip_opcua_tree_item *f_reftype_has_component_subtypes[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_ordered_component),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_arg_descr),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_addin),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_struct_component)
};
static const t_lip_opcua_tree_item_ref_descr f_reftype_has_component_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(aggregates)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, f_reftype_has_component_subtypes)
    LIP_OPCUA_TREE_REF_DECL_END
};


static const t_lip_opcua_tree_item_ref_descr f_reftype_has_ordered_component_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_component))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_property_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(aggregates))
    LIP_OPCUA_TREE_REF_DECL_END
};


static const t_lip_opcua_tree_item_ref_descr f_reftype_has_subtype_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_child))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_modelling_rule_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_type_definition_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_encoding_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_event_source_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(hierarchical)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_notifier))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_notifier_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_event_source))
    LIP_OPCUA_TREE_REF_DECL_END
};


static const t_lip_opcua_tree_item_ref_descr f_reftype_generates_event_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(always_generates_event))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_always_generates_event_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(generates_event))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_arg_descr_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_component)) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_opt_in_arg_descr))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_opt_in_arg_descr_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_arg_descr))
    LIP_OPCUA_TREE_REF_DECL_END
};


static const t_lip_opcua_tree_item_ref_descr f_reftype_has_interface_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_addin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_component))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_is_deprecated_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_struct_component_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_component))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_associated_with_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_ref_descr f_reftype_has_key_value_descr_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical))
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(base) = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_REFERENCES),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("References"),
    .refs = f_reftype_base_refs,
    .class_descr_hdr = &f_item_reftype.hdr
};





LIP_OPCUA_TREE_STD_REFTYPE_DECL(non_hierarchical,       "NonHierarchicalReferences", "", LIP_OPCUA_NODEID_STD_NON_HIERARCHICAL_REFERENCES, base, f_reftype_non_hierarchical_refs, LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_ABSTRACT | LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_SYMMETRIC);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(hierarchical,           "HierarchicalReferences", "", LIP_OPCUA_NODEID_STD_HIERARCHICAL_REFERENCES, base, f_reftype_hierarchical_refs, LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_ABSTRACT);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_child,              "HasChild", "ChildOf", LIP_OPCUA_NODEID_STD_HAS_CHILD, hierarchical, f_reftype_has_child_refs, LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_ABSTRACT);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(aggregates,             "Aggregates", "AggregatedBy", LIP_OPCUA_NODEID_STD_AGGREGATES, has_child,  f_reftype_aggregates_refs, LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_ABSTRACT);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(organizes,              "Organizes", "OrganizedBy", LIP_OPCUA_NODEID_STD_ORGANIZES, hierarchical, f_reftype_organizes_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_component,          "HasComponent", "ComponentOf", LIP_OPCUA_NODEID_STD_HAS_COMPONENT, aggregates, f_reftype_has_component_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_ordered_component,  "HasOrderedComponent", "OrderedComponentOf", LIP_OPCUA_NODEID_STD_HAS_ORDERED_COMPONENT, has_component, f_reftype_has_ordered_component_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_property,           "HasProperty", "PropertyOf", LIP_OPCUA_NODEID_STD_HAS_PROPERTY, aggregates, f_reftype_has_property_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_subtype,            "HasSubtype", "SubtypeOf", LIP_OPCUA_NODEID_STD_HAS_SUBTYPE, has_child, f_reftype_has_subtype_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_modelling_rule,     "HasModellingRule", "ModellingRuleOf", LIP_OPCUA_NODEID_STD_HAS_MODELLING_RULE, non_hierarchical, f_reftype_has_modelling_rule_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_type_definition,    "HasTypeDefinition", "TypeDefinitionOf", LIP_OPCUA_NODEID_STD_HAS_TYPE_DEFINITION, non_hierarchical, f_reftype_has_type_definition_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_encoding,           "HasEncoding", "EncodingOf", LIP_OPCUA_NODEID_STD_HAS_ENCODING, non_hierarchical, f_reftype_has_encoding_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_event_source,       "HasEventSource", "EventSourceOf", LIP_OPCUA_NODEID_STD_HAS_EVENT_SOURCE, hierarchical, f_reftype_has_event_source_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_notifier,           "HasNotifier", "NotifierOf", LIP_OPCUA_NODEID_STD_HAS_NOTIFIER, has_event_source, f_reftype_has_notifier_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(generates_event,        "GeneratesEvent", "GeneratedBy", LIP_OPCUA_NODEID_STD_GENERATES_EVENT, non_hierarchical, f_reftype_generates_event_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(always_generates_event, "AlwaysGeneratesEvent", "AlwaysGeneratedBy", LIP_OPCUA_NODEID_STD_ALWAYS_GENERATES_EVENT, generates_event, f_reftype_always_generates_event_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_arg_descr,          "HasArgumentDescription", "ArgumentDescriptionOf", LIP_OPCUA_NODEID_STD_HAS_ARGUMENT_DESCRIPTION, has_component, f_reftype_has_arg_descr_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_opt_in_arg_descr,   "HasOptionalInputArgumentDescription", "OptionalInputArgumentDescriptionOf", LIP_OPCUA_NODEID_STD_HAS_OPTIONAL_IN_ARGUMENT_DESCRIPTION, has_arg_descr, f_reftype_has_opt_in_arg_descr_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_interface,          "HasInterface", "InterfaceOf", LIP_OPCUA_NODEID_STD_HAS_INTERFACE, non_hierarchical, f_reftype_has_interface_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_addin,              "HasAddIn", "AddInOf", LIP_OPCUA_NODEID_STD_HAS_ADDIN, has_component, f_reftype_has_addin_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(is_deprecated,          "IsDeprecated", "Deprecates", LIP_OPCUA_NODEID_STD_IS_DEPRECATED, non_hierarchical, f_reftype_is_deprecated_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_struct_component,   "HasStructuredComponent", "IsStructuredComponentOf", LIP_OPCUA_NODEID_STD_HAS_STRUCTURED_COMPONENT, has_component, f_reftype_has_struct_component_refs, 0);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(associated_with,        "AssociatedWith", "", LIP_OPCUA_NODEID_STD_ASSOCIATED_WITH, non_hierarchical, f_reftype_associated_with_refs, LIP_OPCUA_TREE_ITEM_REFTYPE_FLAG_SYMMETRIC);
LIP_OPCUA_TREE_STD_REFTYPE_DECL(has_key_value_descr,    "HasKeyValueDescription", "KeyValueDescriptionOf", LIP_OPCUA_NODEID_STD_HAS_KEY_VALUE_DESCR, non_hierarchical, f_reftype_has_key_value_descr_refs, 0);




static const t_lip_opcua_tree_item *f_std_ref_types[] = {
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(base),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(non_hierarchical),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(hierarchical),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_child),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(aggregates),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(organizes),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_component),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_ordered_component),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_property),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_subtype),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_modelling_rule),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_type_definition),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_encoding),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_event_source),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_notifier),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(generates_event),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(always_generates_event),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_arg_descr),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_opt_in_arg_descr),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_interface),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_addin),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(is_deprecated),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_struct_component),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(associated_with),
    &LIP_OPCUA_TREE_STD_REFTYPE_ITEMNAME(has_key_value_descr)
};


t_lip_opcua_err lip_opcua_server_tree_get_reftype(struct st_lip_opcua_session_ctx *session, const t_lip_opcua_nodeid *nodeid, const t_lip_opcua_tree_item **item) {
    t_lip_opcua_err err = LIP_OPCUA_ERR_GOOD;
    const t_lip_opcua_tree_item *fnd_item = NULL;
    if (lip_opcua_nodeid_check_is_stdns_int(nodeid)) {
        for (size_t i = 0; (i < sizeof(f_std_ref_types)/sizeof(f_std_ref_types[0])) && !fnd_item; ++i) {
            const t_lip_opcua_tree_item *check_item = f_std_ref_types[i];
            if (check_item->nodeid.intval == nodeid->intval) {
                fnd_item = check_item;
            }
        }
    }

    if ((err == LIP_OPCUA_ERR_GOOD) && !fnd_item) {
        err = LIP_OPCUA_ERR_BAD_NODEID_UNKNOWN;
    }

    if (item) {
        *item = fnd_item;
    }

    return err;
}



#endif
