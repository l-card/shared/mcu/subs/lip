#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_da_axis.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_reftypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_objtypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_vartypes.h"
#include "lip/app/opcua/server/tree/items/types/lip_opcua_server_tree_std_names.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../lip_opcua_server_tree_std_var_descr.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "../groups/lip_opcua_server_tree_std_types_enum.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"


/* AxisScaleEnumeration EnumStrings */
static const t_lip_opcua_loc_string_ptr_list f_axis_scale_enum_strings_val[] = {
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Linear")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Log")},
    {LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR("Ln")},
};


static const t_lip_opcua_tree_item_ref_descr f_axis_scale_enum_strings_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_datatype_axis_scale) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_property)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_axis_scale_enum_strings = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_AXIS_SCALE_ENUMERATION_ENUM_STRINGS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_ENUM_STRINGS),
    .refs = f_axis_scale_enum_strings_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_LOCTEXT_ARR(f_axis_scale_enum_strings_val)
};


/* AxisScaleEnumeration */
static const t_lip_opcua_tree_item_ref_descr f_datatype_axis_scale_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_enum) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_property, g_lip_opcua_tree_std_item_datatype_axis_scale_enum_strings)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_axis_scale = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_AXIS_SCALE_ENUMERATION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AxisScaleEnumeration"),
    .refs = f_datatype_axis_scale_refs,
    .class_descr_hdr = &g_lip_opcua_tree_datatype_descr_concrete.hdr
};



/* AxisInformation */
static const t_lip_opcua_struct_field f_datatype_axis_info_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("engineeringUnits",   g_lip_opcua_tree_std_data_descr_eu_info),   /* Engineering units for a given axis */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("eURange",            g_lip_opcua_tree_std_data_descr_range),     /* Limits of the range of the axis */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("title",              g_lip_opcua_tree_std_data_descr_localized_text), /* User readable axis title */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("axisScaleType",      g_lip_opcua_tree_std_data_descr_axis_scale), /* LINEAR, LOG, LN, defined by AxisSteps */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("axisSteps",          g_lip_opcua_tree_std_data_descr_double_arr), /* Specific value of each axis steps, can be set to “Null” if not used */
};

static const t_lip_opcua_struct_def f_datatype_axis_info_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_axis_info_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_axis_info_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_axis_info_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_axis_info_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_axis_info_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_axis_info_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_axis_info = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_AXIS_INFORMATION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("AxisInformation"),
    .refs = f_datatype_axis_info_refs,
    .class_descr_hdr = &f_datatype_axis_info_descr.hdr
};

/* AxisInformation Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_axis_info_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_axis_info)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_axis_info_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_AXIS_INFORMATION_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_axis_info_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};



/* XVType */
static const t_lip_opcua_struct_field f_datatype_xv_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("x",       g_lip_opcua_tree_std_data_descr_double), /* Position on the X axis of this value */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("value",   g_lip_opcua_tree_std_data_descr_float),  /* The value itself*/
};

static const t_lip_opcua_struct_def f_datatype_xv_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_xv_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_xv_fields)
};

static const t_lip_opcua_tree_item_descr_datatype f_datatype_xv_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_xv_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_xv_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_xv_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_xv = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_XV_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("XVType"),
    .refs = f_datatype_xv_refs,
    .class_descr_hdr = &f_datatype_xv_descr.hdr
};

/* XVType Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_xv_enc_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_xv)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_xv_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_XV_TYPE_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_xv_enc_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};


#endif
