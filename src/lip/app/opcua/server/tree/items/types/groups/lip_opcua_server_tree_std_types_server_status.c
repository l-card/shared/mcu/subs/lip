#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_types_server_status.h"
#include "lip_opcua_server_tree_std_types_struct.h"
#include "lip_opcua_server_tree_std_types_buildinfo.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "../lip_opcua_server_tree_std_datatypes.h"
#include "../lip_opcua_server_tree_reftypes.h"
#include "../lip_opcua_server_tree_std_datatypes_descr.h"
#include "../lip_opcua_server_tree_std_objtypes.h"
#include "../lip_opcua_server_tree_std_vartypes.h"
#include "../lip_opcua_server_tree_std_names.h"
#include "../../lip_opcua_server_tree_decl_defs.h"
#include "../../objects/lip_opcua_server_tree_server_obj.h"
#include "../../objects/lip_opcua_server_tree_modeling_rules.h"
#include "lip/app/opcua/server/tree/items/lip_opcua_server_tree_std_var_descr.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/types/lip_opcua_struct.h"


/*----------- ServerStatusDataType -------------------------------------------*/




static const t_lip_opcua_struct_field f_datatype_server_status_fields[] = {
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("startTime",           g_lip_opcua_tree_std_data_descr_utc_time), /* Time (UTC) the Server was started. */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("currentTime",         g_lip_opcua_tree_std_data_descr_utc_time), /* The current time (UTC) as known by the Server. */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("state",               g_lip_opcua_tree_std_data_descr_server_state), /* The current state of the Server. */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("buildInfo",           g_lip_opcua_tree_std_data_descr_build_info),
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("secondsTillShutdown", g_lip_opcua_tree_std_data_descr_uint32), /* Approximate number of seconds until the Server will be shut down. */
    LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD("shutdownReason",      g_lip_opcua_tree_std_data_descr_localized_text), /* A localized text indicating the reason for the shutdown. */
};

static const t_lip_opcua_struct_def f_datatype_server_status_typedef = {
    .default_encoding_id = &g_lip_opcua_tree_std_item_datatype_server_status_enc_bin.nodeid,
    .base_data_type_id = &g_lip_opcua_tree_std_item_datatype_struct.nodeid,
    .struct_type = LIP_OPCUA_STRUCT_TYPE_STRUCT,
    LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(f_datatype_server_status_fields)
};

static const t_lip_opcua_tree_item_descr_datatype  f_datatype_server_status_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_DATA_TYPE,
    .is_abstract = false,
    .typedef_variant = LIP_OPCUA_TREE_ITEM_DATATYPE_TYPEDEF_VARIANT_STRUCT,
    .typedef_struct = &f_datatype_server_status_typedef
};

static const t_lip_opcua_tree_item_ref_descr f_datatype_server_status_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_datatype_struct) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_server_status_enc_bin)
    LIP_OPCUA_TREE_REF_DECL_END
};


const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_server_status = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_DATA_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ServerStatusDataType"),
    .refs = f_datatype_server_status_refs,
    .class_descr_hdr = &f_datatype_server_status_descr.hdr
};

/* ServerStatusDataType Binary Encoding */
static const t_lip_opcua_tree_item_ref_descr f_datatype_server_status_bin_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_datatype_enc_type) LIP_OPCUA_TREE_REF_DECL_NEXT
        LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_encoding, g_lip_opcua_tree_std_item_datatype_build_info)
    LIP_OPCUA_TREE_REF_DECL_END
};

const struct st_lip_opcua_tree_item g_lip_opcua_tree_std_item_datatype_server_status_enc_bin = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_DATA_TYPE_BIN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(LIP_OPCUA_TREE_ITEM_STD_NAME_DEF_BIN_ENC),
    .refs = f_datatype_server_status_bin_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};

/* ServerStatusType */
static const t_lip_opcua_tree_item *f_vartype_server_status_components[] = {
    &g_lip_opcua_tree_std_item_vardecl_server_status_start_time,
    &g_lip_opcua_tree_std_item_vardecl_server_status_cur_time,
    &g_lip_opcua_tree_std_item_vardecl_server_status_state,
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info,
    &g_lip_opcua_tree_std_item_vardecl_server_status_secs_till_shutdown,
    &g_lip_opcua_tree_std_item_vardecl_server_status_shutdown_reason,
};

static const t_lip_opcua_tree_item_ref_descr f_vartype_server_status_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_vartype_base_data_variable) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_component, f_vartype_server_status_components)
    LIP_OPCUA_TREE_REF_DECL_END
};





static const t_lip_opcua_tree_item_descr_vartype f_vartype_server_status_descr = {
    .hdr.node_class = LIP_OPCUA_NODECLASS_VARIABLE_TYPE,
    .data_descr = &g_lip_opcua_tree_std_data_descr_server_status,
    .is_abstract = false
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vartype_server_status = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ServerStatusType"),
    .refs = f_vartype_server_status_refs,
    .class_descr_hdr = &f_vartype_server_status_descr.hdr
};

/* ServerStatusType Components */
static const t_lip_opcua_tree_item_ref_descr f_server_status_type_comp_mandatory_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_component, g_lip_opcua_tree_std_item_vartype_server_status) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_base_data_variable) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_start_time = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_START_TIME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("StartTime"),
    .refs = f_server_status_type_comp_mandatory_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UTC_TIME(0)
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_cur_time = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_CURRENT_TIME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("CurrentTime"),
    .refs = f_server_status_type_comp_mandatory_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UTC_TIME(0)
};

static const struct st_lip_opcua_tree_item_descr_variable f_server_status_state_descr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_server_state,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_const_uint32
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_state = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_STATE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("State"),
    .refs = f_server_status_type_comp_mandatory_refs,
    .class_descr_hdr = &f_server_status_state_descr.hdr
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_secs_till_shutdown = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_SECONDS_TILL_SHUTDOWN),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("SecondsTillShutdown"),
    .refs = f_server_status_type_comp_mandatory_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UINT32(0)
};

static const t_lip_opcua_loc_string_ptr_list f_shutdown_reason_placehoder[] = {};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_shutdown_reason = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_SHUTDOWN_REASON),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ShutdownReason"),
    .refs = f_server_status_type_comp_mandatory_refs,
    LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_LOCTEXT_ARR(f_shutdown_reason_placehoder)
};


/* ServerStatusType BuildInfo Components */
static const t_lip_opcua_tree_item *f_serter_status_build_info_components[] = {
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info_product_uri,
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info_manufacturer_name,
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info_product_name,
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info_software_version,
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info_build_number,
    &g_lip_opcua_tree_std_item_vardecl_server_status_build_info_build_date
};


static const t_lip_opcua_tree_item_ref_descr f_server_status_build_info_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_component, g_lip_opcua_tree_std_item_vartype_server_status) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_datatype_build_info) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_has_component, f_serter_status_build_info_components)
    LIP_OPCUA_TREE_REF_DECL_END
};

static const struct st_lip_opcua_tree_item_descr_variable f_server_status_build_info_descr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_build_info,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildInfo"),
    .refs = f_server_status_build_info_refs,
    .class_descr_hdr = &f_server_status_build_info_descr.hdr
};

static const t_lip_opcua_tree_item_ref_descr f_server_status_build_info_type_comp_mandatory_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_component, g_lip_opcua_tree_std_item_vardecl_server_status_build_info) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_vartype_base_data_variable) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_modelling_rule, g_lip_opcua_tree_std_item_modeling_rule_mandatory)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info_product_uri = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_PRODUCT_URI),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ProductUri"),
    .refs = f_server_status_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info_manufacturer_name = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_MANUFACTURER_NAME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ManufacturerName"),
    .refs = f_server_status_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info_product_name = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_PRODUCT_NAME),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ProductName"),
    .refs = f_server_status_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info_software_version = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_SOFTWARE_VERSION),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("SoftwareVersion"),
    .refs = f_server_status_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info_build_number = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_BUILD_NUMBER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildNumber"),
    .refs = f_server_status_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_str.hdr
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_vardecl_server_status_build_info_build_date = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_BUILD_DATE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("BuildDate"),
    .refs = f_server_status_build_info_type_comp_mandatory_refs,
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_utc_time.hdr
};


#endif
