#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_std_var_descr.h"
#include "types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "types/lip_opcua_server_tree_std_datatypes.h"
#include "lip/app/opcua/server/tree/items/lip_opcua_server_tree_std_var_descr.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/lip_opcua_err_codes.h"
#include "lip/app/opcua/server/tree/lip_opcua_server_tree.h"

t_lip_opcua_err lip_opcua_tree_std_item_cb_read_null(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_null(&val->value);
    return LIP_OPCUA_ERR_GOOD;
}


/* const bool */
static t_lip_opcua_err f_item_std_const_bool_read(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_bool(&val->value, item->user.uint);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_bool = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_bool,
    .cbs.acc.value_read = &f_item_std_const_bool_read
};


/* const uint8 */
static t_lip_opcua_err f_item_std_const_uint8_read(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_uint8(&val->value, item->user.uint);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_uint8 = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_byte,
    .cbs.acc.value_read = &f_item_std_const_uint8_read
};



/* const uint32 */
t_lip_opcua_err lip_opcua_tree_std_item_cb_read_const_uint32(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_uint32(&val->value, item->user.uint);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_uint32 = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_uint32,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_const_uint32
};

/* const double */
t_lip_opcua_err lip_opcua_tree_std_item_cb_read_const_double(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_double(&val->value, item->user.vdouble);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_double = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_double,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_const_double
};


/* const uint64 */
static t_lip_opcua_err f_item_std_const_uint64_read(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_uint64(&val->value, item->user.uint64);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_utc_time = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
   .data_descr = &g_lip_opcua_tree_std_data_descr_utc_time,
   .cbs.acc.value_read = &f_item_std_const_uint64_read
};

/* const uint32 */
t_lip_opcua_err lip_opcua_tree_std_item_cb_read_const_str(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_str(&val->value, &item->user.str);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_str = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
   .data_descr = &g_lip_opcua_tree_std_data_descr_str,
   .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_const_str
};



/* const LocalizedText */
static t_lip_opcua_err f_item_std_const_localized_text_read(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    t_lip_opcua_loc_string_ptr loctxt;
    lip_opcua_server_tree_get_loc_text(item->user.loctext, session, &loctxt);
    lip_opcua_variant_set_loctxt(&val->value, &loctxt);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_localized_text = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_localized_text,
    .cbs.acc.value_read = &f_item_std_const_localized_text_read
};


/* const LocalizedText[] */
static t_lip_opcua_err f_item_std_const_localized_text_arr_read(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val) {
    lip_opcua_variant_set_array_ext(&val->value, LIP_OPCUA_BUILTIN_DATATYPE_LOCTEXT, item->user.carr.data, item->user.carr.size);
    return LIP_OPCUA_ERR_GOOD;
}

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_localized_text_arr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_localized_text_arr,
    .cbs.acc.value_read = &f_item_std_const_localized_text_arr_read
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_range = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_range,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_eu_info  = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_eu_info,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_axis_scale  = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_axis_scale,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_axis_info  = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_axis_info,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_axis_info_arr  = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_axis_info_arr,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};






const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_localized_text = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_localized_text,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_localized_text_arr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_localized_text_arr,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_enum_value_arr = {
    LIP_OPCUA_TREE_ITEM_VAR_INIT
    .data_descr = &g_lip_opcua_tree_std_data_descr_enum_value_arr,
    .cbs.acc.value_read = &lip_opcua_tree_std_item_cb_read_null
};

#endif
