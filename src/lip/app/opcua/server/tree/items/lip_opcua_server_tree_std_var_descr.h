#ifndef LIP_OPCUA_SERVER_TREE_STD_VAR_DESCR_H
#define LIP_OPCUA_SERVER_TREE_STD_VAR_DESCR_H

#include "../lip_opcua_tree_item.h"
#if 1
#define LIP_OPCUA_TREE_ITEM_DECL_VAR_STD_START(varname, bname_val, id_int, irefs, descr) \
    const t_lip_opcua_tree_item varname = {\
        .nodeid = LIP_OPCUA_NODEID_STDNS_INT(id_int), \
        .browse_name.ns_idx = LIP_OPCUA_NAMESPACE_IDX_OPCUA, \
        .browse_name.name.data = (bname_val), \
        .browse_name.name.size = sizeof(bname_val)-1, \
        .refs = (irefs), \
        .class_descr_hdr = &(descr).hdr,

#define LIP_OPCUA_TREE_ITEM_DECL_VAR_STD_CONST_UINT8(varname, bname_val, id, refs, val) \
    LIP_OPCUA_TREE_ITEM_DECL_VAR_STD_START(varname, bname_val, id, NULL, lip_opcua_tree_std_var_descr_const_uint8) \
    .user.uint = val \
}
#endif


#define LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UINT8(val) \
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_uint8.hdr, \
    .user.uint = val

#define LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UINT32(val) \
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_uint32.hdr, \
    .user.uint = val
#define LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_UTC_TIME(val) \
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_utc_time.hdr, \
    .user.uint64 = val

#define LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_LOCTXT(ltxt) \
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_localized_text.hdr, \
    .user.loctext = &(ltxt)



#define LIP_OPCUA_TREE_ITEM_FILL_VAR_USER_CONST_ARR(arrvar) \
    .user.carr.data = (arrvar), \
    .user.carr.size = sizeof(arrvar)/sizeof((arrvar)[0]) \

#define LIP_OPCUA_TREE_ITEM_FILL_VAR_CONST_LOCTEXT_ARR(arrvar) \
    .class_descr_hdr = &lip_opcua_tree_std_var_descr_const_localized_text_arr.hdr, \
    LIP_OPCUA_TREE_ITEM_FILL_VAR_USER_CONST_ARR(arrvar)



extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_bool;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_uint8;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_uint32;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_double;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_utc_time;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_str;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_localized_text;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_const_localized_text_arr;

extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_range;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_eu_info;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_axis_scale;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_axis_info;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_axis_info_arr;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_localized_text;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_localized_text_arr;
extern const struct st_lip_opcua_tree_item_descr_variable lip_opcua_tree_std_var_descr_null_enum_value_arr;


t_lip_opcua_err lip_opcua_tree_std_item_cb_read_null(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val);
t_lip_opcua_err lip_opcua_tree_std_item_cb_read_const_str(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val);
t_lip_opcua_err lip_opcua_tree_std_item_cb_read_const_uint32(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val);
t_lip_opcua_err lip_opcua_tree_std_item_cb_read_const_double(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *val);

#endif // LIP_OPCUA_SERVER_TREE_STD_VAR_DESCR_H
