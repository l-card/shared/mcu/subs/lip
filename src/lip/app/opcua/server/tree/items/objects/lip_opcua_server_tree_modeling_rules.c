#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_tree_modeling_rules.h"
#include "../lip_opcua_server_tree_decl_defs.h"
#include "../types/lip_opcua_server_tree_std_datatypes.h"
#include "../types/lip_opcua_server_tree_std_objtypes.h"
#include "../types/lip_opcua_server_tree_reftypes.h"
#include "lip/app/opcua/lip_opcua_std_nodeids.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"

/* --------------- ModellingRuleType --------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_objtype_modeling_rules_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_has_subtype, g_lip_opcua_tree_std_item_objtype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_objtype_modeling_rule = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MODELLING_RULE_TYPE),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ModellingRuleType"),
    .refs = f_objtype_modeling_rules_refs,
    .class_descr_hdr = &g_lip_opcua_tree_objtype_descr_concrete.hdr
};



static const t_lip_opcua_tree_item_ref_descr f_modeling_rule_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_modeling_rule)
    LIP_OPCUA_TREE_REF_DECL_END
};

/* --------------- ExposesItsArray --------------------------------------------*/
const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_modeling_rule_exposes_its_array = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MODELLING_RULE_EXPOSES_ITS_ARRAY),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ExposesItsArray"),
    .refs = f_modeling_rule_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};

/* --------------- Mandatory --------------------------------------------*/
const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_modeling_rule_mandatory = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MODELLING_RULE_MANDATORY),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Mandatory"),
    .refs = f_modeling_rule_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};

/* --------------- Optional --------------------------------------------*/
const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_modeling_rule_optional = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MODELLING_RULE_OPTIONAL),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Optional"),
    .refs = f_modeling_rule_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};

/* --------------- OptionalPlaceholder --------------------------------------------*/
const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_modeling_rule_optional_placeholder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MODELLING_RULE_OPTIONAL_PLACEHOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("OptionalPlaceholder"),
    .refs = f_modeling_rule_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};

/* --------------- MandatoryPlaceholder --------------------------------------------*/
const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_modeling_rule_mandatory_placeholder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_MODELLING_RULE_MANDATORY_PLACEHOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("MandatoryPlaceholder"),
    .refs = f_modeling_rule_refs,
    .class_descr_hdr = &g_lip_opcua_tree_obj_descr_noevt.hdr
};







#endif
