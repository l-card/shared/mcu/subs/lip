#include "lip/app/opcua/server/lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip/app/opcua/server/tree/lip_opcua_tree_item.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_ref.h"
#include "lip_opcua_server_tree_folders.h"
#include "lip_opcua_server_tree_server_obj.h"
#include "../types/lip_opcua_server_tree_std_datatypes.h"
#include "../types/lip_opcua_server_tree_std_objtypes.h"
#include "../types/lip_opcua_server_tree_std_vartypes.h"
#include "../types/lip_opcua_server_tree_reftypes.h"
#include "../lip_opcua_server_tree_decl_defs.h"

#include "lip/app/opcua/lip_opcua_std_nodeids.h"


static const t_lip_opcua_tree_item_ref_descr f_folder_type_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_has_type_definition, g_lip_opcua_tree_std_item_objtype_folder)
    LIP_OPCUA_TREE_REF_DECL_END
};

static const t_lip_opcua_tree_item_descr_object f_item_std_folder_descr = {
    LIP_OPCUA_TREE_ITEM_OBJECT_INIT
    .hdr.refs = f_folder_type_refs,
};


/*----------- Root folder ----------------------------------------------------*/
static const t_lip_opcua_tree_item *f_root_folder_organizes[] = {
    &g_lip_opcua_tree_std_item_views_folder,
    &g_lip_opcua_tree_std_item_objects_folder,
    &g_lip_opcua_tree_std_item_types_folder,
};
static const t_lip_opcua_tree_item_ref_descr f_root_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_organizes, f_root_folder_organizes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_root_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_ROOT_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Root"),
    .refs = f_root_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};


/*----------- Objects folder --------------------------------------------------*/
static const t_lip_opcua_tree_item *f_object_folder_organizes[] = {
    &g_lip_opcua_tree_std_item_server_obj,
    &g_lip_opcua_tree_std_item_locations_folder,
    &g_lip_opcua_tree_std_item_quantities_folder,
};

static const t_lip_opcua_tree_item_ref_descr f_objects_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_root_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_organizes, f_object_folder_organizes)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_objects_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_OBJECTS_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Objects"),
    .refs = f_objects_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};


/*----------- Locations folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_locations_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_objects_folder)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_locations_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_LOCATIONS),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Locations"),
    .refs = f_locations_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};


/*----------- Quantities folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_quantities_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_objects_folder)
LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_quantities_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_QUANTITIES),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Quantities"),
    .refs = f_quantities_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};



/*----------- Types folder ---------------------------------------------------*/
static const t_lip_opcua_tree_item *f_types_folder_organizes[] = {
    &g_lip_opcua_tree_std_item_object_types_folder,
    &g_lip_opcua_tree_std_item_variable_types_folder,
    &g_lip_opcua_tree_std_item_data_types_folder,
    &g_lip_opcua_tree_std_item_ref_types_folder,
    &g_lip_opcua_tree_std_item_event_types_folder,
    &g_lip_opcua_tree_std_item_iface_types_folder,
};
static const t_lip_opcua_tree_item_ref_descr f_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_root_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_MULTI_FWD(g_lip_opcua_tree_std_item_reftype_organizes, f_types_folder_organizes)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_TYPES_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Types"),
    .refs = f_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};


/*----------- Views folder ---------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_views_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_root_folder)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_views_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_VIEWS_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("Views"),
    .refs = f_views_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};



/*----------- ObjectTypes folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_object_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_objtype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};


const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_object_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_OBJECT_TYPES_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ObjectTypes"),
    .refs = f_object_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};

/*----------- VariableTypes folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_variable_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_vartype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_variable_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_VARIABLE_TYPES_FOLDER),    
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("VariableTypes"),
    .refs = f_variable_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};

/*----------- DataTypes folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_data_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_datatype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_data_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_DATA_TYPES_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("DataTypes"),
    .refs = f_data_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};

/*----------- ReferenceTypes folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_ref_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_types_folder) LIP_OPCUA_TREE_REF_DECL_NEXT
    LIP_OPCUA_TREE_REF_DECL_SINGLE_FWD(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_reftype_base)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_ref_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_REFERENCE_TYPES_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("ReferenceTypes"),
    .refs = f_ref_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};

/*----------- EventTypes folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_event_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_types_folder)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_event_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_EVENT_TYPES_FOLDER),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("EventTypes"),
    .refs = f_event_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};


/*----------- EventTypes folder --------------------------------------------------*/
static const t_lip_opcua_tree_item_ref_descr f_iface_types_folder_refs[] = {
    LIP_OPCUA_TREE_REF_DECL_SINGLE_INV(g_lip_opcua_tree_std_item_reftype_organizes, g_lip_opcua_tree_std_item_types_folder)
    LIP_OPCUA_TREE_REF_DECL_END
};

const t_lip_opcua_tree_item g_lip_opcua_tree_std_item_iface_types_folder = {
    .nodeid = LIP_OPCUA_NODEID_STDNS_INT(LIP_OPCUA_NODEID_STD_INTERFACE_TYPES),
    LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME("InterfaceTypes"),
    .refs = f_iface_types_folder_refs,
    .class_descr_hdr = &f_item_std_folder_descr.hdr
};


#endif
