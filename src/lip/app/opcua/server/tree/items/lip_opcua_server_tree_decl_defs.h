#ifndef LIP_OPCUA_SERVER_TREE_DECL_DEFS_H
#define LIP_OPCUA_SERVER_TREE_DECL_DEFS_H


#define LIP_OPCUA_TREE_ITEM_FILL_STD_BROWSE_NAME(bname_val) \
    .browse_name.ns_idx = LIP_OPCUA_NAMESPACE_IDX_OPCUA, \
    .browse_name.name.data = (bname_val), \
    .browse_name.name.size = sizeof(bname_val)-1

#define LIP_OPCUA_TREE_ITEM_FILL_STD_NAME(name_val) \
    .name.data = (name_val), \
    .name.size = sizeof(name_val)-1

#define LIP_OPCUA_TREE_ITEM_FILL_CONST_STR(str_val) \
    .data = (str_val), \
    .size = sizeof(str_val)-1


#define LIP_OPCUA_TREE_ITEM_FILL_CONST_LOC_STR(str_val) {{LIP_OPCUA_TREE_ITEM_FILL_CONST_STR(str_val)}}



#define LIP_OPCUA_TREE_ITEM_DECL_STRUCT_FIELD(fname, dtype) {LIP_OPCUA_TREE_ITEM_FILL_STD_NAME(fname), .data_descr = &(dtype)}

#define LIP_OPCUA_TREE_ITEM_FILL_STRUCT_DEF_FIELDS(farray) \
    .fields_cnt = sizeof(farray)/sizeof((farray)[0]), \
    .fields = farray

#endif // LIP_OPCUA_SERVER_TREE_DECL_DEFS_H
