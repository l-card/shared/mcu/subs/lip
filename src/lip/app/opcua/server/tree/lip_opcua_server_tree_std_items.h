#ifndef LIP_OPCUA_SERVER_TREE_STD_ITEMS_H
#define LIP_OPCUA_SERVER_TREE_STD_ITEMS_H

#include "items/lip_opcua_server_tree_decl_defs.h"
#include "items/types/lip_opcua_server_tree_std_datatypes.h"
#include "items/types/lip_opcua_server_tree_std_objtypes.h"
#include "items/types/lip_opcua_server_tree_std_vartypes.h"
#include "items/types/lip_opcua_server_tree_reftypes.h"
#include "items/types/lip_opcua_server_tree_std_datatypes_descr.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_struct.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_enum.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_buildinfo.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_server_status.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_server.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_da_range.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_da_complex_num.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_da_axis.h"
#include "items/types/groups/lip_opcua_server_tree_std_types_da_items.h"
#include "items/objects/lip_opcua_server_tree_folders.h"
#include "items/objects/lip_opcua_server_tree_server_obj.h"
#include "items/objects/lip_opcua_server_tree_modeling_rules.h"
#include "items/lip_opcua_server_tree_std_var_descr.h"

#endif // LIP_OPCUA_SERVER_TREE_STD_ITEMS_H
