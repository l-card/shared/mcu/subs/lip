#ifndef LIP_OPCUA_SERVER_TREE_H
#define LIP_OPCUA_SERVER_TREE_H

#include "lip_opcua_tree_item.h"
#include "lip_opcua_tree_item_ref.h"
#include "lip/app/opcua/types/lip_opcua_nodeid.h"
#include "lip/app/opcua/types/lip_opcua_readvalueid.h"
#include "lip/app/opcua/types/lip_opcua_write_value.h"



struct st_lip_opcua_session_ctx;

typedef struct {
    const t_lip_opcua_tree_item *ref_type;
    uint8_t include_subtypes;
    uint8_t browse_dir;
    uint16_t node_class_mask;
    t_lip_opcua_qualified_name qname;
} t_lip_opcua_tree_item_browse_params;

void lip_opcua_server_tree_init(void);

t_lip_errs lip_opcua_server_tree_item_add(const t_lip_opcua_tree_item *item);
t_lip_errs lip_opcua_server_tree_item_remove(const t_lip_opcua_tree_item *item);
t_lip_errs lip_opcua_server_tree_item_add_extref(const t_lip_opcua_tree_item *item, const t_lip_opcua_tree_item_ref_descr *ref_descr);

t_lip_opcua_err lip_opcua_server_tree_get_item(struct st_lip_opcua_session_ctx *session, const t_lip_opcua_nodeid *nodeid, const t_lip_opcua_tree_item **item);
t_lip_opcua_err lip_opcua_server_tree_read(struct st_lip_opcua_session_ctx *session, const t_lip_opcua_read_value_id *readid, t_lip_opcua_ret_tstmp_types tstmps, t_lip_opcua_data_value *value);
t_lip_opcua_err lip_opcua_server_tree_write(struct st_lip_opcua_session_ctx *session, const t_lip_opcua_write_value *wrval);


t_lip_opcua_err lip_opcua_server_tree_item_update_tstmps(const struct st_lip_opcua_tree_item *item, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *value);
t_lip_opcua_err lip_opcua_server_tree_item_read_value(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lip_opcua_data_value *value);
t_lip_opcua_err lip_opcua_server_tree_item_write_value(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_write_params *params, const t_lip_opcua_data_value *value);
t_lip_opcua_err lip_opcua_server_tree_item_get_display_name(const struct st_lip_opcua_tree_item *item, struct st_lip_opcua_session_ctx *session, t_lip_opcua_loc_string_ptr *locstr);
t_lip_opcua_err lip_opcua_server_tree_get_loc_text(const t_lip_opcua_loc_string_ptr_list *locvar, struct st_lip_opcua_session_ctx *session, t_lip_opcua_loc_string_ptr *locstr);
t_lip_opcua_err lip_opcua_server_tree_item_browse(const t_lip_opcua_tree_item *item, const t_lip_opcua_tree_item_browse_params *params, t_lip_opcua_tree_item_ref *refs, uint32_t *refs_cnt);
const t_lip_opcua_nodeid * lip_opcua_server_tree_item_get_typedef_id(const struct st_lip_opcua_tree_item *item);


void lip_opcua_server_set_server_uri(const char *uri, int32_t len);

#endif // LIP_OPCUA_SERVER_TREE_H
