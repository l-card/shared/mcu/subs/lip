#ifndef LIP_OPCUA_SERVER_REQ_H
#define LIP_OPCUA_SERVER_REQ_H

#include "lip/app/opcua/types/lip_opcua_types.h"
#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"

/* Информация о запросе, которая необходима для подготовки ответа */
typedef struct {
    uint32_t req_id;
    uint32_t req_handle;
} t_lip_opcua_req_resp_info;

typedef struct {
    t_lip_opcua_req_resp_info resp;
    t_lip_opcua_nodeid node;
    t_lip_opcua_nodeid auth_token;
    t_lip_opcua_datetime  tstmp;
    t_lip_opcua_string_ptr audit_entry;
    uint32_t ret_diagnostic;
    uint32_t tout_hint;
} t_lip_opcua_req_info;


t_lip_opcua_err lip_opcua_req_parse_header(t_lip_proc_buf *buf, t_lip_opcua_req_info *req);
t_lip_opcua_err lip_opcua_req_put_resp_header(t_lip_proc_buf *buf, uint32_t req_handle, const t_lip_opcua_nodeid *node_id, t_lip_opcua_err status_code);

#endif // LIP_OPCUA_SERVER_REQ_H
