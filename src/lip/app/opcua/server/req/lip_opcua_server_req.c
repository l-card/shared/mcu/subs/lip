#include "../lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip_opcua_server_req.h"
#include "../lip_opcua_server_time.h"



t_lip_opcua_err lip_opcua_req_parse_header(t_lip_proc_buf *buf, t_lip_opcua_req_info *req) {
    t_lip_opcua_err err = lip_opcua_procbuf_get_nodeid(buf, &req->node);
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_get_nodeid(buf, &req->auth_token);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_get_datetime(buf, &req->tstmp);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_get_uint32(buf, &req->resp.req_handle);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_get_uint32(buf, &req->ret_diagnostic);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_get_string(buf, &req->audit_entry);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_get_uint32(buf, &req->tout_hint);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        t_lip_opcua_extobj_info addit_params;
        err = lip_opcua_procbuf_get_extobj_info(buf, &addit_params);        
    }

    if (err == LIP_OPCUA_ERR_GOOD) {
        lip_opcua_update_client_tstmp(&req->tstmp);
    }
    return err;
}

t_lip_opcua_err lip_opcua_req_put_resp_header(t_lip_proc_buf *buf, uint32_t req_handle, const t_lip_opcua_nodeid *node_id, t_lip_opcua_err status_code) {
    t_lip_opcua_err err = lip_opcua_procbuf_put_nodeid(buf, node_id);
    if (err == LIP_OPCUA_ERR_GOOD) {
        t_lip_opcua_tstmp tstmp;
        lip_opcua_get_cur_tstmp(&tstmp);
        err = lip_opcua_procbuf_put_datetime(buf, tstmp.ns100);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_put_uint32(buf, req_handle);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        err = lip_opcua_procbuf_put_uint32(buf, status_code);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        /* Не передаем diagnostic info (encoding  = 0) */
        err = lip_opcua_procbuf_put_uint8(buf, 0);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        /* Не передаем string table для diagnostic info (array size = -1) */
        err = lip_opcua_procbuf_put_int32(buf,  -1);
    }
    if (err == LIP_OPCUA_ERR_GOOD) {
        t_lip_opcua_extobj_info addit_params;
        addit_params.type_id.type = LIP_OPCUA_NODEID_TYPE_INVALID;
        addit_params.body_len = 0;
        err = lip_opcua_procbuf_put_extobj(buf, &addit_params);
    }

    return err;
}

#endif
