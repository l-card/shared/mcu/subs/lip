#ifndef LIP_OPCUA_SERVER_SVCSET_SUBSCRIPTION_H
#define LIP_OPCUA_SERVER_SVCSET_SUBSCRIPTION_H

#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"
#include "../lip_opcua_server_req.h"

struct st_lip_opcua_con;

t_lip_opcua_err lip_opcua_server_procreq_subscription_create(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_subscription_modify(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_subscription_set_pubmode(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_subscription_publish(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_subscription_republish(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_subscription_transfer(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_subscription_delete(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);

t_lip_proc_buf *lip_opcua_server_subscription_pub_resp_prepare(struct st_lip_opcua_con *con, t_lip_opcua_req_resp_info *req, t_lip_opcua_intid subid, t_lip_proc_buf *more_notify_buf);
t_lip_opcua_err lip_opcua_server_subscription_pub_resp_finish(struct st_lip_opcua_con *con, t_lip_proc_buf *buf);

#endif // LIP_OPCUA_SERVER_SVCSET_SUBSCRIPTION_H
