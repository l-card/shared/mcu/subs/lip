#ifndef LIP_OPCUA_SERVER_SVCSET_DISCOVERY_H
#define LIP_OPCUA_SERVER_SVCSET_DISCOVERY_H

#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"
#include "../lip_opcua_server_req.h"
#include "../../lip_opcua_server.h"

struct st_lip_opcua_con;
struct st_lip_opcua_session_ctx;

t_lip_opcua_err lip_opcua_server_procreq_find_servers(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_procreq_get_endpoints(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_procbuf_put_ep_list(t_lip_proc_buf *resp_buf, const t_lip_opcua_ep_description **ep_descr_list, int ep_descr_cnt, struct st_lip_opcua_session_ctx *session);

#endif // LIP_OPCUA_SERVER_SVCSET_DISCOVERY_H
