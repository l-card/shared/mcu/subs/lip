#ifndef LIP_OPCUA_SERVER_SVCSET_MONITEM_H
#define LIP_OPCUA_SERVER_SVCSET_MONITEM_H

#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"
#include "../lip_opcua_server_req.h"

struct st_lip_opcua_con;

t_lip_opcua_err lip_opcua_server_svcset_monitem_create(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_svcset_monitem_modify(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_svcset_monitem_set_mode(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_svcset_monitem_set_trig(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);
t_lip_opcua_err lip_opcua_server_svcset_monitem_delete(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);

#endif // LIP_OPCUA_SERVER_SVCSET_MONITEM_H
