#ifndef LIP_OPCUA_SERVER_REQPROC_H
#define LIP_OPCUA_SERVER_REQPROC_H

#include "lip/app/opcua/server/lip_opcua_server_con.h"
#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"
#include "lip_opcua_server_req.h"

t_lip_opcua_err lip_opcua_server_req_process(struct st_lip_opcua_con *con, const t_lip_opcua_req_info *req, t_lip_proc_buf *data_buf);

void lip_opcua_server_resp_fault(struct st_lip_opcua_con *con, const t_lip_opcua_req_resp_info *req, t_lip_opcua_err resp_err);

#endif // LIP_OPCUA_SERVER_REQPROC_H
