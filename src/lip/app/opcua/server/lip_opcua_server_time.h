#ifndef LIP_OPCUA_SERVER_TIME_H
#define LIP_OPCUA_SERVER_TIME_H

#include "lip/app/opcua/types/lip_opcua_tstmp.h"


void lip_opcua_server_time_init(void);
t_lip_opcua_datetime lip_opcua_server_start_time(void) ;
void lip_opcua_update_client_tstmp(const t_lip_opcua_datetime *tstmp);
void lip_opcua_get_cur_tstmp(t_lip_opcua_tstmp *tstmp);

#endif // LIP_OPCUA_SERVER_TIME_H
