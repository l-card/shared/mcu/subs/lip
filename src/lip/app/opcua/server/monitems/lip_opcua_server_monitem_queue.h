#ifndef LIP_OPCUA_SERVER_MONITEM_QUEUE_H
#define LIP_OPCUA_SERVER_MONITEM_QUEUE_H

#include "lip_opcua_server_monitem_notification.h"

typedef struct {
    uint16_t size;
    uint16_t rdy_cnt;
    uint16_t pub_rdy;
    union {
#if  LIP_OPCUA_SERVER_MONITEMS_QUEUE_ENABLE
        struct {
            uint8_t  discart_oldest;
            uint8_t  set_next_ov;
            uint16_t get_pos;
            uint32_t ov_status;
            t_lip_opcua_notification_info *notifications;
        } queue;
#endif
        struct {
            t_lip_opcua_notification_info notification;
        } local;
    };

} t_lip_opcua_monitem_notification_ctx;


void lip_opcua_server_monitem_queues_init(void);
void lip_opcua_server_monitem_queue_init(t_lip_opcua_monitem_notification_ctx *ctx, t_lip_opcua_counter req_size, bool discart_oldest);
void lip_opcua_server_monitem_queue_delete(t_lip_opcua_monitem_notification_ctx *ctx);
void lip_opcua_server_monitem_queue_clear(t_lip_opcua_monitem_notification_ctx *ctx);
void lip_opcua_server_monitem_queue_update_size(t_lip_opcua_monitem_notification_ctx *ctx, t_lip_opcua_counter req_size, bool discart_oldest);




t_lip_opcua_notification_info *lip_opcua_server_monitem_queue_get_start(t_lip_opcua_monitem_notification_ctx *ctx);
void lip_opcua_server_monitem_queue_get_finish(t_lip_opcua_monitem_notification_ctx *ctx);
t_lip_opcua_notification_info *lip_opcua_server_monitem_put_start(t_lip_opcua_monitem_notification_ctx *ctx);
void lip_opcua_server_monitem_put_finish(t_lip_opcua_monitem_notification_ctx *ctx);
void lip_opcua_server_monitem_queue_drop_oldest(t_lip_opcua_monitem_notification_ctx *ctx);

static inline uint16_t lip_opcua_server_monitem_queue_rdy_cnt(t_lip_opcua_monitem_notification_ctx *ctx) {
    return ctx->rdy_cnt;
}

static inline uint16_t lip_opcua_server_monitem_queue_free_cnt(t_lip_opcua_monitem_notification_ctx *ctx) {
    return
#if  LIP_OPCUA_SERVER_MONITEMS_QUEUE_ENABLE
        ctx->size > 1 ? ctx->size - ctx->rdy_cnt :
#endif
        1;
}




#endif // LIP_OPCUA_SERVER_MONITEM_QUEUE_H
