#ifndef LIP_OPCUA_SERVER_MONITEM_NOTIFICATION_H
#define LIP_OPCUA_SERVER_MONITEM_NOTIFICATION_H

#include "lip/app/opcua/types/lip_opcua_datavalue.h"

typedef enum {
    LIP_OPCUA_NOTIFICATION_TYPE_DATA_CHANGE = 1
} t_lip_opcua_notification_type;

typedef struct {
    t_lip_opcua_notification_type type;
    union {
        t_lip_opcua_data_value data;
    };
} t_lip_opcua_notification_info;

#endif // LIP_OPCUA_SERVER_MONITEM_NOTIFICATION_H
