#ifndef LIP_OPCUA_SERVER_MONITEM_PARAMS_H
#define LIP_OPCUA_SERVER_MONITEM_PARAMS_H

#include "lip/app/opcua/types/lip_opcua_base_types.h"
#include "lip/app/opcua/types/lip_opcua_data_change_filter.h"


typedef struct {
    t_lip_opcua_intid       client_handle;
    t_lip_opcua_duration    sampling_interval;
    t_lip_opcua_counter     queue_size;
    bool                    discart_oldest;
    t_lip_opcua_filter      filter;
} t_lip_opcua_monitoring_params;

#endif // LIP_OPCUA_SERVER_MONITEM_PARAMS_H
