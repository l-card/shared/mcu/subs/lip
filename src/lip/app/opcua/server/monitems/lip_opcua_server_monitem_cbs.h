#ifndef LIP_OPCUA_SERVER_MONITEM_CBS_H
#define LIP_OPCUA_SERVER_MONITEM_CBS_H

#include "lip/app/opcua/server/monitems/lip_opcua_server_monitem_params.h"
#include "lip/app/opcua/server/tree/lip_opcua_tree_item_acc.h"
#include "lip/app/opcua/types/lip_opcua_datavalue.h"
#include "lclock.h"

struct st_lip_opcua_monitem_ctx;
struct st_lip_opcua_session_ctx;
struct st_lip_opcua_tree_item;


typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_create)(struct st_lip_opcua_monitem_ctx *monitem);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_delete)(struct st_lip_opcua_monitem_ctx *monitem);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_check_params)(struct st_lip_opcua_monitem_ctx *monitem, t_lip_opcua_monitoring_params *params);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_sampling_start)(struct st_lip_opcua_monitem_ctx *monitem, t_lclock_ticks time);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_sampling_stop)(struct st_lip_opcua_monitem_ctx *monitem);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_sample)(struct st_lip_opcua_monitem_ctx *monitem, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lclock_ticks time, t_lip_opcua_data_value *val);
typedef t_lip_opcua_err (*t_lip_opcua_item_cb_mon_get_min_sampling_interval)(const struct st_lip_opcua_tree_item *item, t_lip_opcua_duration *interval);

typedef struct {
    t_lip_opcua_item_cb_mon_create create;
    t_lip_opcua_item_cb_mon_create del;
    t_lip_opcua_item_cb_mon_check_params check_params;
    t_lip_opcua_item_cb_mon_sampling_start start;
    t_lip_opcua_item_cb_mon_sampling_stop stop;
    t_lip_opcua_item_cb_mon_sample sample;
    t_lip_opcua_item_cb_mon_get_min_sampling_interval get_min_sampling_interval;
} t_lip_opcua_tree_item_mon_cbs;

extern const t_lip_opcua_tree_item_mon_cbs g_lip_opcua_tree_item_mon_cbs_def_value_read;

#endif // LIP_OPCUA_SERVER_MONITEM_CBS_H
