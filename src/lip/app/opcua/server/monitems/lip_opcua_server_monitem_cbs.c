#include "lip/app/opcua/lip_opcua_cfg_defs.h"
#if LIP_OPCUA_SERVER_SUBS_ENABLE
#include "lip_opcua_server_monitem_cbs.h"
#include "lip_opcua_server_monitem.h"
#include "lip/app/opcua/types/lip_opcua_statuscode.h"
#include "lip/app/opcua/server/tree/lip_opcua_server_tree.h"
#include "lip/app/opcua/server/lip_opcua_server_params.h"
#include "lip/app/opcua/lip_opcua_err_codes.h"

static t_lip_opcua_err f_rd_val_cb_mon_sample(struct st_lip_opcua_monitem_ctx *monitem, struct st_lip_opcua_session_ctx *session, const t_lip_opcua_item_read_params *params, t_lclock_ticks time, t_lip_opcua_data_value *val) {
    t_lip_opcua_err err = LIP_OPCUA_ERR_GOOD;
    const t_lip_opcua_tree_item_descr_variable *var_descr = lip_opcua_tree_item_variable_descr(monitem->item);
    if (!var_descr) {
        err = LIP_OPCUA_ERR_BAD_NODEID_UNKNOWN;
    } else {
        err = lip_opcua_server_tree_item_read_value(monitem->item, session, params, val);
    }
    return err;
}

static t_lip_opcua_err f_cb_mon_check_params_default(struct st_lip_opcua_monitem_ctx *monitem, t_lip_opcua_monitoring_params *params) {
    if (params->sampling_interval < 0) {
        params->sampling_interval = LIP_OPCUA_SERVER_PARAM_DEFAULT_SAMPLE_RATE_MS;
    } else if (params->sampling_interval < LIP_OPCUA_SERVER_PARAM_MIN_SAMPLE_RATE_MS) {
        params->sampling_interval = LIP_OPCUA_SERVER_PARAM_MIN_SAMPLE_RATE_MS;
    }
    return LIP_OPCUA_ERR_GOOD;
}


const t_lip_opcua_tree_item_mon_cbs g_lip_opcua_tree_item_mon_cbs_def_value_read = {
    .check_params = f_cb_mon_check_params_default,
    .sample = f_rd_val_cb_mon_sample
};


#endif
