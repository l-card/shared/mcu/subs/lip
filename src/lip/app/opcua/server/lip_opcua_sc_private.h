#ifndef LIP_OPCUA_SC_PRIVATE_H
#define LIP_OPCUA_SC_PRIVATE_H

#include "lip_opcua_sc.h"

typedef struct st_lip_opcua_sc_ctx {
    uint8_t valid;
    t_lip_opcua_sc_token cur_token; /* используемый в настоящее время токен для проверки принимаемых сообщений */
    t_lip_opcua_sc_token new_token; /* новый токен с момента приема запроса на обновления и до приема первого сообщения с новым tokenID, т.к. до этого момента можем принимать еще запросы со старым */
    /* номер последовательности для отправляемых и принимаемых сообщений. независим от токена */
    uint32_t rx_seq_num;
    uint32_t tx_seq_num;
    struct st_lip_opcua_session_ctx *cur_session;
} t_lip_opcua_sc_ctx;


#endif // LIP_OPCUA_SC_PRIVATE_H
