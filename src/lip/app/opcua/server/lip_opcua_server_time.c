#include "lip_opcua_server_cfg_defs.h"
#if LIP_OPCUA_SERVER_ENABLE
#include "lip/lip_defs.h"
#include "lip/app/opcua/types/lip_opcua_tstmp.h"
#if LIP_PTP_ENABLE
#include "lip/app/ptp/lip_ptp.h"
#endif
#include "ltimer.h"


static t_lip_opcua_datetime f_client_last_tsmp;
static t_lclock_ticks       f_client_last_tsmp_update_ticks;
#if LIP_PTP_ENABLE
static t_lip_ptp_tstmp      f_server_start_loc_time;
#endif

void lip_opcua_update_client_tstmp(const t_lip_opcua_datetime *tstmp) {
    f_client_last_tsmp = *tstmp;
    f_client_last_tsmp_update_ticks = lclock_get_ticks();
}

void lip_opcua_get_cur_tstmp(t_lip_opcua_tstmp *tstmp) {

    bool processed = false;
#if LIP_PTP_ENABLE
    /* при возможности используем время PTP */
    if (lip_ptp_gm_time_valid()) {
        t_lip_ptp_tstmp ptp_loc_time;
        if (lip_ptp_get_local_time(&ptp_loc_time)) {
            t_lip_ptp_tstmp ptp_gm_time;
            lip_ptp_tstmp_local_to_gm(&ptp_loc_time, &ptp_gm_time);

            lip_opcua_tstmp_from_ptp(&ptp_gm_time, tstmp, lip_ptp_gm_stats()->time_info);

            processed = true;
        }
    }
#endif
    /* если не удалось получить время, используем на основе последнего времени клиента */
    if (!processed) {
        tstmp->ns100 = f_client_last_tsmp + (lclock_get_ticks() - f_client_last_tsmp_update_ticks) * (10000000 / LCLOCK_TICKS_PER_SECOND);
    }
}

bool lip_ll_tstmp_is_enabled(void);

void lip_opcua_server_time_init(void) {
#if LIP_PTP_ENABLE
    lip_ptp_get_local_time(&f_server_start_loc_time);
#else
    #error  non ptp start time setup
#endif
}


t_lip_opcua_datetime lip_opcua_server_start_time(void) {
    t_lip_opcua_tstmp ua_ts;
#if LIP_PTP_ENABLE
    if (lip_ptp_gm_time_valid()) {
        t_lip_ptp_tstmp ptp_gm_time;
        lip_ptp_tstmp_local_to_gm(&f_server_start_loc_time, &ptp_gm_time);
        lip_opcua_tstmp_from_ptp(&ptp_gm_time, &ua_ts, lip_ptp_gm_stats()->time_info);
    } else {
        lip_opcua_tstmp_from_ptp(&f_server_start_loc_time, &ua_ts, 0);
    }
#else
    /** @todo */
#endif
    return ua_ts.ns100;
}

#endif
