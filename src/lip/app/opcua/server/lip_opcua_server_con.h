#ifndef LIP_OPCUA_SERVER_CON_H
#define LIP_OPCUA_SERVER_CON_H

#include "lip/app/opcua/binary/lip_opcua_binary_types_procbuf.h"
#include "lip/app/opcua/server/req/lip_opcua_server_req.h"
#include "lip/app/opcua/server/lip_opcua_sc.h"

struct st_lip_opcua_con;

void lip_opcua_server_con_init(void);
void lip_opcua_server_con_pull(void);
void lip_opcua_server_con_close(void);



t_lip_proc_buf *lip_opcua_con_resp_prepare(struct st_lip_opcua_con *con, const t_lip_opcua_req_resp_info *req, const t_lip_opcua_nodeid *nodeid, t_lip_opcua_err status);
void lip_opcua_con_resp_finish(struct st_lip_opcua_con *con, t_lip_proc_buf *buf);
struct st_lip_opcua_sc_ctx *lip_opcua_con_sc(struct st_lip_opcua_con *con);
struct st_lip_opcua_session_ctx *lip_opcua_con_session(struct st_lip_opcua_con *con);
void lip_opcua_con_sc_close(struct st_lip_opcua_con *con);
struct st_lip_opcua_con *lip_opcua_sc_get_con(struct st_lip_opcua_sc_ctx *sc);


#endif // LIP_OPCUA_SERVER_CON_H
