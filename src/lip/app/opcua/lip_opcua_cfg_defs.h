#ifndef LIP_OPCUA_CFG_DEFS_H
#define LIP_OPCUA_CFG_DEFS_H

#include "lip_config.h"
#include "server/lip_opcua_server_cfg_defs.h"
#include "pubsub/uadp/lip_opcua_uadp_cfg_defs.h"

#ifdef LIP_CFG_OPCUA_ENABLE
    #define LIP_OPCUA_ENABLE                LIP_CFG_OPCUA_ENABLE
#else
    #define LIP_OPCUA_ENABLE                0
#endif

#ifdef LIP_CFG_OPCUA_TSTMP_PICOSEC_EN
    #define LIP_OPCUA_TSTMP_PICOSEC_EN      LIP_CFG_OPCUA_TSTMP_PICOSEC_EN
#else
    #define LIP_OPCUA_TSTMP_PICOSEC_EN      0
#endif

#ifdef LIP_CFG_OPCUA_STRING_ARR_MAX_SIZE
    #define LIP_OPCUA_STRING_ARR_MAX_SIZE   LIP_CFG_OPCUA_STRING_ARR_MAX_SIZE
#else
    #define LIP_OPCUA_STRING_ARR_MAX_SIZE   64
#endif

#ifdef LIP_CFG_OPCUA_BROWSE_REFS_MAX
    #define LIP_OPCUA_BROWSE_REFS_MAX       LIP_CFG_OPCUA_BROWSE_REFS_MAX
#else
    #define LIP_OPCUA_BROWSE_REFS_MAX       32
#endif


#ifdef LIP_CFG_OPCUA_LOCALE_CNT
    #define LIP_OPCUA_LOCALE_CNT            LIP_CFG_OPCUA_LOCALE_CNT
#else
    #define LIP_OPCUA_LOCALE_CNT            1
#endif






#endif // LIP_OPCUA_CFG_DEFS_H
