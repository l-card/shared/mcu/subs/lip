#ifndef LIP_OPCUA_ERR_CODES_H
#define LIP_OPCUA_ERR_CODES_H

#define LIP_OPCUA_ERR_GOOD                                      0x00000000 /* The operation succeeded. */
#define LIP_OPCUA_ERR_UNCERTAIN                                 0x40000000 /* Uncertain - The operation was uncertain. */
#define LIP_OPCUA_ERR_BAD                                       0x80000000 /* Bad - The operation failed. */
#define LIP_OPCUA_ERR_BAD_UNEXPECTED_ERROR                      0x80010000 /* BadUnexpectedError - An unexpected error occurred. */
#define LIP_OPCUA_ERR_BAD_INTERNAL_ERROR                       	0x80020000 /* BadInternalError - An internal error occurred as a result of a programming or configuration error. */
#define LIP_OPCUA_ERR_BAD_OUT_OF_MEMORY                         0x80030000 /* BadOutOfMemory - Not enough memory to complete the operation. */
#define LIP_OPCUA_ERR_BAD_RESOURCE_UNAVAILABLE                  0x80040000 /* BadResourceUnavailable - An operating system resource is not available. */
#define LIP_OPCUA_ERR_BAD_COMMUNICATION_ERROR                   0x80050000 /* BadCommunicationError - A low level communication error occurred. */
#define LIP_OPCUA_ERR_BAD_ENCODING_ERROR                        0x80060000 /* BadEncodingError - Encoding halted because of invalid data in the objects being serialized. */
#define LIP_OPCUA_ERR_BAD_DECODING_ERROR                        0x80070000 /* BadDecodingError - Decoding halted because of invalid data in the stream. */
#define LIP_OPCUA_ERR_BAD_ENCODING_LIMITS_EXCEEDED              0x80080000 /* BadEncodingLimitsExceeded - The message encoding/decoding limits imposed by the stack have been exceeded. */
#define LIP_OPCUA_ERR_BAD_REQUEST_TOO_LARGE                     0x80B80000 /* BadRequestTooLarge - The request message size exceeds limits set by the server. */
#define LIP_OPCUA_ERR_BAD_RESPONSE_TOO_LARGE                    0x80B90000 /* BadResponseTooLarge - The response message size exceeds limits set by the client or server. */
#define LIP_OPCUA_ERR_BAD_UNKNOWN_RESPONSE                      0x80090000 /* BadUnknownResponse - An unrecognized response was received from the server. */
#define LIP_OPCUA_ERR_BAD_TIMEOUT                               0x800A0000 /* BadTimeout - The operation timed out. */
#define LIP_OPCUA_ERR_BAD_SERVICE_UNSUPPORTED                   0x800B0000 /* BadServiceUnsupported - The server does not support the requested service. */
#define LIP_OPCUA_ERR_BAD_SHUTDOWN                              0x800C0000 /* BadShutdown - The operation was cancelled because the application is shutting down. */
#define LIP_OPCUA_ERR_BAD_SERVER_NOT_CONNECTED                  0x800D0000 /* BadServerNotConnected - The operation could not complete because the client is not connected to the server. */
#define LIP_OPCUA_ERR_BAD_SERVER_HALTED                         0x800E0000 /* BadServerHalted - The server has stopped and cannot process any requests. */
#define LIP_OPCUA_ERR_BAD_NOTHING_TO_DO                         0x800F0000 /* BadNothingToDo - No processing could be done because there was nothing to do. */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_OPERATIONS                   0x80100000 /* BadTooManyOperations - The request could not be processed because it specified too many operations. */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_MONITORED_ITEMS              0x80DB0000 /* BadTooManyMonitoredItems - The request could not be processed because there are too many monitored items in the subscription. */
#define LIP_OPCUA_ERR_BAD_DATA_TYPE_ID_UNKNOWN                  0x80110000 /* BadDataTypeIdUnknown - The extension object cannot be (de)serialized because the data type id is not recognized. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_INVALID                   0x80120000 /* BadCertificateInvalid - The certificate provided as a parameter is not valid. */
#define LIP_OPCUA_ERR_BAD_SECURITY_CHECKS_FAILED                0x80130000 /* BadSecurityChecksFailed - An error occurred verifying security. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_POLICY_CHECK_FAILED       0x81140000 /* BadCertificatePolicyCheckFailed - The certificate does not meet the requirements of the security policy. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_TIME_INVALID              0x80140000 /* BadCertificateTimeInvalid - The certificate has expired or is not yet valid. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_ISSUER_TIME_INVALID       0x80150000 /* BadCertificateIssuerTimeInvalid - An issuer certificate has expired or is not yet valid. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_HOST_NAME_INVALID         0x80160000 /* BadCertificateHostNameInvalid - The HostName used to connect to a server does not match a HostName in the certificate. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_URI_INVALID               0x80170000 /* BadCertificateUriInvalid - The URI specified in the ApplicationDescription does not match the URI in the certificate. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_USE_NOT_ALLOWED           0x80180000 /* BadCertificateUseNotAllowed - The certificate may not be used for the requested operation. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_ISSUER_USE_NOT_ALLOWED    0x80190000 /* BadCertificateIssuerUseNotAllowed - The issuer certificate may not be used for the requested operation. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_UNTRUSTED                 0x801A0000 /* BadCertificateUntrusted - The certificate is not trusted. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_REVOATION_UNKNOWN         0x801B0000 /* BadCertificateRevocationUnknown - It was not possible to determine if the certificate has been revoked. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_ISSUER_REVOCATION_UNKNOWN 0x801C0000 /* BadCertificateIssuerRevocationUnknown - It was not possible to determine if the issuer certificate has been revoked. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_REVOKED                   0x801D0000 /* BadCertificateRevoked - The certificate has been revoked. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_ISSUER_REVOKED            0x801E0000 /* BadCertificateIssuerRevoked - The issuer certificate has been revoked. */
#define LIP_OPCUA_ERR_BAD_CERTIFICATE_CHAIN_INCOMPLETE          0x810D0000 /* BadCertificateChainIncomplete - The certificate chain is incomplete. */
#define LIP_OPCUA_ERR_BAD_USER_ACCESS_DENIED                    0x801F0000 /* BadUserAccessDenied - User does not have permission to perform the requested operation. */
#define LIP_OPCUA_ERR_BAD_IDENTITY_TOKEN_INVALID                0x80200000 /* BadIdentityTokenInvalid - The user identity token is not valid. */
#define LIP_OPCUA_ERR_BAD_IDENTITY_TOKEN_REJECTED               0x80210000 /* BadIdentityTokenRejected - The user identity token is valid but the server has rejected it. */
#define LIP_OPCUA_ERR_BAD_CHANNEL_ID_INVALID                    0x80220000 /* BadSecureChannelIdInvalid - The specified secure channel is no longer valid. */
#define LIP_OPCUA_ERR_BAD_INVALID_TIMESTAMP                     0x80230000 /* BadInvalidTimestamp - The timestamp is outside the range allowed by the server. */
#define LIP_OPCUA_ERR_BAD_NONCE_INVALID                         0x80240000 /* BadNonceInvalid - The nonce does appear to be not a random value or it is not the correct length. */
#define LIP_OPCUA_ERR_BAD_SESSION_ID_INVALID                    0x80250000 /* BadSessionIdInvalid - The session id is not valid. */
#define LIP_OPCUA_ERR_BAD_SESSION_CLOSED                        0x80260000 /* BadSessionClosed - The session was closed by the client */
#define LIP_OPCUA_ERR_BAD_SESSION_NOT_ACTIVATED                 0x80270000 /* BadSessionNotActivated - The session cannot be used because ActivateSession has not been called. */
#define LIP_OPCUA_ERR_BAD_SUBSCRIPTION_ID_INVALID               0x80280000 /* BadSubscriptionIdInvalid - The subscription id is not valid. */
#define LIP_OPCUA_ERR_BAD_REQUEST_HEADER_INVALID                0x802A0000 /* BadRequestHeaderInvalid - The header for the request is missing or invalid. */
#define LIP_OPCUA_ERR_BAD_TIMESTAMPS_TO_RETURN_INVALID          0x802B0000 /* BadTimestampsToReturnInvalid - The timestamps to return parameter is invalid. */
#define LIP_OPCUA_ERR_BAD_REQUEST_CANCELLED_BY_CLIENT           0x802C0000 /* BadRequestCancelledByClient - The request was cancelled by the client. */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_ARGUMENTS                    0x80E50000 /* BadTooManyArguments - Too many arguments were provided. */
#define LIP_OPCUA_ERR_BAD_LICENSE_EXPIRED                       0x810E0000 /* BadLicenseExpired - The server requires a license to operate in general or to perform a service or operation, but existing license is expired. */
#define LIP_OPCUA_ERR_BAD_LICENSE_LIMITS_EXEEDED                0x810F0000 /* BadLicenseLimitsExceeded - The server has limits on number of allowed operations / objects, based on installed licenses, and these limits where exceeded. */
#define LIP_OPCUA_ERR_BAD_LICENSE_NOT_AVAILABLE                 0x81100000 /* BadLicenseNotAvailable - The server does not have a license which is required to operate in general or to perform a service or operation. */
#define LIP_OPCUA_ERR_BAD_SERVER_TOO_BUSY                       0x80EE0000 /* BadServerTooBusy - The Server does not have the resources to process the request at this time. */
#define LIP_OPCUA_ERR_GOOD_PASSWORD_CHANGE_REQUIRED             0x00EF0000 /* GoodPasswordChangeRequired - The log-on for the user succeeded but the user is required to change the password. */
#define LIP_OPCUA_ERR_GOOD_SUBSCRIPTION_TRANSFERRED             0x002D0000 /* GoodSubscriptionTransferred - The subscription was transferred to another session. */
#define LIP_OPCUA_ERR_GOOD_COMPLETES_ASYNCHRONOUSLY             0x002E0000 /* GoodCompletesAsynchronously - The processing will complete asynchronously. */
#define LIP_OPCUA_ERR_GOOD_OVERLOAD                             0x002F0000 /* GoodOverload - Sampling has slowed down due to resource limitations. */
#define LIP_OPCUA_ERR_GOOD_CLAMPED                              0x00300000 /* GoodClamped - The value written was accepted but was clamped. */
#define LIP_OPCUA_ERR_BAD_NO_COMMUNICATION                      0x80310000 /* BadNoCommunication - Communication with the data source is defined, but not established, and there is no last known value available. */
#define LIP_OPCUA_ERR_BAD_WAITING_FOR_INITIAL_DATA              0x80320000 /* BadWaitingForInitialData - Waiting for the server to obtain values from the underlying data source. */
#define LIP_OPCUA_ERR_BAD_NODEID_INVALID                        0x80330000 /* BadNodeIdInvalid - The syntax the node id is not valid or refers to a node that is not valid for the operation. */
#define LIP_OPCUA_ERR_BAD_NODEID_UNKNOWN                        0x80340000 /* BadNodeIdUnknown - The node id refers to a node that does not exist in the server address space. */
#define LIP_OPCUA_ERR_BAD_ATTRIBUTEID_INVALID                   0x80350000 /* BadAttributeIdInvalid - The attribute is not supported for the specified Node. */
#define LIP_OPCUA_ERR_BAD_INDEX_RANGE_INVALID                   0x80360000 /* BadIndexRangeInvalid - The syntax of the index range parameter is invalid. */
#define LIP_OPCUA_ERR_BAD_INDEX_RANGE_NODATA                    0x80370000 /* BadIndexRangeNoData - No data exists within the range of indexes specified. */
#define LIP_OPCUA_ERR_BAD_INDEX_RANGE_DATA_MISMATCH             0x80EA0000 /* BadIndexRangeDataMismatch - The written data does not match the IndexRange specified. */
#define LIP_OPCUA_ERR_BAD_DATA_ENCODING_INVALID                 0x80380000 /* BadDataEncodingInvalid - The data encoding is invalid. */
#define LIP_OPCUA_ERR_BAD_DATA_ENCODING_UNSUPPORTED             0x80390000 /* BadDataEncodingUnsupported - The server does not support the requested data encoding for the node. */
#define LIP_OPCUA_ERR_BAD_NOT_READABLE                          0x803A0000 /* BadNotReadable - The access level does not allow reading or subscribing to the Node. */
#define LIP_OPCUA_ERR_BAD_NOT_WRITABLE                          0x803B0000 /* BadNotWritable - The access level does not allow writing to the Node. */
#define LIP_OPCUA_ERR_BAD_OUT_OF_RANGE                          0x803C0000 /* BadOutOfRange - The value was out of range. */
#define LIP_OPCUA_ERR_BAD_NOT_SUPPORTED                         0x803D0000 /* BadNotSupported - The requested operation is not supported. */
#define LIP_OPCUA_ERR_BAD_NOT_FOUND                             0x803E0000 /* BadNotFound - A requested item was not found or a search operation ended without success. */
#define LIP_OPCUA_ERR_BAD_OBJECT_DELETED                        0x803F0000 /* BadObjectDeleted - The object cannot be used because it has been deleted. */
#define LIP_OPCUA_ERR_BAD_NOT_IMPLEMENTED                       0x80400000 /* BadNotImplemented - Requested operation is not implemented. */
#define LIP_OPCUA_ERR_BAD_MONITORING_MODE_INVALID               0x80410000 /* BadMonitoringModeInvalid - The monitoring mode is invalid. */
#define LIP_OPCUA_ERR_BAD_MONITORED_ITEM_ID_INVALID             0x80420000 /* BadMonitoredItemIdInvalid - The monitoring item id does not refer to a valid monitored item. */
#define LIP_OPCUA_ERR_BAD_MONITORED_ITEM_FILTER_INVALID         0x80430000 /* BadMonitoredItemFilterInvalid - The monitored item filter parameter is not valid. */
#define LIP_OPCUA_ERR_BAD_MONITORED_ITEM_FILTER_UNSUPPORTED     0x80440000 /* BadMonitoredItemFilterUnsupported - The server does not support the requested monitored item filter. */
#define LIP_OPCUA_ERR_BAD_FILTER_NOT_ALLOWED                    0x80450000 /* BadFilterNotAllowed - A monitoring filter cannot be used in combination with the attribute specified. */
#define LIP_OPCUA_ERR_BAD_STRUCTURE_MISSING                     0x80460000 /* BadStructureMissing - A mandatory structured parameter was missing or null. */
#define LIP_OPCUA_ERR_BAD_EVENT_FILTER_INVALID                  0x80470000 /* BadEventFilterInvalid - The event filter is not valid. */
#define LIP_OPCUA_ERR_BAD_CONTENT_FILTER_INVALID                0x80480000 /* BadContentFilterInvalid - The content filter is not valid. */
#define LIP_OPCUA_ERR_BAD_FILTER_OPERATOR_INVALID               0x80C10000 /* BadFilterOperatorInvalid - An unrecognized operator was provided in a filter. */
#define LIP_OPCUA_ERR_BAD_FILTER_OPERATOR_UNSUPPORTED           0x80C20000 /* BadFilterOperatorUnsupported - A valid operator was provided, but the server does not provide support for this filter operator. */
#define LIP_OPCUA_ERR_BAD_FILTER_OPERAND_COUNT_MISMATCH         0x80C30000 /* BadFilterOperandCountMismatch - The number of operands provided for the filter operator was less then expected for the operand provided. */
#define LIP_OPCUA_ERR_BAD_FILTER_OPERAND_INVALID                0x80490000 /* BadFilterOperandInvalid - The operand used in a content filter is not valid. */
#define LIP_OPCUA_ERR_BAD_FILTER_ELEMENT_INVALID                0x80C40000 /* BadFilterElementInvalid - The referenced element is not a valid element in the content filter. */
#define LIP_OPCUA_ERR_BAD_FILTER_LITERAL_INVALID                0x80C50000 /* BadFilterLiteralInvalid - The referenced literal is not a valid value. */
#define LIP_OPCUA_ERR_BAD_CONTINUATION_POINT_INVALID            0x804A0000 /* BadContinuationPointInvalid - The continuation point provide is longer valid. */
#define LIP_OPCUA_ERR_BAD_NO_CONTINUATION_POINT                 0x804B0000 /* BadNoContinuationPoints - The operation could not be processed because all continuation points have been allocated. */
#define LIP_OPCUA_ERR_BAD_REFTYPE_ID_INVALID                    0x804C0000 /* BadReferenceTypeIdInvalid - The reference type id does not refer to a valid reference type node. */
#define LIP_OPCUA_ERR_BAD_BROWSE_DIRECTION_INVALID              0x804D0000 /* BadBrowseDirectionInvalid - The browse direction is not valid. */
#define LIP_OPCUA_ERR_BAD_NODE_NOT_IN_VIEW                      0x804E0000 /* BadNodeNotInView - The node is not part of the view. */
#define LIP_OPCUA_ERR_BAD_NUMERIC_OVERFLOW                      0x81120000 /* BadNumericOverflow - The number was not accepted because of a numeric overflow. */
#define LIP_OPCUA_ERR_BAD_LOCALE_NOT_SUPPORTED                  0x80ED0000 /* BadLocaleNotSupported - The locale in the requested write operation is not supported. */
#define LIP_OPCUA_ERR_BAD_NO_VALUE                              0x80F00000 /* BadNoValue - The variable has no default value and no initial value. */
#define LIP_OPCUA_ERR_BAD_SERVER_URI_INVALID                    0x804F0000 /* BadServerUriInvalid - The ServerUri is not a valid URI. */
#define LIP_OPCUA_ERR_BAD_SERVER_NAME_MISSING                   0x80500000 /* BadServerNameMissing - No ServerName was specified. */
#define LIP_OPCUA_ERR_BAD_DISCOVERY_URL_MISSING                 0x80510000 /* BadDiscoveryUrlMissing - No DiscoveryUrl was specified. */
#define LIP_OPCUA_ERR_BAD_SEMAPHORE_FILE_MISSING                0x80520000 /* BadSemaphoreFileMissing - The semaphore file specified by the client is not valid. */
#define LIP_OPCUA_ERR_BAD_REQUEST_TYPE_INVALID                  0x80530000 /* BadRequestTypeInvalid - The security token request type is not valid. */
#define LIP_OPCUA_ERR_BAD_SECURITY_MODE_REJECTED                0x80540000 /* BadSecurityModeRejected - The security mode does not meet the requirements set by the server. */
#define LIP_OPCUA_ERR_BAD_SECURITY_POLICY_REJECTED              0x80550000 /* BadSecurityPolicyRejected - The security policy does not meet the requirements set by the server */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_SESSIONS                     0x80560000 /* BadTooManySessions - The server has reached its maximum number of sessions. */
#define LIP_OPCUA_ERR_BAD_USER_SIGNATURE_INVALID                0x80570000 /* BadUserSignatureInvalid - The user token signature is missing or invalid. */
#define LIP_OPCUA_ERR_BAD_APPLICATION_SIGNATURE_INVALID         0x80580000 /* BadApplicationSignatureInvalid - The signature generated with the client certificate is missing or invalid. */
#define LIP_OPCUA_ERR_BAD_NO_VALID_CERTIFICATES                 0x80590000 /* BadNoValidCertificates - The client did not provide at least one software certificate that is valid and meets the profile requirements for the server. */
#define LIP_OPCUA_ERR_BAD_IDENTITY_CHANGE_NOT_SUPPORTED         0x80C60000 /* BadIdentityChangeNotSupported - The server does not support changing the user identity assigned to the session. */
#define LIP_OPCUA_ERR_BAD_REQUEST_CANCELLED_BY_REQUEST          0x805A0000 /* BadRequestCancelledByRequest - The request was cancelled by the client with the Cancel service. */
#define LIP_OPCUA_ERR_BAD_PARENT_NODEID_INVALID                 0x805B0000 /* BadParentNodeIdInvalid - The parent node id does not to refer to a valid node. */
#define LIP_OPCUA_ERR_BAD_REFERENCE_NOT_ALLOWED                 0x805C0000 /* BadReferenceNotAllowed - The reference could not be created because it violates constraints imposed by the data model. */
#define LIP_OPCUA_ERR_BAD_NODEID_REJECTED                       0x805D0000 /* BadNodeIdRejected - The requested node id was reject because it was either invalid or server does not allow node ids to be specified by the client. */
#define LIP_OPCUA_ERR_BAD_NODEID_EXISTS                         0x805E0000 /* BadNodeIdExists - The requested node id is already used by another node. */
#define LIP_OPCUA_ERR_BAD_NODE_CLASS_INVALID                    0x805F0000 /* BadNodeClassInvalid - The node class is not valid. */
#define LIP_OPCUA_ERR_BAD_BROWSE_NAME_INVALID                   0x80600000 /* BadBrowseNameInvalid - The browse name is invalid. */
#define LIP_OPCUA_ERR_BAD_BROWSE_NAME_DUPLICATED                0x80610000 /* BadBrowseNameDuplicated - The browse name is not unique among nodes that share the same relationship with the parent. */
#define LIP_OPCUA_ERR_BAD_NODE_ATTRIBUTES_INVALID               0x80620000 /* BadNodeAttributesInvalid - The node attributes are not valid for the node class. */
#define LIP_OPCUA_ERR_BAD_TYPE_DEFINITION_INVALID               0x80630000 /* BadTypeDefinitionInvalid - The type definition node id does not reference an appropriate type node. */
#define LIP_OPCUA_ERR_BAD_SOURCE_NODEID_INVALID                 0x80640000 /* BadSourceNodeIdInvalid - The source node id does not reference a valid node. */
#define LIP_OPCUA_ERR_BAD_TARGET_NODEID_INVALID                 0x80650000 /* BadTargetNodeIdInvalid - The target node id does not reference a valid node. */
#define LIP_OPCUA_ERR_BAD_DUPLICATE_REFERENCE_NOT_ALLOWED       0x80660000 /* BadDuplicateReferenceNotAllowed - The reference type between the nodes is already defined. */
#define LIP_OPCUA_ERR_BAD_INVALID_SELF_REFERENCE                0x80670000 /* BadInvalidSelfReference - The server does not allow this type of self reference on this node. */
#define LIP_OPCUA_ERR_BAD_REFERENCE_LOCAL_ONLY                  0x80680000 /* BadReferenceLocalOnly - The reference type is not valid for a reference to a remote server. */
#define LIP_OPCUA_ERR_BAD_NO_DELETE_RIGHTS                      0x80690000 /* BadNoDeleteRights - The server will not allow the node to be deleted. */
#define LIP_OPCUA_ERR_UNCERTAIN_REFERENCE_NOT_DELETED           0x40BC0000 /* UncertainReferenceNotDeleted - The server was not able to delete all target references. */
#define LIP_OPCUA_ERR_BAD_SERVER_INDEX_INVALID                  0x806A0000 /* BadServerIndexInvalid - The server index is not valid. */
#define LIP_OPCUA_ERR_BAD_VIEW_ID_UNKNOWN                       0x806B0000 /* BadViewIdUnknown - The view id does not refer to a valid view node. */
#define LIP_OPCUA_ERR_BAD_VIEW_TIMESTAMP_INVALID                0x80C90000 /* BadViewTimestampInvalid - The view timestamp is not available or not supported. */
#define LIP_OPCUA_ERR_BAD_VIEW_PARAMETER_MISMATCH               0x80CA0000 /* BadViewParameterMismatch - The view parameters are not consistent with each other. */
#define LIP_OPCUA_ERR_BAD_VIEW_VERSION_INVALID                  0x80CB0000 /* BadViewVersionInvalid - The view version is not available or not supported. */
#define LIP_OPCUA_ERR_UNCERTAIN_NOT_ALL_NODES_AVAILABLE         0x40C00000 /* UncertainNotAllNodesAvailable,,"The list of references may not be complete because the underlying system is not available. */
#define LIP_OPCUA_ERR_GOOD_RESULTS_MAY_BE_INCOMPLETE            0x00BA0000 /* GoodResultsMayBeIncomplete - The server should have followed a reference to a node in a remote server but did not. The result set may be incomplete. */
#define LIP_OPCUA_ERR_BAD_NOT_TYPE_DEFINITION                   0x80C80000 /* BadNotTypeDefinition - The provided Nodeid was not a type definition nodeid. */
#define LIP_OPCUA_ERR_UNCERTAIN_REFERENCE_OUT_OF_SERVER         0x406C0000 /* UncertainReferenceOutOfServer - One of the references to follow in the relative path references to a node in the address space in another server. */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_MATCHED                      0x806D0000 /* BadTooManyMatches- The requested operation has too many matches to return. */
#define LIP_OPCUA_ERR_BAD_QUERY_TOO_COMPLEX                     0x806E0000 /* BadQueryTooComplex - The requested operation requires too many resources in the server. */
#define LIP_OPCUA_ERR_BAD_NO_MATCH                              0x806F0000 /* BadNoMatch- The requested operation has no match to return. */
#define LIP_OPCUA_ERR_BAD_MAX_AGE_INVALID                       0x80700000 /* BadMaxAgeInvalid - The max age parameter is invalid. */
#define LIP_OPCUA_ERR_BAD_SECURITY_MODE_INSUFFICIENT            0x80E60000 /* BadSecurityModeInsufficient - The operation is not permitted over the current secure channel. */
#define LIP_OPCUA_ERR_BAD_HISTORY_OPERATION_INVALID             0x80710000 /* BadHistoryOperationInvalid - The history details parameter is not valid. */
#define LIP_OPCUA_ERR_BAD_HISTORY_OPERATION_UNSUPPORTED         0x80720000 /* BadHistoryOperationUnsupported,,"The server does not support the requested operation. */
#define LIP_OPCUA_ERR_BAD_INVALID_TIMESTAMP_ARGUMENT            0x80BD0000 /* BadInvalidTimestampArgument - The defined timestamp to return was invalid. */
#define LIP_OPCUA_ERR_BAD_WRITE_NOT_SUPPORTED                   0x80730000 /* BadWriteNotSupported - The server does not support writing the combination of value, status and timestamps provided. */
#define LIP_OPCUA_ERR_BAD_TYPE_MISMATCH                         0x80740000 /* BadTypeMismatch - The value supplied for the attribute is not of the same type as the attribute's value. */
#define LIP_OPCUA_ERR_BAD_METHOD_INVALID                        0x80750000 /* BadMethodInvalid - The method id does not refer to a method for the specified object. */
#define LIP_OPCUA_ERR_BAD_ARGUMENTS_MISSING                     0x80760000 /* BadArgumentsMissing - The client did not specify all of the input arguments for the method. */
#define LIP_OPCUA_ERR_BAD_NOT_EXECUTABLE                        0x81110000 /* BadNotExecutable - The executable attribute does not allow the execution of the method. */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_SUBSCRIPTIONS                0x80770000 /* BadTooManySubscriptions - The server has reached its maximum number of subscriptions. */
#define LIP_OPCUA_ERR_BAD_TOO_MANY_PUBLISH_REQUESTS             0x80780000 /* BadTooManyPublishRequests - The server has reached the maximum number of queued publish requests. */
#define LIP_OPCUA_ERR_BAD_NO_SUBSCRIPTION                       0x80790000 /* BadNoSubscription - There is no subscription available for this session. */
#define LIP_OPCUA_ERR_BAD_SEQUENCE_NUMBER_UNKNOWN               0x807A0000 /* BadSequenceNumberUnknown - The sequence number is unknown to the server. */
#define LIP_OPCUA_ERR_GOOD_RETRANSMISSION_QUEUE_NOT_SUPPORTED   0x00DF0000 /* GoodRetransmissionQueueNotSupported - The Server does not support retransmission queue and acknowledgement of sequence numbers is not available. */
#define LIP_OPCUA_ERR_BAD_MESSAGE_NOT_AVAILABLE                 0x807B0000 /* BadMessageNotAvailable - The requested notification message is no longer available. */
#define LIP_OPCUA_ERR_BAD_INSUFFICIENT_CLIENT_PROFILE           0x807C0000 /* BadInsufficientClientProfile - The client of the current session does not support one or more Profiles that are necessary for the subscription. */
#define LIP_OPCUA_ERR_BAD_STATE_NOT_ACTIVE                      0x80BF0000 /* BadStateNotActive - The sub-state machine is not currently active. */
#define LIP_OPCUA_ERR_BAD_ALREADY_EXISTS                        0x81150000 /* BadAlreadyExists - An equivalent rule already exists. */
#define LIP_OPCUA_ERR_BAD_TCP_SERVER_TOO_BUSY                   0x807D0000 /* BadTcpServerTooBusy - The server cannot process the request because it is too busy. */
#define LIP_OPCUA_ERR_BAD_TCP_MESSAGE_TYPE_INVALID              0x807E0000 /* BadTcpMessageTypeInvalid - The type of the message specified in the header invalid. */
#define LIP_OPCUA_ERR_BAD_TCP_SECURE_CHANNEL_UNKNOWN            0x807F0000 /* BadTcpSecureChannelUnknown - The SecureChannelId and/or TokenId are not currently in use. */
#define LIP_OPCUA_ERR_BAD_TCP_MESSAGE_TOO_LARGE                 0x80800000 /* BadTcpMessageTooLarge - The size of the message chunk specified in the header is too large. */
#define LIP_OPCUA_ERR_BAD_TCP_NOT_ENOUGH_RESOURCES              0x80810000 /* BadTcpNotEnoughResources  - There are not enough resources to process the request. */
#define LIP_OPCUA_ERR_BAD_TCP_INTERNAL_ERROR                    0x80820000 /* BadTcpInternalError - An internal error occurred. */
#define LIP_OPCUA_ERR_BAD_TCP_ENDPOINT_URL_INVALID              0x80830000 /* BadTcpEndpointUrlInvalid - The server does not recognize the QueryString specified. */
#define LIP_OPCUA_ERR_BAD_REQUEST_INTERRUPTED                   0x80840000 /* BadRequestInterrupted - The request could not be sent because of a network interruption. */
#define LIP_OPCUA_ERR_BAD_REQUEST_TIMEOUT                       0x80850000 /* BadRequestTimeout - Timeout occurred while processing the request. */
#define LIP_OPCUA_ERR_BAD_SECURE_CHANNEL_CLOSED                 0x80860000 /* BadSecureChannelClosed - The secure channel has been closed. */
#define LIP_OPCUA_ERR_BAD_SECURE_CHANNEL_TOKEN_UNKNOWN          0x80870000 /* BadSecureChannelTokenUnknown - The token has expired or is not recognized. */
#define LIP_OPCUA_ERR_BAD_SEQUENCE_NUMBER_INVALID               0x80880000 /* BadSequenceNumberInvalid - The sequence number is not valid. */
#define LIP_OPCUA_ERR_BAD_PROTOCOL_VERSION_UNSUPPORTED          0x80BE0000 /* BadProtocolVersionUnsupported - The applications do not have compatible protocol versions. */
#define LIP_OPCUA_ERR_BAD_CONFIGURATION_ERROR                   0x80890000 /* BadConfigurationError - There is a problem with the configuration that affects the usefulness of the value. */
#define LIP_OPCUA_ERR_BAD_NOT_CONNECTED                         0x808A0000 /* BadNotConnected - The variable should receive its value from another variable, but has never been configured to do so. */
#define LIP_OPCUA_ERR_BAD_DEVICE_FAILURE                        0x808B0000 /* BadDeviceFailure - There has been a failure in the device/data source that generates the value that has affected the value. */
#define LIP_OPCUA_ERR_BAD_SENSOR_FAILURE                        0x808C0000 /* BadSensorFailure - There has been a failure in the sensor from which the value is derived by the device/data source. */
#define LIP_OPCUA_ERR_BAD_OUT_OF_SERVICE                        0x808D0000 /* BadOutOfService - The source of the data is not operational. */
#define LIP_OPCUA_ERR_BAD_DEADBAND_FILTER_INVALID               0x808E0000 /* BadDeadbandFilterInvalid - The deadband filter is not valid. */
#define LIP_OPCUA_ERR_UNCERTAIN_COMMUNICATION_LAST_USABLE_VALUE 0x408F0000 /* UncertainNoCommunicationLastUsableValue - Communication to the data source has failed. The variable value is the last value that had a good quality. */
#define LIP_OPCUA_ERR_UNCERTAIN_LAST_USABLE_VALUE               0x40900000 /* UncertainLastUsableValue - Whatever was updating this value has stopped doing so. */
#define LIP_OPCUA_ERR_UNCERTAIN_SUBSTITUTE_VALUE                0x40910000 /* UncertainSubstituteValue - The value is an operational value that was manually overwritten. */
#define LIP_OPCUA_ERR_UNCERTAIN_INITIAL_VALUE                   0x40920000 /* UncertainInitialValue - The value is an initial value for a variable that normally receives its value from another variable. */
#define LIP_OPCUA_ERR_UNCERTAIN_SENSOR_NOT_ACCURATE             0x40930000 /* UncertainSensorNotAccurate - The value is at one of the sensor limits. */
#define LIP_OPCUA_ERR_UNCERTAIN_ENGINEERING_UNITS_EXCEEDED      0x40940000 /* UncertainEngineeringUnitsExceeded - The value is outside of the range of values defined for this parameter. */
#define LIP_OPCUA_ERR_UNCERTAIN_SUB_NORMAL                      0x40950000 /* UncertainSubNormal - The data value is derived from multiple sources and has less than the required number of Good sources. */
#define LIP_OPCUA_ERR_GOOD_LOCAL_OVERRIDE                       0x00960000 /* GoodLocalOverride - The value has been overridden. */
#define LIP_OPCUA_ERR_GOOD_SUB_NORMAL                           0x00EB0000 /* GoodSubNormal - The value is derived from multiple sources and has the required number of Good sources, but less than the full number of Good sources. */
#define LIP_OPCUA_ERR_BAD_REFRESH_IN_PROGRESS                   0x80970000 /* BadRefreshInProgress - This Condition refresh failed, a Condition refresh operation is already in progress. */
#define LIP_OPCUA_ERR_BAD_CONDITION_ALREADY_DISABLED            0x80980000 /* BadConditionAlreadyDisabled - This condition has already been disabled. */
#define LIP_OPCUA_ERR_BAD_CONDITION_ALREADY_ENABLED             0x80CC0000 /* BadConditionAlreadyEnabled - This condition has already been enabled. */
#define LIP_OPCUA_ERR_BAD_CONDITION_DISABLED                    0x80990000 /* BadConditionDisabled - Property not available, this condition is disabled. */
#define LIP_OPCUA_ERR_BAD_EVENT_ID_UNKNOWN                      0x809A0000 /* BadEventIdUnknown - The specified event id is not recognized. */
#define LIP_OPCUA_ERR_BAD_EVENT_NOT_ACKNOWLEDGEABLE             0x80BB0000 /* BadEventNotAcknowledgeable - The event cannot be acknowledged. */
#define LIP_OPCUA_ERR_BAD_DIALOG_NOT_ACTIVE                     0x80CD0000 /* BadDialogNotActive - The dialog condition is not active. */
#define LIP_OPCUA_ERR_BAD_DIALOG_RESPONSE_INVALID               0x80CE0000 /* BadDialogResponseInvalid - The response is not valid for the dialog. */
#define LIP_OPCUA_ERR_BAD_CONDITION_BRANCH_ALREADY_ACKED        0x80CF0000 /* BadConditionBranchAlreadyAcked - The condition branch has already been acknowledged. */
#define LIP_OPCUA_ERR_BAD_CONDITION_BRANCH_ALREADY_CONFIRMED    0x80D00000 /* BadConditionBranchAlreadyConfirmed - The condition branch has already been confirmed. */
#define LIP_OPCUA_ERR_BAD_CONDITION_ALREADY_SHELVED             0x80D10000 /* BadConditionAlreadyShelved - The condition has already been shelved. */
#define LIP_OPCUA_ERR_BAD_CONDITION_NOT_SHELVED                 0x80D20000 /* BadConditionNotShelved - The condition is not currently shelved. */
#define LIP_OPCUA_ERR_BAD_SHELVING_TIME_OUT_OF_RANGE            0x80D30000 /* BadShelvingTimeOutOfRange - The shelving time not within an acceptable range. */
#define LIP_OPCUA_ERR_BAD_NO_DATA                               0x809B0000 /* BadNoData - No data exists for the requested time range or event filter. */
#define LIP_OPCUA_ERR_BAD_BOUND_NOT_FOUND                       0x80D70000 /* BadBoundNotFound - No data found to provide upper or lower bound value. */
#define LIP_OPCUA_ERR_BAD_BOUND_NOT_SUPPORTED                   0x80D80000 /* BadBoundNotSupported - The server cannot retrieve a bound for the variable. */
#define LIP_OPCUA_ERR_BAD_DATA_LOST                             0x809D0000 /* BadDataLost - Data is missing due to collection started/stopped/lost. */
#define LIP_OPCUA_ERR_BAD_DATA_UNAVAILABLE                      0x809E0000 /* BadDataUnavailable - Expected data is unavailable for the requested time range due to an un-mounted volume, an off-line archive or tape, or similar reason for temporary unavailability. */
#define LIP_OPCUA_ERR_BAD_ENTRY_EXISTS                          0x809F0000 /* BadEntryExists - The data or event was not successfully inserted because a matching entry exists. */
#define LIP_OPCUA_ERR_BAD_NO_ENTRY_EXISTS                       0x80A00000 /* BadNoEntryExists - The data or event was not successfully updated because no matching entry exists. */
#define LIP_OPCUA_ERR_BAD_TIMESTAMP_NOT_SUPPORTED               0x80A10000 /* BadTimestampNotSupported - The Client requested history using a TimestampsToReturn the Server does not support. */
#define LIP_OPCUA_ERR_GOOD_ENTRY_INSERTED                       0x00A20000 /* GoodEntryInserted - The data or event was successfully inserted into the historical database. */
#define LIP_OPCUA_ERR_GOOD_ENTRY_REPLACED                       0x00A30000 /* GoodEntryReplaced - The data or event field was successfully replaced in the historical database. */
#define LIP_OPCUA_ERR_UNCERTAIN_DATA_SUB_NORMAL                 0x40A40000 /* UncertainDataSubNormal - The aggregate value is derived from multiple values and has less than the required number of Good values. */
#define LIP_OPCUA_ERR_GOOD_NO_DATA                              0x00A50000 /* GoodNoData - No data exists for the requested time range or event filter. */
#define LIP_OPCUA_ERR_GOOD_MORE_DATA                            0x00A60000 /* GoodMoreData - More data is available in the time range beyond the number of values requested. */
#define LIP_OPCUA_ERR_BAD_AGGREGATE_LIST_MISMATCH               0x80D40000 /* BadAggregateListMismatch - The requested number of Aggregates does not match the requested number of NodeIds. */
#define LIP_OPCUA_ERR_BAD_AGGREGATE_NOT_SUPPORTED               0x80D50000 /* BadAggregateNotSupported - The requested Aggregate is not support by the server. */
#define LIP_OPCUA_ERR_BAD_AGGREGATE_INVALID_INPUTS              0x80D60000 /* BadAggregateInvalidInputs - The aggregate value could not be derived due to invalid data inputs. */
#define LIP_OPCUA_ERR_BAD_AGGREGATE_CONFIGURATION_REJECTED      0x80DA0000 /* BadAggregateConfigurationRejected - The aggregate configuration is not valid for specified node. */
#define LIP_OPCUA_ERR_GOOD_DATA_IGNORED                         0x00D90000 /* GoodDataIgnored - The request specifies fields which are not valid for the EventType or cannot be saved by the historian. */
#define LIP_OPCUA_ERR_BAD_REQUEST_NOT_ALLOWED                   0x80E40000 /* BadRequestNotAllowed - The request was rejected by the server because it did not meet the criteria set by the server. */
#define LIP_OPCUA_ERR_BAD_REQUEST_NOT_COMPLETE                  0x81130000 /* BadRequestNotComplete - The request has not been processed by the server yet. */
#define LIP_OPCUA_ERR_BAD_TRANSACTION_PENDING                   0x80E80000 /* BadTransactionPending - The operation is not allowed because a transaction is in progress. */
#define LIP_OPCUA_ERR_BAD_TICKET_REQUIRED                       0x811F0000 /* BadTicketRequired - The device identity needs a ticket before it can be accepted. */
#define LIP_OPCUA_ERR_BAD_TICKET_INVALID                        0x81200000 /* BadTicketInvalid - The device identity needs a ticket before it can be accepted. */
#define LIP_OPCUA_ERR_BAD_LOCKED                                0x80E90000 /* BadLocked - The requested operation is not allowed, because the Node is locked by a different application. */
#define LIP_OPCUA_ERR_BAD_REQUIRES_LOCK                         0x80EC0000 /* BadRequiresLock - The requested operation is not allowed, because the Node is not locked by the application. */
#define LIP_OPCUA_ERR_GOOD_EDITED                               0x00DC0000 /* GoodEdited - The value does not come from the real source and has been edited by the server. */
#define LIP_OPCUA_ERR_GOOD_POST_ACTION_FAILED                   0x00DD0000 /* GoodPostActionFailed - There was an error in execution of these post-actions. */
#define LIP_OPCUA_ERR_UNCERTAIN_DOMINANT_VALUE_CHANGED          0x40DE0000 /* UncertainDominantValueChanged - The related EngineeringUnit has been changed but the Variable Value is still provided based on the previous unit. */
#define LIP_OPCUA_ERR_GOOD_DEPENDENT_VALUE_CHANGED              0x00E00000 /* GoodDependentValueChanged - A dependent value has been changed but the change has not been applied to the device. */
#define LIP_OPCUA_ERR_BAD_DOMINANT_VALUE_CHANGED                0x80E10000 /* BadDominantValueChanged - The related EngineeringUnit has been changed but this change has not been applied to the device. The Variable Value is still dependent on the previous unit but its status is currently Bad. */
#define LIP_OPCUA_ERR_UNCERTAIN_DEPENDENT_VALUE_CHANGED         0x40E20000 /* UncertainDependentValueChanged - A dependent value has been changed but the change has not been applied to the device. The quality of the dominant variable is uncertain. */
#define LIP_OPCUA_ERR_BAD_DEPENDENT_VALUE_CHANGED               0x80E30000 /* BadDependentValueChanged - A dependent value has been changed but the change has not been applied to the device. The quality of the dominant variable is Bad. */
#define LIP_OPCUA_ERR_GOOD_EDITED_DEPENDENT_VALUE_CHANGED       0x01160000 /* GoodEdited_DependentValueChanged - It is delivered with a dominant Variable value when a dependent Variable has changed but the change has not been applied. */
#define LIP_OPCUA_ERR_GOOD_EDITED_DOMINANT_VALUE_CHANGED        0x01170000 /* GoodEdited_DominantValueChanged - It is delivered with a dependent Variable value when a dominant Variable has changed but the change has not been applied. */
#define LIP_OPCUA_ERR_GOOD_EDITED_DOMINANT_DEPENDENT_VALUE_CHANGED              0x01180000 /* GoodEdited_DominantValueChanged_DependentValueChanged - It is delivered with a dependent Variable value when a dominant or dependent Variable has changed but change has not been applied. */
#define LIP_OPCUA_ERR_BAD_EDITED_OUT_OF_RANGE                                   0x81190000 /* BadEdited_OutOfRange - It is delivered with a Variable value when Variable has changed but the value is not legal. */
#define LIP_OPCUA_ERR_BAD_INITIAL_VALUE_OUT_OF_RANGE                            0x811A0000 /* BadInitialValue_OutOfRange - It is delivered with a Variable value when a source Variable has changed but the value is not legal. */
#define LIP_OPCUA_ERR_BAD_OUT_OF_RANGE_DOMINANT_VALUE_CHANGED                   0x811B0000 /* BadOutOfRange_DominantValueChanged - It is delivered with a dependent Variable value when a dominant Variable has changed and the value is not legal. */
#define LIP_OPCUA_ERR_BAD_EDITED_OUT_OF_RANGE_DOMINANT_VALUE_CHANGED            0x811C0000 /* BadEdited_OutOfRange_DominantValueChanged - It is delivered with a dependent Variable value when a dominant Variable has changed, the value is not legal and the change has not been applied. */
#define LIP_OPCUA_ERR_BAD_OUT_OF_RANGE_DOMINANT_DEPENDENT_VALUE_CHANGED         0x811D0000 /* BadOutOfRange_DominantValueChanged_DependentValueChanged - It is delivered with a dependent Variable value when a dominant or dependent Variable has changed and the value is not legal. */
#define LIP_OPCUA_ERR_BAD_EDITED_OUT_OF_RANGE_DOMINANT_DEPENDENT_VALUE_CHANGED  0x811E0000 /* BadEdited_OutOfRange_DominantValueChanged_DependentValueChanged - It is delivered with a dependent Variable value when a dominant or dependent Variable has changed, the value is not legal and the change has not been applied. */
#define LIP_OPCUA_ERR_GOOD_COMMUNICATION_EVENT                  0x00A70000 /* GoodCommunicationEvent - The communication layer has raised an event. */
#define LIP_OPCUA_ERR_GOOD_SHUTDOWN_EVENT                       0x00A80000 /* GoodShutdownEvent - The system is shutting down */
#define LIP_OPCUA_ERR_GOOD_CALL_AGAIN                           0x00A90000 /* GoodCallAgain - The operation is not finished and needs to be called again. */
#define LIP_OPCUA_ERR_GOOD_NON_CRITICAL_TIMEOUT                 0x00AA0000 /* GoodNonCriticalTimeout - A non-critical timeout occurred. */
#define LIP_OPCUA_ERR_BAD_INVALID_ARGUMENT                      0x80AB0000 /* BadInvalidArgument - One or more arguments are invalid. */
#define LIP_OPCUA_ERR_BAD_CONNECTION_REJECTED                   0x80AC0000 /* BadConnectionRejected - Could not establish a network connection to remote server. */
#define LIP_OPCUA_ERR_BAD_DISCONNECT                            0x80AD0000 /* BadDisconnect - The server has disconnected from the client. */
#define LIP_OPCUA_ERR_BAD_CONNECTION_CLOSED                     0x80AE0000 /* BadConnectionClosed - The network connection has been closed. */
#define LIP_OPCUA_ERR_BAD_INVALID_STATE                         0x80AF0000 /* BadInvalidState - The operation cannot be completed because the object is closed, uninitialized or in some other invalid state. */
#define LIP_OPCUA_ERR_BAD_END_OF_STREAM                         0x80B00000 /* BadEndOfStream - Cannot move beyond end of the stream. */
#define LIP_OPCUA_ERR_BAD_NO_DATA_AVAILABLE                     0x80B10000 /* BadNoDataAvailable - No data is currently available for reading from a non-blocking stream. */
#define LIP_OPCUA_ERR_BAD_WAITING_FOR_RESPONSE                  0x80B20000 /* BadWaitingForResponse - The asynchronous operation is waiting for a response. */
#define LIP_OPCUA_ERR_BAD_OPERATION_ABANDONED                   0x80B30000 /* BadOperationAbandoned - The asynchronous operation was abandoned by the caller. */
#define LIP_OPCUA_ERR_BAD_EXPECTED_STREAM_TO_BLOCK              0x80B40000 /* BadExpectedStreamToBlock - The stream did not return all data requested (possibly because it is a non-blocking stream). */
#define LIP_OPCUA_ERR_BAD_WOULD_BLOCK                           0x80B50000 /* BadWouldBlock - Non blocking behaviour is required and the operation would block. */
#define LIP_OPCUA_ERR_BAD_SYNTAX_ERROR                          0x80B60000 /* BadSyntaxError - A value had an invalid syntax. */
#define LIP_OPCUA_ERR_BAD_MAX_CONNECTIONS_REACHED               0x80B70000 /* BadMaxConnectionsReached - The operation could not be finished because all available connections are in use. */
#define LIP_OPCUA_ERR_UNCERTAIN_TRANSDUCER_IN_MANUAL            0x42080000 /* UncertainTransducerInManual - The value may not be accurate because the transducer is in manual mode. */
#define LIP_OPCUA_ERR_UNCERTAIN_SIMULATED_VALUE                 0x42090000 /* UncertainSimulatedValue - The value is simulated. */
#define LIP_OPCUA_ERR_UNCERTAIN_SENSOR_CALIBRATION              0x420A0000 /* UncertainSensorCalibration - The value may not be accurate due to a sensor calibration fault. */
#define LIP_OPCUA_ERR_UNCERTAIN_CONFIGURATION_ERROR             0x420F0000 /* UncertainConfigurationError - The value may not be accurate due to a configuration issue. */
#define LIP_OPCUA_ERR_GOOD_CASCADE_INITIALIZATION_ACKNOWLEDGED  0x04010000 /* GoodCascadeInitializationAcknowledged - The value source supports cascade handshaking and the value has been Initialized based on an initialization request from a cascade secondary. */
#define LIP_OPCUA_ERR_GOOD_CASCADE_INITIALIZATION_REQUEST       0x04020000 /* GoodCascadeInitializationRequest - The value source supports cascade handshaking and is requesting initialization of a cascade primary. */
#define LIP_OPCUA_ERR_GOOD_CASCADE_NOT_INVITED                  0x04030000 /* GoodCascadeNotInvited - The value source supports cascade handshaking, however, the source’s current state does not allow for cascade. */
#define LIP_OPCUA_ERR_GOOD_CASCADE_NOT_SELECTED                 0x04040000 /* GoodCascadeNotSelected - The value source supports cascade handshaking, however, the source has not selected the corresponding cascade primary for use. */
#define LIP_OPCUA_ERR_GOOD_FAULT_STATE_ACTIVE                   0x04070000 /* GoodFaultStateActive - There is a fault state condition active in the value source. */
#define LIP_OPCUA_ERR_GOOD_INITIATE_FAULT_STATE                 0x04080000 /* GoodInitiateFaultState - A fault state condition is being requested of the destination. */
#define LIP_OPCUA_ERR_GOOD_CASCADE                              0x04090000 /* GoodCascade - The value is accurate, and the signal source supports cascade handshaking. */
#define LIP_OPCUA_ERR_BAD_DATASET_ID_INVALID                    0x80E70000 /* BadDataSetIdInvalid - The DataSet specified for the DataSetWriter creation is invalid. */


#endif // LIP_OPCUA_ERR_CODES_H
