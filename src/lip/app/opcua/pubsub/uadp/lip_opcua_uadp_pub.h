#ifndef LIP_OPCUA_UADP_PUB_H
#define LIP_OPCUA_UADP_PUB_H


#include "lip_opcua_uadp_defs.h"
#include "lip/ll/lip_ll.h"
#include "lip/link/lip_eth.h"

typedef struct {
    t_lip_tx_frame_info frame_info;
    t_lip_mac_addr da;
} t_lip_uadp_pub_transport;

typedef struct {
    uint8_t flags1;
    uint8_t exflags1;
    uint8_t groupflags;
} t_lip_uadp_pub_group_ctx_internal;

typedef struct {
    const t_lip_uadp_pub_transport *transp;
    t_lip_uadp_pubid pubid;
    t_lip_uadp_group_info group_info;
    t_lip_uadp_pub_group_ctx_internal internal;
} t_lip_uadp_pub_group_ctx;



t_lip_errs lip_uadp_pub_group_init(t_lip_uadp_pub_group_ctx *pubgr_ctx, const t_lip_uadp_pub_transport *transp,
                                   const t_lip_uadp_pubid *pubid, uint16_t group_id, uint16_t group_flags);
t_lip_errs lip_uadp_pub_group_send(t_lip_uadp_pub_group_ctx *pubgr_ctx, const t_lip_uadp_ds_info *ds_list, uint8_t ds_cnt);

#endif // LIP_OPCUA_UADP_PUB_H
