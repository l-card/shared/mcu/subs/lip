#ifndef LIP_OPC_UADP_PROTO_DEFS_H
#define LIP_OPC_UADP_PROTO_DEFS_H

#include <stdint.h>

#define LIP_UADP_NETMSG_VERSION_NUM                 1

/* Version/Flags1 */
#define LIP_UADP_NETMSG_FLAGS1_VERSION              (0x0F << 0) /* Version of the UADP NetworkMessage */
#define LIP_UADP_NETMSG_FLAGS1_PUBID_EN             (0x01 << 4) /* Publisher ID */
#define LIP_UADP_NETMSG_FLAGS1_GROUPHDR_EN          (0x01 << 5) /* GroupHeader enabled */
#define LIP_UADP_NETMSG_FLAGS1_PLHDR_EN             (0x01 << 6) /* PayloadHeader enabled */
#define LIP_UADP_NETMSG_FLAGS1_EXFLAGS1_EN          (0x01 << 7) /* ExtendedFlags1 enabled */


/* ExtendedFlags1 */
#define LIP_UADP_NETMSG_EXFLAGS1_PUBID_TYPE         (0x07 << 0) /* PublisherId Type */
#define LIP_UADP_NETMSG_EXFLAGS1_DSET_CLASSID_EN    (0x01 << 3) /* DataSetClassId enabled */
#define LIP_UADP_NETMSG_EXFLAGS1_SECHDR_EN          (0x01 << 4) /* SecurityHeader enable */
#define LIP_UADP_NETMSG_EXFLAGS1_TSTMP_EN           (0x01 << 5) /* Timestamp enabled */
#define LIP_UADP_NETMSG_EXFLAGS1_PICOSEC_EN         (0x01 << 6) /* PicoSeconds enabled */
#define LIP_UADP_NETMSG_EXFLAGS1_EXFLAGS2_EN        (0x01 << 7) /*ExtendedFlags2 enabled */

#define LIP_UADP_NETMSG_EXFLAGS1_PUBID_TYPE_BYTE    0
#define LIP_UADP_NETMSG_EXFLAGS1_PUBID_TYPE_UINT16  1
#define LIP_UADP_NETMSG_EXFLAGS1_PUBID_TYPE_UINT32  2
#define LIP_UADP_NETMSG_EXFLAGS1_PUBID_TYPE_UINT64  3
#define LIP_UADP_NETMSG_EXFLAGS1_PUBID_TYPE_STR     4

/* ExtendedFlags2 */
#define LIP_UADP_NETMSG_EXFLAGS2_CHUNK              (0x01 << 0) /* Chunk message */
#define LIP_UADP_NETMSG_EXFLAGS2_PROMOTED_EN        (0x01 << 1) /* PromotedFields */
#define LIP_UADP_NETMSG_EXFLAGS2_NETMSG_TYPE        (0x07 << 2) /*  UADP NetworkMessage type */

#define LIP_UADP_NETMSG_EXFLAGS2_NETMSG_TYPE_DATA   0 /* DataSetMessage */
#define LIP_UADP_NETMSG_EXFLAGS2_NETMSG_TYPE_DPROBE 1 /* Discovery probe */
#define LIP_UADP_NETMSG_EXFLAGS2_NETMSG_TYPE_DANN   2 /* Discovery announcement payload */

/* Group Flags */
#define LIP_UADP_GRPHDR_FLAGS_WR_GROUP_ID_EN        (0x01 << 0) /* WriterGroupId enabled */
#define LIP_UADP_GRPHDR_FLAGS_GROUP_VER_EN          (0x01 << 1) /* GroupVersion enabled */
#define LIP_UADP_GRPHDR_FLAGS_NETMSG_NUM_EN         (0x01 << 2) /* NetworkMessageNumber enabled*/
#define LIP_UADP_GRPHDR_FLAGS_SEQNUM_EN             (0x01 << 3) /* SequenceNumber enabled */

/* DataSetFlags1 */
#define LIP_UADP_DSHDR_FLAGS1_VALID                 (0x01 << 0) /* DataSetMessage is valid */
#define LIP_UADP_DSHDR_FLAGS1_FLD_ENC               (0x03 << 1) /* Field Encoding */
#define LIP_UADP_DSHDR_FLAGS1_SEQNUM_EN             (0x01 << 3) /* DataSetMessageSequenceNumber enabled */
#define LIP_UADP_DSHDR_FLAGS1_STATUS_EN             (0x01 << 4) /* Status enabled */
#define LIP_UADP_DSHDR_FLAGS1_CFGVER_MJ_EN          (0x01 << 5) /* ConfigurationVersionMajorVersion enabled */
#define LIP_UADP_DSHDR_FLAGS1_CFGVER_MN_EN          (0x01 << 6) /* ConfigurationVersionMinorVersion enabled */
#define LIP_UADP_DSHDR_FLAGS1_DSFLAGS2_EN           (0x01 << 7) /* DataSetFlags2 enabled */


#define LIP_UADP_DSHDR_FLAGS1_FLD_ENC_VARIANT       0 /*  The DataSet fields are encoded as Variant (bad status or status + value) */
#define LIP_UADP_DSHDR_FLAGS1_FLD_ENC_RAW           1 /*  RawData Field Encoding */
#define LIP_UADP_DSHDR_FLAGS1_FLD_ENC_VALUE         2 /*  DataValue Field Encoding */

/* DataSetFlags2 */
#define LIP_UADP_DSHDR_FLAGS2_MSGTYPE               (0x0F << 0) /* UADP DataSetMessage type */
#define LIP_UADP_DSHDR_FLAGS2_TSTMP_EN              (0x01 << 4) /* Timestamp enabled */
#define LIP_UADP_DSHDR_FLAGS2_PICOSEC_EN            (0x01 << 5) /* PicoSeconds enabled */



#define LIP_UADP_DSHDR_FLAGS2_MSGTYPE_DATA_KEY      0 /* Data Key Frame */
#define LIP_UADP_DSHDR_FLAGS2_MSGTYPE_DATA_DELTA    1 /* Data Delta Frame */
#define LIP_UADP_DSHDR_FLAGS2_MSGTYPE_EVT           2 /* Event */
#define LIP_UADP_DSHDR_FLAGS2_MSGTYPE_KEEP_ALIVE    3 /* Keep Alive */


#endif // LIP_OPC_UADP_PROTO_DEFS_H
