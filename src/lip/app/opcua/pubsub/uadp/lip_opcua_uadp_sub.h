#ifndef LIP_OPC_UADP_H
#define LIP_OPC_UADP_H

#include "lip_opcua_uadp_defs.h"
#include "lip/ll/lip_ll.h"

typedef void (* t_lip_uadp_sub_msg_proc_cb)(const t_lip_uadp_pubid *pubid, const t_lip_uadp_group_info *group,
                                           const t_lip_uadp_ds_info *ds_list, int ds_cnt, void *ctx,
                                           const t_lip_rx_frame *rx_frame);

t_lip_errs lip_uadp_sub_add(const t_lip_uadp_pubid *pubid, t_lip_uadp_sub_msg_proc_cb cb, void *cb_ctx);
void lip_uadp_sub_rem(const t_lip_uadp_pubid *pubid);
void lip_uadp_sub_clear(void);


#endif // LIP_OPC_UADP_H
