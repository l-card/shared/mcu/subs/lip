#ifndef LIP_OPCUA_UADP_PRIVATE_H
#define LIP_OPCUA_UADP_PRIVATE_H

#include "lip/lip_defs.h"
#include "lip/link/lip_eth_private.h"

/* Обработка принятого пакета */
t_lip_errs lip_opcua_uadp_process_packet(t_lip_payload *pl, const t_lip_eth_rx_packet *eth_packet);


#endif // LIP_OPCUA_UADP_PRIVATE_H
