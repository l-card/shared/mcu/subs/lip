#ifndef LIP_OPCUA_SVC_FMT_DEFS_H
#define LIP_OPCUA_SVC_FMT_DEFS_H


#define LIP_OPCUA_SECPOLICY_URL_NONE        "http://opcfoundation.org/UA/SecurityPolicy#None"


#define LIP_OPCUA_NAMESPACE_NAME_OPCUA          "http://opcfoundation.org/UA/"
#define LIP_OPCUA_NAMESPACE_IDX_OPCUA           0 /* index 0 is reserved for the OPC UA namespace */
#define LIP_OPCUA_NAMESPACE_IDX_LOCAL_SERVER    1 /* index 1 is reserved for the local Server */


typedef enum {
    LIP_OPCUA_NODECLASS_UNSPECIFIED         = 0,    /* No value is specified. */
    LIP_OPCUA_NODECLASS_OBJECT              = 1,    /* The Node is an Object. */
    LIP_OPCUA_NODECLASS_VARIABLE            = 2,    /* The Node is a Variable. */
    LIP_OPCUA_NODECLASS_METHOD              = 4,    /* The Node is a Method. */
    LIP_OPCUA_NODECLASS_OBJECT_TYPE         = 8,    /* The Node is an ObjectType. */
    LIP_OPCUA_NODECLASS_VARIABLE_TYPE       = 16,   /* The Node is a VariableType. */
    LIP_OPCUA_NODECLASS_REFERENCE_TYPE      = 32,   /* The Node is a ReferenceType. */
    LIP_OPCUA_NODECLASS_DATA_TYPE           = 64,   /* The Node is a DataType. */
    LIP_OPCUA_NODECLASS_VIEW                = 128,  /* The Node is a View. */
} t_lip_opcua_nodeclass;

/* MessageSecurityMode */
typedef enum {
    LIP_OPCUA_MSG_SECMODE_INVALID           = 0, /* The MessageSecurityMode is invalid */
    LIP_OPCUA_MSG_SECMODE_NONE              = 1, /* No security is applie */
    LIP_OPCUA_MSG_SECMODE_SIGN              = 2, /* All messages are signed but not encrypted */
    LIP_OPCUA_MSG_SECMODE_SIGNANDENCRYPT    = 3, /* All messages are signed and encrypted */
} t_lip_opcua_msg_secmode;


/* SecurityToken RequestType */
typedef enum {
    LIP_OPCUA_SECTOKEN_REQTYPE_ISSUE          = 0, /* Creates a new SecurityToken for a new SecureChannel. */
    LIP_OPCUA_SECTOKEN_REQTYPE_RENEW          = 1, /* Creates a new SecurityToken for an existing SecureChannel. */
} t_lip_opcua_sectoken_reqtype;


/* OPC 10000-004 ApplicationType */
typedef enum  {
    LIP_OPCUA_APPTYPE_SERVER            = 0,
    LIP_OPCUA_APPTYPE_CLIENT            = 1,
    LIP_OPCUA_APPTYPE_CLIENTANDSERVER   = 2,
    LIP_OPCUA_APPTYPE_DISCOVERYSERVER   = 3,
} t_lip_opcua_app_type;


/* OPC 10000-004 UserTokenType */
typedef enum {
    LIP_OPCUA_USER_TOKEN_TYPE_ANONYMOUS     = 0, /* No token is required */
    LIP_OPCUA_USER_TOKEN_TYPE_USERNAME      = 1, /* A username/password token. */
    LIP_OPCUA_USER_TOKEN_TYPE_CERTIFICATE   = 2, /* An X.509 v3 Certificate token. */
    LIP_OPCUA_USER_TOKEN_TYPE_ISSUEDTOKEN   = 3, /* Any token issued by an Authorization Service. */
} t_lip_opcua_user_token_type;

/* OPC 10000-004 TimestampsToReturn */
typedef enum {
    LIP_OPCUA_RET_TSTMP_TYPE_SOURCE         = 0, /* Return the source timestamp */
    LIP_OPCUA_RET_TSTMP_TYPE_SERVER         = 1, /* Return the Server timestamp */
    LIP_OPCUA_RET_TSTMP_TYPE_BOTH           = 2, /* Return both the source and Server timestamps */
    LIP_OPCUA_RET_TSTMP_TYPE_NEITHER        = 3, /* Return neither timestamp */
    LIP_OPCUA_RET_TSTMP_TYPE_INVALID        = 4, /* No value specified */
} t_lip_opcua_ret_tstmp_types;

typedef enum {
    LIP_OPCUA_MONMODE_DISABLED              = 0, /* The item being monitored is not sampled or evaluated, and Notifications are not generated or queued. Notification reporting is disabled. */
    LIP_OPCUA_MONMODE_SAMPLING              = 1, /* The item being monitored is sampled and evaluated, and Notifications are generated and queued. Notification reporting is disabled. */
    LIP_OPCUA_MONMODE_REPORTING             = 2, /* The item being monitored is sampled and evaluated, and Notifications are generated and queued. Notification reporting is enabled. */
} t_lip_opcua_monmode;


/* OPC 10000-006  Identifiers assigned to Attributes */
typedef enum {
    LIP_OPCUA_ATTR_ID_NODE_ID               = 1,
    LIP_OPCUA_ATTR_ID_NODE_CLASS            = 2,
    LIP_OPCUA_ATTR_ID_BROWSE_NAME           = 3,
    LIP_OPCUA_ATTR_ID_DISPLAY_NAME          = 4,
    LIP_OPCUA_ATTR_ID_DESCRIPTION           = 5,
    LIP_OPCUA_ATTR_ID_WRITE_MASK            = 6,
    LIP_OPCUA_ATTR_ID_USER_WRITE_MASK       = 7,
    LIP_OPCUA_ATTR_ID_IS_ABSTRACT           = 8,
    LIP_OPCUA_ATTR_ID_SYMMETRIC             = 9,
    LIP_OPCUA_ATTR_ID_INVERSE_NAME          = 10,
    LIP_OPCUA_ATTR_ID_CONTAINS_NO_LOOPS     = 11,
    LIP_OPCUA_ATTR_ID_EVENT_NOTIFIER        = 12,
    LIP_OPCUA_ATTR_ID_VALUE                 = 13,
    LIP_OPCUA_ATTR_ID_DATA_TYPE             = 14,
    LIP_OPCUA_ATTR_ID_VALUE_RANK            = 15,
    LIP_OPCUA_ATTR_ID_ARRAY_DIMENSIONS      = 16,
    LIP_OPCUA_ATTR_ID_ACCESS_LEVEL          = 17,
    LIP_OPCUA_ATTR_ID_USER_ACCESS_LEVEL     = 18,
    LIP_OPCUA_ATTR_ID_MIN_SAMPLING_INTERVAL = 19,
    LIP_OPCUA_ATTR_ID_HISTORIZING           = 20,
    LIP_OPCUA_ATTR_ID_EXECUTABLE            = 21,
    LIP_OPCUA_ATTR_ID_USER_EXECUTABLE       = 22,
    LIP_OPCUA_ATTR_ID_DATA_TYPE_DEFINITION  = 23,
    LIP_OPCUA_ATTR_ID_ROLE_PERMISSION       = 24,
    LIP_OPCUA_ATTR_ID_USER_ROLE_PERMISSION  = 25,
    LIP_OPCUA_ATTR_ID_ACCESS_RESTRICTION    = 26,
    LIP_OPCUA_ATTR_ID_ACCESS_LEVEL_EX       = 27,
} t_lip_opcua_attr_id;

/* Варианты атрибута Rank для переменных */
#define LIP_OPCUA_VARIABLE_RANK_DIM(n)           n /* Value is an array with the specified number of dimensions (n >= 1) */
#define LIP_OPCUA_VARIABLE_RANK_DIM1_OR_MORE     0 /* OneOrMoreDimensions (0): The value is an array with one or more dimensions. */
#define LIP_OPCUA_VARIABLE_RANK_SCALAR          -1 /* Scalar (−1): The value is not an array. */
#define LIP_OPCUA_VARIABLE_RANK_ANY             -2 /* Any (−2): The value can be a scalar or an array with any number of dimensions. */
#define LIP_OPCUA_VARIABLE_RANK_SCALAR_OR_DIM1  -3 /* ScalarOrOneDimension (−3): The value can be a scalar or a one dimensional array. */

/* AccessLevelType */
typedef enum {
    LIP_OPCUA_ACCESS_LEVEL_CUR_RD           = 1 << 0, /* Indicates if the current value is readable. It also indicates if the current value of the Variable is available.  */
    LIP_OPCUA_ACCESS_LEVEL_CUR_WR           = 1 << 1, /* Indicates if the current value is writeable. It also indicates if the current value of the Variable is available.  */
    LIP_OPCUA_ACCESS_LEVEL_HISTORY_RD       = 1 << 2, /* Indicates if the history of the value is readable. It also indicates if the history of the Variable is available via the OPC UA Server. */
    LIP_OPCUA_ACCESS_LEVEL_HISTORY_WR       = 1 << 3, /* Indicates if the history of the value is writeable. It also indicates if the history of the Variable is available via the OPC UA Server. */
    LIP_OPCUA_ACCESS_LEVEL_SEMANTIC_CHANGE  = 1 << 4, /* This flag is set for Properties that define semantic aspects of the parent Node of the Property and where the Property Value, and thus the semantic, may change during operation. */
    LIP_OPCUA_ACCESS_LEVEL_STATUS_WR        = 1 << 5, /* Indicates if the current StatusCode of the value is writeable */
    LIP_OPCUA_ACCESS_LEVEL_TIMESTAMP_WR     = 1 << 6, /* Indicates if the current SourceTimestamp is writeab */
} t_lip_opcua_access_level_type_flags;


/* ServerState enum */
typedef enum {
    LIP_OPCUA_SERVER_STATE_RUNNING              = 0, /* The Server is running normally. This is the usual state for a Server. */
    LIP_OPCUA_SERVER_STATE_FAILED               = 1, /* Fatal error has occurred within the Server. */
    LIP_OPCUA_SERVER_STATE_NO_CONFIGURATION     = 2, /* The Server is running but has no configuration information loaded and therefore does not transfer data. */
    LIP_OPCUA_SERVER_STATE_SUSPENDED            = 3, /* The Server has been temporarily suspended by some vendor-specific method and is not receiving or sending data. */
    LIP_OPCUA_SERVER_STATE_SHUTDOWN             = 4, /* The Server initiated a shut down or is in the process of shutting down */
    LIP_OPCUA_SERVER_STATE_TEST                 = 5, /* The Server is in Test Mode */
    LIP_OPCUA_SERVER_STATE_COMMUNICATION_FAULT  = 6, /* The Server is running properly, but is having difficulty accessing data from its data sources */
    LIP_OPCUA_SERVER_STATE_UNKNOWN              = 7, /* OPC UA Server does not know the state of underlying system. */
} t_lip_opcua_server_state;

typedef enum {
    LIP_OPCUA_EVENT_NOTIFIER_FLAG_SUB_TO_EVTS   = 1 << 0, /* Indicates if it can be used to subscribe to Events */
    LIP_OPCUA_EVENT_NOTIFIER_FLAG_HISTORY_RD    = 1 << 2, /* Indicates if the history of the Events is readable */
    LIP_OPCUA_EVENT_NOTIFIER_FLAG_HISTORY_WR    = 1 << 3, /* Indicates if the history of the Events is writeable */
} t_lip_opcua_event_notifier_flags;

typedef enum {
    LIP_OPCUA_BROWSE_DIR_FORWARD                = 0, /* Select only forward References. */
    LIP_OPCUA_BROWSE_DIR_INVERSE                = 1, /* Select only inverse References. */
    LIP_OPCUA_BROWSE_DIR_BOTH                   = 2, /* Select forward and inverse References. */
    LIP_OPCUA_BROWSE_DIR_INVALID                = 3, /* No value specified. */
} t_lip_opcua_browse_dir;


typedef enum {
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_OBJECT          = 1 << 0,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_VARIABLE        = 1 << 1,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_METHOD          = 1 << 2,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_OBJECT_TYPE     = 1 << 3,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_VARIABLE_TYPE   = 1 << 4,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_REFERENCE_TYPE  = 1 << 5,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_DATA_TYPE       = 1 << 6,
    LIP_OPCUA_BROWSE_NEDECLASS_MASK_VIEW            = 1 << 7,
} t_lip_opcua_browse_nodeclass_mask_flags;

#endif // LIP_OPCUA_SVC_FMT_DEFS_H
