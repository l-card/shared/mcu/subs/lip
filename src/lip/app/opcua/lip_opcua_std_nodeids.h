#ifndef LIP_OPCUA_STD_NODEIDS_H
#define LIP_OPCUA_STD_NODEIDS_H


#define LIP_OPCUA_NODEID_STD_BOOLEAN                                            1 /* Boolean DataType */
#define LIP_OPCUA_NODEID_STD_SBYTE                                              2 /* SByte DataType */
#define LIP_OPCUA_NODEID_STD_BYTE                                               3 /* Byte DataType */
#define LIP_OPCUA_NODEID_STD_INT16                                              4 /* Int16 DataType */
#define LIP_OPCUA_NODEID_STD_UINT16                                             5 /* UInt16 DataType */
#define LIP_OPCUA_NODEID_STD_INT32                                              6 /* Int16 DataType */
#define LIP_OPCUA_NODEID_STD_UINT32                                             7 /* UInt16 DataType */
#define LIP_OPCUA_NODEID_STD_INT64                                              8 /* Int64 DataType */
#define LIP_OPCUA_NODEID_STD_UINT64                                             9 /* UInt64 DataType */
#define LIP_OPCUA_NODEID_STD_FLOAT                                              10 /* Float DataType */
#define LIP_OPCUA_NODEID_STD_DOUBLE                                             11 /* Double DataType */
#define LIP_OPCUA_NODEID_STD_STRING                                             12 /* String DataType */
#define LIP_OPCUA_NODEID_STD_DATETIME                                           13 /* DateTime DataType */
#define LIP_OPCUA_NODEID_STD_GUID                                               14 /* Guid DataType */
#define LIP_OPCUA_NODEID_STD_BYTESTRING                                         15 /* ByteString DataType */
#define LIP_OPCUA_NODEID_STD_XML_ELEMENT                                        16 /* XmlElement DataType */
#define LIP_OPCUA_NODEID_STD_NODEID                                             17 /* NodeId DataType */
#define LIP_OPCUA_NODEID_STD_EXPANDED_NODEID                                    18 /* ExpandedNodeId DataType */
#define LIP_OPCUA_NODEID_STD_STATUS_CODE                                        19 /* StatusCode DataType */
#define LIP_OPCUA_NODEID_STD_QUALIFIED_NAME                                     20 /* QualifiedName DataType */
#define LIP_OPCUA_NODEID_STD_LOCALIZED_TEXT                                     21 /* LocalizedText DataType */
#define LIP_OPCUA_NODEID_STD_STRUCTURE                                          22 /* Structure DataType */
#define LIP_OPCUA_NODEID_STD_DATA_VALUE                                         23 /* DataValue DataType */
#define LIP_OPCUA_NODEID_STD_BASE_DATA_TYPE                                     24 /* BaseDataType DataType */
#define LIP_OPCUA_NODEID_STD_DIAGNOSTIC_INFO                                    25 /* DiagnosticInfo DataType */
#define LIP_OPCUA_NODEID_STD_NUMBER                                             26 /* Number DataType */
#define LIP_OPCUA_NODEID_STD_INTEGER                                            27 /* Integer DataType */
#define LIP_OPCUA_NODEID_STD_UINTEGER                                           28 /* UInteger DataType */
#define LIP_OPCUA_NODEID_STD_ENUMERATION                                        29 /* Enumeration DataType */
#define LIP_OPCUA_NODEID_STD_IMAGE                                              30 /* Image DataType */
#define LIP_OPCUA_NODEID_STD_REFERENCES                                         31 /* References ReferenceType */
#define LIP_OPCUA_NODEID_STD_NON_HIERARCHICAL_REFERENCES                        32 /* NonHierarchicalReferences ReferenceType */
#define LIP_OPCUA_NODEID_STD_HIERARCHICAL_REFERENCES                            33 /* HierarchicalReferences ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_CHILD                                          34 /* HierarchicalReferences ReferenceType */
#define LIP_OPCUA_NODEID_STD_ORGANIZES                                          35 /* Organizes ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_EVENT_SOURCE                                   36 /* HasEventSource ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_MODELLING_RULE                                 37 /* HasModellingRule ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_ENCODING                                       38 /* HasEncoding ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_DESCRIPTION                                    39 /* HasDescription ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_TYPE_DEFINITION                                40 /* HasTypeDefinition ReferenceType */
#define LIP_OPCUA_NODEID_STD_GENERATES_EVENT                                    41 /* GeneratesEvent ReferenceType */
#define LIP_OPCUA_NODEID_STD_AGGREGATES                                         44 /* Aggregates ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_SUBTYPE                                        45 /* HasSubtype ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_PROPERTY                                       46 /* HasProperty ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_COMPONENT                                      47 /* HasComponent ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_NOTIFIER                                       48 /* HasNotifier ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_ORDERED_COMPONENT                              49 /* HasOrderedComponent ReferenceType */
#define LIP_OPCUA_NODEID_STD_DECIMAL                                            50 /* Decimal DataType */

#define LIP_OPCUA_NODEID_STD_BASE_OBJECT_TYPE                                   58 /* BaseObjectType ObjectType*/
#define LIP_OPCUA_NODEID_STD_FOLDER_TYPE                                        61 /* FolderType ObjectType */
#define LIP_OPCUA_NODEID_STD_BASE_VARIABLE_TYPE                                 62 /* BaseVariableType VariableType */
#define LIP_OPCUA_NODEID_STD_BASE_DATA_VARIABLE_TYPE                            63 /* BaseDataVariableType VariableType */


#define LIP_OPCUA_NODEID_STD_PROPERTY_TYPE                                      68 /* PropertyType VariableType */

#define LIP_OPCUA_NODEID_STD_DATA_TYPE_ENCODING_TYPE                            76 /* DataTypeEncodingType,76,ObjectType */
#define LIP_OPCUA_NODEID_STD_MODELLING_RULE_TYPE                                77 /* ModellingRuleType,77,ObjectType */
#define LIP_OPCUA_NODEID_STD_MODELLING_RULE_MANDATORY                           78 /* ModellingRule_Mandatory,78,Object */
#define LIP_OPCUA_NODEID_STD_MODELLING_RULE_OPTIONAL                            80 /* ModellingRule_Optional,80,Object */
#define LIP_OPCUA_NODEID_STD_MODELLING_RULE_EXPOSES_ITS_ARRAY                   83 /* ModellingRule_ExposesItsArray,83,Object */
#define LIP_OPCUA_NODEID_STD_ROOT_FOLDER                                        84 /* RootFolder,84,Object */
#define LIP_OPCUA_NODEID_STD_OBJECTS_FOLDER                                     85 /* ObjectsFolder,85,Object */
#define LIP_OPCUA_NODEID_STD_TYPES_FOLDER                                       86 /* TypesFolder,86,Object */
#define LIP_OPCUA_NODEID_STD_VIEWS_FOLDER                                       87 /* ViewsFolder,87,Object */
#define LIP_OPCUA_NODEID_STD_OBJECT_TYPES_FOLDER                                88 /* ObjectTypesFolder,88,Object */
#define LIP_OPCUA_NODEID_STD_VARIABLE_TYPES_FOLDER                              89 /* VariableTypesFolder,89,Object */
#define LIP_OPCUA_NODEID_STD_DATA_TYPES_FOLDER                                  90 /* DataTypesFolder,90,Object */
#define LIP_OPCUA_NODEID_STD_REFERENCE_TYPES_FOLDER                             91 /* ReferenceTypesFolder,91,Object */

#define LIP_OPCUA_NODEID_STD_ACCESS_RESTRICTION_TYPE                            95 /* AccessRestrictionType,95,DataType */

#define LIP_OPCUA_NODEID_STD_DATA_TYPE_DEFINITION                               97 /* DataTypeDefinition,97,DataType */
#define LIP_OPCUA_NODEID_STD_STRUCTURE_TYPE                                     98 /* StructureType,98,DataType */
#define LIP_OPCUA_NODEID_STD_STRUCTURE_DEFINITION                               99 /* StructureDefinition,99,DataType */
#define LIP_OPCUA_NODEID_STD_ENUM_DEFINITION                                    100 /* EnumDefinition,100,DataType */
#define LIP_OPCUA_NODEID_STD_STRUCTURE_FIELD                                    101 /* StructureField,101,DataType */
#define LIP_OPCUA_NODEID_STD_ENUM_FIELD                                         102 /* EnumField,102,DataType */

#define LIP_OPCUA_NODEID_STD_DATATYPE_DEFINITION_BIN                            121 /* DataTypeDefinition_Encoding_DefaultBinary Object binary encoding */
#define LIP_OPCUA_NODEID_STD_STRUCTURE_DEFINITION_BIN                           122 /* StructureDefinition_Encoding_DefaultBinary Object binary encoding */
#define LIP_OPCUA_NODEID_STD_ENUM_DEFINITION_BIN                                123 /* EnumDefinition_Encoding_DefaultBinary Object binary encoding */

#define LIP_OPCUA_NODEID_STD_HAS_ARGUMENT_DESCRIPTION                           129 /* HasArgumentDescription ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_OPTIONAL_IN_ARGUMENT_DESCRIPTION               131 /* HasOptionalInputArgumentDescription ReferenceType */

#define LIP_OPCUA_NODEID_STD_INTEGER_ID                                         288 /* IntegerId,288,DataType */
#define LIP_OPCUA_NODEID_STD_COUNTER                                            289 /* Counter,289,DataType */
#define LIP_OPCUA_NODEID_STD_DURATION                                           290 /* Duration,290,DataType */
#define LIP_OPCUA_NODEID_STD_NUMERIC_RANGE                                      291 /* NumericRange,291,DataType */
#define LIP_OPCUA_NODEID_STD_UTC_TIME                                           294 /* UtcTime,294,DataType */

#define LIP_OPCUA_NODEID_STD_ANONYMOUS_IDENTITY_TOKEN_BIN                       321 /* AnonymousIdentityToken binary encoding */
#define LIP_OPCUA_NODEID_STD_USERNAME_IDENTITY_TOKEN_BIN                        324 /* UserNameIdentityToken binary encoding */
#define LIP_OPCUA_NODEID_STD_X509_IDENTITY_TOKEN_BIN                            327 /* X509IdentityToken binary encoding */

#define LIP_OPCUA_NODEID_STD_BUILDINFO                                          338 /* BuildInfo DataType */
#define LIP_OPCUA_NODEID_STD_BUILDINFO_BIN                                      340 /* BuildInfo_Encoding_DefaultBinary Object */


#define LIP_OPCUA_NODEID_STD_SERVICE_FAULT_BIN                                  397 /* ServiceFault binary encoding */

#define LIP_OPCUA_NODEID_STD_FIND_SERVERS_REQUEST_BIN                           422 /* FindServersRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_FIND_SERVERS_RESPONSE_BIN                          425 /* FindServersRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_GET_ENDPOINTS_REQUEST_BIN                          428 /* GetEndpointsRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_GET_ENDPOINTS_RESPONSE_BIN                         431 /* GetEndpointsResponse binary encoding */

#define LIP_OPCUA_NODEID_STD_OPEN_SECURE_CHANNEL_REQUEST_BIN                    446 /* OpenSecureChannelRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_OPEN_SECURE_CHANNEL_RESPONSE_BIN                   449 /* OpenSecureChannelResponse binary encoding */
#define LIP_OPCUA_NODEID_STD_CLOSE_SECURE_CHANNEL_REQUEST_BIN                   452 /* CloseSecureChannelRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_CLOSE_SECURE_CHANNEL_RESPONSE_BIN                  455 /* CloseSecureChannelResponse binary encoding */

#define LIP_OPCUA_NODEID_STD_CREATE_SESSION_REQUEST_BIN                         461 /* CreateSessionRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_CREATE_SESSION_RESPONSE_BIN                        464 /* CreateSessionResponse binary encoding */
#define LIP_OPCUA_NODEID_STD_ACTIVATE_SESSION_REQUEST_BIN                       467 /* ActivateSessionRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_ACTIVATE_SESSION_RESPONSE_BIN                      470 /* ActivateSessionResponse binary encoding */
#define LIP_OPCUA_NODEID_STD_CLOSE_SESSION_REQUEST_BIN                          473 /* CloseSessionRequest binary encoding */
#define LIP_OPCUA_NODEID_STD_CLOSE_SESSION_RESPONSE_BIN                         476 /* CloseSessionResponse binary encoding */


#define LIP_OPCUA_NODEID_STD_BROWSE_REQUEST_BIN                                 527 /* BrowseReques binary encoding Object */
#define LIP_OPCUA_NODEID_STD_BROWSE_RESPONSE_BIN                                530 /* BrowseResponse binary encoding Object */

#define LIP_OPCUA_NODEID_STD_TRANSLATE_BROWSE_PATHS_TO_NODEIDS_REQUEST_BIN      554 /* TranslateBrowsePathsToNodeIdsRequest_Encoding_DefaultBinary,554,Object */
#define LIP_OPCUA_NODEID_STD_TRANSLATE_BROWSE_PATHS_TO_NODEIDS_RESPONSE_BIN     557 /* TranslateBrowsePathsToNodeIdsResponse_Encoding_DefaultBinary,557,Object */
#define LIP_OPCUA_NODEID_STD_REGISTER_NODES_REQUEST_BIN                         560 /* RegisterNodesRequest_Encoding_DefaultBinary,560,Object */
#define LIP_OPCUA_NODEID_STD_REGISTER_NODES_RESPONSE_BIN                        563 /* RegisterNodesResponse_Encoding_DefaultBinary,563,Object */
#define LIP_OPCUA_NODEID_STD_UNREGISTER_NODES_REQUEST_BIN                       566 /* UnregisterNodesRequest_Encoding_DefaultBinary,566,Object */
#define LIP_OPCUA_NODEID_STD_UNREGISTER_NODES_RESPONSE_BIN                      569 /* UnregisterNodesResponse_Encoding_DefaultBinary,569,Object */

#define LIP_OPCUA_NODEID_STD_READ_REQUEST_BIN                                   631 /* ReadRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_READ_RESPONSE_BIN                                  634 /* ReadResponse binary encoding Object */

#define LIP_OPCUA_NODEID_STD_HISTORY_READ_REQUEST_BIN                           664 /* HistoryReadRequest_Encoding_DefaultBinary,664,Object */
#define LIP_OPCUA_NODEID_STD_HISTORY_READ_RESPONSE_BIN                          667 /* HistoryReadResponse_Encoding_DefaultBinary,667,Object */

#define LIP_OPCUA_NODEID_STD_WRITE_REQUEST_BIN                                  673 /* WriteRequest_Encoding_DefaultBinary,673,Object */
#define LIP_OPCUA_NODEID_STD_WRITE_RESPONSE_BIN                                 676 /* WriteResponse_Encoding_DefaultBinary,676,Object */

#define LIP_OPCUA_NODEID_STD_HISTORY_UPDATE_REQUEST_BIN                         700 /* HistoryUpdateRequest_Encoding_DefaultBinary,700,Object */
#define LIP_OPCUA_NODEID_STD_HISTORY_UPDATE_RESPONSE_BIN                        703 /* HistoryUpdateResponse_Encoding_DefaultBinary,703,Object */


#define LIP_OPCUA_NODEID_STD_DATA_CHANGE_FILTER                                 722 /* DataChangeFilter,722,DataType */
#define LIP_OPCUA_NODEID_STD_DATA_CHANGE_FILTER_BIN                             724 /* DataChangeFilter_Encoding_DefaultBinary,724,Object */
#define LIP_OPCUA_NODEID_STD_EVENT_FILTER                                       725 /* EventFilter,725,DataType */
#define LIP_OPCUA_NODEID_STD_EVENT_FILTER_BIN                                   727 /* EventFilter_Encoding_DefaultBinary,727,Object */
#define LIP_OPCUA_NODEID_STD_AGGREGATE_FILTER                                   728 /* AggregateFilter,728,DataType */
#define LIP_OPCUA_NODEID_STD_AGGREGATE_FILTER_BIN                               730 /* AggregateFilter_Encoding_DefaultBinary,730,Object */

#define LIP_OPCUA_NODEID_STD_CREATE_MONITORED_ITEMS_REQUEST_BIN                 751 /* CreateMonitoredItemsRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_CREATE_MONITORED_ITEMS_RESPONSE_BIN                754 /* CreateMonitoredItemsResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_MODIFY_MONITORED_ITEMS_REQUEST_BIN                 763 /* ModifyMonitoredItemsRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_MODIFY_MONITORED_ITEMS_RESPONSE_BIN                766 /* ModifyMonitoredItemsResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SET_MONITORING_MODE_REQUEST_BIN                    769 /* SetMonitoringModeRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SET_MONITORING_MODE_RESPONSE_BIN                   772 /* SetMonitoringModeResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SET_TRIGGERING_REQUEST_BIN                         775 /* SetTriggeringRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SET_TRIGGERING_RESPONSE_BIN                        778 /* SetTriggeringResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_DELETE_MONITORED_ITEMS_REQUEST_BIN                 781 /* DeleteMonitoredItemsRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_DELETE_MONITORED_ITEMS_RESPONSE_BIN                784 /* DeleteMonitoredItemsResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_CREATE_SUBSCRIPTION_REQUEST_BIN                    787 /* CreateSubscriptionRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_CREATE_SUBSCRIPTION_RESPONSE_BIN                   790 /* CreateSubscriptionResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_MODIFY_SUBSCRIPTION_REQUEST_BIN                    793 /* ModifySubscriptionRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_MODIFY_SUBSCRIPTION_RESPONSE_BIN                   796 /* ModifySubscriptionResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SET_PUBLISHING_MODE_REQUEST_BIN                    799 /* SetPublishingModeRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SET_PUBLISHING_MODE_RESPONSE_BIN                   802 /* SetPublishingModeResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_NOTIFICATION_MESSAGE_BIN                           805 /* NotificationMessage  binary encoding Object */
#define LIP_OPCUA_NODEID_STD_MONITORED_ITEM_NOTIFICATION_BIN                    808 /* MonitoredItemNotification binary encoding Object */
#define LIP_OPCUA_NODEID_STD_DATA_CHANGE_NOTIFICATION_BIN                       811 /* DataChangeNotification binary encoding Object */
#define LIP_OPCUA_NODEID_STD_STATUS_CHANGE_NOTIFICATION_BIN                     820 /* StatusChangeNotification binary encoding Object */
#define LIP_OPCUA_NODEID_STD_SUBSCRIPTION_ACKNOWLEDGEMENT_BIN                   823 /* SubscriptionAcknowledgement binary encoding Object */
#define LIP_OPCUA_NODEID_STD_PUBLISH_REQUEST_BIN                                826 /* PublishRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_PUBLISH_RESPONSE_BIN                               829 /* PublishResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_REPUBLISH_REQUEST_BIN                              832 /* RepublishRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_REPUBLISH_RESPONSE_BIN                             835 /* RepublishResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_TRANSFER_RESULT_BIN                                838 /* TransferResult binary encoding Object */
#define LIP_OPCUA_NODEID_STD_TRANSFER_SUBSCRIPTIONS_REQUEST_BIN                 841 /* TransferSubscriptionsRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_TRANSFER_SUBSCRIPTIONS_RESPONSE_BIN                844 /* TransferSubscriptionsResponse binary encoding Object */
#define LIP_OPCUA_NODEID_STD_DELETE_SUBSCRIPTIONS_REQUEST_BIN                   847 /* DeleteSubscriptionsRequest binary encoding Object */
#define LIP_OPCUA_NODEID_STD_DELETE_SUBSCRIPTIONS_RESPONSE_BIN                  850 /* DeleteSubscriptionsResponse binary encoding Object  */

#define LIP_OPCUA_NODEID_STD_SERVER_STATE                                       852 /* ServerState DataType */

#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_DATA_TYPE                            862 /* ServerStatusDataType DataType */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_DATA_TYPE_BIN                        864 /* ServerStatusDataType_Encoding_DefaultBinary Object */

#define LIP_OPCUA_NODEID_STD_RANGE                                              884 /* Range,884,DataType */
#define LIP_OPCUA_NODEID_STD_RANGE_BIN                                          886 /* Range_Encoding_DefaultBinary,886,Object */
#define LIP_OPCUA_NODEID_STD_EU_INFORMATION                                     887 /* EUInformation,887,DataType */
#define LIP_OPCUA_NODEID_STD_EU_INFORMATION_BIN                                 889 /* EUInformation_Encoding_DefaultBinary,889,Object */

#define LIP_OPCUA_NODEID_STD_SERVER_TYPE                                        2004 /* ServerType ObjectType */
#define LIP_OPCUA_NODEID_STD_SERVER_TYPE_SERVER_ARRAY                           2005 /* ServerType_ServerArray Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_TYPE_NAMESPACE_ARRAY                        2006 /* ServerType_NamespaceArray Variable */

#define LIP_OPCUA_NODEID_STD_SERVER_TYPE_SERVICE_LEVEL                          2008 /* ServerType_ServiceLevel Variable */

#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE                                 2138 /* ServerStatusType VariableType */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_START_TIME                      2139 /* ServerStatusType_StartTime Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_CURRENT_TIME                    2140 /* ServerStatusType_CurrentTime Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_STATE                           2141 /* ServerStatusType_State Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO                      2142 /* ServerStatusType_BuildInfo Variable */


#define LIP_OPCUA_NODEID_STD_SERVER                                             2253 /* Server, Object */
#define LIP_OPCUA_NODEID_STD_SERVER_SERVER_ARRAY                                2254 /* Server_ServerArray Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_NAMESPACE_ARRAY                             2255 /* Server_NamespaceArray Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_SERVER_STATUS                               2256 /* Server_ServerStatus Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_START_TIME                           2257 /* Server_ServerStatus_StartTime Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_CURRENT_TIME                         2258 /* Server_ServerStatus_CurrentTime Variable (UtcTime BaseDataVariableType, Mandatory) */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_STATE                                2259 /* Server_ServerStatus_State Variable (ServerState BaseDataVariableType, Mandatory) */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO                           2260 /* Server_ServerStatus_BuildInfo Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO_PRODUCT_NAME              2261 /* Server_ServerStatus_BuildInfo_ProductName Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO_PRODUCT_URI               2262 /* Server_ServerStatus_BuildInfo_ProductUri Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO_MANUFACTURER_NAME         2263 /* Server_ServerStatus_BuildInfo_ManufacturerName,2263,Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO_SOFTWARE_VERSION          2264 /* Server_ServerStatus_BuildInfo_SoftwareVersion,2264,Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO_BUILD_NUMBER              2265 /* Server_ServerStatus_BuildInfo_BuildNumber,2265,Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_BUILD_INFO_BUILD_DATE                2266 /* Server_ServerStatus_BuildInfo_BuildDate,2266,Variable */

#define LIP_OPCUA_NODEID_STD_SERVER_SERVICE_LEVEL                               2267 /* Server_ServiceLevel Variable */

#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MIN_SUP_SAMPLE_RATE                    2272 /* Server_ServerCapabilities_MinSupportedSampleRate (Duration PropertyType, Mandatory) */

#define LIP_OPCUA_NODEID_STD_DATA_ITEM_TYPE                                     2365 /* DataItemType,2365,VariableType */
#define LIP_OPCUA_NODEID_STD_DATA_ITEM_TYPE_DEFINITION                          2366 /* DataItemType_Definition,2366,Variabl */
#define LIP_OPCUA_NODEID_STD_DATA_ITEM_TYPE_VALUE_PRECISION                     2367 /* DataItemType_ValuePrecision,2367,Variable */
#define LIP_OPCUA_NODEID_STD_ANALOG_ITEM_TYPE                                   2368 /* AnalogItemType,2368,VariableType */
#define LIP_OPCUA_NODEID_STD_ANALOG_ITEM_TYPE_EURANGE                           2369 /* AnalogItemType_EURange,2369,Variable */
#define LIP_OPCUA_NODEID_STD_DISCRETE_ITEM_TYPE                                 2372 /* DiscreteItemType,2372,VariableType */
#define LIP_OPCUA_NODEID_STD_TWO_STATE_DISCRETE_TYPE                            2373 /* TwoStateDiscreteType,2373,VariableType */
#define LIP_OPCUA_NODEID_STD_TWO_STATE_DISCRETE_TYPE_FALSE_STATE                2374 /* TwoStateDiscreteType_FalseState,2374,Variable */
#define LIP_OPCUA_NODEID_STD_TWO_STATE_DISCRETE_TYPE_TRUE_STATE                 2375 /* TwoStateDiscreteType_TrueState,2375,Variable */
#define LIP_OPCUA_NODEID_STD_MULTI_STATE_DISCRETE_TYPE                          2376 /* MultiStateDiscreteType,2376,VariableType */
#define LIP_OPCUA_NODEID_STD_MULTI_STATE_DISCRETE_TYPE_ENUM_STRINGS             2377 /* MultiStateDiscreteType_EnumStrings,2377,Variable */


#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MAX_BROWSE_CONT_PTS                    2735 /* Server_ServerCapabilities_MaxBrowseContinuationPoints (UInt16 PropertyType, Mandatory) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MAX_QUERY_CONT_PTS                     2736 /* Server_ServerCapabilities_MaxQueryContinuationPoints (UInt16 PropertyType, Mandatory) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MAX_HISTORY_CONT_PTS                   2737 /* Server_ServerCapabilities_MaxHistoryContinuationPoints (UInt16 PropertyType, Mandatory) */


#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_SECONDS_TILL_SHUTDOWN           2752 /* ServerStatusType_SecondsTillShutdown Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_SHUTDOWN_REASON                 2753 /* ServerStatusType_ShutdownReason Variable */

#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_SECONDS_TILL_SHUTDOWN                2992 /* Server_ServerStatus_SecondsTillShutdown,2992,Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_SHUTDOWN_REASON                      2993 /* Server_ServerStatus_ShutdownReason,2993,Variable */


#define LIP_OPCUA_NODEID_STD_EVENT_TYPES_FOLDER                                 3048 /* EventTypesFolder,3048,Object3048 */

#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE                                    3051 /* BuildInfoType VariableType */
#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_PRODUCT_URI                        3052 /* BuildInfoType_ProductUri Variable */
#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_MANUFACTURER_NAME                  3053 /* BuildInfoType_ManufacturerName Variable */
#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_PRODUCT_NAME                       3054 /* BuildInfoType_ProductName Variable */
#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_SOFTWARE_VERSION                   3055 /* BuildInfoType_SoftwareVersion Variable */
#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_BUILD_NUMBER                       3056 /* BuildInfoType_BuildNumber Variable */
#define LIP_OPCUA_NODEID_STD_BUILD_INFO_TYPE_BUILD_DATE                         3057 /* BuildInfoType_BuildDate Variable */


#define LIP_OPCUA_NODEID_STD_ALWAYS_GENERATES_EVENT                             3065 /* AlwaysGeneratesEvent ReferenceType */

#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_PRODUCT_URI          3698 /* ServerStatusType_BuildInfo_ProductUri Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_MANUFACTURER_NAME    3699 /* ServerStatusType_BuildInfo_ManufacturerName Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_PRODUCT_NAME         3700 /* ServerStatusType_BuildInfo_ProductName Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_SOFTWARE_VERSION     3701 /* ServerStatusType_BuildInfo_SoftwareVersion Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_BUILD_NUMBER         3702 /* ServerStatusType_BuildInfo_BuildNumber Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_STATUS_TYPE_BUILD_INFO_BUILD_DATE           3703 /* ServerStatusType_BuildInfo_BuildDate Variable */


#define LIP_OPCUA_NODEID_STD_ENUM_VALUE_TYPE                                    7594 /* EnumValueType,7594,DataType */

#define LIP_OPCUA_NODEID_STD_SERVER_STATE_ENUM_STRINGS                          7612 /* ServerState_EnumStrings Variable */

#define LIP_OPCUA_NODEID_STD_ENUM_VALUE_TYPE_BIN                                8251 /* EnumValueType_Encoding_DefaultBinary,8251,Object */

#define LIP_OPCUA_NODEID_STD_MULTI_STATE_VALUE_DISCRETE_TYPE                    11238 /* MultiStateValueDiscreteType,11238,VariableType */
#define LIP_OPCUA_NODEID_STD_MULTI_STATE_VALUE_DISCRETE_TYPE_ENUM_VALUES        11241 /* MultiStateValueDiscreteType_EnumValues,11241,Variable */

#define LIP_OPCUA_NODEID_STD_MULTI_STATE_VALUE_DISCRETE_TYPE_VALUE_AS_TEXT      11461 /* MultiStateValueDiscreteType_ValueAsText,11461,Variable */

#define LIP_OPCUA_NODEID_STD_MODELLING_RULE_OPTIONAL_PLACEHOLDER                11508 /* ModellingRule_OptionalPlaceholder Object */
#define LIP_OPCUA_NODEID_STD_MODELLING_RULE_MANDATORY_PLACEHOLDER               11510 /* ModellingRule_MandatoryPlaceholder Object */



#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MAX_ARRAY_LEN                          11702 /* Server_ServerCapabilities_MaxArrayLength (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MAX_STRING_LEN                         11703 /* Server_ServerCapabilities_MaxStringLength (UInt32 PropertyType, Optional) */

#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_RD              11705 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerRead Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_WR              11707 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerWrite Variable (UInt32 PropertyType, Optional) */

#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_METHOD_CALL     11709 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerMethodCall Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_BROWSE          11710 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerBrowse Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_REG_NODES       11711 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerRegisterNodes Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_TRANS_PATHS     11712 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerTranslateBrowsePathsToNodeIds Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_NODE_MNGMNT     11713 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerNodeManagement Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_MONITEM_PER_CALL          11714 /* Server_ServerCapabilities_OperationLimits_MaxMonitoredItemsPerCall (UInt32 PropertyType, Optional) */

#define LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE                                    12021 /* ArrayItemType,12021,VariableType */
#define LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_INSTRUMENT_RANGE                   12024 /* ArrayItemType_InstrumentRange,12024,Variable */
#define LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_EU_RANGE                           12025 /* ArrayItemType_EURange,12025,Variable */
#define LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_ENGINEERING_UNITS                  12026 /* ArrayItemType_EngineeringUnits,12026,Variable */
#define LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_TITLE                              12027 /* ArrayItemType_Title,12027,Variable */
#define LIP_OPCUA_NODEID_STD_ARRAY_ITEM_TYPE_AXIS_SCALE_TYPE                    12028 /* ArrayItemType_AxisScaleType,12028,Variable */
#define LIP_OPCUA_NODEID_STD_Y_ARRAY_ITEM_TYPE                                  12029 /* YArrayItemType,12029,VariableType */
#define LIP_OPCUA_NODEID_STD_Y_ARRAY_ITEM_TYPE_X_AXIS_DEFINITION                12037 /* YArrayItemType_XAxisDefinition,12037,Variable */
#define LIP_OPCUA_NODEID_STD_XY_ARRAY_ITEM_TYPE                                 12038 /* XYArrayItemType,12038,VariableType */
#define LIP_OPCUA_NODEID_STD_XY_ARRAY_ITEM_TYPE_X_AXIS_DEFINITION               12046 /* XYArrayItemType_XAxisDefinition,12046,Variable */
#define LIP_OPCUA_NODEID_STD_IMAGE_ITEM_TYPE                                    12047 /* ImageItemType,12047,VariableType */
#define LIP_OPCUA_NODEID_STD_IMAGE_ITEM_TYPE_X_AXIS_DEFINITION                  12055 /* ImageItemType_XAxisDefinition,12055,Variable */
#define LIP_OPCUA_NODEID_STD_IMAGE_ITEM_TYPE_Y_AXIS_DEFINITION                  12056 /* ImageItemType_YAxisDefinition,12056,Variable */
#define LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE                                     12057 /* CubeItemType,12057,VariableType */
#define LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE_X_AXIS_DEFINITION                   12065 /* CubeItemType_XAxisDefinition,12065,Variable */
#define LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE_Y_AXIS_DEFINITION                   12066 /* CubeItemType_YAxisDefinition,12066,Variable */
#define LIP_OPCUA_NODEID_STD_CUBE_ITEM_TYPE_Z_AXIS_DEFINITION                   12067 /* CubeItemType_ZAxisDefinition,12067,Variable */
#define LIP_OPCUA_NODEID_STD_N_DIMENSION_ARRAY_ITEM_TYPE                        12068 /* NDimensionArrayItemType,12068,VariableType */
#define LIP_OPCUA_NODEID_STD_N_DIMENSION_ARRAY_ITEM_TYPE_AXIS_DEFINITION        12076 /* NDimensionArrayItemType_AxisDefinition,12076,Variable */
#define LIP_OPCUA_NODEID_STD_AXIS_SCALE_ENUMERATION                             12077 /* AxisScaleEnumeration,12077,DataType */
#define LIP_OPCUA_NODEID_STD_AXIS_SCALE_ENUMERATION_ENUM_STRINGS                12078 /* AxisScaleEnumeration_EnumStrings,12078,Variable */
#define LIP_OPCUA_NODEID_STD_AXIS_INFORMATION                                   12079 /* AxisInformation,12079,DataType */
#define LIP_OPCUA_NODEID_STD_XV_TYPE                                            12080 /* XVType,12080,DataType */

#define LIP_OPCUA_NODEID_STD_AXIS_INFORMATION_BIN                               12089 /* AxisInformation_Encoding_DefaultBinary,12089,Object */
#define LIP_OPCUA_NODEID_STD_XV_TYPE_BIN                                        12090 /* XVType_Encoding_DefaultBinary,12090,Object */

#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_HIST_RD_DATA    12165 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerHistoryReadData Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_HIST_RD_EVT     12166 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerHistoryReadEvents Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_HIST_UP_DATA    12167 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerHistoryUpdateData Variable (UInt32 PropertyType, Optional) */
#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_OPLIMITS_MAX_NODES_PER_HIST_UP_EVT     12168 /* Server_ServerCapabilities_OperationLimits_MaxNodesPerHistoryUpdateEvents Variable (UInt32 PropertyType, Optional) */

#define LIP_OPCUA_NODEID_STD_COMPLEX_NUMBER_TYPE                                12171 /* ComplexNumberType,12171,DataType */
#define LIP_OPCUA_NODEID_STD_DOUBLE_COMPLEX_NUMBER_TYPE                         12172 /* DoubleComplexNumberType,12172,DataType */

#define LIP_OPCUA_NODEID_STD_COMPLEX_NUMBER_TYPE_BIN                            12181 /* ComplexNumberType_Encoding_DefaultBinary,12181,Object */
#define LIP_OPCUA_NODEID_STD_DOUBLE_COMPLEX_NUMBER_TYPE_BIN                     12182 /* DoubleComplexNumberType_Encoding_DefaultBinary,12182,Object */



#define LIP_OPCUA_NODEID_STD_SERVER_CAPS_MAX_BYTESTRING_LEN                     12911 /* Server_ServerCapabilities_MaxByteStringLength (UInt32 PropertyType, Optional) */

#define LIP_OPCUA_NODEID_STD_STRUCTURE_TYPE_ENUM_STRINGS                        14528 /* StructureType_EnumStrings  Variable */

#define LIP_OPCUA_NODEID_STD_STRUCTURE_FIELD_BIN                                14844 /* StructureField_Encoding_DefaultBinary Object */

#define LIP_OPCUA_NODEID_STD_SERVER_TYPE_URIS_VERSION                           15003 /* ServerType_UrisVersion Variable */
#define LIP_OPCUA_NODEID_STD_SERVER_URIS_VERSION                                15004 /* Server_UrisVersion Variable */

#define LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE                                   15318 /* BaseAnalogType,15318,VariableType */

#define LIP_OPCUA_NODEID_STD_ANALOG_UNIT_TYPE                                   17497 /* AnalogUnitType,17497,VariableType */
#define LIP_OPCUA_NODEID_STD_ANALOG_UNIT_TYPE_ENGINEERING_UNITS                 17502 /* AnalogUnitType_EngineeringUnits,17502,Variable */

#define LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE_INSTRUMENT_RANGE                  17567 /* BaseAnalogType_InstrumentRange,17567,Variable */
#define LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE_EU_RANGE                          17568 /* BaseAnalogType_EURange,17568,Variable */
#define LIP_OPCUA_NODEID_STD_BASE_ANALOG_TYPE_ENGINEERING_UNITS                 17569 /* BaseAnalogType_EngineeringUnits,17569,Variable */
#define LIP_OPCUA_NODEID_STD_ANALOG_UNIT_RANGE_TYPE                             17570 /* AnalogUnitRangeType,17570,VariableType */
#define LIP_OPCUA_NODEID_STD_ANALOG_UNIT_RANGE_TYPE_ENGINEERING_UNITS           17575 /* AnalogUnitRangeType_EngineeringUnits,17575,Variable */

#define LIP_OPCUA_NODEID_STD_INDEX                                              17588 /* Index,17588,DataType */

#define LIP_OPCUA_NODEID_STD_HAS_INTERFACE                                      17603 /* HasInterface ReferenceType */
#define LIP_OPCUA_NODEID_STD_HAS_ADDIN                                          17604 /* HasAddIn ReferenceType */


#define LIP_OPCUA_NODEID_STD_INTERFACE_TYPES                                    17708 /* InterfaceTypes,17708,Object */

#define LIP_OPCUA_NODEID_STD_VERSION_TIME                                       20998 /* VersionTime DataType */


#define LIP_OPCUA_NODEID_STD_IS_DEPRECATED                                      23562 /* IsDeprecated ReferenceType */

#define LIP_OPCUA_NODEID_STD_HAS_STRUCTURED_COMPONENT                           24136 /* HasStructuredComponent ReferenceType */
#define LIP_OPCUA_NODEID_STD_ASSOCIATED_WITH                                    24137 /* AssociatedWith ReferenceType */

#define LIP_OPCUA_NODEID_STD_LOCATIONS                                          31915 /* Locations,31915,Object */

#define LIP_OPCUA_NODEID_STD_HAS_KEY_VALUE_DESCR                                32407 /* HasKeyValueDescription ReferenceType */

#define LIP_OPCUA_NODEID_STD_QUANTITIES                                         32530 /* Quantities,32530,Object */


#endif // LIP_OPCUA_STD_NODEIDS_H

