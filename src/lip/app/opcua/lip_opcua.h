#ifndef LIP_OPCUA_H
#define LIP_OPCUA_H

#include "lip_opcua_cfg_defs.h"
#include "lip_opcua_err_codes.h"
#include "types/lip_opcua_types.h"

#if LIP_OPCUA_SERVER_ENABLE
    #include "server/lip_opcua_server.h"    
#endif
#if LIP_OPCUA_UADP_SUB_ENABLE
    #include "pubsub/uadp/lip_opcua_uadp_sub.h"
#endif
#if LIP_OPCUA_UADP_PUB_ENABLE
    #include "pubsub/uadp/lip_opcua_uadp_pub.h"
#endif

#endif // LIP_OPCUA_H
