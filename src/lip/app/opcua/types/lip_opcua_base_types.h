#ifndef LIP_OPCUA_BASE_TYPES_H
#define LIP_OPCUA_BASE_TYPES_H

#include "lip/lip_defs.h"

typedef uint32_t t_lip_opcua_err;
typedef double t_lip_opcua_duration;
typedef uint32_t t_lip_opcua_intid;
typedef uint32_t t_lip_opcua_counter;
typedef uint32_t t_lip_opcua_enumval;
typedef int32_t t_lip_opcua_arrsize;


#endif // LIP_OPCUA_BASE_TYPES_H
