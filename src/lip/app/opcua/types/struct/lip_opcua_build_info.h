#ifndef LIP_OPCUA_BUILD_INFO_H
#define LIP_OPCUA_BUILD_INFO_H

#include "../lip_opcua_string.h"
#include "../lip_opcua_tstmp.h"

typedef struct st_lip_opcua_data_build_info {
    t_lip_opcua_string_ptr product_uri;
    t_lip_opcua_string_ptr manufacturer_name;
    t_lip_opcua_string_ptr product_name;
    t_lip_opcua_string_ptr software_version;
    t_lip_opcua_string_ptr build_number;
    t_lip_opcua_datetime build_date;
} t_lip_opcua_data_build_info;

#endif // LIP_OPCUA_BUILD_INFO_H
