#ifndef LIP_OPCUA_SERVER_STATUS_H
#define LIP_OPCUA_SERVER_STATUS_H

#include "lip_opcua_build_info.h"

typedef struct st_lip_opcua_data_server_status {
    t_lip_opcua_datetime start_time;
    t_lip_opcua_datetime current_time;
    uint8_t server_state;
    const  t_lip_opcua_data_build_info *build_info;
    //secondsTillShutdown, shutdownReason -> сейчас не включаем
} t_lip_opcua_data_server_status;

#endif // LIP_OPCUA_SERVER_STATUS_H
