#ifndef LIP_OPCUA_TYPES_H
#define LIP_OPCUA_TYPES_H

#include "lip_opcua_base_types.h"
#include "lip_opcua_string.h"
#include "lip_opcua_extobj.h"
#include "lip_opcua_nodeid.h"
#include "lip_opcua_tstmp.h"
#include "lip_opcua_datavalue.h"
#include "lip_opcua_variant.h"
#include "lip_opcua_guid.h"
#include "lip_opcua_browse_node_params.h"
#include "lip_opcua_data_change_filter.h"
#include "lip_opcua_enum.h"
#include "lip_opcua_struct.h"
#include "lip_opcua_readvalueid.h"
#include "lip_opcua_types_da.h"
#include "lip_opcua_viewdescr.h"




typedef struct {
    t_lip_opcua_string_ptr algorithm;
    t_lip_opcua_string_ptr signature;
} t_lip_opcua_signature_data;







#endif // LIP_OPCUA_TYPES_H
