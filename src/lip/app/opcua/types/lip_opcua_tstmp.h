#ifndef LIP_OPCUA_TSTMP_H
#define LIP_OPCUA_TSTMP_H

#include "lip/app/opcua/lip_opcua_cfg_defs.h"
#include "lip/app/ptp/lip_ptp_cfg_defs.h"
#include "lip/app/ptp/lip_ptp_defs.h"
#include "lip/app/ptp/lip_ptp.h"

#define LIP_OPCUA_TSTMP_SEC_TO_PTP_EPOCH        11644473600LL
#define LIP_OPCUA_TSTMP_SEC_TO_UNIX_EPOCH       11644473600LL
#define LIP_OPCUA_TSTMP_TICK_SEC                10000000

typedef uint64_t t_lip_opcua_datetime;



typedef struct {
    t_lip_opcua_datetime ns100;
#if LIP_OPCUA_TSTMP_PICOSEC_EN
    uint16_t ps;
#endif
} t_lip_opcua_tstmp;


#if LIP_PTP_ENABLE
    void lip_opcua_tstmp_to_ptp(const t_lip_opcua_tstmp *opcua_tstmp, t_lip_ptp_tstmp *ptp_tstmp, const t_lip_ptp_time_info *time_info);
    void lip_opcua_tstmp_from_ptp(const t_lip_ptp_tstmp *ptp_tstmp, t_lip_opcua_tstmp *opcua_tstmp, const t_lip_ptp_time_info *time_info);

    #if LIP_OPCUA_TSTMP_PICOSEC_EN
        void lip_opcua_pubsub_tstmp_to_ptp(const t_lip_opcua_tstmp *opcua_tstmp, t_lip_ptp_tstmp *ptp_tstmp, uint8_t leap_sec);
        void lip_opcua_pubsub_tstmp_from_ptp(const t_lip_ptp_tstmp *ptp_tstmp, t_lip_opcua_tstmp *opcua_tstmp, uint8_t leap_sec);
    #else
        #define lip_opcua_pubsub_tstmp_to_ptp       lip_opcua_tstmp_to_ptp
        #define lip_opcua_pubsub_tstmp_from_ptp     lip_opcua_tstmp_from_ptp
    #endif
#endif


#endif // LIP_OPCUA_TSTMP_H
