#ifndef LIP_OPCUA_DATA_CHANGE_FILTER_H
#define LIP_OPCUA_DATA_CHANGE_FILTER_H

#include "lip_opcua_base_types.h"

typedef enum {
    LIP_OPCUA_MONITORING_FILTER_TYPE_NONE               = 0,
    LIP_OPCUA_MONITORING_FILTER_TYPE_DATA_CHANGE        = 1,
    LIP_OPCUA_MONITORING_FILTER_TYPE_EVENT              = 2,
    LIP_OPCUA_MONITORING_FILTER_TYPE_AGGREGATE          = 3,
} t_lip_opcua_monitoring_filter_type;

typedef enum {
    LIP_OPCUA_DATA_CHANGE_TRIGGER_STATUS                = 0, /* Report a notification ONLY if the StatusCode associated with the value change */
    LIP_OPCUA_DATA_CHANGE_TRIGGER_STATUS_VALUE          = 1, /* Report a notification if either the StatusCode or the value change. The Deadband filter can be used. */
    LIP_OPCUA_DATA_CHANGE_TRIGGER_STATUS_VALUE_TSTMP    = 2, /* Report a notification if either StatusCode, value or the SourceTimestamp change. */
} t_lip_opcua_data_change_trigger;

typedef enum {
    LIP_OPCUA_DATA_CHANGE_DEADBAND_TYPE_NONE            = 0, /* No Deadband calculation should be applied */
    LIP_OPCUA_DATA_CHANGE_DEADBAND_TYPE_ABSOLUTE        = 1, /* Report a notification if either the StatusCode or the value change. The Deadband filter can be used. */
    LIP_OPCUA_DATA_CHANGE_DEADBAND_TYPE_PERCENT         = 2, /* 0-100 percent from eurange (OPC 10000-8) */
} t_lip_opcua_data_change_deadband_type;


typedef struct st_lip_opcua_data_change_filter {
    uint8_t type;
    uint8_t trigger;
    uint8_t deadband_type;
    double deadband_value;
} t_lip_opcua_data_change_filter;

typedef union st_lip_opcua_filter {
    uint8_t type; /* Тип фильтра из t_lip_opcua_monitoring_filter_type */
    t_lip_opcua_data_change_filter data_change;
} t_lip_opcua_filter;


#endif // LIP_OPCUA_DATA_CHANGE_FILTER_H
