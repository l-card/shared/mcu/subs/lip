#include "lip/app/opcua/lip_opcua_cfg_defs.h"

#if LIP_OPCUA_ENABLE
#include "lip_opcua_variant.h"

bool lip_opcua_variant_is_null(const t_lip_opcua_variant *var) {
    return var->is_array ? (var->val.array.items_cnt1 <= 0) :
               ((var->type == LIP_OPCUA_BUILTIN_DATATYPE_NULL)
#if 0
           || (((var->type == LIP_OPCUA_BUILTIN_DATATYPE_SBYTE)
                || (var->type == LIP_OPCUA_BUILTIN_DATATYPE_INT16)
                || (var->type == LIP_OPCUA_BUILTIN_DATATYPE_INT32)) && (var->val.sint == 0))
           || (((var->type == LIP_OPCUA_BUILTIN_DATATYPE_BYTE)
                || (var->type == LIP_OPCUA_BUILTIN_DATATYPE_UINT16)
                || (var->type == LIP_OPCUA_BUILTIN_DATATYPE_UINT32)) && (var->val.uint == 0))
            || ((var->type == LIP_OPCUA_BUILTIN_DATATYPE_INT64)  && (var->val.sint64 == 0))
            || ((var->type == LIP_OPCUA_BUILTIN_DATATYPE_UINT64)  && (var->val.uint64 == 0))
#endif
            || ((var->type == LIP_OPCUA_BUILTIN_DATATYPE_STRING)  && (var->val.str.size <= 0))
            || ((var->type == LIP_OPCUA_BUILTIN_DATATYPE_DATETIME)  && (var->val.datetime == 0))
         /** @todo другие типы */
        );
}


#endif
