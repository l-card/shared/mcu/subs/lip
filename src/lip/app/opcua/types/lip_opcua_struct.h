#ifndef LIP_OPCUA_STRUCT_H
#define LIP_OPCUA_STRUCT_H

#include "lip_opcua_string.h"
#include "lip_opcua_datatype_descr.h"

typedef enum {
    LIP_OPCUA_STRUCT_TYPE_STRUCT                = 0, /* A Structure without optional fields where none of the fields allow subtyping */
    LIP_OPCUA_STRUCT_TYPE_STRUCT_OPT_FIELDS     = 1, /* A Structure with optional fields where none of the fields allow subtyping */
    LIP_OPCUA_STRUCT_TYPE_UNION                 = 2, /* A Union DataType where none of the fields allow subtyping */
    LIP_OPCUA_STRUCT_TYPE_STRUCT_SUBTYPED       = 3, /* A Structure without optional fields where one or more of the fields allow subtyping */
    LIP_OPCUA_STRUCT_TYPE_UNION_SUBTYPED        = 4, /* A Union DataType where one or more of the fields allow subtyping */
} t_lip_opcua_struct_type;

typedef struct st_lip_opcua_struct_field {
    t_lip_opcua_string_ptr name; /* A name for the field that is unique within the StructureDefinition. */
    const t_lip_opcua_loc_string_ptr_list *display_name; /* A localized description of the field */
    const t_lip_opcua_datatype_descr *data_descr; /* DataType, Rank, Dimensions... */
    uint16_t max_str_len; /* If the dataType field is a String, LocalizedText (text field) or ByteString then this field specifies the maximum supported length in bytes (0 - unknown) */
                          /** @note тип max_str_len сделан uint16_t вместо uint32_t, т.к. не предполагаются строки больше 2^16, но это позволяет уменьшить размер структуры (возможно сделать потом настройкой) */
    uint8_t  is_optional; /* If the structureType is StructureWithOptionalFields this field indicates if a data type field in a Structure is optional.
                             If the structureType is StructureWithSubtypedValues, or UnionWithSubtypedValues this field is used to indicate if the data type field allows subtyping. */
    uint8_t   reserved;
} t_lip_opcua_struct_field;

typedef struct st_lip_opcua_struct_def {
    const t_lip_opcua_nodeid *default_encoding_id; /* The NodeId of the default DataTypeEncoding for the DataType. The default shall always be Default Binary encoding. */
    const t_lip_opcua_nodeid *base_data_type_id; /* The NodeId of the direct supertype of the DataType. This might be the abstract Structure or the Union DataType. */
    uint8_t struct_type; /* An enumeration that specifies the type of Structure from t_lip_opcua_struct_type */
    uint16_t fields_cnt; /* Число полей */
    const t_lip_opcua_struct_field *fields; /* список полей */
} t_lip_opcua_struct_def;




#endif // LIP_OPCUA_STRUCT_H
