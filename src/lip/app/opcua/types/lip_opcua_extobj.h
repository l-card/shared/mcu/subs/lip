#ifndef LIP_OPCUA_EXTOBJ_H
#define LIP_OPCUA_EXTOBJ_H


#include "lip_opcua_nodeid.h"


typedef struct {
    t_lip_opcua_nodeid type_id;
    uint32_t body_len;
    uint8_t *body;
} t_lip_opcua_extobj_info;

#endif // LIP_OPCUA_EXTOBJ_H
