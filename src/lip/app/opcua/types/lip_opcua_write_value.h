#ifndef LIP_OPCUA_WRITE_VALUE_H
#define LIP_OPCUA_WRITE_VALUE_H


#include "lip_opcua_datavalue.h"
#include "lip/app/opcua/lip_opcua_svc_proto_defs.h"

typedef struct  {
    t_lip_opcua_nodeid  nodeid;
    t_lip_opcua_attr_id attr;
    t_lip_opcua_string_ptr range;
    t_lip_opcua_data_value value;
} t_lip_opcua_write_value;

#endif // LIP_OPCUA_WRITE_VALUE_H
