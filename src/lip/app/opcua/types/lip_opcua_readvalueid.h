#ifndef LIP_OPCUA_READVALUEID_H
#define LIP_OPCUA_READVALUEID_H

#include "lip_opcua_nodeid.h"
#include "lip_opcua_string.h"
#include "lip/app/opcua/lip_opcua_svc_proto_defs.h"


typedef struct  {
    t_lip_opcua_nodeid  nodeid;
    t_lip_opcua_attr_id attr;
    t_lip_opcua_string_ptr range;
    t_lip_opcua_qualified_name data_enc;
} t_lip_opcua_read_value_id;


#endif // LIP_OPCUA_READVALUEID_H
