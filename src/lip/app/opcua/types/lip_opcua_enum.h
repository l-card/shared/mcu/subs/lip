#ifndef LIP_OPCUA_ENUM_H
#define LIP_OPCUA_ENUM_H


#include "lip_opcua_string.h"


typedef struct st_lip_opcua_enum_value {
    int64_t value; /* The Integer representation of an Enumeration. */
    const t_lip_opcua_loc_string_ptr_list *display_name; /* A human-readable representation of the Value of the Enumeration. */
    const t_lip_opcua_loc_string_ptr_list *description; /* A localized description of the enumeration value */
} t_lip_opcua_enum_value;

#endif // LIP_OPCUA_ENUM_H
