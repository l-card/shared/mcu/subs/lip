#ifndef LIP_OPCUA_DA_H
#define LIP_OPCUA_DA_H

#include "lip_opcua_string.h"




typedef struct {
    double low;
    double high;
} t_lip_opcua_da_range;


#define LIP_OPCUA_EU_UNECE_NAMESPACE "http://www.opcfoundation.org/UA/units/un/cefact"

typedef struct {
    t_lip_opcua_string_ptr namesapce_uri; /* Identifies the organization (company, standards organization) that defines the EUInformation. */
    int32_t unit_id; /* Identifier for programmatic lookup (-1 - not available) */
    t_lip_opcua_loc_string_ptr_list display_name; /* Abbreviation of the engineering unit ("h" for hour or "m/s" for meter per second) */
    t_lip_opcua_loc_string_ptr_list description; /* Full name of the engineering unit such as "hour" or "meter per second". */
} t_lip_opcua_da_eu_info;

typedef struct {
    float r; /* Value real part */
    float im; /* Value imaginary part */
} t_lip_opcua_da_complex_num;

typedef struct {
    double r; /* Value real part */
    double im; /* Value imaginary part */
} t_lip_opcua_da_double_complex_num;

typedef enum {
    LIP_OPCUA_DA_AXIS_SCALE_TYPE_LINEAR    = 0, /* Linear scale */
    LIP_OPCUA_DA_AXIS_SCALE_TYPE_LOG       = 1, /* Log base 10 scale */
    LIP_OPCUA_DA_AXIS_SCALE_TYPE_LN        = 2, /* Log base e scale */
} t_lip_opcua_da_axis_scale_type;

typedef struct {
    const t_lip_opcua_da_eu_info *eu;      /* Engineering units for a given axis */
    t_lip_opcua_da_range range;            /* Limits of the range of the axis */
    t_lip_opcua_loc_string_ptr_list title; /* User readable axis title */
    t_lip_opcua_da_axis_scale_type type;   /* LINEAR, LOG, LN, defined by AxisSteps */
    const double *steps;                   /* Specific value of each axis steps, can be set to “Null” if not used */
    int32_t steps_cnt;
} t_lip_opcua_da_axis_info;

typedef struct {
    double x;       /* Position on the X axis of this value */
    float  value;   /* The value itself */
} t_lip_opcua_da_xv;


#endif // LIP_OPCUA_DA_H
