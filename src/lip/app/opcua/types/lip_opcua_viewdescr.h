#ifndef LIP_OPCUA_VIEWDESCR_H
#define LIP_OPCUA_VIEWDESCR_H

#include "lip_opcua_tstmp.h"
#include "lip_opcua_nodeid.h"

typedef struct {
    t_lip_opcua_nodeid   view_id;   /* NodeId of the View to Query. A null value indicates the entire AddressSpace. */
    t_lip_opcua_datetime timestamp; /* The UTC time date desired */
    uint32_t             view_ver;  /* The version number for the View desired. */
} t_lip_opcua_view_descr;

#endif // LIP_OPCUA_VIEWDESCR_H
