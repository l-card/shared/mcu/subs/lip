#ifndef LIP_OPCUA_BROWSE_PARAMS_H
#define LIP_OPCUA_BROWSE_PARAMS_H

#include "lip_opcua_nodeid.h"
#include "lip/app/opcua/lip_opcua_svc_proto_defs.h"





typedef struct  {
    t_lip_opcua_nodeid  nodeid;
    t_lip_opcua_nodeid  ref_type;
    uint8_t include_subtypes;
    t_lip_opcua_browse_dir browse_dir;
    uint32_t node_class_mask;
} t_lip_opcua_browse_node_params;

#endif // LIP_OPCUA_BROWSE_PARAMS_H
