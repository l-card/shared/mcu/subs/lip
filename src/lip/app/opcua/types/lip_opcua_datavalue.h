#ifndef LIP_OPCUA_DATAVALUE_H
#define LIP_OPCUA_DATAVALUE_H

#include "lip_opcua_base_types.h"
#include "lip_opcua_variant.h"
#include "lip_opcua_tstmp.h"


typedef struct {
    t_lip_opcua_variant value;
    t_lip_opcua_err status;
    t_lip_opcua_tstmp src_time;
    t_lip_opcua_tstmp srv_time;
} t_lip_opcua_data_value;


#endif // LIP_OPCUA_DATAVALUE_H
