#ifndef LIP_OPCUA_DATATYPE_DESCR_H
#define LIP_OPCUA_DATATYPE_DESCR_H

#include "lip_opcua_nodeid.h"

typedef struct st_lip_opcua_datatype_descr {
    const t_lip_opcua_nodeid *type_id;
    int32_t rank;
    union {
        uint32_t value; /* для одномерного массива */
        const uint32_t *list; /* для многомерного массива */
    } dimension;
} t_lip_opcua_datatype_descr;


#endif // LIP_OPCUA_DATATYPE_DESCR_H
