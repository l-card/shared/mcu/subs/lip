#ifndef LIP_OPCUA_STATUS_H
#define LIP_OPCUA_STATUS_H

#define LIP_OPCUA_STATUSCODE_SEVERITY               (0x0003UL << 30)
#define LIP_OPCUA_STATUSCODE_SUBCODE                (0x0FFFUL << 16)
#define LIP_OPCUA_STATUSCODE_STRUCTURE_CHANGED      (0x0001UL << 15) /* Indicates that the structure of the associated data value has changed since the last Notification. */
#define LIP_OPCUA_STATUSCODE_SEMANTICS_CHANGED      (0x0001UL << 14) /* Indicates that the semantics of the associated data value have changed. */
#define LIP_OPCUA_STATUSCODE_INFO_TYPE              (0x0003UL << 10) /* The type of information contained in the info bits (9:0) */


#define LIP_OPCUA_STATUSCODE_SEVERITY_GOOD          0x00 /* Indicates that the operation was successful and the associated results may be used. */
#define LIP_OPCUA_STATUSCODE_SEVERITY_UNCERTAIN     0x01 /* Indicates that the operation was partially successful and that associated results might not be suitable for some purposes. */
#define LIP_OPCUA_STATUSCODE_SEVERITY_BAD           0x02 /* Indicates that the operation failed and any associated results cannot be used.*/

#define LIP_OPCUA_STATUSCODE_INFO_TYPE_NOT_USED     0x00 /* The info bits are not used and shall be set to zero. */
#define LIP_OPCUA_STATUSCODE_INFO_TYPE_DATAVAL      0x01 /* The StatusCode and its info bits are associated with a data value returned from the Server. */


/* InfoBits for InfoType == DataValue */
#define LIP_OPCUA_STATUSCODE_DATAVAL_LIMIT          (0x0003UL << 8) /* The limit bits associated with the data value */
#define LIP_OPCUA_STATUSCODE_DATAVAL_OVERFLOW       (0x0001UL << 7) /* not every detected change has been returned since the Server’s queue buffer for the MonitoredItem reached its limit and had to purge out data */
#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST           (0x001FUL << 0)

#define LIP_OPCUA_STATUSCODE_DATAVAL_LIMIT_NONE     0x00 /* The value is free to change. */
#define LIP_OPCUA_STATUSCODE_DATAVAL_LIMIT_LOW      0x01 /* The value is at the lower limit for the data source */
#define LIP_OPCUA_STATUSCODE_DATAVAL_LIMIT_HIGH     0x02 /* The value is at the higher limit for the data source */
#define LIP_OPCUA_STATUSCODE_DATAVAL_LIMIT_CONST    0x03 /* The value is constant and cannot change */

#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST_RAW       0x00 /* XXX00  A raw data value.  */
#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST_CALC      0x01 /* XXX01  A data value which was calculated.  */
#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST_INTERPOL  0x02 /* XXX10  A data value which was interpolated.  */
#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST_PARTIAL   0x04 /* XX1XX  A data value which was calculated with an incomplete interval. */
#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST_EXTRA     0x08 /* XX1XX  A raw data value that hides other data at the same timestamp. */
#define LIP_OPCUA_STATUSCODE_DATAVAL_HIST_MULTI     0x10 /* 1XXXX  Multiple values match the Aggregate criteria. */

#endif // LIP_OPCUA_STATUS_H
