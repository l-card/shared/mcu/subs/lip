#ifndef LIP_MB_TCP_SERVER_PRIVATE_H
#define LIP_MB_TCP_SERVER_PRIVATE_H

void lip_mb_tcp_server_init(void);
void lip_mb_tcp_server_pull(void);

#endif // LIP_MB_TCP_SERVER_PRIVATE_H
