#ifndef MODBUS_TCP_DEFS_H
#define MODBUS_TCP_DEFS_H

#define LIP_MB_TCP_DEFAULT_PORT  502

#define LIP_MB_TCP_HDR_POS_PROTO    2
#define LIP_MB_TCP_HDR_POS_PROTO_H  (LIP_MB_TCP_HDR_POS_PROTO)
#define LIP_MB_TCP_HDR_POS_PROTO_L  (LIP_MB_TCP_HDR_POS_PROTO + 1)
#define LIP_MB_TCP_HDR_POS_LEN      4
#define LIP_MB_TCP_HDR_POS_LEN_H    (LIP_MB_TCP_HDR_POS_LEN)
#define LIP_MB_TCP_HDR_POS_LEN_L    (LIP_MB_TCP_HDR_POS_LEN + 1)
#define LIP_MB_TCP_HDR_POS_UNIT_ID  6


typedef struct {
    uint16_t transaction_id; /* id транзакции - копируется в ответе */
    uint16_t proto_id; /* id протокола. всегда 0 */
    uint16_t length; /* длина, включая unit_id */
    uint8_t  unit_id; /* дополнительный адрес устройства для состов TCP->RTU */
} t_lip_mb_tcp_hdr;

#define LIP_MB_TCP_HDR_SIZE      7

#define LIP_MB_TCP_PDU_SIZE_MAX  253

#define LIP_MB_TCP_ADU_SIZE_MAX  (LIP_MB_TCP_HDR_SIZE + LIP_MB_TCP_PDU_SIZE_MAX)

#endif // MODBUS_TCP_DEFS_H

