#ifndef LIP_MB_SERVER_PRIVATE_H
#define LIP_MB_SERVER_PRIVATE_H

#include "lip_mb_server.h"

void lip_mb_exec_func(const t_lip_mb_con_info *coninfo, uint8_t devaddr, uint8_t *data, uint16_t data_size, uint16_t *resp_size);

#endif // LIP_MB_SERVER_PRIVATE_H
