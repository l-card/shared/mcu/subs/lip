#ifndef LIP_MB_DEFS_H
#define LIP_MB_DEFS_H

/***************************************************************************//**
 @addtogroup lip_mb_server
 @{
 @file lip_mb_defs.h
 @brief Файл содержит определения констант из спецификации Modbus
 @author Borisov Alexey <borisov@lcard.ru>
*******************************************************************************/

#define LIP_MB_READ_REGS_MAX_CNT        125
#define LIP_MB_WRITE_REGS_MAX_CNT       123
#define LIP_MB_RW_WRITE_REGS_MAX_CNT    121
#define LIP_MB_READ_BITS_MAX_CNT        2000
#define LIP_MB_WRITE_BITS_MAX_CNT       1968

typedef enum {
    // bit access
    LIP_MB_FUNC_READ_COILS                  = 1,
    LIP_MB_FUNC_READ_INPUTS                 = 2,
    LIP_MB_FUNC_WRITE_SINGLE_COIL           = 5,
    LIP_MB_FUNC_WRITE_MULTIPLE_COILS        = 15,
    // registers access
    LIP_MB_FUNC_READ_INPUT_REGS             = 4,
    LIP_MB_FUNC_READ_HOLDING_REGS           = 3,
    LIP_MB_FUNC_WRITE_SINGLE_REG            = 6,
    LIP_MB_FUNC_WRITE_MULTIPLE_REGS         = 16,
    LIP_MB_FUNC_RW_MULTIPLE_REGS            = 23,
    LIP_MB_FUNC_MASK_WRITE_REG              = 22,
    LIP_MB_FUNC_READ_FIFO_QUEUE             = 24,
    // file record access
    LIP_MB_FUNC_READ_FILE_RECORD            = 20,
    LIP_MB_FUNC_WRITE_FILE_RECORD           = 21,
    // diagnostics
    LIP_MB_FUNC_READ_EXCEPTION_STATUS       = 7,
    LIP_MB_FUNC_DIAGNOSTIC                  = 8,
    LIP_MB_FUNC_GET_COM_EVENT_COUNTER       = 11,
    LIP_MB_FUNC_GET_COM_EVENT_LOG           = 12,
    LIP_MB_FUNC_REPORT_SERVER_ID            = 17,
    LIP_MB_FUNC_READ_DEVICE_IDENTIFICATION  = 43,
} t_lip_mb_func_code;

/** Коды исключений */
typedef enum {
    LIP_MB_EXC_OK                           = 0x0, /**< Функция выполнена успешно */
    LIP_MB_EXC_ILLEGAL_FUNCTION             = 0x1, /**< Неверный номер функции */
    LIP_MB_EXC_ILLEGAL_DATA_ADDRESS         = 0x2, /**< Неверный адрес регистра или битового поля */
    LIP_MB_EXC_ILLEGAL_DATA_VALUE           = 0x3, /**< Неверное значение данных в запросе */
    LIP_MB_EXC_SERVER_DEVICE_FAILURE        = 0x4, /**< Произошла невосстанавливаемая ошибка при попытке выполнить запрос */
    LIP_MB_EXC_ACKNOWLEDGE                  = 0x5, /**< Предназначена для возможности указания, что запрос принят, но
                                                        еще не выполнен из-за большего времени операции */
    LIP_MB_EXC_SERVER_DEVICE_BUSY           = 0x6, /**< Сервер выполняет длительную команду и клиент должен повторить запрос */
    LIP_MB_EXC_MEMORY_PARITY_ERROR          = 0x8, /**< Ошибка четности в памяти при чтении файла */
    LIP_MB_EXC_GATEWAY_PATH_UNAVAILABLE     = 0xA, /**< Шлюз не может найти путь от входа к выходу */
    LIP_MB_EXC_GATEWAY_DEVICE_FAILD_RESPOND = 0xB  /**< Шлюз не получил ответа от целевого устройства */
} t_lip_mb_exception_code;

/** @}*/

#endif // LIP_MB_DEFS_H
