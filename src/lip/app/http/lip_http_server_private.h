#ifndef LIP_HTTP_SERVER_PRIVATE_H
#define LIP_HTTP_SERVER_PRIVATE_H

void lip_http_server_init(void);
void lip_http_server_pull(void);

#endif // LIP_HTTP_SERVER_PRIVATE_H
