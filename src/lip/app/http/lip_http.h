#ifndef LIP_HTTP_H
#define LIP_HTTP_H

#define LIP_HTTP_DEFAULT_PORT 80

#define LIP_HTTP_MAX_SUPPORTED_VERSION 1


#define LIP_HTTP_HDRFIELD_CONTENTLEN                "content-length"
#define LIP_HTTP_HDRFIELD_CONTENTLEN_SIZE           14

#define LIP_HTTP_HDRFIELD_CONNECTION                "connection"
#define LIP_HTTP_HDRFIELD_CONNECTION_SIZE           10

#define LIP_HTTP_HDRFIELD_EXPECT                    "expect"
#define LIP_HTTP_HDRFIELD_EXPECT_SIZE               6

#define LIP_HTTP_HDRFIELD_TRANSFER_ENCODING         "transfer-encoding"
#define LIP_HTTP_HDRFIELD_TRANSFER_ENCODING_SIZE    17

#define LIP_HTTP_HDRFIELD_CONTENT_TYPE              "content-type"
#define LIP_HTTP_HDRFIELD_CONTENT_TYPE_SIZE         12


#define LIP_HTTP_CONNECTION_OPT_CLOSE               "close"
#define LIP_HTTP_CONNECTION_OPT_CLOSE_SIZE          5

#define LIP_HTTP_TRANSF_ENCODING_NAME_CHUNKED       "chunked"
#define LIP_HTTP_TRANSF_ENCODING_NAME_CHUNKED_SIZE  7
#define LIP_HTTP_TRANSF_ENCODING_NAME_COMPRESS       "compress"
#define LIP_HTTP_TRANSF_ENCODING_NAME_COMPRESS_SIZE  8
#define LIP_HTTP_TRANSF_ENCODING_NAME_XCOMPRESS      "x-compress"
#define LIP_HTTP_TRANSF_ENCODING_NAME_XCOMPRESS_SIZE 10
#define LIP_HTTP_TRANSF_ENCODING_NAME_DEFLATE        "deflate"
#define LIP_HTTP_TRANSF_ENCODING_NAME_DEFLATE_SIZE   7
#define LIP_HTTP_TRANSF_ENCODING_NAME_GZIP           "gzip"
#define LIP_HTTP_TRANSF_ENCODING_NAME_GZIP_SIZE      4
#define LIP_HTTP_TRANSF_ENCODING_NAME_XGZIP          "x-gzip"
#define LIP_HTTP_TRANSF_ENCODING_NAME_XGZIP_SIZE     6

#define LIP_HTTP_STATUS_IS_INFORMATIONAL(status)  (((status) >= 100) && ((status) < 200))

typedef enum {
    /**< Признак, что успешно принят заголовок и сервер ожидает приема данных */
    LIP_HTTP_STATUS_CONTINUE                   = 100,
    /**< Для доступа к ресурсу нужно изменить проткол через Upgrade */
    LIP_HTTP_STATUS_SWITCHING_PROTOCOL         = 101,

    /** Запрос выполнен успешно */
    LIP_HTTP_STATUS_OK                         = 200,
    /** Запрос успешно выполнен и во время выполнения создан новый ресурс */
    LIP_HTTP_STATUS_CREATED                    = 201,

    LIP_HTTP_STATUS_ACCEPTED                   = 202,
    LIP_HTTP_STATUS_NON_AUTHORITATIVE          = 203,
    LIP_HTTP_STATUS_NO_CONTENT                 = 204,
    LIP_HTTP_STATUS_RESET_CONTENT              = 205,
    LIP_HTTP_STATUS_PARTIAL_CONTENT            = 206,

    LIP_HTTP_STATUS_MULTIPLE_CHOICES           = 300,
    LIP_HTTP_STATUS_MOVED_PERMANENTLY          = 301,
    LIP_HTTP_STATUS_FOUND                      = 302,
    LIP_HTTP_STATUS_SEE_OTHER                  = 303,
    LIP_HTTP_STATUS_NOT_MODIFIED               = 304,
    LIP_HTTP_STATUS_USE_PROXY                  = 305,
    LIP_HTTP_STATUS_TEMPORARY_REDIRECT         = 307,

    LIP_HTTP_STATUS_BAD_REQUEST                = 400,
    LIP_HTTP_STATUS_UNAUTHORIZED               = 401,
    LIP_HTTP_STATUS_PAYMENT_REQUIRED           = 402,
    LIP_HTTP_STATUS_FORBIDDEN                  = 403,
    LIP_HTTP_STATUS_NOT_FOUND                  = 404,
    LIP_HTTP_STATUS_METHOD_NOT_ALLOWED         = 405,
    LIP_HTTP_STATUS_NOT_ACCEPTABLE             = 406,
    LIP_HTTP_STATUS_PROXY_AUTH_REQIRED         = 407,
    LIP_HTTP_STATUS_REQUEST_TIMEOUT            = 408,
    LIP_HTTP_STATUS_CONFLICT                   = 409,
    LIP_HTTP_STATUS_GONE                       = 410,
    LIP_HTTP_STATUS_LENGTH_REQUIRED            = 411,
    LIP_HTTP_STATUS_PRECONDITION_FAILED        = 412,
    LIP_HTTP_STATUS_PAYLOAD_TOO_LARGE          = 413,
    LIP_HTTP_STATUS_URI_TOO_LONG               = 414,
    LIP_HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE     = 415,
    LIP_HTTP_STATUS_RANGE_NOT_SATISFIABLE      = 416,
    LIP_HTTP_STATUS_EXPECTATION_FAILED         = 417,
    LIP_HTTP_STATUS_UPGRADE_REQUIRED           = 418,

    LIP_HTTP_STATUS_INTERNAL_SERVER_ERROR      = 500,
    LIP_HTTP_STATUS_NOT_IMPLEMENTED            = 501,
    LIP_HTTP_STATUS_BAD_GATEWAY                = 502,
    LIP_HTTP_STATUS_SERVICE_UNAVAILABLE        = 503,
    LIP_HTTP_STATUS_GATEWAY_TIMEOUT            = 504,
    LIP_HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED = 505
} t_lip_http_status_code;


typedef enum {
    LIP_HTTP_METHOD_GET,
    LIP_HTTP_METHOD_HEAD,
    LIP_HTTP_METHOD_POST,
    LIP_HTTP_METHOD_PUT,
    LIP_HTTP_METHOD_DELETE,
    LIP_HTTP_METHOD_CONNECT,
    LIP_HTTP_METHOD_OPTIONS,
    LIP_HTTP_METHOD_TRACE
} t_lip_http_method;

/** Способ кодирования тела запроса при передаче */
typedef enum {
    LIP_HTTP_TRANSF_ENCODING_NONE,
    LIP_HTTP_TRANSF_ENCODING_CHUNKED,
    LIP_HTTP_TRANSF_ENCODING_COMPRESS,
    LIP_HTTP_TRANSF_ENCODING_DEFLATE,
    LIP_HTTP_TRANSF_ENCODING_GZIP
} t_lip_http_transf_encoding;

#endif // LIP_HTTP_H
