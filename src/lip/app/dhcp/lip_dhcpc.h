/*
 * lip_dhcp.h
 *
 *  Created on: 01.09.2010
 *      Author: borisov
 */

#ifndef LIP_DHCP_H_
#define LIP_DHCP_H_

#include "lip/lip_defs.h"
#include "lip/net/lip_ip.h"
#include "lip/link/lip_eth.h"
#include "lcspec.h"

/* размер IAID - см. rfc3315 10 page 23 + rfc 4361 */
#define LIP_DHCP_IAID_SIZE        4

/* типы DUID (rfc3315 9) */
#define LIP_DHCP_DUID_TYPE_LLT    1 /*link-layer addr + time */
#define LIP_DHCP_DUID_TYPE_EN     2 /*enterprise DUID*/
#define LIP_DHCP_DUID_TYPE_LL     3 /*link-layer addr */

#include "lcspec_pack_start.h"
/* структура, описывающие содержимое DUID
 * для типа LIP_DHCP_DUID_TYPE_LLT для ETHERNET*/
struct st_lip_dhcp_duid_llt_eth {
    uint16_t type;
    uint16_t hrd_type;
    uint32_t time;
    uint8_t  hrd_addr[LIP_MAC_ADDR_SIZE];
} LATTRIBUTE_PACKED;
typedef struct st_lip_dhcp_duid_llt_eth t_lip_dhcp_duid_llt_eth;

/* структура, описывающие содержимое DUID
 * для типа LIP_DHCP_DUID_TYPE_LL для ETHERNET*/
struct st_lip_dhcp_duid_ll_eth {
    uint16_t type;
    uint16_t hrd_type;
    uint8_t  hrd_addr[LIP_MAC_ADDR_SIZE];
} LATTRIBUTE_PACKED;
typedef struct st_lip_dhcp_duid_ll_eth t_lip_dhcp_duid_ll_eth;

/* структура, описывающие содержимое DUID
 * для типа LIP_DHCP_DUID_TYPE_EN */
struct st_lip_dhcp_duid_en {
    uint16_t type;
    uint32_t en_num;
    uint8_t  id[0];
} LATTRIBUTE_PACKED;
typedef struct st_lip_dhcp_duid_en t_lip_dhcp_duid_en;

#include "lcspec_pack_restore.h"

/* флаги, которые могут находится в st_lip_dhcp_info.flags */
#define LIP_DHCP_INFOFLAGS_OVERLOAD_FILE          0x01
#define LIP_DHCP_INFOFLAGS_OVERLOAD_SNAME         0x02
#define LIP_DHCP_INFOFLAGS_RAPID_COMMIT_PRESENT   0x04
#define LIP_DHCP_INFOFLAGS_MANUAL_REQ_ADDR        0x08

/* информация о принятом сообщении от сервера */
struct st_lip_dhcp_info {
    uint8_t ip_addr[LIP_IPV4_ADDR_SIZE];
    uint8_t msk[LIP_IPV4_ADDR_SIZE];
    uint8_t dns[LIP_IPV4_ADDR_SIZE];
    uint8_t gate[LIP_IPV4_ADDR_SIZE];
    uint8_t serverid[LIP_IPV4_ADDR_SIZE];
    uint32_t lease_time;
    uint32_t t1;
    uint32_t t2;
    uint8_t flags;
};
typedef struct st_lip_dhcp_info t_lip_dhcp_info;


int lip_dhcpc_is_enabled(void);
void lip_dhcpc_enable(uint8_t en);
t_lip_errs lip_dhcpc_set_requested_addr(const uint8_t *addr);

#ifdef LIP_DHCPC_CONFIGURED_CB
    void lip_dhcpc_configured_cb(t_lip_dhcp_info *info);
#endif
#ifdef LIP_DHCPC_FAILED_CB
    void lip_dhcpc_failed_cb(void);
#endif

#ifdef LIP_DHCPC_MANUAL_DUID
    /* пользовательский callback для получения DUID. функция должна
     * скопировать DUID в буфер, передаваемый параметром DUID, при этом
     * не больше размера max_size и вернуть реальный размер duid (не больше 251!)
     * если возвращаенное значение > max_size => стек будет считать, что буфера не
     * достаточно
     */
    uint8_t lip_dhcpc_get_duid_cb(uint8_t* duid, int max_size);
#endif



#endif /* LIP_DHCP_H_ */
