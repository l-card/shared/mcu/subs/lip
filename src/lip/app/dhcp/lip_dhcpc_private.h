/*
 * lip_dhcpc_private.h
 *
 *  Created on: 05.08.2011
 *      Author: Borisov
 */

#ifndef LIP_DHCPC_PRIVATE_H_
#define LIP_DHCPC_PRIVATE_H_

#include "lip/app/dhcp/lip_dhcpc.h"

int lip_dhcpc_in_progress(void);
t_lip_errs lip_dhcpc_init(void);
t_lip_errs lip_dhcpc_link_reconnect(void);
t_lip_errs lip_dhcpc_pull(void);
void lip_dhcpc_close(unsigned tout);

#endif /* LIP_DHCPC_PRIVATE_H_ */
