/*
 * lip_dhcp_const.h
 *  Файл содержит определения констант для DHCP-протокола
 *  Created on: 31.07.2011
 *      Author: Melkor
 */

#ifndef LIP_DHCP_CONST_H_
#define LIP_DHCP_CONST_H_


/*  порты UDP, используемые DHCP  */
#define LIP_UDP_PORT_DHCP_SERVER  67
#define LIP_UDP_PORT_DHCP_CLIENT  68

/*   опкоды DHCP сообщений  */
#define LIP_DHCP_OP_BOOTREQUEST   1
#define LIP_DHCP_OP_BOOTREPLY     2

/*          флаги (поле flags)       */
#define LIP_DHCP_FLAGS_BROADCAST 0x8000

/* Значение, соответствующее бесконечному времени в опциях */
#define LIP_DHCP_TIME_INFINITY   0xffffffff


/* коды опций DHCP
 * http://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xml
 * */
#define LIP_DHCP_OPT_PAD                 0 /* [RFC2132] None*/
#define LIP_DHCP_OPT_SUBNET_MASK         1 /* [RFC2132] Subnet Mask Value*/
#define LIP_DHCP_OPT_TIME_OFFSET         2 /* [RFC2132] Time Offset in Seconds from UTC
                                                   (note: deprecated by 100 and 101) */
#define LIP_DHCP_OPT_ROUTER              3 /* [RFC2132] N/4 Router addresses */
#define LIP_DHCP_OPT_TIME_SERVER         4 /* [RFC2132] N/4 Timeserver addresses */
#define LIP_DHCP_OPT_NAME_SERVER         5 /* [RFC2132] N/4 IEN-116 Server addresses */
#define LIP_DHCP_OPT_DOMAIN_SERVER       6 /* [RFC2132] N/4 DNS Server addresses */
#define LIP_DHCP_OPT_LOG_SERVER          7 /* [RFC2132] N/4 Logging Server addresses */
#define LIP_DHCP_OPT_QUOTES_SERVER       8 /* [RFC2132] N/4 Quotes Server addresses */
#define LIP_DHCP_OPT_LPR_SERVER          9 /* [RFC2132] N/4 Printer Server addresses */
#define LIP_DHCP_OPT_IMPRESS_SERVER     10 /* [RFC2132] N/4 Impress Server addresses */
#define LIP_DHCP_OPT_RLP_SERVER         11 /* [RFC2132] N/4 Resource Location Server addresses */
#define LIP_DHCP_OPT_HOSTNAME           12 /* [RFC2132] Hostname string */
#define LIP_DHCP_OPT_BOOT_FILE_SIZE     13 /* [RFC2132] Size of boot file in 512 byte chunks */
#define LIP_DHCP_OPT_MERIT_DUMP_FILE    14 /* [RFC2132] Client to dump and name the file to dump it to */
#define LIP_DHCP_OPT_DOMAIN_NAME        15 /* [RFC2132] The DNS domain name of the client */
#define LIP_DHCP_OPT_SWAP_SERVER        16 /* [RFC2132] Swap Server address */
#define LIP_DHCP_OPT_ROOT_PATH          17 /* [RFC2132] Path name for root disk */
#define LIP_DHCP_OPT_EXTENSION_FILE     18 /* [RFC2132] Path name for more BOOTP info */
#define LIP_DHCP_OPT_FORWARD_ONOFF      19 /* [RFC2132] Enable/Disable IP Forwarding */
#define LIP_DHCP_OPT_SRC_RTE_ONOFF      20 /* [RFC2132] Enable/Disable Source Routing */
#define LIP_DHCP_OPT_POLICY_FILTER      21 /* [RFC2132] Routing Policy Filters */
#define LIP_DHCP_OPT_MAX_DH_ASSEMBLY    22 /* [RFC2132] Max Datagram Reassembly Size */
#define LIP_DHCP_OPT_DEFAULT_IP_TTL     23 /* [RFC2132] Default IP Time to Live */
#define LIP_DHCP_OPT_MTU_TIMEOUT        24 /* [RFC2132] Path MTU Aging Timeout */
#define LIP_DHCP_OPT_MTU_PLATEAU        25 /* [RFC2132] Path MTU Plateau Table */
#define LIP_DHCP_OPT_MTU_INTERFACE      26 /* [RFC2132] Interface MTU Size */
#define LIP_DHCP_OPT_MTU_SUBNET         27 /* [RFC2132] All Subnets are Local */
#define LIP_DHCP_OPT_BROADCAST_ADDRESS  28 /* [RFC2132] Broadcast Address */
#define LIP_DHCP_OPT_MASK_DISCOVERY     29 /* [RFC2132] Perform Mask Discovery */
#define LIP_DHCP_OPT_MASK_SUPPLIER      30 /* [RFC2132] Provide Mask to Others */
#define LIP_DHCP_OPT_ROUTER_DISCOVERY   31 /* [RFC2132] Perform Router Discovery */
#define LIP_DHCP_OPT_ROUTER_REQUEST     32 /* [RFC2132] Router Solicitation Address */
#define LIP_DHCP_OPT_STATIC_ROUTE       33 /* [RFC2132] Static Routing Table */
#define LIP_DHCP_OPT_TRAILERS           34 /* [RFC2132] Trailer Encapsulation */
#define LIP_DHCP_OPT_ARP_TIMEOUT        35 /* [RFC2132] ARP Cache Timeout */
#define LIP_DHCP_OPT_ETHERNET           36 /* [RFC2132] Ethernet Encapsulation */
#define LIP_DHCP_OPT_DEFAULT_TCP_TTL    37 /* [RFC2132] Default TCP Time to Live */
#define LIP_DHCP_OPT_KEEPALIVE_TIME     38 /* [RFC2132] TCP Keepalive Interval */
#define LIP_DHCP_OPT_KEEPALIVE_DATA     39 /* [RFC2132] TCP Keepalive Garbage */
#define LIP_DHCP_OPT_NIS_DOMAIN         40 /* [RFC2132] NIS Domain Name */
#define LIP_DHCP_OPT_NIS_SERVERS        41 /* [RFC2132] NIS Server Addresses */
#define LIP_DHCP_OPT_NTP_SERVERS        42 /* [RFC2132] NTP Server Addresses */
#define LIP_DHCP_OPT_VENDOR_SPECIFIC    43 /* [RFC2132] Vendor Specific Information */
#define LIP_DHCP_OPT_NETBIOS_NAME_SRV   44 /* [RFC2132] NETBIOS Name Servers */
#define LIP_DHCP_OPT_NETBIOS_DIST_SRV   45 /* [RFC2132] NETBIOS Datagram Distribution */
#define LIP_DHCP_OPT_NETBIOS_NODE_TYPE  46 /* [RFC2132] NETBIOS Node Type */
#define LIP_DHCP_OPT_NETBIOS_SCOPE      47 /* [RFC2132] NETBIOS Scope */
#define LIP_DHCP_OPT_X_WINDOW_FONT      48 /* [RFC2132] Window Font Server */
#define LIP_DHCP_OPT_X_WINDOW_MANAGER   49 /* [RFC2132] X Window Display Manager */
#define LIP_DHCP_OPT_ADDRESS_REQUEST    50 /* [RFC2132] Requested IP Address */
#define LIP_DHCP_OPT_ADDRESS_TIME       51 /* [RFC2132] IP Address Lease Time*/
#define LIP_DHCP_OPT_OVERLOAD           52 /* [RFC2132] Overload "sname" or "file"  */
#define LIP_DHCP_OPT_MSG_TYPE           53 /* [RFC2132] DHCP Message Type */
#define LIP_DHCP_OPT_SERVER_ID          54 /* [RFC2132] DHCP Server Identification */
#define LIP_DHCP_OPT_PAREMETER_LIST     55 /* [RFC2132] Parameter Request List */
#define LIP_DHCP_OPT_MESSAGE            56 /* [RFC2132] DHCP Error Message */
#define LIP_DHCP_OPT_MAX_MSG_SIZE       57 /* [RFC2132] DHCP Maximum Message Size */
#define LIP_DHCP_OPT_RENEWAL_TIME       58 /* [RFC2132] DHCP Renewal (T1) Time (seconds) */
#define LIP_DHCP_OPT_REBINDING_TIME     59 /* [RFC2132] DHCP Rebinding (T2) Time (seconds) */
#define LIP_DHCP_OPT_CLASS_ID           60 /* [RFC2132] Class Identifier */
#define LIP_DHCP_OPT_CLIENT_ID          61 /* [RFC2132] Client Identifier */
#define LIP_DHCP_OPT_NETWARE_DOMAIN     62 /* [RFC2242] NetWare/IP Domain Name */
#define LIP_DHCP_OPT_NETWARE_OPTION     63 /* [RFC2242] NetWare/IP sub Options */
#define LIP_DHCP_OPT_NIS_DIMAIN_NAME    64 /* [RFC2132] NIS+ v3 Client Domain Name */
#define LIP_DHCP_OPT_NIS_SERVER_ADDR    65 /* [RFC2132] NIS+ v3 Server Addresses */
#define LIP_DHCP_OPT_SERVER_NAME        66 /* [RFC2132] TFTP Server Name */
#define LIP_DHCP_OPT_BOOTFILE_NAME      67 /* [RFC2132] Boot File Name */
#define LIP_DHCP_OPT_HOME_AGENT_ADDRS   68 /* [RFC2132] Home Agent Addresses */
#define LIP_DHCP_OPT_SMTP_SERVER        69 /* [RFC2132] Simple Mail Server Addresses */
#define LIP_DHCP_OPT_POP3_SERVER        70 /* [RFC2132] Post Office Server Addresses */
#define LIP_DHCP_OPT_NNTP_SERVER        71 /* [RFC2132] Network News Server Addresses*/
#define LIP_DHCP_OPT_WWW_SERVER         72 /* [RFC2132] WWW Server Addresses */
#define LIP_DHCP_OPT_FINGER_SERVER      73 /* [RFC2132] Finger Server Addresses */
#define LIP_DHCP_OPT_IRC_SERVER         74 /* [RFC2132] Chat Server Addresses */
#define LIP_DHCP_OPT_STREETTALK_SERVER  75 /* [RFC2132] StreetTalk Server Addresses */
#define LIP_DHCP_OPT_STDA_SERVER        76 /* [RFC2132] ST Directory Assist. Addresses */
#define LIP_DHCP_OPT_USER_CLASS         77 /* [RFC3004] User Class Information */
#define LIP_DHCP_OPT_DIRECTORY_AGENT    78 /* [RFC2610] directory agent information */
#define LIP_DHCP_OPT_SERVER_SCOPE       79 /* [RFC2610] service location agent scope */
#define LIP_DHCP_OPT_RAPID_COMMIT       80 /* [RFC4039] Rapid Commit */
#define LIP_DHCP_OPT_CLIENT_FQDN        81 /* [RFC4702] Fully Qualified Domain Name */
#define LIP_DHCP_OPT_RELAY_AGENT_INFO   82 /* [RFC3046] Relay Agent Information */
#define LIP_DHCP_OPT_ISNS               83 /* [RFC4174] Internet Storage Name Service */
#define LIP_DHCP_OPT_NDS_SERVERS        85 /* [RFC2241] Novell Directory Services */
#define LIP_DHCP_OPT_NDS_TREE_NAME      86 /* [RFC2241] Novell Directory Services */
#define LIP_DHCP_OPT_NDS_CONTEXT        87 /* [RFC2241] Novell Directory Services */
#define LIP_DHCP_OPT_BCMCS_DOMAIN_NAMES 88 /* [RFC4280] BCMCS Controller Domain Name list */
#define LIP_DHCP_OPT_BCMCS_IPV4_ADDRESS 89 /* [RFC4280] BCMCS Controller IPv4 address option */
#define LIP_DHCP_OPT_AUTHENTICATION     90 /* [RFC3118] Authentication */
#define LIP_DHCP_OPT_CLIENT_LAST_TRANS_TIME  91 /* [RFC4388] client-last-transaction-time option */
#define LIP_DHCP_OPT_ASSOCIATED_IP      92 /* [RFC4388] associated-ip option */
#define LIP_DHCP_OPT_CLIENT_SYSTEM      93 /* [RFC4578] Client System Architecture */
#define LIP_DHCP_OPT_CLIENT_NDI         94 /* [RFC4578] Client Network Device Interface */
#define LIP_DHCP_OPT_LDAP               95 /* [RFC3679] Lightweight Directory Access Protocol */
#define LIP_DHCP_OPT_UUID_GUID          97 /* [RFC4578] UUID/GUID-based Client Identifier */
#define LIP_DHCP_OPT_USER_AUTH          98 /* [RFC2485] Open Group's User Authentication */
#define LIP_DHCP_OPT_GEOCONF_CIVIC      99 /* [RFC4776] */
#define LIP_DHCP_OPT_PCODE             100 /* [RFC4833] IEEE 1003.1 TZ String */
#define LIP_DHCP_OPT_TCODE             101 /* [RFC4833] Reference to the TZ Database */
#define LIP_DHCP_OPT_NETINFO_ADDRESS   112 /* [RFC3679] NetInfo Parent Server Address */
#define LIP_DHCP_OPT_NETINFO_TAG       113 /* [RFC3679] NetInfo Parent Server Tag */
#define LIP_DHCP_OPT_URL               114 /* [RFC3679] URL */
#define LIP_DHCP_OPT_AUTO_CONFIG       116 /* [RFC2563] DHCP Auto-Configuration */
#define LIP_DHCP_OPT_NAME_SERVICE_SEARCH 117 /* [RFC2937] Name Service Search */
#define LIP_DHCP_OPT_SUBNET_SELECTION  118 /* [RFC3011] Subnet Selection Option */
#define LIP_DHCP_OPT_DOMAIN_SEARCH     119 /* [RFC3397] DNS domain search list */
#define LIP_DHCP_OPT_SIP_SERVERS       120 /* [RFC3361] SIP Servers DHCP Option */
#define LIP_DHCP_OPT_CLASSLESS_STATIC_ROUTE 121 /* [RFC3442] Classless Static Route Option */
#define LIP_DCHP_OPT_CCC               122 /* [RFC3495] CableLabs Client Configuration */
#define LIP_DHCP_OPT_GEO_CONF          123 /* [RFC-ietf-geopriv-rfc3825bis-17] GeoConf Option */
#define LIP_DHCP_OPT_VI_VENDOR_CLASS   124 /* [RFC3925] Vendor-Identifying Vendor Class */
#define LIP_DHCP_OPT_VI_VENDOR_SPEC_INFO 125 /* [RFC3925] Vendor-Identifying Vendor-Specific Information */
#define LIP_DHCP_OPT_V4_LOST           137 /* [RFC5223] OPTION_V4_LOST */
#define LIP_DHCP_OPT_CAPWAP_AC_V4      138 /* [RFC5417] CAPWAP Access Controller addresses */
#define LIP_DHCP_OPT_IPV4_ADDRESS_MOS  139 /* [RFC5678] a series of suboptions */
#define LIP_DHCP_OPT_IPV4_FQDN_MOS     140 /* [RFC5678] a series of suboptions */
#define LIP_DHCP_OPT_SIP_UA_CONFIG_SRV_DOMAINS 141 /* [RFC6011] List of domain names to search
                                                   for SIP User Agent Configuration */
#define LIP_DHCP_OPT_IPV4_ADDRESS_ANDSF 142 /* [RFC6153] ANDSF IPv4 Address Option for DHCPv4 */
#define LIP_DHCP_OPT_IPV6_ADDRESS_ANDSF 143 /* [RFC6153] ANDSF IPv6 Address Option for DHCPv6 */
#define LIP_DHCP_OPT_GEO_LOC            144 /* [RFC-ietf-geopriv-rfc3825bis-17]
                                                Geospatial Location with Uncertainty */
#define LIP_DHCP_OPT_TFTP_SERVER_ADDR   150 /* [RFC5859] TFTP server address */
#define LIP_DHCP_OPT_PXELINUX_MAGIC     208 /* [RFC5071][Deprecated] magic string = F1:00:74:7E */
#define LIP_DHCP_OPT_CONFIGURATION_FILE 209 /* [RFC5071] Configuration file */
#define LIP_DHCP_OPT_PATH_PREFIX        210 /* [RFC5071] Path Prefix Option */
#define LIP_DHCP_OPT_REBOOT_TIME        211 /* [RFC5071] Reboot Time */
#define LIP_DHCP_OPT_6RD                212 /* [RFC5969] OPTION_6RD with N/4 6rd BR addresses */
#define LIP_DHCP_OPT_V4_ACESS_DOMAIN    213 /* [RFC5986] Access Network Domain Name */
#define LIP_DHCP_OPT_END                255 /* [RFC2132] */


/*                фиксированные длины опций            */
#define LIP_DHCP_OPTLEN_PAD               0
#define LIP_DHCP_OPTLEN_SUBNET_MASK       4
#define LIP_DHCP_OPTLEN_TIME_OFFSET       4
#define LIP_DHCP_OPTLEN_BOOT_FILE_SIZE    2
#define LIP_DHCP_OPTLEN_FORWARD_ONOFF     1
#define LIP_DHCP_OPTLEN_SRC_RTE_ONOFF     1
#define LIP_DHCP_OPTLEN_MAX_DH_ASSEMBLY   2
#define LIP_DHCP_OPTLEN_DEFAULT_IP_TTL    1
#define LIP_DHCP_OPTLEN_MTU_TIMEOUT       4
#define LIP_DHCP_OPTLEN_MTU_INTERFACE     2
#define LIP_DHCP_OPTLEN_MTU_SUBNET        1
#define LIP_DHCP_OPTLEN_BROADCAST_ADDRESS 4
#define LIP_DHCP_OPTLEN_MASK_DISCOVERY    1
#define LIP_DHCP_OPTLEN_MASK_SUPPLIER     1
#define LIP_DHCP_OPTLEN_ROUTER_DISCOVERY  1
#define LIP_DHCP_OPTLEN_ROUTER_REQUEST    4
#define LIP_DHCP_OPTLEN_TRAILERS          1
#define LIP_DHCP_OPTLEN_ARP_TIMEOUT       4
#define LIP_DHCP_OPTLEN_ETHERNET          1
#define LIP_DHCP_OPTLEN_DEFAULT_TCP_TTL   1
#define LIP_DHCP_OPTLEN_KEEPALIVE_TIME    4
#define LIP_DHCP_OPTLEN_KEEPALIVE_DATA    1
#define LIP_DHCP_OPTLEN_NETBIOS_NODE_TYPE 1
#define LIP_DHCP_OPTLEN_ADDRESS_REQUEST   4
#define LIP_DHCP_OPTLEN_ADDRESS_TIME      4
#define LIP_DHCP_OPTLEN_OVERLOAD          1
#define LIP_DHCP_OPTLEN_MSG_TYPE          1
#define LIP_DHCP_OPTLEN_SERVER_ID         4
#define LIP_DHCP_OPTLEN_MAX_MSG_SIZE      2
#define LIP_DHCP_OPTLEN_RENEWAL_TIME      4
#define LIP_DHCP_OPTLEN_REBINDING_TIME    4
#define LIP_DHCP_OPTLEN_RAPID_COMMIT      0
#define LIP_DHCP_OPTLEN_SUBNET_SELECTION  4
#define LIP_DHCP_OPTLEN_GEO_CONF         16
#define LIP_DHCP_OPTLEN_GEO_LOC          16
#define LIP_DHCP_OPTLEN_REBOOT_TIME       4
#define LIP_DHCP_OPTLEN_END               0


/*                типы сообщений DHCP                 */
#define LIP_DHCP_MSGTYPE_DISCOVER        1  //[RFC2132]
#define LIP_DHCP_MSGTYPE_OFFER           2  //[RFC2132]
#define LIP_DHCP_MSGTYPE_REQUEST         3  //[RFC2132]
#define LIP_DHCP_MSGTYPE_DECLINE         4  //[RFC2132]
#define LIP_DHCP_MSGTYPE_ACK             5  //[RFC2132]
#define LIP_DHCP_MSGTYPE_NAK             6  //[RFC2132]
#define LIP_DHCP_MSGTYPE_RELEASE         7  //[RFC2132]
#define LIP_DHCP_MSGTYPE_INFORM          8  //[RFC2132]
#define LIP_DHCP_MSGTYPE_FORCERENEW      9  //[RFC3203]
#define LIP_DHCP_MSGTYPE_LEASEQUERY      10 //[RFC4388]
#define LIP_DHCP_MSGTYPE_LEASEUNASSIGNED 11 //[RFC4388]
#define LIP_DHCP_MSGTYPE_LEASEUNKNOWN    12 //[RFC4388]
#define LIP_DHCP_MSGTYPE_DHCPLEASEACTIVE 13 //[RFC4388]


/* значения в полях опций */
#define LIP_DHCP_OPTVAL_OVERLOAD_FILE     1 /* the 'file' field is used to hold options */
#define LIP_DHCP_OPTVAL_OVERLOAD_SNAME    2 /* the 'sname' field is used to hold options */
#define LIP_DHCP_OPTVAL_OVERLOAD_BOTH     3 /* the 'file' & the 'sname' fields  are used to hold options */



#endif /* LIP_DHCP_CONST_H_ */
