/***************************************************************************//**
  @addtogroup lip_dns_sd
  @{

  @internal @file lip_dns_sd_private.h
  @brief Файл содержит определения функций и констант для @ref lip_dns_sd,
         которые используются только самим стеком.
  @author Borisov Alexey <borisov@lcard.ru>
  @date   05.09.2011
 ******************************************************************************/

#ifndef LIP_DNS_SD_PRIVATE_H_
#define LIP_DNS_SD_PRIVATE_H_

#include "lip_dns_sd.h"

/** @internal @brief Инициализация модуля DNS-SD. */
void lip_dnssd_init(void);
/** @internal @brief Закрытие модуля DNS-SD. */
void lip_dnssd_close(void);
/** @internal @brief Вызывается при изменении имени хоста */
void lip_dnssd_hostname_ch_cb(void);

#endif /* LIP_DNS_SD_PRIVATE_H_ */

/** @}*/
