/***************************************************************************//**
  @addtogroup lip_mdns
  @{

  @internal @file lip_mdns_private.h
  @brief Файл содержит определения функций и констант для @ref lip_mdns,
         которые используются только самим стеком.
  @author Borisov Alexey <borisov@lcard.ru>
  @date   05.08.2011
 ******************************************************************************/

#ifndef LIP_MDNS_PRIV_H_
#define LIP_MDNS_PRIV_H_

#include "lip_mdns.h"
#include "lip/net/lip_ip.h"

/** @internal @brief Инициализация модуля mDNS. */
void lip_mdns_init(void);
/** @internal @brief Закрытие модуля mDNS. */
void lip_mdns_close(void);
/** @internal @brief Сообщение о смене линка для mDNS. */
void lip_mdns_reconnect(void);
/** @internal @brief Выполнение фоновых задач mDNS. */
void lip_mdns_pull(void);
/** @internal @brief Вызывается при смене активного адреса хоста */
void lip_mdns_ip_addr_changed(t_lip_ip_type type);

/* флаги при добавлении записей, используемые только внутри стека */
typedef enum {
    /* используется внутри модуля mdns для добавления адресных записей, под которые
       отдельно всегда резервируются места, которые нельзя занять записями извне */
    LIP_MDNS_ADDFLAGS_INTERNAL_ADDR     = 0x80000000,
    /* признак, что данная PTR запись указывает на сервис DNS-SD. Нужен, так
       как к такой записи предъявляются отдельные требования для передачи
       дополнительных записей */
    LIP_MDNS_ADDFLAGS_DNS_SD_PTR        = 0x40000000,
    /* Признак, что данная запись является обратной адресной записью
        (так как для нее нет проверок, но она считается уникальной) */
    LIP_MDNS_ADDFLAGS_REVERSE_ADDR_MAP  = 0x20000000
} t_lip_mdns_addfalgs_private;



#endif /* LIP_MDNS_PRIV_H_ */

/** @}*/
