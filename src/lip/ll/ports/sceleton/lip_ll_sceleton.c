/**************************************************************************//**
  @file lip_ll_sceleton.c

  Файл является заглушкой при реализации порта и предназначен в качестве начального
  шаблона при реализации нового порта.

  @author Borisov Alexey <borisov@lcard.ru>
  ******************************************************************************/

#include "lip_ll.h"
#include "lip_log.h"
#include "link/lip_eth.h"
#include "phy/lip_phy.h"

t_lip_errs lip_ll_init(t_lip_init_stage stage, int flags) {
    t_lip_errs err = LIP_ERR_SUCCESS;
    switch (stage) {
        case LIP_INIT_STAGE_PHY_INTF_INIT:

            break;
        case LIP_INIT_STAGE_MAC_INIT:

            break;
        case LIP_INIT_STAGE_MAC_START:

            break;
    }
    return err;
}


void lip_ll_close(void) {

}

void lip_ll_set_mac_addr(const uint8_t *mac_addr) {

}


t_lip_errs lip_ll_mdio_write_start(uint8_t phyaddr, uint8_t regaddr, uint16_t value) {
    t_lip_errs err = LIP_ERR_SUCCESS;

    return err;
}


t_lip_errs lip_ll_mdio_read_start(uint8_t phyaddr, uint8_t regaddr) {
    t_lip_errs err = LIP_ERR_SUCCESS;

    return err;
}

bool lip_ll_mdio_rdy(void) {

    return 0;
}

unsigned lip_ll_mdio_data(void) {

    return 1;
}

uint8_t* lip_ll_tx_get_buf(int* max_length) {

    return NULL;
}


t_lip_errs lip_ll_tx_flush_split(size_t size, const uint8_t* data, int data_size) {
    t_lip_errs err = LIP_ERR_BUF_NOT_PREPARED;


    return err;
}

uint8_t* lip_ll_rx_get_buf(size_t *length) {

    return NULL;
}


t_lip_errs lip_ll_rx_drop(void) {
    t_lip_errs err = LIP_ERR_BUF_NOT_PREPARED;


    return err;
}


#ifdef LIP_USE_IGMP
    t_lip_errs lip_ll_join(const uint8_t* mac) {

        return LIP_ERR_SUCCESS;
    }

    t_lip_errs lip_ll_leave(const uint8_t* mac) {

        return LIP_ERR_SUCCESS;
    }
#endif
