#ifndef LIP_PORT_CONF_H_
#define LIP_PORT_CONF_H_

/** Макрос для задания расположения буферов приема и передачи пакетов
    (без #pragma) */
#define LIP_PORT_BUF_MEM(var)    __attribute__ ((section (".eth_ram"))) var
/** Макрос для задания расположения дескрипторов DMA для Ethernet
    (без #pragma) */
#define LIP_PORT_DESCR_MEM(var) __attribute__ ((section (".eth_ram"))) var

#endif /* UIP_PORT_CONF_H_ */
