/*
 * lpc17xx_eth_descr_bit.h
 *
 *  Created on: 30.08.2010
 *      Author: borisov
 */

#ifndef LPC17XX_ETH_DESCR_BIT_H_
#define LPC17XX_ETH_DESCR_BIT_H_

//Table 175. Receive Descriptor Control Word
#define LPC_ETH_DESCR_RXCTL_Size_Pos               (0)
#define LPC_ETH_DESCR_RXCTL_Size_Msk               (0x7FFUL << LPC_ETH_DESCR_RXCTL_Size_Pos)

#define LPC_ETH_DESCR_RXCTL_Interrupt_Pos          (31)
#define LPC_ETH_DESCR_RXCTL_Interrupt_Msk          (0x1UL << LPC_ETH_DESCR_RXCTL_Interrupt_Pos)

//Receive Status HashCRC Word
#define LPC_ETH_STATUS_RXHASH_SAHashCRC_Pos        (0)
#define LPC_ETH_STATUS_RXHASH_SAHashCRC_Msk        (0xFFUL << LPC_ETH_STATUS_RXHASH_SAHashCRC_Pos)

#define LPC_ETH_STATUS_RXHASH_DAHashCRC_Pos        (16)
#define LPC_ETH_STATUS_RXHASH_DAHashCRC_Msk        (0xFFUL << LPC_ETH_STATUS_RXHASH_DAHashCRC_Pos)


//Receive status information word
#define LPC_ETH_STATUS_RXCTL_RxSize_Pos            (0)
#define LPC_ETH_STATUS_RXCTL_RxSize_Msk            (0x7FFUL << LPC_ETH_STATUS_RXCTL_RxSize_Pos)

#define LPC_ETH_STATUS_RXCTL_ControlFrame_Pos      (18)
#define LPC_ETH_STATUS_RXCTL_ControlFrame_Msk      (0x1UL << LPC_ETH_STATUS_RXCTL_ControlFrame_Pos)

#define LPC_ETH_STATUS_RXCTL_VLAN_Pos              (19)
#define LPC_ETH_STATUS_RXCTL_VLAN_Msk              (0x1UL << LPC_ETH_STATUS_RXCTL_VLAN_Pos)

#define LPC_ETH_STATUS_RXCTL_FailFilter_Pos        (20)
#define LPC_ETH_STATUS_RXCTL_FailFilter_Msk        (0x1UL << LPC_ETH_STATUS_RXCTL_FailFilter_Pos)

#define LPC_ETH_STATUS_RXCTL_Multicast_Pos         (21)
#define LPC_ETH_STATUS_RXCTL_Multicast_Msk         (0x1UL << LPC_ETH_STATUS_RXCTL_Multicast_Pos)

#define LPC_ETH_STATUS_RXCTL_Broadcast_Pos         (22)
#define LPC_ETH_STATUS_RXCTL_Broadcast_Msk         (0x1UL << LPC_ETH_STATUS_RXCTL_Broadcast_Pos)

#define LPC_ETH_STATUS_RXCTL_CRCError_Pos          (23)
#define LPC_ETH_STATUS_RXCTL_CRCError_Msk          (0x1UL << LPC_ETH_STATUS_RXCTL_CRCError_Pos)

#define LPC_ETH_STATUS_RXCTL_SymbolError_Pos       (24)
#define LPC_ETH_STATUS_RXCTL_SymbolError_Msk       (0x1UL << LPC_ETH_STATUS_RXCTL_SymbolError_Pos)

#define LPC_ETH_STATUS_RXCTL_LengthError_Pos       (25)
#define LPC_ETH_STATUS_RXCTL_LengthError_Msk       (0x1UL << LPC_ETH_STATUS_RXCTL_LengthError_Pos)

#define LPC_ETH_STATUS_RXCTL_RangeError_Pos        (26)
#define LPC_ETH_STATUS_RXCTL_RangeError_Msk        (0x1UL << LPC_ETH_STATUS_RXCTL_RangeError_Pos)

#define LPC_ETH_STATUS_RXCTL_AlignmentError_Pos    (27)
#define LPC_ETH_STATUS_RXCTL_AlignmentError_Msk    (0x1UL << LPC_ETH_STATUS_RXCTL_AlignmentError_Pos)

#define LPC_ETH_STATUS_RXCTL_Overrun_Pos           (28)
#define LPC_ETH_STATUS_RXCTL_Overrun_Msk           (0x1UL << LPC_ETH_STATUS_RXCTL_Overrun_Pos)

#define LPC_ETH_STATUS_RXCTL_NoDescriptor_Pos      (29)
#define LPC_ETH_STATUS_RXCTL_NoDescriptor_Msk      (0x1UL << LPC_ETH_STATUS_RXCTL_NoDescriptor_Pos)

#define LPC_ETH_STATUS_RXCTL_LastFlag_Pos          (30)
#define LPC_ETH_STATUS_RXCTL_LastFlag_Msk          (0x1UL << LPC_ETH_STATUS_RXCTL_LastFlag_Pos)

#define LPC_ETH_STATUS_RXCTL_Error_Pos             (31)
#define LPC_ETH_STATUS_RXCTL_Error_Msk             (0x1UL << LPC_ETH_STATUS_RXCTL_Error_Pos)




//Ethernet Transmit descriptor control word
#define LPC_ETH_DESCR_TXCTL_Size_Pos               (0)
#define LPC_ETH_DESCR_TXCTL_Size_Msk               (0x7FFUL << LPC_ETH_DESCR_TXCTL_Size_Pos)

#define LPC_ETH_DESCR_TXCTL_Override_Pos           (26)
#define LPC_ETH_DESCR_TXCTL_Override_Msk           (0x1UL << LPC_ETH_DESCR_TXCTL_Override_Pos)

#define LPC_ETH_DESCR_TXCTL_Huge_Pos               (27)
#define LPC_ETH_DESCR_TXCTL_Huge_Msk               (0x1UL << LPC_ETH_DESCR_TXCTL_Huge_Pos)

#define LPC_ETH_DESCR_TXCTL_Pad_Pos                (28)
#define LPC_ETH_DESCR_TXCTL_Pad_Msk                (0x1UL << LPC_ETH_DESCR_TXCTL_Pad_Pos)

#define LPC_ETH_DESCR_TXCTL_CRC_Pos                (29)
#define LPC_ETH_DESCR_TXCTL_CRC_Msk                (0x1UL << LPC_ETH_DESCR_TXCTL_CRC_Pos)

#define LPC_ETH_DESCR_TXCTL_Last_Pos               (30)
#define LPC_ETH_DESCR_TXCTL_Last_Msk               (0x1UL << LPC_ETH_DESCR_TXCTL_Last_Pos)

#define LPC_ETH_DESCR_TXCTL_Interrupt_Pos          (31)
#define LPC_ETH_DESCR_TXCTL_Interrupt_Msk          (0x1UL << LPC_ETH_DESCR_TXCTL_Interrupt_Pos)

//Transmit status information word
#define LPC_ETH_STATUS_TXCTL_CollisionCount_Pos       (21)
#define LPC_ETH_STATUS_TXCTL_CollisionCount_Msk       (0xFUL << LPC_ETH_STATUS_TXCTL_CollisionCount_Pos)

#define LPC_ETH_STATUS_TXCTL_Defer_Pos                (25)
#define LPC_ETH_STATUS_TXCTL_Defer_Msk                (0x1UL << LPC_ETH_STATUS_TXCTL_Defer_Pos)

#define LPC_ETH_STATUS_TXCTL_ExcessiveDefer_Pos       (26)
#define LPC_ETH_STATUS_TXCTL_ExcessiveDefer_Msk       (0x1UL << LPC_ETH_STATUS_TXCTL_ExcessiveDefer_Pos)

#define LPC_ETH_STATUS_TXCTL_ExcessiveCollision_Pos   (27)
#define LPC_ETH_STATUS_TXCTL_ExcessiveCollision_Msk   (0x1UL << LPC_ETH_STATUS_TXCTL_ExcessiveCollision_Pos)

#define LPC_ETH_STATUS_TXCTL_LateCollision_Pos        (28)
#define LPC_ETH_STATUS_TXCTL_LateCollision_Msk        (0x1UL << LPC_ETH_STATUS_TXCTL_LateCollision_Pos)

#define LPC_ETH_STATUS_TXCTL_Underrun_Pos             (29)
#define LPC_ETH_STATUS_TXCTL_Underrun_Msk             (0x1UL << LPC_ETH_STATUS_TXCTL_Underrun_Msk)

#define LPC_ETH_STATUS_TXCTL_NoDescriptor_Pos         (30)
#define LPC_ETH_STATUS_TXCTL_NoDescriptor_Msk         (0x1UL << LPC_ETH_STATUS_TXCTL_NoDescriptor_Pos)

#define LPC_ETH_STATUS_TXCTL_Error_Pos                (31)
#define LPC_ETH_STATUS_TXCTL_Error_Msk                (0x1UL << LPC_ETH_STATUS_TXCTL_Error_Pos)





#endif /* LPC17XX_ETH_DESCR_BIT_H_ */
