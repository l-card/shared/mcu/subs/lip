/*****************************SIMULATION INTERFACE*********************************************//**
  ******************************************************************************/

#include "lip_ll.h"
#include "lip_log.h"
#include "link/lip_eth.h"
#include "phy/lip_phy.h"

#define WEAK_DECL __attribute__((weak))

t_lip_errs WEAK_DECL sim_lip_ll_init_phy_intf_init(int flags){return LIP_ERR_SUCCESS;}
t_lip_errs WEAK_DECL sim_lip_ll_init_mac_init(int flags){return LIP_ERR_SUCCESS;}
t_lip_errs WEAK_DECL sim_lip_ll_init_mac_start(int flags){return LIP_ERR_SUCCESS;}
void WEAK_DECL sim_lip_ll_close(void){}
void WEAK_DECL sim_lip_ll_set_mac_addr(const uint8_t *mac_addr){}
t_lip_errs WEAK_DECL sim_lip_ll_mdio_write_start(unsigned dev_addr, unsigned reg_addr, unsigned value){return LIP_ERR_SUCCESS;}
t_lip_errs WEAK_DECL sim_lip_ll_mdio_read_start(unsigned dev_addr, unsigned reg_addr){return LIP_ERR_SUCCESS;}
int WEAK_DECL sim_lip_ll_mdio_rdy(void){return 0;}
unsigned WEAK_DECL sim_lip_ll_mdio_data(void) {return 1;}
uint8_t* WEAK_DECL sim_lip_ll_tx_get_buf(int* max_length){return NULL;}
t_lip_errs WEAK_DECL sim_lip_ll_tx_flush_split(size_t size, const uint8_t* data, int data_size){return LIP_ERR_BUF_NOT_PREPARED;}
uint8_t* WEAK_DECL sim_lip_ll_rx_get_buf(size_t *length){return NULL;}
t_lip_errs WEAK_DECL sim_lip_ll_rx_drop(void){return LIP_ERR_BUF_NOT_PREPARED;}


t_lip_errs lip_ll_init(t_lip_init_stage stage, int flags) {
    t_lip_errs err = LIP_ERR_SUCCESS;
    switch (stage) {
        case LIP_INIT_STAGE_PHY_INTF_INIT:
            err = sim_lip_ll_init_phy_intf_init(flags);
            break;
        case LIP_INIT_STAGE_MAC_INIT:
            err = sim_lip_ll_init_mac_init(flags);
            break;
        case LIP_INIT_STAGE_MAC_START:
            err = sim_lip_ll_init_mac_start(flags);
            break;
    }
    return err;
}


void lip_ll_close(void) {
    sim_lip_ll_close();
}

/*********************************************************************
 *  установить mac-адрес для фильтра mac-контроллера
 *********************************************************************/
void lip_ll_set_mac_addr(const uint8_t *mac_addr) {
    sim_lip_ll_set_mac_addr(mac_addr);
}


t_lip_errs lip_ll_mdio_write_start(unsigned dev_addr, unsigned reg_addr, unsigned value) {
    return sim_lip_ll_mdio_write_start(dev_addr,reg_addr,value);
}


t_lip_errs lip_ll_mdio_read_start(unsigned dev_addr, unsigned reg_addr) {
    return sim_lip_ll_mdio_read_start(dev_addr,reg_addr);
}

int lip_ll_mdio_rdy(void) {
    return sim_lip_ll_mdio_rdy();
}

unsigned lip_ll_mdio_data(void) {
    return sim_lip_ll_mdio_data();
}

/****************************************************************************
 * Возвращает указатель на буфер, в котором можно формировать следующий
 * пакет (0, если нет свободного места для пакета)
 * В max_length возвращается максимальный размер пакета
 *   (в данной реализации - всегда LIP_ETH_TX_BUF_SIZE)
 ****************************************************************************/
uint8_t* lip_ll_tx_get_buf(int* max_length) {
    return sim_lip_ll_tx_get_buf(max_length);
}


t_lip_errs lip_ll_tx_flush_split(size_t size, const uint8_t* data, int data_size) {
    return sim_lip_ll_tx_flush_split(size,data,data_size);
}

uint8_t* lip_ll_rx_get_buf(size_t *length) {
    return sim_lip_ll_rx_get_buf(length);
}


t_lip_errs lip_ll_rx_drop(void) {
    return sim_lip_ll_rx_drop();
}


#ifdef LIP_USE_IGMP
    t_lip_errs lip_ll_join(const uint8_t* mac) {

        return LIP_ERR_SUCCESS;
    }

    t_lip_errs lip_ll_leave(const uint8_t* mac) {

        return LIP_ERR_SUCCESS;
    }
#endif
