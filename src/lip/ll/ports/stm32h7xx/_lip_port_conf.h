#ifndef LIP_PORT_CONFIG_H
#define LIP_PORT_CONFIG_H

/* определение назначения для сигналов, которые могут быть назначены разным ножкам процессора */
#define LIP_ETH_PIN_TX_EN CHIP_PIN_PG11_ETH_RMII_TX_EN
#define LIP_ETH_PIN_TXD0  CHIP_PIN_PG13_ETH_RMII_TXD0
#define LIP_ETH_PIN_TXD1  CHIP_PIN_PG12_ETH_RMII_TXD1

/** Макрос для задания расположения буферов приема и передачи пакетов */
#define LIP_PORT_BUF_MEM(var)    __attribute__ ((section (".eth_ram"))) var
/** Макрос для задания расположения дескрипторов DMA для Ethernet*/
#define LIP_PORT_DESCR_MEM(var) __attribute__ ((section (".eth_ram"))) var

#endif // LIP_PORT_CONFIG_H
