#ifndef LIP_LL_FEATURES_H
#define LIP_LL_FEATURES_H


/** Признак, что порту требуется отслеживание смены IP-адреса машины.
 * Порт должен реализовать функцию lip_ll_ip_addr_changed(), которая
 * будет вызываться при каждой смене IP-адреса */
#define LIP_LL_PORT_FEATURE_IPADDR_TRACKING         1

/** Признак, что порт поддерживает аппаратный ответ на ARP-запросы.
 *  Если был аппаратный ответ, то порт должен установить флаг
 *  #LIP_LL_RXFLAGS_ARP_RESP_SENT при получении через lip_ll_rx_get_frame() пакета,
 *  соответствующего ARP-запросу, обработанному аппаратно */
#define LIP_LL_PORT_FEATURE_HW_ARP_RESP             1

/** Признак, что порт поддерживает аппаратную вставку ethernet source address
 *  при отправке пакетов. В этом случае данное поле вообще не включается при
 *  подготовке пакета на отправку */
#define LIP_LL_PORT_FEATURE_HW_TX_ETH_SA_INS        1

/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для IPv4.
 *  Стек оставляет это поле нулевым */
#define LIP_LL_PORT_FEATURE_HW_TX_IPV4_CHECKSUM     1
/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для ICMP.
 *  Стек оставляет это поле нулевым */
#define LIP_LL_PORT_FEATURE_HW_TX_ICMP_CHECKSUM     1
/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для UDP.
 *  Стек оставляет это поле нулевым */
#define LIP_LL_PORT_FEATURE_HW_TX_UDP_CHECKSUM      1
/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для TCP.
 *  Стек оставляет это поле нулевым */
#define LIP_LL_PORT_FEATURE_HW_TX_TCP_CHECKSUM      1

/** Поддержка удаление VLAN тэга */
#define LIP_LL_PORT_FEATURE_HW_VLAN_RX_STRIP        1

/** Признак, что стек поддерживает аппаратное получение временных меток принятых
 *  и переданных пакетов */
#define LIP_LL_PORT_FEATURE_HW_PTP_TSTMP            1


#endif // LIP_LL_FEATURES_H
