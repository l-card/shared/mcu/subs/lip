#ifndef LPC43XX_ENET_DESCR_H_
#define LPC43XX_ENET_DESCR_H_


/* ---------------------  RX descriptor fields ------------------------ */

/*  RXDES0 - Status  */
#define RXDESCR_ST_ESA      (1UL <<  0) //extended status available
#define RXDESCR_ST_CE       (1UL <<  1) //CRC error
#define RXDESCR_ST_DE       (1UL <<  2) //dribbe bit error
#define RXDESCR_ST_RE       (1UL <<  3) //receive error
#define RXDESCR_ST_RWT      (1UL <<  4) //receive watchdog timeout
#define RXDESCR_ST_FT       (1UL <<  5) //ethernet or 802.3 frame type
#define RXDESCR_ST_LC       (1UL <<  6) //late collision
#define RXDESCR_ST_TSA      (1UL <<  7) //timestamp available
#define RXDESCR_ST_LS       (1UL <<  8) //last descr
#define RXDESCR_ST_FS       (1UL <<  9) //last descr
#define RXDESCR_ST_VLAN     (1UL << 10) //VLAN tag
#define RXDESCR_ST_OE       (1UL << 11) //overflow error
#define RXDESCR_ST_LE       (1UL << 12) //length error
#define RXDESCR_ST_SAF      (1UL << 13) //source addr filter fail
#define RXDESCR_ST_DESCRE   (1UL << 14) //descriptor error
#define RXDESCR_ST_ES       (1UL << 15) //error summary
#define RXDESCR_ST_AFM      (1UL << 30) //destination address filter fail
#define RXDESCR_ST_OWN      (1UL << 31) //own

#define RXDESCR_ST_GET_FRATE_LEN(val) ((val >> 16) & 0x3FFF)

/*  RXDES1 - Control  */ 
#define RXDESCR_CTL_RCH  (1<<14UL)
#define RXDESCR_CTL_RER  (1<<15UL)


/* ---------------------  TX descriptor fields ------------------------ */
/*  TXDES0 - Control/Status  */
#define TXDESCR_ST_DB             (0x1UL <<  0) /* Deferred bit */
#define TXDESCR_ST_UF             (0x1UL <<  1) /* Underflow error */
#define TXDESCR_ST_ED             (0x1UL <<  2) /* Excessive Deferral */
#define TXDESCR_ST_CC             (0x7UL <<  3) /* Collision Count */
#define TXDESCR_CTL_SLOTNUM       (0x7UL <<  3) /* Slot Number in AV Mode (?) */
#define TXDESCR_ST_VF             (0x1UL <<  7) /* VLAN frame */
#define TXDESCR_ST_EC             (0x1UL <<  8) /* Excessive collision */
#define TXDESCR_ST_LATE_COL       (0x1UL <<  9) /* Late collision */
#define TXDESCR_ST_NC             (0x1UL << 10) /* No carrier */
#define TXDESCR_ST_LOSS_CAR       (0x1UL << 11) /* Loss of carrier */
#define TXDESCR_ST_IPE            (0x1UL << 12) /* IP Payload Error */
#define TXDESCR_ST_FF             (0x1UL << 13) /* Frame flushed */
#define TXDESCR_ST_JT             (0x1UL << 14) /* Jabber timeout */
#define TXDESCR_ST_ES             (0x1UL << 15) /* Error summary */
#define TXDESCR_ST_IHE            (0x1UL << 16) /* IP-header error */
#define TXDESCR_ST_TTSS           (0x1UL << 17) /* Transmit timestamp status */
#define TXDESCR_CTL_TCH           (0x1UL << 20) /* Transmit address chained */
#define TXDESCR_CTL_TER           (0x1UL << 21) /* Transmit End of Ring */
#define TXDESCR_CTL_TTSE          (0x1UL << 25) /* Transmit Timestamp Enable */
#define TXDESCR_CTL_DP            (0x1UL << 26) /* Disable Pad */
#define TXDESCR_CTL_DC            (0x1UL << 27) /* Disable CRC */
#define TXDESCR_CTL_FS            (0x1UL << 28) /* First segment */
#define TXDESCR_CTL_LS            (0x1UL << 29) /* Last segment */
#define TXDESCR_CTL_IC            (0x1UL << 30) /* Interrupt on completion */

#define TXDESCR_CTLST_OWN       (1UL << 31) //own


typedef struct {
    uint32_t ctl_st;
    uint16_t size1;
    uint16_t size2;
    const uint8_t* addr1;
    const uint8_t* addr2;
//#ifdef LIP_PORT_EXT_DESCR
    uint32_t res[2];
    uint32_t timestamp_l;
    uint32_t timestamp_h;
//#endif
} t_eth_tx_descr;

typedef struct {
    uint32_t status;
    uint16_t ctl_size1;
    uint16_t size2;
    uint8_t* addr1;
    uint8_t* addr2;
//#ifdef LIP_PORT_EXT_DESCR
    uint32_t ext_status;
    uint32_t res;
    uint32_t timestamp_l;
    uint32_t timestamp_h;
//#endif
} t_eth_rx_descr;




#endif
