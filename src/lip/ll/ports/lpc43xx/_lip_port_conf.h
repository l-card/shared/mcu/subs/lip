/*
 * lip_port_conf.h
 *  Файл с настройкаси порта TCP/IP стека LIP
 *  Created on: 03.02.2013
 *      Author: borisov
 */

#ifndef LIP_PORT_CONF_H_
#define LIP_PORT_CONF_H_

/** Используемая конфигурация пина для линии MDC */
#define LIP_PHY_PIN_MDC  CHIP_PIN_P7_7_ENET_MDC

/** Макрос для задания расположения буферов приема и передачи пакетов
    (без #pragma) */
#define LIP_PORT_BUF_MEM(var)    __attribute__ ((section (".eth_ram"))) var
/** Макрос для задания расположения дескрипторов DMA для Ethernet
    (без #pragma) */
#define LIP_PORT_DESCR_MEM(var) __attribute__ ((section (".eth_ram"))) var


/** Настройка клоков Phy через таймер */
#define LIP_PORT_PHY_CLK_TIMER

/** Если определено, используются функции управления клоком из LpcOpen,
 *  иначе - макрос LPC_SYSCLK и опционально LPC_CCU_CFG */
#define LIP_PORT_USE_LPCOPEN_CLKCTL

#endif /* UIP_PORT_CONF_H_ */
