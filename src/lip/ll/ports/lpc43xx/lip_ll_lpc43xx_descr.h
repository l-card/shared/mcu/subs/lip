/**************************************************************************//**
  @file lip_ll_lpc43xx_descr.h

  Файл содержит функции для работы с дескрипторами Ethernet для контроллера
  lpc43xx.
  Работа с дескрипторами не включает работу с регистрами и не привязывается
  к расположению регистров какого-то контроллера, в связи с чем может использоваться
  другими чипами, которые используют модифицированную версию данного контроллера
  Ethernet.

  В настоящее время используется в портах для lpc43xx и bf609

  @author Borisov Alexey <borisov@lcard.ru>
  ******************************************************************************/

#ifndef LIP_LL_LPC43XX_DESCR_H
#define LIP_LL_LPC43XX_DESCR_H

#include "lip.h"
#include "lpc43xx_enet_descr.h"

/* количество блоков на прием (оно же - кол-во дескрипторов) */
#define LIP_ETH_RX_DESCR_CNT       28
/* размер одного блока */
#define LIP_ETH_RX_BUF_SIZE       256
/* размер последнего блока (=макс. размеру пакета + размер CRC + выравнивание на слово) */
#define LIP_ETH_RX_LAST_BUF_SIZE  (LIP_ETH_MAX_PACKET_SIZE + 4 + (LIP_ETH_MAX_PACKET_SIZE%4 ? (4 - LIP_ETH_MAX_PACKET_SIZE%4) : 0))
/* общий размер всей памяти на прием */
#define LIP_ETH_RX_FULL_BUF_SIZE ((LIP_ETH_RX_DESCR_CNT-1)*LIP_ETH_RX_BUF_SIZE + LIP_ETH_RX_LAST_BUF_SIZE)


/* кол-во дескрипторов на передачу */
#define LIP_ETH_TX_DESCR_CNT      16
/* макс. размер пакета на передачу */
#define LIP_ETH_TX_BUF_SIZE       LIP_ETH_MAX_PACKET_SIZE
/* размер циклического буфера на передачу */
#define LIP_ETH_TX_FULL_BUF_SIZE  (4*LIP_ETH_TX_BUF_SIZE)





uint32_t lip_ll_lpc43descr_rx_init(void);
uint32_t lip_ll_lpc43descr_tx_init(void);

uint8_t *lip_ll_lpc43descr_rx_get_buf(size_t *length);
t_lip_errs lip_ll_lpc43descr_rx_drop(void);

uint8_t *lip_ll_lpc43descr_tx_get_buf(int* max_length) ;
t_lip_errs lip_ll_lpc43descr_tx_flush_split(size_t size, const uint8_t* data, int data_size);

extern void lip_ll_lpc43descr_tx_poll_req(void);
extern void lip_ll_lpc43descr_clear_stat(void);
extern void lip_ll_lpc43descr_dmb(void);

#endif // LIP_LL_LPC43XX_DESCR_H

