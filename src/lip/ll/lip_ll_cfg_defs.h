#ifndef LIP_LL_CFG_DEFS_H
#define LIP_LL_CFG_DEFS_H

#include "lip_ll_port_features.h"


/** Признак, что порту требуется отслеживание смены IP-адреса машины.
 * Порт должен реализовать функцию lip_ll_ip_addr_changed(), которая
 * будет вызываться при каждой смене IP-адреса */
#ifndef LIP_LL_PORT_FEATURE_IPADDR_TRACKING
    #define LIP_LL_PORT_FEATURE_IPADDR_TRACKING         0
#endif

/** Признак, что порт поддерживает аппаратный ответ на ARP-запросы.
 *  Если был аппаратный ответ, то порт должен установить флаг
 *  #LIP_LL_RXFLAGS_ARP_RESP_SENT при получении через lip_ll_rx_get_frame() пакета,
 *  соответствующего ARP-запросу, обработанному аппаратно */
#ifndef LIP_LL_PORT_FEATURE_HW_ARP_RESP
    #define LIP_LL_PORT_FEATURE_HW_ARP_RESP             0
#endif

/** Признак, что порт поддерживает аппаратную вставку ethernet source address
 *  при отправке пакетов. В этом случае данное поле вообще не включается при
 *  подготовке пакета на отправку */
#ifndef LIP_LL_PORT_FEATURE_HW_TX_ETH_SA_INS
    #define LIP_LL_PORT_FEATURE_HW_TX_ETH_SA_INS        0
#endif

/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для IPv4.
 *  Стек оставляет это поле нулевым */
#ifndef LIP_LL_PORT_FEATURE_HW_TX_IPV4_CHECKSUM
    #define LIP_LL_PORT_FEATURE_HW_TX_IPV4_CHECKSUM     0
#endif

/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для ICMP.
 *  Стек оставляет это поле нулевым */
#ifndef LIP_LL_PORT_FEATURE_HW_TX_ICMP_CHECKSUM
    #define LIP_LL_PORT_FEATURE_HW_TX_ICMP_CHECKSUM     0
#endif

/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для UDP.
 *  Стек оставляет это поле нулевым */
#ifndef LIP_LL_PORT_FEATURE_HW_TX_UDP_CHECKSUM
    #define LIP_LL_PORT_FEATURE_HW_TX_UDP_CHECKSUM      0
#endif

/** Признак, что порт поддерживает аппаратный рассчет контрольной суммы для TCP.
 *  Стек оставляет это поле нулевым */
#ifndef LIP_LL_PORT_FEATURE_HW_TX_TCP_CHECKSUM
    #define LIP_LL_PORT_FEATURE_HW_TX_TCP_CHECKSUM      0
#endif

/** Признак, поддерживает ли порт дробные части нс при генерации метки времени */
#ifndef LIP_LL_PORT_FEATURE_TSTMP_FRACT_NS
    #define LIP_LL_PORT_FEATURE_TSTMP_FRACT_NS          0
#endif

#endif // LIP_LL_CFG_DEFS_H
